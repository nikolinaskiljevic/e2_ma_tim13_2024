
/**
 * Import function triggers from their respective submodules:
 *
 * import {onCall} from "firebase-functions/v2/https";
 * import {onDocumentWritten} from "firebase-functions/v2/firestore";
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */

//import {onRequest} from "firebase-functions/v2/https";
//import * as logger from "firebase-functions/logger";
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
admin.initializeApp();

exports.sendNotificationOnNewReport = functions.database.ref('/reports/{reportId}')
    .onCreate((snapshot: functions.database.DataSnapshot, context: functions.EventContext) => {
        const report = snapshot.val();
        const payload = {
            notification: {
                title: 'New Company Report',
                body: `Reason: ${report.reason}`
            },
            topic: 'admin'
        };

        return admin.messaging().send(payload)
            .then((response: string) => { // Ovdje se koristi string umjesto MessagingTopicResponse
                console.log('Successfully sent message:', response);
            })
            .catch((error: Error) => {
                console.log('Error sending message:', error);
            });
    });

// Start writing functions
// https://firebase.google.com/docs/functions/typescript

// export const helloWorld = onRequest((request, response) => {
//   logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });
