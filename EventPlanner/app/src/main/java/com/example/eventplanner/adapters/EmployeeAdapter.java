package com.example.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.EmployeeWorkHoursFragment;
import com.example.eventplanner.fragments.RegisterPUPV2Fragment;
import com.example.eventplanner.fragments.UpdateEmployeeWorkHoursFragment;
import com.example.eventplanner.fragments.WorkingCalendarFragment;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder> {
    private List<Employee> employeeList;
    private Context context;
    private User user;
    private FirebaseAuth mAuth;

    public EmployeeAdapter(List<Employee> employeeList, Context context) {
        this.employeeList = employeeList;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }


    @NonNull
    @Override
    public EmployeeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_employee_card, parent, false);
        return new EmployeeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeViewHolder holder, int position) {
        user= new User();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {

           user= getUserById(currentUser.getUid(), holder);
        }

        Employee employee = employeeList.get(position);

        // Postavljanje podataka o zaposleniku u ViewHolder
        holder.textViewFirstName.setText(employee.getFirstName());
        holder.textViewLastName.setText(employee.getLastName());
        holder.textViewAddress.setText(employee.getAddress());
        holder.textViewEmail.setText(employee.getEmail());
        holder.textViewPhoneNumber.setText(employee.getPhoneNumber());
       // holder.imageViewProfilePicture.setText(employee.getProfilePicture());
        Picasso.get().load(employee.getProfilePicture()).into(holder.imageViewProfilePicture);




        holder.buttonActivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Employee employee = employeeList.get(holder.getAdapterPosition());


                updateEmployeeInDatabase(employee,true);
                notifyDataSetChanged();
                sendEmailVerification();

            }
        });

        /*
        holder.buttonWorkHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EmployeeWorkHoursFragment fragment = new EmployeeWorkHoursFragment();
                FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();

                // Početak transakcije fragmenta
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
*/
        holder.buttonWorkHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dobijte ID zaposlenog na koji je kliknuto
                String employeeId = employeeList.get(position).getId();

                // Kreirajte fragment sa potrebnim argumentima
                EmployeeWorkHoursFragment fragment = new EmployeeWorkHoursFragment();
                Bundle bundle = new Bundle();
                bundle.putString("employeeId", employeeId); // Prosledite ID zaposlenog kao argument fragmentu
                fragment.setArguments(bundle);

                // Prebacite se na WorkhoursFragment
                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
/*
        holder.buttonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkingCalendarFragment fragment = new WorkingCalendarFragment();
                FragmentManager fragmentManager = ((AppCompatActivity) context).getSupportFragmentManager();

                // Početak transakcije fragmenta
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

 */

        holder.buttonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dobijte ID zaposlenog na koji je kliknuto
                String employeeId = employeeList.get(position).getId();

                WorkingCalendarFragment fragment = new WorkingCalendarFragment();
                Bundle bundle = new Bundle();
                bundle.putString("employeeId", employeeId); // Prosledite ID zaposlenog kao argument fragmentu
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        holder.buttonDeactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Employee employee = employeeList.get(holder.getAdapterPosition());

                employee.setActive(false);
                updateEmployeeInDatabase(employee,false);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return employeeList.size();
    }

    public static class EmployeeViewHolder extends RecyclerView.ViewHolder {
        TextView textViewFirstName, textViewLastName, textViewAddress, textViewEmail, textViewPhoneNumber;
        ImageView imageViewProfilePicture;
        Button buttonActivate, buttonDeactivate;
        ImageButton buttonWorkHours,buttonCalendar;

        public EmployeeViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewFirstName = itemView.findViewById(R.id.textViewFirstName);
            textViewLastName = itemView.findViewById(R.id.textViewLastName);
            textViewAddress = itemView.findViewById(R.id.textViewAddress);
            textViewEmail = itemView.findViewById(R.id.textViewEmail);
            textViewPhoneNumber = itemView.findViewById(R.id.textViewPhoneNumber);
            imageViewProfilePicture = itemView.findViewById(R.id.imageViewUser);
            buttonActivate = itemView.findViewById(R.id.buttonActivate);
            buttonDeactivate = itemView.findViewById(R.id.buttonDeactivate);
            buttonWorkHours=itemView.findViewById(R.id.buttonWorkHours);
            buttonCalendar=itemView.findViewById(R.id.buttonCalendar);
        }
    }

    public void filterList(List<Employee> filteredList) {
        employeeList.clear();

        for (Employee employee : filteredList) {
            if (!employeeList.contains(employee)) { // Provjerite je li employeeList već sadrži ovog zaposlenika
                employeeList.add(employee); // Ako ne sadrži, dodajte ga
            }
        }
        notifyDataSetChanged();
    }
    public void setEmployeeList(List<Employee> newList) {
        employeeList = newList;
        notifyDataSetChanged();
    }

    private void updateEmployeeInDatabase(Employee employee,Boolean status) {
        DatabaseReference employeeRef = FirebaseDatabase.getInstance().getReference().child("employees").child(employee.getId());

        employeeRef.child("active").setValue(status);

    }
    public User getUserById(String userId,EmployeeViewHolder holder){
        FirebaseDatabase database = FirebaseDatabase.getInstance();


        DatabaseReference usersRef = database.getReference("Users");

        usersRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // Korisnik s proslijeđenim ID-om je pronađen
                    user = dataSnapshot.getValue(User.class);
                    Log.w("TAG", "ROLE" + user.getRole());
                    if(user!=null && user.getRole().equals("employee")){
                        setNotVisible(holder);
                    }else{
                        setVisible(holder);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Greška prilikom dohvatanja podataka
                Log.w("TAG", "Greška prilikom dohvatanja korisnika.", databaseError.toException());
            }
        });

        return user;
    }

    public void setNotVisible(EmployeeViewHolder holder){
        holder.buttonActivate.setVisibility(View.GONE);
        holder.buttonDeactivate.setVisibility(View.GONE);
    }

    public void setVisible(EmployeeViewHolder holder){
        holder.buttonActivate.setVisibility(View.VISIBLE);
        holder.buttonDeactivate.setVisibility(View.VISIBLE);
    }

    private void sendEmailVerification() {
        FirebaseUser user = mAuth.getCurrentUser(); // Dohvati trenutno prijavljenog korisnika
        if (user != null) {
            user.sendEmailVerification()
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                // Uspješno poslata verifikaciona email poruka
                                Toast.makeText(context, "Verification email sent to " + user.getEmail(), Toast.LENGTH_SHORT).show();
                            } else {
                                // Greška prilikom slanja verifikacione email poruke
                              //  Toast.makeText(context, "Failed to send verification email.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            // Korisnik nije prijavljen
            Toast.makeText(context, "User not authenticated.", Toast.LENGTH_SHORT).show();
        }
    }

}

