package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ServiceAdapter;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ServiceFragment extends Fragment {

    private ListView listViewServices;
    private ServiceAdapter adapter;
    private List<Service> serviceList;

    public ServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_service, container, false);

        // Initialize ListView
        listViewServices = view.findViewById(R.id.listViewServices);

        // Initialize Firebase database reference
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("services");

        // Initialize serviceList
        serviceList = new ArrayList<>();

        // Initialize adapter
        adapter = new ServiceAdapter(requireContext(), R.layout.fragment_service_card, serviceList);
        listViewServices.setAdapter(adapter);

        // Read data from Firebase database
        // Read data from Firebase database
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                serviceList.clear(); // Clear the service list before adding new ones from the database

                // Iterate through all services in the "services" collection
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Service service = snapshot.getValue(Service.class);
                    if (service != null && !service.isDeleted()) { // Check if the service is not marked as deleted
                        serviceList.add(service);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle errors
            }
        });


        // Set edit button click listener
        adapter.setOnEditButtonClickListener(new ServiceAdapter.EditButtonClickListener() {
            @Override
            public void onEditButtonClick(Service service) {
                openEditServiceFragment(service);
            }
        });
        adapter.setOnDeleteButtonClickListener(new ServiceAdapter.DeleteButtonClickListener() {
            @Override
            public void onDeleteButtonClick(Service service) {
                deleteService(service);
            }
        });
        // Set OnClickListener for editing service
        return view;
    }

    private void openEditServiceFragment(Service service) {
        // Kreiranje instance fragmenta za izmenu
        EditServiceFragment editServiceFragment = EditServiceFragment.newInstance(service);

        // Zamena trenutnog fragmenta fragmentom za izmenu
        requireActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, editServiceFragment)
                .addToBackStack(null)  // Dodavanje fragmenta na back stack
                .commit();
    }
    private void deleteService(Service service) {
        // Mark the product as deleted (logical deletion)
        service.setDeleted(true);

        // Notify database about the change
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("services").child(String.valueOf(service.getId()));
        databaseRef.setValue(service);
    }
}
