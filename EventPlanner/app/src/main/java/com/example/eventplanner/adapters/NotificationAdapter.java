package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductReservation;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private ArrayList<Notification> notifications;
    private String type;
    private Context context;

    public NotificationAdapter(Context context, ArrayList<Notification> notifications, String type) {
        this.context = context;
        this.notifications = notifications;
        this.type = type;
    }

    @NonNull
    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_card, parent, false);
        return new NotificationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationAdapter.ViewHolder holder, int position) {
        Notification n = notifications.get(position);
        holder.textViewNotificationText.setText(n.getMessage());
        holder.textViewNotificationStatus.setText(n.getStatus().toString());

        if(type.equals("Read")) {
            holder.buttonReadNotification.setVisibility(View.GONE);
        }

        if(n.getStatus() == Notification.Status.READ) {
            holder.buttonReadNotification.setVisibility(View.GONE);
        }

        // Postavljanje slušača klika na dugme za prikaz detalja
        holder.buttonReadNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                readNotification(n);
            }
        });

    }

    private void readNotification(Notification n) {
        DatabaseReference notifRef = FirebaseDatabase.getInstance().getReference().child("notifications").child(n.getId());
        n.setStatus(Notification.Status.READ);

        notifRef.setValue(n).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText((FragmentActivity) context, "Notification is read!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText((FragmentActivity) context, "Reading error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewNotificationText;
        public TextView textViewNotificationStatus;
        public ImageButton buttonReadNotification;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewNotificationText = itemView.findViewById(R.id.textViewNotificationText);
            textViewNotificationStatus = itemView.findViewById(R.id.textViewNotificationStatus);
            buttonReadNotification = itemView.findViewById(R.id.buttonReadNotification);
        }
    }
}
