package com.example.eventplanner.model;

public class Event2 {

    public String id;

    public String name;

    public String employeeId;

    public DailyWorkHours dailyWorkHours;

    public String eventType;

    public String date;

    public Event2(){}

    public Event2(String name, String employeeId, DailyWorkHours dailyWorkHours, String eventType, String date) {
        this.name = name;
        this.employeeId = employeeId;
        this.dailyWorkHours = dailyWorkHours;
        this.eventType = eventType;
        this.date=date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String  employeeId) {
        this.employeeId = employeeId;
    }

    public DailyWorkHours getDailyWorkHours() {
        return dailyWorkHours;
    }

    public void setDailyWorkHours(DailyWorkHours dailyWorkHours) {
        this.dailyWorkHours = dailyWorkHours;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
