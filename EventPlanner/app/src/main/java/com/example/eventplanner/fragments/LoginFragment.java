package com.example.eventplanner.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class LoginFragment extends Fragment {

    FirebaseAuth mAuth;
    private List<User> allUsers = new ArrayList<>();
    User user;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        user = new User();
        mAuth = FirebaseAuth.getInstance();

        Button buttonLogin = view.findViewById(R.id.buttonLogin);
        Button buttonRegisterPUPV = view.findViewById(R.id.buttonRegisterPUPV);
        Button buttonRegisterOD = view.findViewById(R.id.buttonRegisterOD);
        Button buttonViewCatalog = view.findViewById(R.id.buttonViewCatalog);

        EditText editTextEmail = view.findViewById(R.id.editTextEmail);
        EditText editTextPassword = view.findViewById(R.id.editTextPassword);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email, password;
                email = editTextEmail.getText().toString();
                password = editTextPassword.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(requireContext(), "Enter email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(requireContext(), "Enter password", Toast.LENGTH_SHORT).show();
                    return;
                }

                mAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                                    if (currentUser != null) {
                                        checkIfUserIsBlocked(currentUser.getUid());
                                    }
                                } else {
                                    Toast.makeText(requireContext(), "Login failed.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        buttonRegisterPUPV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new RegisterPUPVFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        buttonRegisterOD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new RegisterODFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        buttonViewCatalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new DetailsFragment())
                        .addToBackStack(null)
                        .commit();
            }
        });

        return view;
    }

    private void checkIfUserIsBlocked(String userId) {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);
        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null && user.isBlocked()) {
                        // User is blocked, log them out and show a message
                        mAuth.signOut();
                        Toast.makeText(requireContext(), "Your account is blocked. Please contact support.", Toast.LENGTH_LONG).show();
                    } else {
                        // User is not blocked, proceed with login
                        proceedWithLogin(user);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("LoginFragment", "Error checking if user is blocked", databaseError.toException());
            }
        });
    }

    private void proceedWithLogin(User user) {
        Log.w("TAG", "ROLE" + user.getRole());
        if ("employee".equals(user.getRole())) {
            Toast.makeText(requireContext(), "Login success.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish(); // Finish the AuthActivity if needed
        } else {
            Toast.makeText(requireContext(), "Login success.", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish(); // Finish the AuthActivity if needed
        }
    }

}
