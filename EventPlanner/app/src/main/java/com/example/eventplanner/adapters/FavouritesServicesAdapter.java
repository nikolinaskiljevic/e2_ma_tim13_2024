package com.example.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.ChatFragment;
import com.example.eventplanner.fragments.CompanyProfileFragment;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FavouritesServicesAdapter extends RecyclerView.Adapter<FavouritesServicesAdapter.ViewHolder>{

    private ArrayList<Service> allServices;
    private ArrayList<User> allUsers = new ArrayList<>();
    private ArrayList<Event> organizerEvents;
    private User currentUser;
    private Context context;

    public FavouritesServicesAdapter(Context context, ArrayList<Service> services, User user, ArrayList<Event> events) {
        this.context = context;
        this.allServices = services;
        this.currentUser = user;
        this.organizerEvents = events;
    }

    @NonNull
    @Override
    public FavouritesServicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favourites_service_card, parent, false);
        return new FavouritesServicesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouritesServicesAdapter.ViewHolder holder, int position) {
        Service s = allServices.get(position);
        holder.textViewServiceName.setText(s.getServiceName());
        holder.textViewServiceCategory.setText(s.getCategory());
        holder.textViewServiceSubcategory.setText(s.getSubcategory());
        holder.textViewServiceDescription.setText("Description: " + s.getDescription());
        holder.textViewServiceSpecifics.setText("Specifics: " + s.getSpecifics());
        holder.textViewServicePrice.setText("Price: " + String.format("%.2f", s.getPrice()) + "$");
        holder.textViewServiceDiscount.setText("Discount: " + String.format("%.2f", s.getDiscount()) + "%");
        holder.textViewServiceDuration.setText("Duration: " + s.getDuration());
        holder.textViewServiceBookingDeadline.setText("Booking deadline: " + s.getBookingDeadline());
        holder.textViewServiceCancellationDeadline.setText("Cancellation deadline: " + s.getCancellationDeadline());
        holder.textViewServiceReservationConfirmation.setText("Confirmation method: " + s.getReservationConfirmation());
        holder.textViewServiceAvailable.setText("Availability: " + (s.isAvailable() ? "Available" : "Unavailable"));
        holder.buttonReserveService.setText("Reserve " + s.getServiceName());
        setupSpinner(holder.spinnerOrganizerEvents);
        if(s.isAvailable()) {
            holder.buttonReserveService.setEnabled(true);
        }
        if(!currentUser.getFavouritesServicesIds().contains(s.getId())) {
            holder.buttonRemoveFromFavourites.setEnabled(false);
        }

        holder.buttonRemoveFromFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Service s = allServices.get(position);
                    // Ovde radite šta je potrebno sa objektom product, na primer dodavanje u omiljene
                    removeFromFavourites(s);
                }
            }
        });

        holder.buttonReserveService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event selectedEvent = findSelectedEvent(holder.spinnerOrganizerEvents);
                if (selectedEvent != null) {
                    reserveService(s, selectedEvent);
                } else {
                    Toast.makeText(context, "Please select an event to reserve the service.", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void removeFromFavourites(Service s) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());
        List<String> favServices = currentUser.getFavouritesServicesIds();
        favServices.removeIf(serv -> serv.equals(s.getId()));
        currentUser.setFavouritesServicesIds(favServices);

        userRef.setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText((FragmentActivity) context, "Removed!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText((FragmentActivity) context, "Removing error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setupSpinner(Spinner spinner) {
        // Kreiramo listu imena dogadjaja
        List<String> events = new ArrayList<>();
        for (Event e : organizerEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>((FragmentActivity) context, android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinner.setAdapter(spinnerAdapter);
    }

    private Event findSelectedEvent(Spinner spinner) {
        String selected = "";
        if(spinner.getSelectedItem() != null) {
            selected = spinner.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selected);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            return findEvent(eventName);
        } else {
            System.out.println("Not founded event");
            return null;
        }
    }

    private Event findEvent(String eventName) {
        for(Event e: organizerEvents) {
            if(e.getEventName().equals(eventName)) {
                Toast.makeText((FragmentActivity) context, "Selected event: " + e.getEventName(), Toast.LENGTH_SHORT).show();
                return e;
            }
        }

        return null;
    }

    private void reserveService(Service service, Event event) {

    }

    @Override
    public int getItemCount() {
        return allServices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewServiceName;
        public TextView textViewServiceCategory;
        public TextView textViewServiceSubcategory;
        public TextView textViewServiceDescription;
        public TextView textViewServiceSpecifics;
        public TextView textViewServicePrice;
        public TextView textViewServiceDiscount;
        public TextView textViewServiceDuration;
        public TextView textViewServiceBookingDeadline;
        public TextView textViewServiceCancellationDeadline;
        public TextView textViewServiceReservationConfirmation;
        public TextView textViewServiceAvailable;
        public Button buttonReserveService;
        public Button buttonRemoveFromFavourites;
        public Spinner spinnerOrganizerEvents;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewServiceName = itemView.findViewById(R.id.textViewServiceName);
            textViewServiceCategory = itemView.findViewById(R.id.textViewServiceCategory);
            textViewServiceSubcategory = itemView.findViewById(R.id.textViewServiceSubcategory);
            textViewServiceDescription = itemView.findViewById(R.id.textViewServiceDescription);
            textViewServiceSpecifics = itemView.findViewById(R.id.textViewServiceSpecifics);
            textViewServicePrice = itemView.findViewById(R.id.textViewServicePrice);
            textViewServiceDiscount = itemView.findViewById(R.id.textViewServiceDiscount);
            textViewServiceDuration = itemView.findViewById(R.id.textViewServiceDuration);
            textViewServiceBookingDeadline = itemView.findViewById(R.id.textViewServiceBookingDeadline);
            textViewServiceCancellationDeadline = itemView.findViewById(R.id.textViewServiceCancellationDeadline);
            textViewServiceReservationConfirmation = itemView.findViewById(R.id.textViewServiceReservationConfirmation);
            textViewServiceAvailable = itemView.findViewById(R.id.textViewServiceAvailable);
            buttonReserveService = itemView.findViewById(R.id.buttonReserveService);
            buttonRemoveFromFavourites = itemView.findViewById(R.id.buttonRemoveFromFavourites);
            spinnerOrganizerEvents = itemView.findViewById(R.id.spinnerOrganizerEvents);
        }
    }
}
