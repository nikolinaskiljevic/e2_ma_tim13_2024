package com.example.eventplanner.model;

import java.util.List;

public class PackageReservation {
    private String  id;
    private String  packageId;
    private String   odId;
    private List<String> serviceReservations;

    private String   status; // new,pupCanceled,odCanceled,adminCanceled,accepted,realized
    private String  cancellationTimeLimit;

    public PackageReservation(String packageId, String odId, List<String> serviceReservations, String status, String cancellationTimeLimit) {
        this.packageId = packageId;
        this.odId = odId;
        this.serviceReservations = serviceReservations;
        this.status = status;
        this.cancellationTimeLimit = cancellationTimeLimit;
    }

    public PackageReservation(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getOdId() {
        return odId;
    }

    public void setOdId(String odId) {
        this.odId = odId;
    }

    public List<String> getServiceReservations() {
        return serviceReservations;
    }

    public void setServiceReservations(List<String> serviceReservations) {
        this.serviceReservations = serviceReservations;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCancellationTimeLimit() {
        return cancellationTimeLimit;
    }

    public void setCancellationTimeLimit(String cancellationTimeLimit) {
        this.cancellationTimeLimit = cancellationTimeLimit;
    }
}
