package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.example.eventplanner.R;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.UUID;


public class ReportCompanyGradeFragment extends Fragment {

    private EditText editTextReason;

    private Button reportButton;

    private String companyGradeId;

    private String loggedInId;

    private NotificationRepository notificationRepository = new NotificationRepository();
    public ReportCompanyGradeFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_report_company_grade, container, false);

        editTextReason = view.findViewById(R.id.editTextReason);
        reportButton = view.findViewById(R.id.reportButton);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            loggedInId = currentUser.getUid();
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            companyGradeId = bundle.getString("companyGradeId");

        }

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = editTextReason.getText().toString();

                Date date= new Date();
                CompanyGradeReport report = new CompanyGradeReport(companyGradeId,date,loggedInId,reason,"REPORTED");
                saveReport(report);
                //prima admin 8RPmBrSkLlSU62mqeRkUOzTfO5R2
                saveMessage("New Report on Company Grade","8RPmBrSkLlSU62mqeRkUOzTfO5R2",report.getPupv());
            }
        });

        return view;
    }

    public void saveReport(CompanyGradeReport report){
        String id = UUID.randomUUID().toString();
        report.setReport(id);
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("CompanyGradeReports");
        databaseReference.child(id).setValue(report);
    }

    private void saveMessage(String content,String recipientId,String senderId) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference().child("messages");
        String messageId = messageRef.push().getKey();

        String date = LocalDateTime.now().toString();
        Message newMessage = new Message(messageId, senderId, recipientId, date, content, "unread");

        messageRef.child(messageId).setValue(newMessage);



        saveNotification(newMessage,senderId);
    }

    private void saveNotification(Message newMessage,String sender) {
        Notification notification = new Notification();
        notification.setStatus(Notification.Status.CREATED);
        notification.setMessage(newMessage.getContent());
        notification.setText("New message from " + sender+  ": " + newMessage.getContent());
        notification.setUserId(newMessage.getRecipientId());
        notificationRepository.createNotification(notification);
    }
}