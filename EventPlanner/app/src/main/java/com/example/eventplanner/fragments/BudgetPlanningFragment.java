package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.BudgetItemAdapter;
import com.example.eventplanner.model.BudgetItem;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventAgenda;
import com.example.eventplanner.model.EventCategory;
import com.example.eventplanner.model.EventSubcategory;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductCategory;
import com.example.eventplanner.model.ProductSubcategory;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BudgetPlanningFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BudgetPlanningFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Spinner spinnerSubcategory;
    private List<Event> allEvents = new ArrayList<>();
    private List<Event> organizerEvents = new ArrayList<>();
    private List<ProductCategory> allProductCategories = new ArrayList<>();
    private List<ProductSubcategory> allProductSubcategories = new ArrayList<>();
    private List<BudgetItem> allBudgetItems = new ArrayList<>();
    private ArrayList<BudgetItem> organizerBudgetItems = new ArrayList<>();
    private double budget = 0;
    private RecyclerView recyclerViewItems;
    private TextView textViewTotalBudget;
    private Spinner spinnerEvents;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public BudgetPlanningFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BudgetPlanningFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BudgetPlanningFragment newInstance(String param1, String param2) {
        BudgetPlanningFragment fragment = new BudgetPlanningFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_budget_planning, container, false);

        spinnerEvents = rootView.findViewById(R.id.spinnerOrganizerEvents);
        recyclerViewItems = rootView.findViewById(R.id.recyclerView);

        getAllEventsFromDatabase(spinnerEvents);
        getAllBudgetItems();
        //trebaju ti kategorije i podkategorije prozivoda i usluga

        RadioGroup radioGroup = rootView.findViewById(R.id.radioGroupPlanning);
        Spinner spinnerCategory = rootView.findViewById(R.id.spinnerCategory);
        //getAllProductCategoriesFromDatabase(spinnerCategory);
        spinnerSubcategory = rootView.findViewById(R.id.spinnerSubcategory);

        RadioButton radioButtonProduct = rootView.findViewById(R.id.radioButtonProduct);
        int radioButtonProductId = radioButtonProduct.getId();

        EditText etAmount = rootView.findViewById(R.id.editTextPlannedAmount);
        Button addButton = rootView.findViewById(R.id.buttonAddItem);

        textViewTotalBudget = rootView.findViewById(R.id.textViewTotalBudget);

        // Postavljanje osluškivača događaja za promene izbora u RadioGroup-u
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == radioButtonProductId) {
                    getAllProductCategoriesFromDatabase(spinnerCategory);
                }
                else {
                    // Postavljanje opcija za Spinner kada je odabrana opcija Favor
                    spinnerCategory.setAdapter(ArrayAdapter.createFromResource(
                            getContext(),
                            R.array.static_category_service_list,
                            android.R.layout.simple_spinner_item));

                    spinnerSubcategory.setAdapter(ArrayAdapter.createFromResource(
                            getContext(),
                            R.array.static_subcategory_service_list,
                            android.R.layout.simple_spinner_item));
                }

            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                getAllSubcategoriesFromDatabase(rootView, spinnerSubcategory);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Ne radimo ništa kada nije odabrana stavka
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Dobijanje unetih vrednosti
                Event selectedEvent = findSelectedEvent(spinnerEvents);
                String selectedCatOption = spinnerCategory.getSelectedItem().toString();
                String selectedCategory = selectedCatOption.substring(selectedCatOption.indexOf(":") + 2);
                String selectedSubcatOption = spinnerSubcategory.getSelectedItem().toString();
                String selectedSubcategory = selectedSubcatOption.substring(selectedSubcatOption.indexOf(":") + 2);
                String amountText = etAmount.getText().toString();
                double convertedAmount = Double.parseDouble(amountText);
                String type;
                if(radioButtonProduct.isChecked()) {
                    type = "Product";
                }
                else {
                    type = "Service";
                }

                // Provera da li su svi parametri uneti
                if (!selectedCategory.isEmpty() && !selectedSubcategory.isEmpty() && !amountText.isEmpty()) {
                    BudgetItem budgetItem = new BudgetItem(selectedEvent.getId(), selectedCategory, selectedSubcategory, convertedAmount, type);
                    saveBudgetItem(budgetItem);

                    try {
                        double amount = Double.parseDouble(amountText);
                        //budget += amount;
                        //budget = calculateTotalPlannedBudget();
                        //textViewTotalBudget.setText(String.format("Total budget: $%.2f", budget));
                        etAmount.setText("");
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                } else {
                    // Obavestite korisnika da neki od parametara nije unet i onemogućite dugme
                    Toast.makeText(getContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return rootView;
    }

    private double calculateTotalPlannedBudget() {
        double retVal = 0;
        for(BudgetItem i: organizerBudgetItems) {
            retVal += i.getPlannedAmount();
        }

        return retVal;
    }

    private void saveBudgetItem(BudgetItem budgetItem) {
        DatabaseReference budgetItemsRef = FirebaseDatabase.getInstance().getReference().child("budgetItems");
        String budgetItemId = budgetItemsRef.push().getKey();
        budgetItem.setId(budgetItemId);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        budgetItem.setOrganizerId(user.getUid());
        budgetItemsRef.child(budgetItemId).setValue(budgetItem);
        Toast.makeText(getContext(), "Budget item added successfully!", Toast.LENGTH_SHORT).show();
        getAllBudgetItems();
    }

    private void setupAdapter() {
        recyclerViewItems.setHasFixedSize(true);
        recyclerViewItems.setLayoutManager(new LinearLayoutManager(getContext()));
        BudgetItemAdapter adapter = new BudgetItemAdapter(getContext(), organizerBudgetItems);
        recyclerViewItems.setAdapter(adapter);

        budget = calculateTotalPlannedBudget();
        textViewTotalBudget.setText(String.format("Total budget: $%.2f", budget));
    }

    private void getAllBudgetItems() {
        DatabaseReference budgetItemRef = FirebaseDatabase.getInstance().getReference("budgetItems");
        budgetItemRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allBudgetItems.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    BudgetItem i = snapshot.getValue(BudgetItem.class);
                    if (i != null) {
                        allBudgetItems.add(i);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findOrganizerItems();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllBudgetItems", "Failed to read items", databaseError.toException());
            }
        });
    }

    private void findOrganizerItems() {
        organizerBudgetItems.clear();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        Event event = findSelectedEvent(spinnerEvents);
        String id = "nista";
        if(event != null){
            id = event.getId();
        }
        for(BudgetItem item: allBudgetItems) {
            if(item.getOrganizerId().equals(user.getUid()) && item.getEventId().equals(id)) {
                organizerBudgetItems.add(item);
            }
        }

        setupAdapter();
    }

    private void updateVisibility(View rootView, Spinner spinnerSubcat) {
        Spinner spinnerCategory = rootView.findViewById(R.id.spinnerCategory);
        String selectedOptionCategory = spinnerCategory.getSelectedItem().toString();
        String selectedCategory = selectedOptionCategory.substring(selectedOptionCategory.indexOf(":") + 2);

        for(ProductCategory e: allProductCategories) {
            if(e.getName().equals(selectedCategory)) {
                setupSubcategorySpinner(e);
                break;
            }
        }
    }

    private void getAllSubcategoriesFromDatabase(View rootView, Spinner spinner) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("productSubcategories");
        allProductSubcategories.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ProductSubcategory psc = snapshot.getValue(ProductSubcategory.class);
                    if (psc != null) {
                        allProductSubcategories.add(psc);
                    }
                }
                // Ovdje možete izvršiti željene akcije s listom podataka podkategorija
                // Na primjer, možete pozvati metod koji postavlja Spinner za podkategorije
                // setupSubcategorySpinner();
                updateVisibility(rootView, spinner);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Tretirajte grešku prilikom čitanja podataka iz baze
            }
        });
    }

    private void setupSubcategorySpinner(ProductCategory pc) {
        List<String> subcategories = new ArrayList<>();
        for (ProductSubcategory s : allProductSubcategories) {
            String var = "Subcategory: " + s.getName();
            if(s.getCategoryId().equals(pc.getId())) {
                subcategories.add(var);
            }
        }

        String noneVar = "Subcategory: Other";
        subcategories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSubcategory.setAdapter(spinnerAdapter);
    }

    private void getAllEventsFromDatabase(Spinner spinner) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("events");
        allEvents.clear();
        //databaseRef.orderByChild("ODid").equalTo(ODid).addListenerForSingleValueEvent(new ValueEventListener()
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener(){
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Event e = snapshot.getValue(Event.class);
                    if (e != null) {
                        allEvents.add(e);
                    }
                }
                // Ovdje možete izvršiti željene akcije s listom podataka podkategorija
                // Na primjer, možete pozvati metod koji postavlja Spinner za podkategorije
                // setupSubcategorySpinner();
                findOrganizerEvents(spinner);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Tretirajte grešku prilikom čitanja podataka iz baze
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                findOrganizerItems();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Ne radimo ništa kada nije odabrana stavka
            }
        });
    }

    private void setupEventsSpinner(Spinner spinnerSP) {
        // Kreiramo listu imena zaposlenih
        List<String> events = new ArrayList<>();
        for (Event e : organizerEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        if(organizerEvents.isEmpty()) {
            events.add("You do not have any event.");
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSP.setAdapter(spinnerAdapter);
    }

    private Event findSelectedEvent(Spinner spinner) {
        String selected = "";
        if(spinner.getSelectedItem() != null) {
            selected = spinner.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selected);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            return findEvent(eventName);
        } else {
            System.out.println("Not founded event");
            return null;
        }
    }

    private Event findEvent(String eventName) {
        for(Event e: organizerEvents) {
            if(e.getEventName().equals(eventName)) {
                Toast.makeText(getContext(), "Selected event: " + e.getEventName(), Toast.LENGTH_SHORT).show();
                return e;
            }
        }

        return null;
    }

    private void findOrganizerEvents(Spinner spinnerSP) {
        // Kreiramo listu imena zaposlenih
        organizerEvents.clear();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        for (Event e : allEvents) {
            if(e.getOrganizerId().equals(user.getUid()) ) {
                organizerEvents.add(e);
            }
        }
        setupEventsSpinner(spinnerSP);
    }

    private void getAllProductCategoriesFromDatabase(Spinner spinnerCat) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("productCategories");
        allProductCategories.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ProductCategory pc = snapshot.getValue(ProductCategory.class);
                    if (pc != null) {
                        allProductCategories.add(pc);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                setupCategorySpinner(spinnerCat);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }

    private void setupCategorySpinner(Spinner spinnerSP) {
        // Kreiramo listu imena zaposlenih
        List<String> categories = new ArrayList<>();
        for (ProductCategory e : allProductCategories) {
            String var = "Product category: " + e.getName();
            categories.add(var);
        }

        String noneVar = "Category: Other";
        categories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSP.setAdapter(spinnerAdapter);
    }

}