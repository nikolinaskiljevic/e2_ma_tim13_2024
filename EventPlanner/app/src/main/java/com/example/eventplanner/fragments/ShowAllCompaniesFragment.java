package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CompanyAdapter;
import com.example.eventplanner.adapters.CompanyGradeReportAdapter;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class ShowAllCompaniesFragment extends Fragment {
    private RecyclerView recyclerView;
    private CompanyAdapter adapter;
    private List<Company> companiesList;



    public ShowAllCompaniesFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_show_all_companies, container, false);


        recyclerView = view.findViewById(R.id.recyclerViewCompanies);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        companiesList = new ArrayList<>();
        populateCompanies();
        return view;
    }

    private void populateCompanies(){

        companiesList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference companiesRef = databaseReference.child("Companies");



        companiesRef.addListenerForSingleValueEvent(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<Company> newCompanyList = new ArrayList<>();

                for (DataSnapshot companySnapshot : dataSnapshot.getChildren()) {
                    Company company = companySnapshot.getValue(Company.class);
                    newCompanyList.add(company);
                }


                for (Company com : newCompanyList) {
                    if (!companiesList.contains(com)) {
                        companiesList.add(com);
                    }
                }
                Log.e("TAG","Companije"+ companiesList);
                // Nakon što dobijemo podatke, postavimo adapter za RecyclerView
                adapter = new CompanyAdapter(companiesList, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });

    }



}