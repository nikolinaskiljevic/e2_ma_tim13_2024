package com.example.eventplanner.model;

import java.time.LocalDateTime;

public class EventAgenda {
    private String id;
    private String eventId;
    private String organizerId;
    private String name;
    private String description;
    private String location;
    private String from;
    private String to;

    public EventAgenda() {
    }

    public EventAgenda(String id, String eventId, String organizerId, String name, String description, String location, String from, String to) {
        this.id = id;
        this.eventId = eventId;
        this.organizerId = organizerId;
        this.name = name;
        this.description = description;
        this.location = location;
        this.from = from;
        this.to = to;
    }


    public String getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(String organizerId) {
        this.organizerId = organizerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
