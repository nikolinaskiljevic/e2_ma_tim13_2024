package com.example.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.CompanyProfileFragment;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PackageDetailsAdapter extends RecyclerView.Adapter<PackageDetailsAdapter.ViewHolder> {

    private ArrayList<Package> allPackages;
    private ArrayList<Event> organizerEvents;
    private User currentUser;
    private String role;
    private Context context;

    public PackageDetailsAdapter(Context context, ArrayList<Package> packages, User user, String role, ArrayList<Event> events) {
        this.context = context;
        this.allPackages = packages;
        this.currentUser = user;
        this.role = role;
        this.organizerEvents = events;
    }

    @NonNull
    @Override
    public PackageDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.package_details_card, parent, false);
        return new PackageDetailsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageDetailsAdapter.ViewHolder holder, int position) {
        Package p = allPackages.get(position);
        holder.textViewPackageName.setText(p.getName());
        holder.textViewPackageDescription.setText(p.getDescription());
        holder.textViewPackagePrice.setText("Price: " + String.format("%.2f", p.getPrice()) + "$");
        holder.textViewPackageDiscount.setText("Discount: " + String.format("%d", p.getDiscount()) + "%");
        holder.textViewPackageCategory.setText("Category: " + p.getCategory());
        holder.textViewPackageSubcategories.setText("Subcategories: \n" + preparePrinting(p.getSubcategories()));
        holder.textViewPackageEventTypes.setText("Event types: \n" + preparePrinting(p.getEventTypes()));

        if(p.getSelectedProducts() == null) {
            holder.textViewPackageSelectedProducts.setText("Products: There is no products in this package.");
        }
        else {
            holder.textViewPackageSelectedProducts.setText("Products: \n" + prepareProductsPrinting(p.getSelectedProducts()));
        }

        if(p.getSelectedServices() == null) {
            holder.textViewPackageSelectedServices.setText("Services: There is no services in this package.");
        }
        else {
            holder.textViewPackageSelectedServices.setText("Services: \n" + prepareServicesPrinting(p.getSelectedServices()));
        }
        holder.textViewPackageReservationDeadline.setText("Reservation deadline: " + p.getReservationDeadline());
        holder.textViewPackageCancellationDeadline.setText("Cancellation deadline: " + p.getCancellationDeadline());
        holder.textViewPackageConfirmationMethod.setText("Confirmation method: " + p.getConfirmationMethod());
        holder.textViewPackageAvailable.setText("Availability: " + (p.isAvailable() ? "Available" : "Unavailable"));
        holder.buttonReservePackage.setText("Reserve " + p.getName());
        if(p.isAvailable()) {
            holder.buttonReservePackage.setEnabled(true);
        }
        if(role.equals("organizer")) {
            holder.buttonAddToFavourites.setVisibility(View.VISIBLE);
            holder.spinnerOrganizerEvents.setVisibility(View.VISIBLE);
            setupSpinner(holder.spinnerOrganizerEvents);
        }
        else {
            holder.buttonReservePackage.setVisibility(View.GONE);
            holder.spinnerOrganizerEvents.setVisibility(View.GONE);
        }
        if(role.equals("organizer") && currentUser.getFavouritesPackagesIds().contains(p.getId())) {
            holder.buttonAddToFavourites.setEnabled(false);
        }

        holder.buttonReservePackage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event selectedEvent = findSelectedEvent(holder.spinnerOrganizerEvents);
                // Poziv metode za rezervisanje paketa
            }
        });

        holder.buttonCompanyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Pretpostavljam da vaš proizvod ima atribut companyId
                String companyId = p.getCompanyId();

                // Kreiraj instancu CompanyProfileFragment sa companyId
                CompanyProfileFragment companyProfileFragment = CompanyProfileFragment.newInstance(companyId, role);

                // Pokreni transakciju fragmenta
                FragmentActivity activity = (FragmentActivity) context;
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, companyProfileFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        holder.buttonAddToFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Package p = allPackages.get(position);
                    // Ovde radite šta je potrebno sa objektom product, na primer dodavanje u omiljene
                    addToFavourites(p);
                }
            }
        });
    }

    private Event findSelectedEvent(Spinner spinner) {
        String selected = "";
        if(spinner.getSelectedItem() != null) {
            selected = spinner.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selected);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            return findEvent(eventName);
        } else {
            System.out.println("Not founded event");
            return null;
        }
    }

    private Event findEvent(String eventName) {
        for(Event e: organizerEvents) {
            if(e.getEventName().equals(eventName)) {
                Toast.makeText((FragmentActivity) context, "Selected event: " + e.getEventName(), Toast.LENGTH_SHORT).show();
                return e;
            }
        }

        return null;
    }

    private void setupSpinner(Spinner spinner) {
        // Kreiramo listu imena dogadjaja
        List<String> events = new ArrayList<>();
        for (Event e : organizerEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>((FragmentActivity) context, android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinner.setAdapter(spinnerAdapter);
    }

    private void addToFavourites(Package p) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());
        List<String> favPackages = currentUser.getFavouritesPackagesIds();
        favPackages.add(p.getId());
        currentUser.setFavouritesPackagesIds(favPackages);

        userRef.setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText((FragmentActivity) context, "Well done!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText((FragmentActivity) context, "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private String prepareServicesPrinting(List<Service> services) {
        StringBuilder retval = new StringBuilder();
        int i = 1;
        for(Service s: services) {
            if(retval.toString().isEmpty()) {
                retval = new StringBuilder("\t" + Integer.toString(i) + ". " + s.getServiceName() + "\n" +
                        "\t\t\tCategory: " + s.getCategory() + "\n" +
                        "\t\t\tSubcategory: " + s.getSubcategory() + "\n" +
                        "\t\t\tDescription: " + s.getDescription() + "\n" +
                        "\t\t\tSpecifics: " + s.getSpecifics() + "\n" +
                        "\t\t\tPrice: " + String.format("%.2f", s.getPrice()) + "$\n" +
                        "\t\t\tDiscount: " + String.format("%.2f", s.getDiscount()) + "%\n" +
                        "\t\t\tDuration: " + s.getDuration() + "\n" +
                        "\t\t\tReservation deadline: " + s.getBookingDeadline() + "\n" +
                        "\t\t\tCancellation deadline: " + s.getCancellationDeadline() + "\n" +
                        "\t\t\tConfirmation method: " + s.getReservationConfirmation() + "\n" +
                        "\t\t\tAvailability: " + (s.isAvailable() ? "Available" : "Unavailable") + "\n");
                i++;
            }
            else {
                retval = new StringBuilder(retval + "\t" + Integer.toString(i) + ". " + s.getServiceName() + "\n" +
                        "\t\t\tCategory: " + s.getCategory() + "\n" +
                        "\t\t\tSubcategory: " + s.getSubcategory() + "\n" +
                        "\t\t\tDescription: " + s.getDescription() + "\n" +
                        "\t\t\tSpecifics: " + s.getSpecifics() + "\n" +
                        "\t\t\tPrice: " + String.format("%.2f", s.getPrice()) + "$\n" +
                        "\t\t\tDiscount: " + String.format("%.2f", s.getDiscount()) + "%\n" +
                        "\t\t\tDuration: " + s.getDuration() + "\n" +
                        "\t\t\tReservation deadline: " + s.getBookingDeadline() + "\n" +
                        "\t\t\tCancellation deadline: " + s.getCancellationDeadline() + "\n" +
                        "\t\t\tConfirmation method: " + s.getReservationConfirmation() + "\n" +
                        "\t\t\tAvailability: " + (s.isAvailable() ? "Available" : "Unavailable") + "\n");
                i++;
            }
        }

        return retval.toString();
    }

    private String prepareProductsPrinting(List<Product> products) {
        StringBuilder retval = new StringBuilder();
        int i = 1;
        for(Product p: products) {
            if(retval.toString().isEmpty()) {
                retval = new StringBuilder("\t" + Integer.toString(i) + ". " + p.getName() + "\n" +
                        "\t\t\tCategory: " + p.getCategory() + "\n" +
                        "\t\t\tSubcategory: " + p.getSubcategory() + "\n" +
                        "\t\t\tDescription: " + p.getDescription() + "\n" +
                        "\t\t\tPrice: " + String.format("%.2f", p.getPrice()) + "$\n" +
                        "\t\t\tDiscount: " + String.format("%.2f", p.getDiscount()) + "%\n" +
                        "\t\t\tEvent types: " + p.getEventTypes() + "\n" +
                        "\t\t\tAvailability: " + (p.isAvailable() ? "Available" : "Unavailable") + "\n");
                i++;
            }
            else {
                retval = new StringBuilder(retval + "\t" + Integer.toString(i) + ". " + p.getName() + "\n" +
                        "\t\t\tCategory: " + p.getCategory() + "\n" +
                        "\t\t\tSubcategory: " + p.getSubcategory() + "\n" +
                        "\t\t\tDescription: " + p.getDescription() + "\n" +
                        "\t\t\tPrice: " + String.format("%.2f", p.getPrice()) + "$\n" +
                        "\t\t\tDiscount: " + String.format("%.2f", p.getDiscount()) + "%\n" +
                        "\t\t\tEvent types: " + p.getEventTypes() + "\n" +
                        "\t\t\tAvailability: " + (p.isAvailable() ? "Available" : "Unavailable") + "\n");
                i++;
            }
        }

        return retval.toString();
    }

    private String preparePrinting(List<String> strings) {
        StringBuilder retval = new StringBuilder();
        int i = 1;
        for(String s: strings) {
            if(retval.toString().isEmpty()) {
                retval = new StringBuilder("\t" + Integer.toString(i) + ". " + s + "\n");
                i++;
            }
            else {
                retval.append("\t").append(Integer.toString(i)).append(". ").append(s).append("\n");
                i++;
            }
        }

        return retval.toString();
    }

    @Override
    public int getItemCount() {
        return allPackages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewPackageName;
        public TextView textViewPackageDescription;
        public TextView textViewPackagePrice;
        public TextView textViewPackageDiscount;
        public TextView textViewPackageCategory;
        public TextView textViewPackageSubcategories;
        public TextView textViewPackageEventTypes;
        public TextView textViewPackageSelectedProducts;
        public TextView textViewPackageSelectedServices;
        public TextView textViewPackageReservationDeadline;
        public TextView textViewPackageCancellationDeadline;
        public TextView textViewPackageConfirmationMethod;
        public TextView textViewPackageAvailable;
        public Button buttonReservePackage;
        public Button buttonCompanyProfile;
        public Button buttonAddToFavourites;
        public Spinner spinnerOrganizerEvents;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewPackageName = itemView.findViewById(R.id.textViewPackageName);
            textViewPackageDescription = itemView.findViewById(R.id.textViewPackageDescription);
            textViewPackagePrice = itemView.findViewById(R.id.textViewPackagePrice);
            textViewPackageDiscount = itemView.findViewById(R.id.textViewPackageDiscount);
            textViewPackageCategory = itemView.findViewById(R.id.textViewPackageCategory);
            textViewPackageSubcategories = itemView.findViewById(R.id.textViewPackageSubcategories);
            textViewPackageEventTypes = itemView.findViewById(R.id.textViewPackageEventTypes);
            textViewPackageSelectedProducts = itemView.findViewById(R.id.textViewPackageSelectedProducts);
            textViewPackageSelectedServices = itemView.findViewById(R.id.textViewPackageSelectedServices);
            textViewPackageReservationDeadline = itemView.findViewById(R.id.textViewPackageReservationDeadline);
            textViewPackageCancellationDeadline = itemView.findViewById(R.id.textViewPackageCancellationDeadline);
            textViewPackageConfirmationMethod = itemView.findViewById(R.id.textViewPackageConfirmationMethod);
            textViewPackageAvailable = itemView.findViewById(R.id.textViewPackageAvailable);
            buttonReservePackage = itemView.findViewById(R.id.buttonReservePackage);
            buttonCompanyProfile = itemView.findViewById(R.id.buttonCompanyProfile);
            buttonAddToFavourites = itemView.findViewById(R.id.buttonAddToFavourites);
            spinnerOrganizerEvents = itemView.findViewById(R.id.spinnerOrganizerEvents);
        }
    }

}
