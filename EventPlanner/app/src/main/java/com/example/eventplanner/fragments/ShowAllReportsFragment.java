package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CompanyGradeReportAdapter;
import com.example.eventplanner.adapters.EmployeeAdapter;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class ShowAllReportsFragment extends Fragment {

    private RecyclerView recyclerView;
    private CompanyGradeReportAdapter adapter;
    private List<CompanyGradeReport> reportList;

    public ShowAllReportsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_show_all_reports, container, false);

        recyclerView = view.findViewById(R.id.recyclerViewReports);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        reportList = new ArrayList<>();
        populateReports();

        return view;
    }

    private void populateReports(){

        reportList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference reportsRef = databaseReference.child("CompanyGradeReports");


        Log.e("TAG","USAO"+ reportsRef);
        reportsRef.addListenerForSingleValueEvent(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<CompanyGradeReport> newReportList = new ArrayList<>();

                for (DataSnapshot reportSnapshot : dataSnapshot.getChildren()) {
                    CompanyGradeReport report = reportSnapshot.getValue(CompanyGradeReport.class);
                    newReportList.add(report);
                }


                for (CompanyGradeReport report : newReportList) {
                    if (!reportList.contains(report) ) {
                        reportList.add(report);
                    }
                }

                Log.e("TAG","PRIJE ADAPTERA"+ reportList);
                // Nakon što dobijemo podatke, postavimo adapter za RecyclerView
                adapter = new CompanyGradeReportAdapter(reportList, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });

    }

}