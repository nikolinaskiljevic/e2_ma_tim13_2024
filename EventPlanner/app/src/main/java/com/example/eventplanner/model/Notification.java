package com.example.eventplanner.model;

public class Notification {

    private String id;
    private String text;
    private String message;
    private Status status;
    private String userId;

    public enum Status {
        CREATED, SHOWN, READ
    }

    public Notification() {
    }

    public Notification(String id, String text, String message, Status status, String userId) {
        this.id = id;
        this.text = text;
        this.message = message;
        this.status = status;
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
