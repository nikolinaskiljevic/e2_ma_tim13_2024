
package com.example.eventplanner.notifications;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


public class ForegroundService extends Service {
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (action != null) {
            if (action.equals("ACTION_PLAY")) {
                // Pokrenite plejer ili izvršite odgovarajuću akciju
            }
        }
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
