package com.example.eventplanner.fragments;

import android.os.Bundle;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.SearchResultAdapter;
import com.example.eventplanner.dto.SearchParametersDTO;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.Package;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchResultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchResultFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private SearchParametersDTO mParams;
    private List<Product> allProducts = new ArrayList<>();
    private List<Product> bindingProducts = new ArrayList<>();
    private List<Package> allPackages = new ArrayList<>();
    private List<Package> bindingPackages = new ArrayList<>();
    private List<Service> allServices = new ArrayList<>();
    private List<Service> bindingServices = new ArrayList<>();

    public SearchResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * //@param param1 Parameter 1.
     * @return A new instance of fragment SearchResultFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchResultFragment newInstance(SearchParametersDTO dto) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, dto); // Koristimo putSerializable() za postavljanje objekta u Bundle
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParams = (SearchParametersDTO) getArguments().getSerializable(ARG_PARAM1); // Koristimo getSerializable() za dobijanje objekta iz Bundle-a
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search_result, container, false);

        if(mParams.Filter.equals("Service")) {
            bindingServices.clear();
            getAllServicesFromDatabase(view);
        }
        else if(mParams.Filter.equals("Product")) {
            bindingProducts.clear();
            getAllProductsFromDatabase(view);
        }
        else {
            bindingPackages.clear();
            getAllPackagesFromDatabase(view);
        }

        return view;
    }

    private void getAllProductsFromDatabase(View view) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products");
        allProducts.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product product = snapshot.getValue(Product.class);
                    if (product != null) {
                        allProducts.add(product);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                handleProducts(view);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }

    private void handleProducts(View view) {
        // Ovde se nalazi kod koji obrađuje proizvode
        // Ovaj kod će se izvršiti tek nakon što su podaci o proizvodima učitani iz baze
        // Na primer, možeš pozvati `for` petlju ili bilo koji drugi kod koji zavisi od podataka o proizvodima
        for(Product p: allProducts) {
            boolean subcategory;
            if(mParams.Subcategory.equals("None") || mParams.Subcategory.equals("none") || mParams.Subcategory.equals("")) {
                subcategory = true;
            }
            else {
                subcategory = mParams.Subcategory.equals(p.getSubcategory());
            }

            boolean price;
            if(mParams.MinPrice != 0 && mParams.MaxPrice != 0) {
                price = mParams.MinPrice <= p.getPrice() && p.getPrice() <= mParams.MaxPrice;
            }
            else if(mParams.MinPrice == 0 && mParams.MaxPrice != 0) {
                price = p.getPrice() <= mParams.MaxPrice;
            }
            else if(mParams.MinPrice != 0 && mParams.MaxPrice == 0) {
                price = mParams.MinPrice <= p.getPrice();
            }
            else {
                price = true;
            }

            boolean availability;
            if(mParams.Availability.equals("Available") && p.isAvailable())
                availability = true;
            else if(mParams.Availability.equals("Unavailable") && !p.isAvailable())
                availability = true;
            else if(mParams.Availability.equals("none"))
                availability = true;
            else
                availability = false;

            boolean type = p.getEventTypes().contains(mParams.Type) || mParams.Type.equals("none");
            boolean name = p.getName().toLowerCase().contains(mParams.Name.toLowerCase()) || mParams.Name.isEmpty();
            boolean category = p.getCategory().equals(mParams.Category) || mParams.Category.equals("none");

            if(type && name && subcategory && category && price && availability) {
                System.out.println(p.getName());
                bindingProducts.add(p);
            }
        }

        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewTable);

        // Kreirajte layout manager za RecyclerView (možete koristiti LinearLayoutManager ili GridLayoutManager)
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // Kreirajte adapter sa podacima koje želite prikazati
        SearchResultAdapter adapter = new SearchResultAdapter(bindingProducts, bindingServices, bindingPackages, true, false, false);

        // Postavite adapter na RecyclerView
        recyclerView.setAdapter(adapter);
    }

    private void handlePackages(View view) {
        // Ovde se nalazi kod koji obrađuje proizvode
        // Ovaj kod će se izvršiti tek nakon što su podaci o proizvodima učitani iz baze
        // Na primer, možeš pozvati `for` petlju ili bilo koji drugi kod koji zavisi od podataka o proizvodima
        for(Package p: allPackages) {

            boolean price;
            if(mParams.MinPrice != 0 && mParams.MaxPrice != 0) {
                price = mParams.MinPrice <= p.getPrice() && p.getPrice() <= mParams.MaxPrice;
            }
            else if(mParams.MinPrice == 0 && mParams.MaxPrice != 0) {
                price = p.getPrice() <= mParams.MaxPrice;
            }
            else if(mParams.MinPrice != 0 && mParams.MaxPrice == 0) {
                price = mParams.MinPrice <= p.getPrice();
            }
            else {
                price = true;
            }

            boolean availability;
            if(mParams.Availability.equals("Available") && p.isAvailable())
                availability = true;
            else if(mParams.Availability.equals("Unavailable") && !p.isAvailable())
                availability = true;
            else if(mParams.Availability.equals("none"))
                availability = true;
            else
                availability = false;

            boolean type = p.getEventTypes().contains(mParams.Type) || mParams.Type.equals("none");
            boolean name = p.getName().toLowerCase().contains(mParams.Name.toLowerCase()) || mParams.Name.isEmpty();
            boolean category = p.getCategory().equals(mParams.Category) || mParams.Category.equals("none");

            if(type && name && category && price && availability) {
                //System.out.println(p.getName());
                bindingPackages.add(p);
            }
        }

        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewTable);

        // Kreirajte layout manager za RecyclerView (možete koristiti LinearLayoutManager ili GridLayoutManager)
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // Kreirajte adapter sa podacima koje želite prikazati
        SearchResultAdapter adapter = new SearchResultAdapter(bindingProducts, bindingServices, bindingPackages, false, true, false);

        // Postavite adapter na RecyclerView
        recyclerView.setAdapter(adapter);
    }

    private void handleServices(View view) {
        for(Service s: allServices) {
            boolean type = s.getEventTypes().contains(mParams.Type) || mParams.Type.equals("none");
            boolean name = s.getServiceName().toLowerCase().contains(mParams.Name.toLowerCase()) || mParams.Name.isEmpty();
            boolean category = s.getCategory().equals(mParams.Category) || mParams.Category.equals("none");
            boolean subcategory;
            if(mParams.Subcategory.equals("None") || mParams.Subcategory.equals("none") || mParams.Subcategory.equals("")) {
                subcategory = true;
            }
            else {
                subcategory = mParams.Subcategory.equals(s.getSubcategory());
            }
            boolean provider = false;
            /*for(Employee e: s.getEmployees()) {
                boolean var = mParams.Provider.equals("Stefan") || mParams.Provider.equals("Radoslav") || mParams.Provider.equals("Luka")
                        || mParams.Provider.equals("Slobodan") || mParams.Provider.equals("Vladimir");
                if(var || (e.getFirstName() + " " + e.getLastName()).equals(mParams.Provider) || mParams.Provider.equals("None")) {
                    provider = true;
                }
            }*/
            boolean price;
            if(mParams.MinPrice != 0 && mParams.MaxPrice != 0) {
                price = mParams.MinPrice <= s.getPrice() && s.getPrice() <= mParams.MaxPrice;
            }
            else if(mParams.MinPrice == 0 && mParams.MaxPrice != 0) {
                price = s.getPrice() <= mParams.MaxPrice;
            }
            else if(mParams.MinPrice != 0 && mParams.MaxPrice == 0) {
                price = mParams.MinPrice <= s.getPrice();
            }
            else {
                price = true;
            }
            LocalDate currentDate = LocalDate.now();

            int number = 0;
            String[] parts = s.getBookingDeadline().split(" ");
            // Ako postoje dijelovi i prvi dio može biti pretvoren u broj, pretvori ga
            if (parts.length > 0) {
                try {
                    number = Integer.parseInt(parts[0]); // Pretvaranje prvog dijela u broj
                    System.out.println("Izvučeni broj: " + number);
                } catch (NumberFormatException e) {
                    System.out.println("Nije pronađen broj u stringu.");
                }
            } else {
                System.out.println("Nije pronađen broj u stringu.");
            }

            LocalDate futureDate = currentDate.plusMonths(number);

            boolean dateMatcher;
            if(mParams.StartDate.equals(currentDate) && mParams.EndDate.equals(currentDate)) {
                dateMatcher = true;
            }
            else {
                dateMatcher = (futureDate.isAfter(mParams.StartDate) || futureDate.equals(mParams.StartDate)) && (futureDate.isBefore(mParams.EndDate) || futureDate.equals(mParams.EndDate));
            }

            boolean availability;
            if(mParams.Availability.equals("Available") && s.isAvailable())
                availability = true;
            else if(mParams.Availability.equals("Unavailable") && !s.isAvailable())
                availability = true;
            else if(mParams.Availability.equals("none"))
                availability = true;
            else
                availability = false;

            if(type && name && category && subcategory && provider && price && dateMatcher && availability
                /*s.getLocation().toLowerCase().contains(mParams.Name.toLowerCase())*/) {
                bindingServices.add(s);
            }
        }

        RecyclerView recyclerView = view.findViewById(R.id.recyclerViewTable);

        // Kreirajte layout manager za RecyclerView (možete koristiti LinearLayoutManager ili GridLayoutManager)
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        // Kreirajte adapter sa podacima koje želite prikazati
        SearchResultAdapter adapter = new SearchResultAdapter(bindingProducts, bindingServices, bindingPackages, false, false, true);

        // Postavite adapter na RecyclerView
        recyclerView.setAdapter(adapter);
    }

    private void getAllPackagesFromDatabase(View view) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("packages");
        allPackages.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Package p = snapshot.getValue(Package.class);
                    if (p != null) {
                        allPackages.add(p);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                handlePackages(view);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }

    private void getAllServicesFromDatabase(View view) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("services");
        allServices.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Service service = snapshot.getValue(Service.class);
                    if (service != null) {
                        allServices.add(service);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                handleServices(view);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }


}