package com.example.eventplanner.fragments;

import static com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.DailyWorkHours;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.EmployeeWorkPeriod;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WeeklyWorkHours;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class UpdateEmployeeWorkHoursFragment extends Fragment {

    private List<String> employeeList;
    private Button buttonUpdate;

    private WeeklyWorkHours weeklyWorkHours;
    private Spinner spinnerEmployees;
    private EditText editTextFrom,editTextTo,  editTextMondayStart, editTextMondayEnd, editTextTuesdayStart, editTextTuesdayEnd, editTextWednesdayStart, editTextWednesdayEnd, editTextThursdayStart, editTextFridayStart, editTextFridayEnd, editTextThursdayEnd;

    public UpdateEmployeeWorkHoursFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_update_employee_work_hours, container, false);


        employeeList= new ArrayList<>();

        editTextFrom= view.findViewById(R.id.editTextFrom);
        editTextTo=view.findViewById(R.id.editTextTo);

        editTextMondayStart = view.findViewById(R.id.editTextMondayStart);
        editTextMondayEnd = view.findViewById(R.id.editTextMondayEnd);

        editTextTuesdayStart = view.findViewById(R.id.editTextTuesdayStart);
        editTextTuesdayEnd = view.findViewById(R.id.editTextTuesdayEnd);

        editTextWednesdayStart = view.findViewById(R.id.editTextWednesdayStart);
        editTextWednesdayEnd = view.findViewById(R.id.editTextWednesdayEnd);

        editTextThursdayStart = view.findViewById(R.id.editTextThursdayStart);
        editTextThursdayEnd = view.findViewById(R.id.editTextThursdayEnd);

        editTextFridayStart = view.findViewById(R.id.editTextFridayStart);
        editTextFridayEnd = view.findViewById(R.id.editTextFridayEnd);

        spinnerEmployees = view.findViewById(R.id.spinnerEmployees);

        weeklyWorkHours = new WeeklyWorkHours();
        buttonUpdate = view.findViewById(R.id.buttonUpdate);

        populateEmployees();

        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String selectedEmployee = spinnerEmployees.getSelectedItem().toString();

                DailyWorkHours monday = new DailyWorkHours("Monday", editTextMondayStart.getText().toString(), editTextMondayEnd.getText().toString());
                DailyWorkHours tuesday = new DailyWorkHours("Tuesday", editTextTuesdayStart.getText().toString().trim(), editTextTuesdayEnd.getText().toString().trim());
                DailyWorkHours wednesday = new DailyWorkHours("Wednesday", editTextWednesdayStart.getText().toString().trim(), editTextWednesdayEnd.getText().toString().trim());
                DailyWorkHours thursday = new DailyWorkHours("Thursday", editTextThursdayStart.getText().toString().trim(), editTextThursdayEnd.getText().toString().trim());
                DailyWorkHours friday = new DailyWorkHours("Friday", editTextFridayStart.getText().toString().trim(), editTextFridayEnd.getText().toString().trim());

                List<DailyWorkHours> dailyWorkHoursList = new ArrayList<>();
                dailyWorkHoursList.add(monday);
                dailyWorkHoursList.add(tuesday);
                dailyWorkHoursList.add(wednesday);
                dailyWorkHoursList.add(thursday);
                dailyWorkHoursList.add(friday);


                weeklyWorkHours.setDailyWorkHoursList(dailyWorkHoursList);
                //AKO VEC POSTOJI U BAZI NE KREIRAS NOVI


                EmployeeWorkPeriod employeeWorkPeriod = new EmployeeWorkPeriod(editTextFrom.getText().toString().trim(), editTextTo.getText().toString().trim());
                //  saveWeeklyWorkhours(employeeWorkPeriod, selectedEmployee);
                removeEmployeeIdFromWorkPeriod(selectedEmployee,employeeWorkPeriod);





            }
        });



        return view;
    }

    private void populateEmployees(){

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersRef = databaseReference.child("Users");

        List<User> users= new ArrayList<>();

        // Dohvaćanje podataka iz čvora "employees"
        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Ovdje možete obraditi podatke koje ste dobili iz baze podataka
                // dataSnapshot sadrži sve podatke iz čvora "employees"

                for (DataSnapshot usersSnapshot : dataSnapshot.getChildren()) {
                    User user = usersSnapshot.getValue(User.class);
                    users.add(user);
                }

                populateEmmployeesFromUsers(users,employeeList);

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, employeeList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerEmployees.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void removeEmployeeIdFromWorkPeriod(String employeeId,EmployeeWorkPeriod employeeWorkPeriod) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference workPeriodRef = databaseReference.child("employeeWorkPeriod");

        // Dohvaćanje podataka o EmployeeWorkPeriod iz baze
        workPeriodRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot workPeriodSnapshot : dataSnapshot.getChildren()) {
                    EmployeeWorkPeriod workPeriod = workPeriodSnapshot.getValue(EmployeeWorkPeriod.class);

                    // Pronalazi se EmployeeWorkPeriod koji sadrži proslijeđeni employeeId
                    if (workPeriod != null && workPeriod.getEmployeesId() != null && workPeriod.getEmployeesId().contains(employeeId)) {
                        // Ako je pronađen EmployeeWorkPeriod s traženim employeeId, ukloni ga iz liste
                        workPeriod.getEmployeesId().remove(employeeId);

                        // Provjeri je li lista employeesId prazna nakon uklanjanja
                        if (workPeriod.getEmployeesId().isEmpty()) {
                            // Ako je lista prazna, obriši cijeli EmployeeWorkPeriod iz baze
                            workPeriodRef.child(workPeriodSnapshot.getKey()).removeValue();
                        } else {
                            // Ako lista nije prazna, ažuriraj promjene u bazi podataka
                            workPeriodRef.child(workPeriodSnapshot.getKey()).setValue(workPeriod);
                        }
                    }
                }

                saveWeeklyWorkhours(employeeWorkPeriod, employeeId);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Prikazati poruku o grešci ako je potrebno
            }
        });
    }

    private void saveWeeklyWorkhours(EmployeeWorkPeriod period, String employeeId) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("weeklyWorkHours");

        // Izvršavanje upita za provjeru postojećih podataka
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean exists = false;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    WeeklyWorkHours existingWeeklyWorkHours = snapshot.getValue(WeeklyWorkHours.class);
                    // Provjera jednakosti objekata po dailyWorkHoursList
                    if (existingWeeklyWorkHours != null && isEqualByDailyWorkHoursList(existingWeeklyWorkHours, weeklyWorkHours)) {
                        exists = true;
                        weeklyWorkHours.setId(existingWeeklyWorkHours.getId());
                        period.setWeeklyWorkHoursId(weeklyWorkHours.getId());

                        saveEmployeeWorkPeriod(period, employeeId);
                        break;
                    }
                }
                // Ako objekt već postoji, ne spremamo ga ponovno
                if (!exists) {
                    long lastId = dataSnapshot.getChildrenCount() + 1; // Generisanje novog ID-a

                    String Id = String.valueOf(lastId);

                    weeklyWorkHours.setId(Id);

                    databaseRef.child(Id).setValue(weeklyWorkHours);
                    period.setWeeklyWorkHoursId(weeklyWorkHours.getId());

                    saveEmployeeWorkPeriod(period, employeeId);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Error fetching data", databaseError.toException());
            }
        });
    }
    private boolean isEqualByDailyWorkHoursList(WeeklyWorkHours a, WeeklyWorkHours b) {
        List<DailyWorkHours> aList = a.getDailyWorkHoursList();
        List<DailyWorkHours> bList = b.getDailyWorkHoursList();

        if (aList.size() != bList.size()) {
            return false;
        }

        // Provjera jednakosti elemenata po svakom indeksu
        for (int i = 0; i < aList.size(); i++) {
            DailyWorkHours aDaily = aList.get(i);
            DailyWorkHours bDaily = bList.get(i);
            // Usporedba po svim poljima
            if (!aDaily.getDay().equals(bDaily.getDay()) ||
                    !aDaily.getFrom().equals(bDaily.getFrom()) ||
                    !aDaily.getTo().equals(bDaily.getTo())) {
                return false;
            }
        }

        return true;
    }


    private void saveEmployeeWorkPeriod(EmployeeWorkPeriod employeeWorkPeriod, String newEmployeeId) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("employeeWorkPeriod");

        // Provjera postojećih podataka s istim weeklyWorkHoursId
        databaseRef.orderByChild("weeklyWorkHoursId").equalTo(weeklyWorkHours.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean found = false;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EmployeeWorkPeriod existingEmployeeWorkPeriod = snapshot.getValue(EmployeeWorkPeriod.class);
                    if (existingEmployeeWorkPeriod.getFrom().equals(employeeWorkPeriod.getFrom()) &&
                            existingEmployeeWorkPeriod.getTo().equals(employeeWorkPeriod.getTo())) {
                        found = true;
                        existingEmployeeWorkPeriod.setWeeklyWorkHoursId(weeklyWorkHours.getId());
                        updateEmployeeWorkPeriod(existingEmployeeWorkPeriod, newEmployeeId);

                        break;
                    }
                }
                // Ako ne pronađemo postojeći EmployeeWorkPeriod, stvaramo novi
                if (!found) {

                    // Postavljanje novog ID-a za proizvod i dodavanje u bazu podataka
                    long lastId = dataSnapshot.getChildrenCount() + 1; // Generisanje novog ID-a

                    String Id = String.valueOf(lastId);


                    employeeWorkPeriod.setWeeklyWorkHoursId(weeklyWorkHours.getId());
                    employeeWorkPeriod.setId(Id);
                    List<String> employeesId = new ArrayList<>();
                    employeesId.add(newEmployeeId);
                    employeeWorkPeriod.setEmployeesId(employeesId);
                    databaseRef.child(Id).setValue(employeeWorkPeriod);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Pogreška pri čitanju podataka iz baze
                Log.e(TAG, "Error fetching data", databaseError.toException());
            }
        });
    }

    private void updateEmployeeWorkPeriod(EmployeeWorkPeriod existing, String newEmployeeId) {
        List<String> employeesId = existing.getEmployeesId();
        employeesId.add(newEmployeeId); // Dodajemo novog zaposlenika
        existing.setEmployeesId(employeesId);
        existing.setWeeklyWorkHoursId(weeklyWorkHours.getId());
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("employeeWorkPeriod");
        databaseRef.child(existing.getId()).child("employeesId").setValue(existing.getEmployeesId());
    }


    public void populateEmmployeesFromUsers(List<User> users,List<String> employeesId){
        for(User user:users){
            employeesId.add(user.getUid());
        }
    }

}