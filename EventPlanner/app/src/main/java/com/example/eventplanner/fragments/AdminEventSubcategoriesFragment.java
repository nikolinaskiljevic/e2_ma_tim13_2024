package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.EventSubcategory;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class AdminEventSubcategoriesFragment extends Fragment {

    private LinearLayout layoutCategories;
    private EditText editTextCategoryName;
    private Button buttonAddCategory;
    private String CatId;

    public AdminEventSubcategoriesFragment() {
        // Required empty public constructor
    }

    private DatabaseReference eventSubcategoriesRef;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_event_subcategories, container, false);

        layoutCategories = view.findViewById(R.id.layoutCategories);
        editTextCategoryName = view.findViewById(R.id.editTextCategoryName);
        buttonAddCategory = view.findViewById(R.id.buttonAddCategory);

        Bundle bundle = getArguments();
        if (bundle != null) {
            String categoryName = bundle.getString("categoryName");
            if (categoryName != null) {
                // Set the category info text
                TextView textViewCategoryInfo = view.findViewById(R.id.textViewCategoryInfo);
                textViewCategoryInfo.setText(categoryName + " Subcategories");
            }
            CatId = bundle.getString("categoryId");
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        eventSubcategoriesRef = database.getReference("eventSubcategories");

        buttonAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String categoryName = editTextCategoryName.getText().toString().trim();
                if (!categoryName.isEmpty()) {
                    String key = saveSubcategoryToFirebase(categoryName);
                    addSubcategory( categoryName,key);
                    editTextCategoryName.setText("");
                }
            }
        });

        // Load existing subcategories from Firebase
        loadSubcategoriesFromFirebase();

        return view;
    }

    private String saveSubcategoryToFirebase(String categoryName) {
        String key = eventSubcategoriesRef.push().getKey();
        if (key != null) {
            EventSubcategory eventSubcategory = new EventSubcategory(key,CatId, categoryName);
            eventSubcategoriesRef.child(key).setValue(eventSubcategory);
            return key;
        }
        return null;
    }

    private void addSubcategory( String categoryName,String subcategoryId) {
        // Inflate subcategory item layout
        View subcategoryView = getLayoutInflater().inflate(R.layout.item_subcategory, null);
        TextView textViewCategoryName = subcategoryView.findViewById(R.id.textViewCategoryName);
        TextView textViewCategoryType = subcategoryView.findViewById(R.id.textViewCategoryType);

        Button buttonDelete = subcategoryView.findViewById(R.id.buttonDelete);
        Button buttonEdit = subcategoryView.findViewById(R.id.buttonEdit);
        buttonEdit.setVisibility(View.GONE);

        // Set subcategory name
        textViewCategoryName.setText(categoryName);
        textViewCategoryType.setText("");
        subcategoryView.setTag(subcategoryId);

        // Set padding between items
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 16); // Adjust the padding as needed
        subcategoryView.setLayoutParams(layoutParams);

        // Set click listener for delete button
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String subcategoryId = (String) subcategoryView.getTag();
                deleteSubcategory(subcategoryId);
                layoutCategories.removeView(subcategoryView);
            }
        });

        // Add subcategory view to the layout
        layoutCategories.addView(subcategoryView);
    }

    private void loadSubcategoriesFromFirebase() {
        Query query = eventSubcategoriesRef.orderByChild("categoryId").equalTo(CatId);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    EventSubcategory eventSubcategory = categorySnapshot.getValue(EventSubcategory.class);
                    if (eventSubcategory != null) {
                        addSubcategory( eventSubcategory.getName(), eventSubcategory.getId());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error
            }
        });


    }

    private void deleteSubcategory(String subcategoryId) {
        eventSubcategoriesRef.child(subcategoryId).removeValue();
    }


}