package com.example.eventplanner.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class SearchParametersDTO implements Serializable {
    public String Filter;
    public String Type;

    public String Name;
    public String Location;

    public String Category;
    public String Subcategory;
    public String Provider;
    public int MinPrice;
    public int MaxPrice;
    public LocalDate StartDate;
    public LocalDate EndDate;
    public String Availability;

    public SearchParametersDTO(String filter, String type, String name, String location, String category, String subcategory, String provider, int minPrice, int maxPrice, LocalDate startDate, LocalDate endDate, String availability) {
        Filter = filter;
        Type = type;
        Name = name;
        Location = location;
        Category = category;
        Subcategory = subcategory;
        Provider = provider;
        MinPrice = minPrice;
        MaxPrice = maxPrice;
        StartDate = startDate;
        EndDate = endDate;
        Availability = availability;
    }

    public SearchParametersDTO() {
    }
}
