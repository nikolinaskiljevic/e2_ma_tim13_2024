package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.ProductCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class AdminProductsCategoriesFragment extends Fragment {

    private LinearLayout layoutCategories;
    private EditText editTextCategoryName;
    private EditText editTextCategoryDesciption;
    private Button buttonAddCategory;
    private DatabaseReference categoriesRef; // Database reference for product categories

    public AdminProductsCategoriesFragment() {
        // Required empty public constructor
    }

    private View.OnClickListener addCategoryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String categoryName = editTextCategoryName.getText().toString().trim();
            String categoryDescription = editTextCategoryDesciption.getText().toString().trim();

            if (!categoryName.isEmpty()) {
                if (buttonAddCategory.getText().equals("Edit Category")) {
                    // If in edit mode, handle update
                    String categoryIdToUpdate = buttonAddCategory.getTag().toString();
                    updateCategory(categoryIdToUpdate, categoryName, categoryDescription);
                    buttonAddCategory.setText("Add Category");
                    editTextCategoryName.setText("");
                    editTextCategoryDesciption.setText("");
                } else {
                    // If not in edit mode, handle addition
                    String key = saveCategoryToFirebase(categoryName, categoryDescription);
                    addCategory(key, categoryName, categoryDescription);
                    editTextCategoryName.setText("");
                    editTextCategoryDesciption.setText("");
                }
            }
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_products_categories, container, false);

        layoutCategories = view.findViewById(R.id.layoutCategories);
        editTextCategoryName = view.findViewById(R.id.editTextCategoryName);
        editTextCategoryDesciption = view.findViewById(R.id.editTextCategoryDescription);
        buttonAddCategory = view.findViewById(R.id.buttonAddCategory);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        categoriesRef = database.getReference("productCategories"); // Reference to product categories node in database

        buttonAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String categoryName = editTextCategoryName.getText().toString().trim();
                String categoryDescription = editTextCategoryDesciption.getText().toString().trim();

                if (!categoryName.isEmpty()) {
                    String key = saveCategoryToFirebase(categoryName, categoryDescription);
                    addCategory(key, categoryName, categoryDescription);
                    editTextCategoryName.setText("");
                    editTextCategoryDesciption.setText("");
                }
            }
        });

        // Load existing product categories from Firebase
        loadCategoriesFromFirebase();

        return view;
    }

    private void addCategory(String categoryId, String categoryName, String categoryDescription) {
        // Inflate category item layout
        View categoryView = getLayoutInflater().inflate(R.layout.item_category, null);
        TextView textViewCategoryName = categoryView.findViewById(R.id.textViewCategoryName);
        TextView textViewCategoryInfo = categoryView.findViewById(R.id.textViewAdditionalInfo);
        Button buttonDelete = categoryView.findViewById(R.id.buttonDelete);
        Button buttonInfo = categoryView.findViewById(R.id.buttonInfo);
        Button buttonEdit = categoryView.findViewById(R.id.buttonEdit); // Add Edit button

        // Set category name and description
        textViewCategoryName.setText(categoryName);
        textViewCategoryInfo.setText(categoryDescription);

        // Set padding between items
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 16); // Adjust the padding as needed
        categoryView.setLayoutParams(layoutParams);

        categoryView.setTag(categoryId);

        // Set click listeners for delete, info, and edit buttons
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle delete action
                deleteCategory(categoryId);
                layoutCategories.removeView(categoryView);
            }
        });

        buttonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle info action
                String categoryId = (String) categoryView.getTag(); // Get the category ID
                String categoryName = textViewCategoryName.getText().toString(); // Get the category name

                // Ensure categoryId is not null
                if (categoryId != null) {
                    // Navigate to AdminProductsSubcategoriesFragment and pass category ID and name
                    FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
                    AdminProductsSubcategoriesFragment subcategoriesFragment = new AdminProductsSubcategoriesFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("categoryId", categoryId);
                    bundle.putString("categoryName", categoryName);
                    subcategoriesFragment.setArguments(bundle);
                    transaction.replace(R.id.fragment_container, subcategoriesFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else {
                    // Log or show an error message if categoryId is null
                    Log.e("AdminProductsCategories", "Category ID is null");
                    Toast.makeText(getActivity(), "Failed to load subcategories", Toast.LENGTH_SHORT).show();
                }
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle edit action
                editTextCategoryName.setText(categoryName);
                editTextCategoryDesciption.setText(categoryDescription);
                buttonAddCategory.setText("Edit Category");
                // Update the add button click listener for editing
                buttonAddCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String updatedCategoryName = editTextCategoryName.getText().toString().trim();
                        String updatedCategoryDescription = editTextCategoryDesciption.getText().toString().trim();
                        if (!updatedCategoryName.isEmpty()) {
                            updateCategory(categoryId, updatedCategoryName, updatedCategoryDescription);
                            // Reset EditText fields and button text
                            editTextCategoryName.setText("");
                            editTextCategoryDesciption.setText("");
                            buttonAddCategory.setText("Add Category");
                            // Set back the add button click listener for adding new category
                            buttonAddCategory.setOnClickListener(addCategoryClickListener);
                        }
                    }
                });


            }
        });

        // Add category view to the layout
        layoutCategories.addView(categoryView);
    }

    private void updateCategory(String categoryId, String updatedCategoryName, String updatedCategoryDescription) {
        DatabaseReference categoryRef = categoriesRef.child(categoryId);
        categoryRef.child("name").setValue(updatedCategoryName);
        categoryRef.child("description").setValue(updatedCategoryDescription)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // Category updated successfully
                            Toast.makeText(getActivity(), "Category updated successfully", Toast.LENGTH_SHORT).show();
                            loadCategoriesFromFirebase(); // Reload categories to reflect changes
                        } else {
                            // Failed to update category
                            Toast.makeText(getActivity(), "Failed to update category", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private String saveCategoryToFirebase(String categoryName,String categoryDescription) {
        String key = categoriesRef.push().getKey();
        if (key != null) {
            ProductCategory category = new ProductCategory(key, categoryName, categoryDescription);
            categoriesRef.child(key).setValue(category);
            return key;
        }
        return null;
    }

    private void deleteCategory(String categoryId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("productCategories").child(categoryId);
        databaseReference.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // Category deleted successfully
                    Toast.makeText(getActivity(), "Category deleted successfully", Toast.LENGTH_SHORT).show();
                } else {
                    // Failed to delete category
                    Toast.makeText(getActivity(), "Failed to delete category", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadCategoriesFromFirebase() {
        categoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                layoutCategories.removeAllViews();
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    ProductCategory category = categorySnapshot.getValue(ProductCategory.class);
                    if (category != null) {
                        addCategory(category.getId(), category.getName(), category.getDescription());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error
            }
        });
    }

    private void loadAdminProductsRequestsFragment() {
        FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, new AdminProductsRequestsFragment());
        transaction.addToBackStack(null);
        transaction.commit();
    }
}