package com.example.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.ChatFragment;
import com.example.eventplanner.fragments.CompanyProfileFragment;
import com.example.eventplanner.fragments.DetailsFragment;
import com.example.eventplanner.fragments.EditItemFragment;
import com.example.eventplanner.fragments.ItemDetailsFragment;
import com.example.eventplanner.model.BudgetItem;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventAgenda;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductReservation;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsAdapter.ViewHolder> {

    private ArrayList<Product> allProducts;
    private ArrayList<Event> organizerEvents;
    private User currentUser;
    private NotificationRepository notificationRepository = new NotificationRepository();
    private String companyOwnerId;
    private String role;
    private Context context;
    private User user;

    public ProductDetailsAdapter(Context context, ArrayList<Product> products, User user, String role, ArrayList<Event> events) {
        this.context = context;
        this.allProducts = products;
        this.currentUser = user;
        this.role = role;
        this.organizerEvents = events;
    }

    @NonNull
    @Override
    public ProductDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_details_card, parent, false);
        return new ProductDetailsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductDetailsAdapter.ViewHolder holder, int position) {
        Product p = allProducts.get(position);
        getAllCompanies(p.getCompanyId());
        holder.tvProductName.setText(p.getName());
        holder.textViewCategory.setText(p.getCategory());
        holder.textViewSubcategory.setText(p.getSubcategory());
        holder.textViewDescription.setText("Description: " + p.getDescription());
        holder.textViewPrice.setText("Price: " + String.format("%.2f", p.getPrice()) + "$");
        holder.textViewDiscount.setText("Discount: " + String.format("%.2f", p.getDiscount()) + "%");
        holder.textViewEventTypes.setText("Event types: " + p.getEventTypes());
        holder.textViewAvailable.setText("Availability: " + (p.isAvailable() ? "Available" : "Unavailable"));
        holder.buttonBuyProduct.setText("Buy " + p.getName());
        if(p.isAvailable()) {
            holder.buttonBuyProduct.setEnabled(true);
        }
        if(role.equals("organizer")) {
            holder.buttonAddToFavourites.setVisibility(View.VISIBLE);
            holder.spinnerOrganizerEvents.setVisibility(View.VISIBLE);
            setupSpinner(holder.spinnerOrganizerEvents);
            holder.buttonOpenChat.setVisibility(View.VISIBLE);
        }
        else {
            holder.buttonBuyProduct.setVisibility(View.GONE);
            holder.spinnerOrganizerEvents.setVisibility(View.GONE);
            holder.buttonOpenChat.setVisibility(View.GONE);
        }
        if(role.equals("organizer") && currentUser.getFavouritesProductsIds().contains(p.getId())) {
            holder.buttonAddToFavourites.setEnabled(false);
        }

        holder.buttonAddToFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Product p = allProducts.get(position);
                    // Ovde radite šta je potrebno sa objektom product, na primer dodavanje u omiljene
                    addToFavourites(p);
                }
            }
        });


        // Postavljanje slušača klika na dugme za prikaz detalja
        holder.buttonBuyProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event selectedEvent = findSelectedEvent(holder.spinnerOrganizerEvents);
                reserveProduct(p,selectedEvent);
            }
        });

        holder.buttonCompanyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Pretpostavljam da vaš proizvod ima atribut companyId
                String companyId = p.getCompanyId();

                // Kreiraj instancu CompanyProfileFragment sa companyId
                CompanyProfileFragment companyProfileFragment = CompanyProfileFragment.newInstance(companyId, role);

                // Pokreni transakciju fragmenta
                FragmentActivity activity = (FragmentActivity) context;
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, companyProfileFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        holder.buttonOpenChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Pretpostavljam da vaš proizvod ima atribut companyId
                //getAllCompanies(p.getCompanyId());

                // Kreiraj instancu CompanyProfileFragment sa companyId
                ChatFragment chatFragment = ChatFragment.newInstance(companyOwnerId);

                // Pokreni transakciju fragmenta
                FragmentActivity activity = (FragmentActivity) context;
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, chatFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    private void getAllCompanies(String companyId) {
        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference("Companies");

        companyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Company> allCompanies = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Company c = snapshot.getValue(Company.class);
                    if (c != null) {
                        allCompanies.add(c);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findCompany(allCompanies, companyId);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllCompanies", "Failed to read companies", databaseError.toException());
            }
        });
    }

    private void findCompany(List<Company> allCompanies, String companyId) {
        for(Company c: allCompanies) {
            if(c.getId().equals(companyId)) {
                companyOwnerId = c.getPupvId();
                break;
            }
        }
    }

    private void reserveProduct(Product product, Event event) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reservationsRef = database.getReference("productReservations");

        String reservationId = reservationsRef.push().getKey();
        if (reservationId != null) {
            ProductReservation reservation = new ProductReservation(
                    reservationId,
                    product.getId(),
                    event.getId(), // Use selected event ID
                    currentUser.getUid(),
                    "pending" // Set initial status
            );



            reservationsRef.child(reservationId).setValue(reservation)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(context, "Service reserved successfully!", Toast.LENGTH_SHORT).show();
                                notifHelp(product,companyOwnerId,event);

                            } else {
                                Toast.makeText(context, "Failed to reserve service. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(context, "Error generating reservation ID. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }


    private void notifHelp(Product product,String sellerUid,Event event){
        String userId = currentUser.getUid();
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
        BudgetItem budgetItem = new BudgetItem(event.getId(), product.getCategory(), product.getSubcategory(), product.getPrice(), "Product");
        saveBudgetItem(budgetItem);
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
                saveNotification(product.getName(),user.getFirstName() + " " + user.getLastName(),sellerUid);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                saveNotification(product.getName(),"",sellerUid);
            }
        });

    }

    private void saveBudgetItem(BudgetItem budgetItem) {
        DatabaseReference budgetItemsRef = FirebaseDatabase.getInstance().getReference().child("budgetItems");
        String budgetItemId = budgetItemsRef.push().getKey();
        budgetItem.setId(budgetItemId);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        budgetItem.setOrganizerId(user.getUid());
        budgetItemsRef.child(budgetItemId).setValue(budgetItem);
        Toast.makeText((FragmentActivity) context, "Budget item added successfully!", Toast.LENGTH_SHORT).show();
    }
    private void saveNotification(String item,String user,String recipient) {
        Notification notification = new Notification();
        notification.setStatus(Notification.Status.CREATED);
        notification.setMessage("An item was purchased from your company");
        notification.setText("Item "+item +" was just purchased from your company by user "+user);
        notification.setUserId(recipient);
        notificationRepository.createNotification(notification);
    }

    private Event findSelectedEvent(Spinner spinner) {
        String selected = "";
        if(spinner.getSelectedItem() != null) {
            selected = spinner.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selected);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            return findEvent(eventName);
        } else {
            System.out.println("Not founded event");
            return null;
        }
    }

    private Event findEvent(String eventName) {
        for(Event e: organizerEvents) {
            if(e.getEventName().equals(eventName)) {
                Toast.makeText((FragmentActivity) context, "Selected event: " + e.getEventName(), Toast.LENGTH_SHORT).show();
                return e;
            }
        }

        return null;
    }

    private void setupSpinner(Spinner spinner) {
        // Kreiramo listu imena dogadjaja
        List<String> events = new ArrayList<>();
        for (Event e : organizerEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>((FragmentActivity) context, android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinner.setAdapter(spinnerAdapter);
    }

    private void addToFavourites(Product p) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());
        List<String> favProducts = currentUser.getFavouritesProductsIds();
        favProducts.add(p.getId());
        currentUser.setFavouritesProductsIds(favProducts);

        userRef.setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText((FragmentActivity) context, "Well done!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText((FragmentActivity) context, "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName;
        public TextView textViewCategory;
        public TextView textViewSubcategory;
        public TextView textViewDescription;
        public TextView textViewPrice;
        public TextView textViewDiscount;
        public TextView textViewEventTypes;
        public TextView textViewAvailable;
        public Button buttonBuyProduct;
        public Button buttonCompanyProfile;
        public Button buttonAddToFavourites;
        public Spinner spinnerOrganizerEvents;
        public Button buttonOpenChat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);
            textViewSubcategory = itemView.findViewById(R.id.textViewSubcategory);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
            textViewEventTypes = itemView.findViewById(R.id.textViewEventTypes);
            textViewAvailable = itemView.findViewById(R.id.textViewAvailable);
            buttonBuyProduct = itemView.findViewById(R.id.buttonBuyProduct);
            buttonCompanyProfile = itemView.findViewById(R.id.buttonCompanyProfile);
            buttonAddToFavourites = itemView.findViewById(R.id.buttonAddToFavourites);
            spinnerOrganizerEvents = itemView.findViewById(R.id.spinnerOrganizerEvents);
            buttonOpenChat = itemView.findViewById(R.id.buttonOpenChat);
        }
    }
}
