package com.example.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.GradeCompanyFragment;
import com.example.eventplanner.fragments.ReportCompanyGradeFragment;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.ServiceReservation;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ShowServiceAdapter extends RecyclerView.Adapter<ShowServiceAdapter.ShowServiceViewHolder>{

    private List<Service> services;
    private Context context;
    private User user;
    private FirebaseAuth mAuth;
    private User loggedInUser ;

    private FirebaseUser userr;


    public ShowServiceAdapter(List<Service> services, Context context) {
        this.services = services;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }



    @NonNull
    @Override
    public ShowServiceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_show_service_card, parent, false);
        return new ShowServiceAdapter.ShowServiceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowServiceViewHolder holder, int position) {
        Service res = services.get(position);

        holder.textViewDescription.setText(res.getDescription());
        holder.textViewName.setText(res.getServiceName());





    }

    @Override
    public int getItemCount() {
        return services.size();
    }

    public static class ShowServiceViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName,textViewDescription;

        public ShowServiceViewHolder(@NonNull View itemView) {
            super(itemView);


            textViewName=itemView.findViewById(R.id.textViewName);
            textViewDescription=itemView.findViewById(R.id.textViewDescription);


        }
    }





}
