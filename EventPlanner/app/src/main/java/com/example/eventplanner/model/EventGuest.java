package com.example.eventplanner.model;

public class EventGuest {
    private String id;
    private String name;
    private String surname;
    private String ageRange;
    private boolean invited;
    private boolean acceptedInvite;
    private boolean vegan;
    private boolean vegetarian;

    public EventGuest() {
    }

    public EventGuest(String name, String surname, String ageRange, boolean invited, boolean acceptedInvite, boolean vegan, boolean vegetarian) {
        this.name = name;
        this.surname = surname;
        this.ageRange = ageRange;
        this.invited = invited;
        this.acceptedInvite = acceptedInvite;
        this.vegan = vegan;
        this.vegetarian = vegetarian;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public boolean isInvited() {
        return invited;
    }

    public void setInvited(boolean invited) {
        this.invited = invited;
    }

    public boolean isAcceptedInvite() {
        return acceptedInvite;
    }

    public void setAcceptedInvite(boolean acceptedInvite) {
        this.acceptedInvite = acceptedInvite;
    }

    public boolean isVegan() {
        return vegan;
    }

    public void setVegan(boolean vegan) {
        this.vegan = vegan;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }
}