package com.example.eventplanner.fragments;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.RequestAdapter;
import com.example.eventplanner.model.DailyWorkHours;
import com.example.eventplanner.model.EventCategory;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.PUPVAccountRequest;
import com.example.eventplanner.model.ProductCategory;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationRepository;
import com.example.eventplanner.service.EmailService;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminUsersFragment extends Fragment {

    private ListView list_view_requests;
    private EditText searchEditText;
    private Spinner categorySpinner, eventTypeSpinner, timeSpinner;
    private Button filterButton, resetButton;
    private List<PUPVAccountRequest> requestList;
    private RequestAdapter requestAdapter;
    private DatabaseReference databaseReference;
    private NotificationRepository notificationRepository = new NotificationRepository();
    private User user;
    private FirebaseAuth mAuth;

    private List<EventCategory> eventCategoryList;
    private List<ProductCategory> productCategoryList;
    private ArrayAdapter<EventCategory> eventCategoryAdapter;
    private ArrayAdapter<ProductCategory> productCategoryAdapter;

    public AdminUsersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseReference = FirebaseDatabase.getInstance().getReference("PUPVAccountRequests");
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_admin_users, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        list_view_requests = view.findViewById(R.id.list_view_requests);
        searchEditText = view.findViewById(R.id.search_edit_text);
        filterButton = view.findViewById(R.id.filter_button);
        resetButton = view.findViewById(R.id.reset_button);
        categorySpinner = view.findViewById(R.id.category_spinner);
        eventTypeSpinner = view.findViewById(R.id.event_type_spinner);
        timeSpinner = view.findViewById(R.id.time_spinner);

        requestList = new ArrayList<>();
        requestAdapter = new RequestAdapter(getContext(), requestList);

        list_view_requests.setAdapter(requestAdapter);
        list_view_requests.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PUPVAccountRequest request = requestList.get(position);
                showDetailsDialog(request);
            }
        });

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Implement search and filter functionality here
                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        requestList.clear();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            PUPVAccountRequest request = snapshot.getValue(PUPVAccountRequest.class);
                            requestList.add(request);

                        }
                        requestAdapter.notifyDataSetChanged();
                        String query = searchEditText.getText().toString();
                        filterRequests();
                        requestAdapter.filterRequests(query);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // Handle possible errors.
                    }
                });

            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetFilters();
            }
        });

        eventCategoryList = new ArrayList<>();
        productCategoryList = new ArrayList<>();


        eventCategoryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, eventCategoryList);
        eventCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productCategoryAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, productCategoryList);
        eventCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set adapters to spinners
        categorySpinner.setAdapter(eventCategoryAdapter);
        eventTypeSpinner.setAdapter(productCategoryAdapter);

        // Load categories
        loadEventCategories();
        loadProductCategories();
        loadRequests();
    }

    private void loadEventCategories() {
        DatabaseReference eventCategoryRef = FirebaseDatabase.getInstance().getReference("eventCategories");
        eventCategoryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                eventCategoryList.clear();
                EventCategory allEventCategory = new EventCategory();
                allEventCategory.setId("all");
                allEventCategory.setName("All Categories");
                eventCategoryList.add(allEventCategory);
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EventCategory eventCategory = snapshot.getValue(EventCategory.class);
                    eventCategoryList.add(eventCategory);
                }

                eventCategoryAdapter.notifyDataSetChanged();
            }



            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle possible errors.
            }
        });
    }

    private void loadProductCategories() {
        DatabaseReference productCategoryRef = FirebaseDatabase.getInstance().getReference("productCategories");
        productCategoryRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                productCategoryList.clear();
                ProductCategory allProductCategory = new ProductCategory();
                allProductCategory.setId("all");
                allProductCategory.setName("All Categories");
                productCategoryList.add(allProductCategory);
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ProductCategory productCategory = snapshot.getValue(ProductCategory.class);
                    productCategoryList.add(productCategory);
                }

                productCategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle possible errors.
            }
        });
    }

    private void loadRequests() {
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                requestList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    PUPVAccountRequest request = snapshot.getValue(PUPVAccountRequest.class);
                    requestList.add(request);

                }
                requestAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle possible errors.
            }
        });
    }

    private void filterRequests() {
        EventCategory selectedEventCat = (EventCategory) categorySpinner.getSelectedItem();
        ProductCategory selectedProdCat = (ProductCategory) eventTypeSpinner.getSelectedItem();
        String selectedTime = timeSpinner.getSelectedItem().toString();

        List<PUPVAccountRequest> filteredList = new ArrayList<>();

        for (PUPVAccountRequest request : requestList) {

            boolean matchesCategory = selectedEventCat == null ||
                    selectedEventCat.getId().equals("all") ||
                    (request.getCompanyCategories().getEventIds() != null &&
                            request.getCompanyCategories().getEventIds().contains(selectedEventCat.getId()));

            boolean matchesEventType = selectedProdCat == null ||
                    selectedProdCat.getId().equals("all") ||
                    (request.getCompanyCategories().getProductIds() != null &&
                            request.getCompanyCategories().getProductIds().contains(selectedProdCat.getId()));

            boolean matchesTime = selectedTime.equals("All Time") ||
                    matchesTimeFilter(request, selectedTime);

            if ( matchesCategory && matchesEventType && matchesTime) {
                filteredList.add(request);
            }
        }

        requestAdapter.updateData(filteredList);
    }

    private boolean matchesTimeFilter(PUPVAccountRequest request, String selectedTime) {
        long currentTime = System.currentTimeMillis();
        long requestTime = request.getCreationTimeMillis();

        Calendar calendar = Calendar.getInstance();

        switch (selectedTime) {
            case "This Day":
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                long startOfDay = calendar.getTimeInMillis();
                return requestTime >= startOfDay && requestTime <= currentTime;

            case "This Week":
                calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                long startOfWeek = calendar.getTimeInMillis();
                return requestTime >= startOfWeek && requestTime <= currentTime;

            case "This Month":
                calendar.set(Calendar.DAY_OF_MONTH, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                long startOfMonth = calendar.getTimeInMillis();
                return requestTime >= startOfMonth && requestTime <= currentTime;

            case "This Year":
                calendar.set(Calendar.DAY_OF_YEAR, 1);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);
                long startOfYear = calendar.getTimeInMillis();
                return requestTime >= startOfYear && requestTime <= currentTime;

            default:
                return true;
        }
    }

    private void resetFilters() {
        loadRequests();
        searchEditText.setText("");
        categorySpinner.setSelection(0);
        eventTypeSpinner.setSelection(0);
        timeSpinner.setSelection(0);
        requestAdapter.updateData(requestList);
    }

    private void showDetailsDialog(PUPVAccountRequest request) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_request_details, null);
        builder.setView(dialogView);

        TextView userNameTextView = dialogView.findViewById(R.id.user_name);
        TextView companyNameTextView = dialogView.findViewById(R.id.company_name);
        TextView emailTextView = dialogView.findViewById(R.id.email);
        TextView addressTextView = dialogView.findViewById(R.id.address);
        TextView phoneNumberTextView = dialogView.findViewById(R.id.phone_number);
        TextView workHoursTextView = dialogView.findViewById(R.id.work_hours);
        TextView categoriesEventTextView = dialogView.findViewById(R.id.categoriesEvent);
        TextView categoriesProductTextView = dialogView.findViewById(R.id.categoriesProduct);
        TextView requestDateTextView = dialogView.findViewById(R.id.request_date);
        Button approveButton = dialogView.findViewById(R.id.approve_button);
        Button rejectButton = dialogView.findViewById(R.id.reject_button);

        getUserById(request.getUserPUPVId(), userNameTextView);
        companyNameTextView.setText(request.getCompany().getName());
        emailTextView.setText(request.getCompany().getEmail());
        addressTextView.setText(request.getCompany().getAddress());
        phoneNumberTextView.setText(request.getCompany().getPhoneNumber());

        StringBuilder workHours = new StringBuilder();
        for (DailyWorkHours hours : request.getCompanyWorkHours().getDailyWorkHoursList()) {
            workHours.append(hours.getDay()).append(": ").append(hours.getFrom()).append(" - ").append(hours.getTo()).append("\n");
        }
        workHoursTextView.setText(workHours.toString());

        // Show event categories names
        StringBuilder eventCategories = new StringBuilder();
        for (String categoryId : request.getCompanyCategories().getEventIds()) {
            for (EventCategory category : eventCategoryList) {
                if (category.getId().equals(categoryId)) {
                    eventCategories.append(category.getName()).append("\n");
                    break;
                }
            }
        }
        categoriesEventTextView.setText(eventCategories.toString());

        // Show product categories names
        StringBuilder productCategories = new StringBuilder();
        for (String categoryId : request.getCompanyCategories().getProductIds()) {
            for (ProductCategory category : productCategoryList) {
                if (category.getId().equals(categoryId)) {
                    productCategories.append(category.getName()).append("\n");
                    break;
                }
            }
        }
        categoriesProductTextView.setText(productCategories.toString());

        long requestTimeMillis = request.getCreationTimeMillis();
        String formattedDate = formatDate(requestTimeMillis);
        requestDateTextView.setText("Request Date: " + formattedDate);

        builder.setPositiveButton("OK", null);

        approveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveRequest(request);
            }
        });

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRejectionReasonDialog(request);
            }
        });

        builder.create().show();
    }

    private String formatDate(long millis) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:mm");
        return sdf.format(new Date(millis));
    }

    private void approveRequest(PUPVAccountRequest request) {
        // Update adminVerified to true
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Users");

        Map<String, Object> updates = new HashMap<>();
        updates.put("adminVerified", true); // Use true for approved requests
        usersRef.child(request.getUserPUPVId()).updateChildren(updates);


        usersRef.child(request.getUserPUPVId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        sendActivationEmail(user.getEmail(), request);
                        DatabaseReference requestsRef = database.getReference("PUPVAccountRequests");
                        requestsRef.child(request.getId()).removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("TAG", "Error fetching user.", databaseError.toException());
            }
        });
    }


    private void showRejectionReasonDialog(PUPVAccountRequest request) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Reason for Rejection");

        final EditText input = new EditText(getContext());
        builder.setView(input);

        builder.setPositiveButton("Submit", (dialog, which) -> {
            String reason = input.getText().toString();
            if (!reason.isEmpty()) {
                rejectRequest(request, reason);
            } else {
                Toast.makeText(getContext(), "Reason for rejection cannot be empty.", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    private void rejectRequest(PUPVAccountRequest request, String reason) {
        // Update adminVerified to true
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Users");

        Map<String, Object> updates = new HashMap<>();
        updates.put("adminVerified", true); // Use true for consistency, consider changing the field name if needed
        usersRef.child(request.getUserPUPVId()).updateChildren(updates);

        usersRef.child(request.getUserPUPVId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        sendRejectionEmail(user.getEmail(), reason);
                        DatabaseReference requestsRef = database.getReference("PUPVAccountRequests");
                        requestsRef.child(request.getId()).removeValue();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("TAG", "Error fetching user.", databaseError.toException());
            }
        });


        Toast.makeText(getContext(), "Request rejected and email sent.", Toast.LENGTH_SHORT).show();
    }


    private void sendActivationEmail(String email, PUPVAccountRequest request) {
        String title = "Accepted registration";
        String mail = "Dear user,\n\nyour request has been accepted\n\nPlease log into our app and verify your email.\n\nEvent planner team";
        EmailService.sendEmail(email,title,mail);
        saveNotification(request.getUserPUPVId());
    }

    private void saveNotification(String userid) {
        Notification notification = new Notification();
        notification.setStatus(Notification.Status.CREATED);
        notification.setMessage("Company registration has been verified");
        notification.setText("Your company registration has been verified by our admin" );
        notification.setUserId(userid);
        notificationRepository.createNotification(notification);
    }

    private void sendRejectionEmail(String email, String reason) {
        String title = "Rejected registration";
        String mail = "Dear user,\n\nyour request has been declined for reason:\n\n" + reason + "\n\nEvent planner team";
        EmailService.sendEmail(email,title,mail);
        Log.d("Email", "Rejection email sent to " + email);
    }

    public void getUserById(String userId, TextView text) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Users");

        usersRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.getValue(User.class);
                    text.setText(user.getFirstName() + " " + user.getLastName());
                } else {
                    text.setText("Error fetching name");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("TAG", "Error fetching user.", databaseError.toException());
            }
        });
    }
}
