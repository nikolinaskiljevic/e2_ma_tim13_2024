package com.example.eventplanner.fragments;

import static android.app.Activity.RESULT_OK;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.AuthActivity;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class RegisterODFragment extends Fragment {

    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextAddress;
    private EditText editTextPhoneNumber;
    private ProgressBar progressBar;
    private Button buttonRegisterOD;

    private ImageView imageViewSelectedPhoto;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri picURL = null;

    private FirebaseAuth mAuth;



    public RegisterODFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_od, container, false);

        editTextEmail = view.findViewById(R.id.editTextEmailReg);
        editTextPassword = view.findViewById(R.id.editTextPasswordReg);
        editTextConfirmPassword = view.findViewById(R.id.editTextConfirmPassword);
        editTextFirstName = view.findViewById(R.id.editTextFirstName);
        editTextLastName = view.findViewById(R.id.editTextLastName);
        editTextAddress = view.findViewById(R.id.editTextAddress);
        editTextPhoneNumber = view.findViewById(R.id.editTextPhoneNumber);
        imageViewSelectedPhoto = view.findViewById(R.id.imageViewSelectedPhoto);

        buttonRegisterOD = view.findViewById(R.id.buttonRegisterOD);
        Button buttonAddProfilePic = view.findViewById(R.id.buttonAddProfilePic);
        progressBar =  view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);


        buttonAddProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Open dialog to add profile picture
                openAddPhotoDialog();
            }
        });

        buttonRegisterOD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonRegisterOD.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                if(picURL != null){
                    uploadImage(picURL);
                }else{
                    saveUserDataWithImage(null);
                }


            }
        });

        return view;
    }

    private void openAddPhotoDialog() {
        // Open gallery to select image
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            picURL = data.getData();

            imageViewSelectedPhoto.setVisibility(View.VISIBLE);
            imageViewSelectedPhoto.setImageURI(picURL);
        }
    }

    private void saveUserDataWithImage(String imageUrl) {
        String email = editTextEmail.getText().toString();
        String password = editTextPassword.getText().toString();
        String confirmPassword = editTextConfirmPassword.getText().toString();
        String firstName = editTextFirstName.getText().toString();
        String lastName = editTextLastName.getText().toString();
        String address = editTextAddress.getText().toString();
        String phoneNumber = editTextPhoneNumber.getText().toString();

        if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password) || TextUtils.isEmpty(confirmPassword)
                || TextUtils.isEmpty(firstName) || TextUtils.isEmpty(lastName) || TextUtils.isEmpty(address)
                || TextUtils.isEmpty(phoneNumber)) {
            Toast.makeText(requireContext(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
            buttonRegisterOD.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            return;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(requireContext(), "Passwords do not match", Toast.LENGTH_SHORT).show();
            buttonRegisterOD.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            return;
        }

        // You can add additional validation for email, password strength, etc. here

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(requireActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                            if (firebaseUser != null) {
                                // Create User object
                                User user = new User(firebaseUser.getUid(), "organizer", email, firstName, lastName, address, phoneNumber, false);
                                user.setImageURL(imageUrl);
                                // Save user to database
                                saveUserToDatabase(user);
                                // Send email verification
                                sendEmailVerification(firebaseUser);
                            }
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(requireContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();

                            buttonRegisterOD.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void sendEmailVerification(FirebaseUser user) {
        user.sendEmailVerification()
                .addOnCompleteListener(requireActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(requireContext(),
                                    "Verification email sent to " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(requireActivity(), MainActivity.class));
                            requireActivity().finish();
                        } else {
                            Toast.makeText(requireContext(),
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();
                            buttonRegisterOD.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
    }

    private void uploadImage(Uri imageUri) {
        // Generate a random name for the image
        String imageName = "profile_image_" + System.currentTimeMillis() + ".jpg";

        // Create a reference to the Firebase Storage location
        StorageReference imageRef = FirebaseStorage.getInstance().getReference().child("profile_images").child(imageName);

        // Upload the image
        imageRef.putFile(imageUri)
                .addOnSuccessListener(requireActivity(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Image upload successful, get the download URL
                        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String imageUrl = uri.toString();
                                // Now, you can save this imageUrl along with user data in the database
                                saveUserDataWithImage(imageUrl);
                            }
                        });
                    }
                })
                .addOnFailureListener(requireActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Image upload failed
                        Toast.makeText(requireContext(), "Failed to upload image", Toast.LENGTH_SHORT).show();
                        Log.e("User Creation", "Image upload failed", e);
                        buttonRegisterOD.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void saveUserToDatabase(User user) {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        usersRef.child(user.getUid()).setValue(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("User Creation", "User saved to database");
                        } else {
                            Log.e("User Creation", "Failed to save user to database", task.getException());
                        }
                    }
                });
    }
}