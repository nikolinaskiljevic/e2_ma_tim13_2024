package com.example.eventplanner.notifications;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.eventplanner.adapters.CompanyAdapter;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskCompletionSource;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class NotificationRepository {

    private static final String TAG = "NotificationRepository";

    private final DatabaseReference notificationsRef;
    private final FirebaseAuth mAuth;
    private ValueEventListener listener;
    private List<User> usersList;
    private String userRole;
    private FirebaseUser user;

    public NotificationRepository() {

        this.notificationsRef = FirebaseDatabase.getInstance().getReference("notifications");
        this.mAuth = FirebaseAuth.getInstance();

        user = mAuth.getCurrentUser();
        // fetchUserById(user.getUid(),);

    }


    public Task<Notification> createNotification(Notification notification) {
        Log.d(TAG, "createNotification: Creating notification");
        String notificationId = notificationsRef.push().getKey();
        notification.setId(notificationId);
        return notificationsRef.child(notificationId).setValue(notification)
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createNotification: Notification created successfully");
                        return notification;
                    } else {
                        Log.e(TAG, "createNotification: Error creating notification", task.getException());
                        throw task.getException();
                    }
                });
    }

    public Task<List<Notification>> getAllLoggedUserNotifications() {
        Log.d(TAG, "getAllLoggedUserNotifications: Retrieving all notifications for logged user");
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            Log.e(TAG, "getAllLoggedUserNotifications: User not logged in");
            return Tasks.forException(new Exception("User not logged in"));
        }
        String userEmail = user.getEmail();
        return notificationsRef.orderByChild("email").equalTo(userEmail).get()
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        List<Notification> notifications = new ArrayList<>();
                        for (DataSnapshot document : task.getResult().getChildren()) {
                            notifications.add(document.getValue(Notification.class));
                        }
                        Log.d(TAG, "getAllLoggedUserNotifications: Retrieved " + notifications.size() + " notifications");
                        return notifications;
                    } else {
                        Log.e(TAG, "getAllLoggedUserNotifications: Error retrieving notifications", task.getException());
                        throw task.getException();
                    }
                });
    }

    public Task<List<Notification>> getLoggedUserUnreadNotifications() {
        Log.d(TAG, "getLoggedUserUnreadNotifications: Retrieving unread notifications for logged user");
        FirebaseUser user = mAuth.getCurrentUser();
        if (user == null) {
            Log.e(TAG, "getLoggedUserUnreadNotifications: User not logged in");
            return Tasks.forException(new Exception("User not logged in"));
        }

        TaskCompletionSource<List<Notification>> taskCompletionSource = new TaskCompletionSource<>();
        fetchUserById(user.getUid(), userRole -> {
            notificationsRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    List<Notification> notifications = new ArrayList<>();
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Notification notification = snapshot.getValue(Notification.class);
                        if (notification != null && user.getUid().equals(notification.getUserId())) {
                            notifications.add(notification);
                        }
                    }
                    Log.d(TAG, "getLoggedUserUnreadNotifications: Retrieved " + notifications.size() + " notifications");
                    taskCompletionSource.setResult(notifications);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG, "getLoggedUserUnreadNotifications: Error retrieving notifications", databaseError.toException());
                    taskCompletionSource.setException(databaseError.toException());
                }
            });
        });

        return taskCompletionSource.getTask();
    }

    public Task<Void> setNotificationAsRead(String notificationId) {
        Log.d(TAG, "setNotificationAsRead: Setting notification as read");
        DatabaseReference notificationRef = notificationsRef.child(notificationId);
        return notificationRef.child("status").setValue("READ")
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "setNotificationAsRead: Notification marked as read");
                    } else {
                        Log.e(TAG, "setNotificationAsRead: Error setting notification as read", task.getException());
                        throw task.getException();
                    }
                    return null;
                });
    }

    public Task<Void> setNotificationAsShown(String notificationId) {
        Log.d(TAG, "setNotificationAsShown: Setting notification as shown");
        DatabaseReference notificationRef = notificationsRef.child(notificationId);
        return notificationRef.child("status").setValue("SHOWN")
                .continueWith(task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "setNotificationAsShown: Notification marked as shown");
                    } else {
                        Log.e(TAG, "setNotificationAsShown: Error setting notification as shown", task.getException());
                        throw task.getException();
                    }
                    return null;
                });
    }

    public void listenForCreatedNotifications(ValueEventListener listener) {
        FirebaseAuth auth =  FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        Log.d(TAG, "listenForCreatedNotifications: Setting up listener for created notifications");

        if (user != null) {
            fetchUserById(user.getUid(), userRole -> {
                Query query = notificationsRef.orderByChild("status").equalTo("CREATED");
                query.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Notification notification = snapshot.getValue(Notification.class);
                            if (notification != null && user.getUid().equals(notification.getUserId())) {
                                // Calling the listener only for relevant notifications
                                listener.onDataChange(dataSnapshot);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, "listenForCreatedNotifications: Error listening for notifications", databaseError.toException());
                        listener.onCancelled(databaseError);
                    }
                });
            });
        } else {
            Log.e(TAG, "listenForCreatedNotifications: User not logged in");
        }
    }

    private void fetchUserById(String userId, OnSuccessListener<String> onSuccessListener) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    String userRole = user.getRole();
                    onSuccessListener.onSuccess(userRole);
                    Log.d("fetchUserById", "User found: " + user.toString());

                    // Možeš ovdje dalje raditi s korisnikom
                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }


    public void removeListener() {
        Log.d(TAG, "removeListener: Removing listener for created notifications");
        if (this.listener != null) {
            notificationsRef.removeEventListener(this.listener);
        }
    }


}
