package com.example.eventplanner.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.dto.SearchParametersDTO;
import com.example.eventplanner.model.BudgetItem;
/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditItemFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private BudgetItem mParam1;

    public EditItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment EditItemFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditItemFragment newInstance(BudgetItem param1) {
        EditItemFragment fragment = new EditItemFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = (BudgetItem) getArguments().getSerializable(ARG_PARAM1); // Koristimo getSerializable() za dobijanje objekta iz Bundle-a
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_edit_item, container, false);
        TextView tvOldAmount = rootView.findViewById(R.id.textViewOldAmount);
        tvOldAmount.setText(String.format("Old amount: ", String.format("%.2f", mParam1.getPlannedAmount())));
        EditText etNewAmount = rootView.findViewById(R.id.editTextNewAmount);
        Button confirmButton = rootView.findViewById(R.id.buttonConfirm);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newAmount = etNewAmount.getText().toString();
                //updateAmount(newAmount);
                requireActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return rootView;
    }

    private void updateAmount(String newAmount) {
        //mParam1.setAmount(newAmount);
    }
}