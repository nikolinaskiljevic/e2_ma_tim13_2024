package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackageAdapter;
import com.example.eventplanner.adapters.ProductAdapter;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class PackagesFragment extends Fragment {

    private ListView listView;
    private PackageAdapter packageAdapter;
    private List<Package> packages;

    public PackagesFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_packages, container, false);

        // Initialize ListView
        listView = view.findViewById(R.id.listViewPackages);

        // Initialize product list
        packages = new ArrayList<>();

        // Initialize adapter
        packageAdapter = new PackageAdapter(getContext(), R.layout.fragment_package_card, packages);
        listView.setAdapter(packageAdapter);

        packageAdapter.setOnEditButtonClickListener(new PackageAdapter.EditButtonClickListener() {
            @Override
            public void onEditButtonClick(Package packagee) {
                // Handle edit button click here
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                //fragmentTransaction.replace(R.id.fragment_container, EditProduct.newInstance(packagee));
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();


            }
        });

        packageAdapter.setOnDeleteButtonClickListener(new PackageAdapter.DeleteButtonClickListener() {
            @Override
            public void onDeleteButtonClick(Package packagee) {

                deletePackage(packagee);

            }
        });
        // Fetch products from the Firebase database
        fetchPackagesFromDatabase();

        // Set item click listener for ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Handle item click if needed
            }
        });

        return view;
    }

    private void fetchPackagesFromDatabase() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("packages");

        // Add listener to read data from the Firebase database
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                packages.clear(); // Clear the product list before adding new ones from the database

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Package product = snapshot.getValue(Package.class);
                    if (!product.isDeleted()) { // Check if the product is not marked as deleted
                        packages.add(product);
                    }
                }

                // Notify the adapter that the data has changed
                packageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error if there is a problem reading data from the database
                Toast.makeText(getContext(), "Error reading products from the database", Toast.LENGTH_SHORT).show();
            }
        });
    }




    private void deletePackage(Package product) {
        // Mark the product as deleted (logical deletion)
        product.setDeleted(true);

        // Notify database about the change
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("packages").child(String.valueOf(product.getId()));
        databaseRef.setValue(product);
    }
}