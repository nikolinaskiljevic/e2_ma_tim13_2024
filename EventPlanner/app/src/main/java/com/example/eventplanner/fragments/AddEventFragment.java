package com.example.eventplanner.fragments;

import static com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.model.DailyWorkHours;
import com.example.eventplanner.model.EmployeeWorkPeriod;
import com.example.eventplanner.model.Event2;
import com.example.eventplanner.model.Notification;

import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WeeklyWorkHours;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class AddEventFragment extends Fragment {


    public EditText editTextName,editTextFrom,editTextTo,editFrom,editTo;

    public Spinner spinnerType,spinnerDay;

    public Button buttonCreate;

    public List<String> employeeList;

   // public List<String> types;

    public List<String> days;

    public CalendarView calendarView;
    public String selectedDate;

    public String employeeId;

    public String workPeriodId;



    public WeeklyWorkHours weekly;


    public AddEventFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =inflater.inflate(R.layout.fragment_add_event, container, false);

        editTextName=view.findViewById(R.id.editTextName);
        editTextFrom= view.findViewById(R.id.editTextFrom);
        editTextTo=view.findViewById(R.id.editTextTo);
        editFrom= view.findViewById(R.id.editFrom);
        editTo= view.findViewById(R.id.editTo);
        calendarView=view.findViewById(R.id.calendarView);
        calendarView=view.findViewById(R.id.calendarView);

        weekly= new WeeklyWorkHours();

      //  spinnerDay=view.findViewById(R.id.spinnerDay);
       // spinnerType=view.findViewById(R.id.spinnerType);

        Bundle bundle = getArguments();
        if (bundle != null) {
            employeeId = bundle.getString("employeeId");

        }
        Log.e(TAG, "EmployeeId u create EVENT" + employeeId);


        buttonCreate=view.findViewById(R.id.buttonCreate);
        employeeList= new ArrayList<>();
       // types= new ArrayList<>();
        days=new ArrayList<>();

        //types.add("RESERVED");
       // types.add("TAKEN");
        days.add("Monday");
        days.add("Tuesday");
        days.add("Wednesday");
        days.add("Thursday");
        days.add("Friday");

        populatePeriod();
       // populateEmployees();
       // populateTypes();
       // populateDays();


        /*
        spinnerEmployees.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Pozivamo populatePeriod metodu
                String selectedEmployeeId = spinnerEmployees.getSelectedItem().toString();
                populatePeriod();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Nije odabrano ništa
            }
        });
*/
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                // Pretvaranje odabranih datuma u format koji možete koristiti za usporedbu
                 selectedDate = dayOfMonth + "." + (month + 1) + "." + year;
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

                try {
                    Date selected = sdf.parse(selectedDate);
                    Date fromDate = sdf.parse(editFrom.getText().toString().trim());
                    Date toDate = sdf.parse(editTo.getText().toString().trim());

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(calendarView.getDate());
                    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                    String selectedDay = getDayOfWeekName(dayOfWeek);


                    DailyWorkHours dailyWorkHours= new DailyWorkHours("Monday",editTextFrom.getText().toString().trim(),editTextTo.getText().toString().trim());



                    // Provjera je li odabrani datum između editFrom i editTo
                    if (selected.before(fromDate) || selected.after(toDate)) {
                        // Ako nije, izbaci poruku
                        Toast.makeText(getActivity(), "Choose date between " + editFrom.getText().toString().trim() + " i " + editTo.getText().toString().trim(), Toast.LENGTH_SHORT).show();
                    }else{
                        for(DailyWorkHours daily : weekly.getDailyWorkHoursList()){
                            if(daily.getDay().equals("Monday")){
                                String fromInput = editTextFrom.getText().toString().trim();
                                String toInput = editTextTo.getText().toString().trim();




                                Toast.makeText(getActivity(), "Must be "+ daily.getFrom()+"to"+ daily.getTo()
                                        , Toast.LENGTH_LONG).show();



                            }
                        }

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });


        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               // String selectedDay = spinnerDay.getSelectedItem().toString();
               // String selectedType = spinnerType.getSelectedItem().toString();

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(calendarView.getDate());
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                String selectedDay = getDayOfWeekName(dayOfWeek);
                DailyWorkHours dailyWorkHours= new DailyWorkHours(selectedDay,editTextFrom.getText().toString().trim(),editTextTo.getText().toString().trim());
                Event2 newEvent= new Event2(editTextName.getText().toString().trim(),employeeId,dailyWorkHours,"TAKEN",selectedDate);



                createEvent(newEvent);

            }
        });

        return view;
    }

    /*
    private void populateEmployees(){

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference employeesRef = databaseReference.child("employees");

        // Dohvaćanje podataka iz čvora "employees"
        employeesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Ovdje možete obraditi podatke koje ste dobili iz baze podataka
                // dataSnapshot sadrži sve podatke iz čvora "employees"

                for (DataSnapshot employeeSnapshot : dataSnapshot.getChildren()) {
                    Employee employee = employeeSnapshot.getValue(Employee.class);
                    employeeList.add(employee.getId());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, employeeList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerEmployees.setAdapter(adapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });

    }

     */

    /*
    public void populateTypes(){

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, types);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapter);
    }

     */

    public void populateDays(){
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, days);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDay.setAdapter(adapter);
    }


    private void createEvent(Event2 event) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference eventsRef = databaseReference.child("eventss");

        // Provjeri postoji li već događaj s istim employeeId i dailyWorkhours
        eventsRef.orderByChild("employeeId").equalTo(event.getEmployeeId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        boolean eventExists = false;

                        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                            Event2 existingEvent = snapshot.getValue(Event2.class);
                            // Provjera da li je dailyWorkhours isti
                            if (existingEvent != null && isEqualByDailyWorkHoursList(existingEvent.getDailyWorkHours(),event.getDailyWorkHours())) {
                                // Postoji već događaj s istim employeeId i dailyWorkhours
                                eventExists = true;
                                break;
                            }
                        }

                        if (!eventExists) {
                            // Dodavanje novog događaja u bazu podataka ako ne postoji već takav događaj
                            long lastId = dataSnapshot.getChildrenCount() + 1; // Generisanje novog ID-a

                            // Postavljanje novog ID-a za proizvod i dodavanje u bazu podataka
                            String eventId = String.valueOf(lastId);
                            eventsRef.child(eventId).setValue(event)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            // Uspješno dodan događaj
                                            Toast.makeText(getActivity(), "Event created successfully", Toast.LENGTH_SHORT).show();
                                            createNotification(event.getEmployeeId(),event);
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            // Greška prilikom dodavanja događaja
                                            Toast.makeText(getActivity(), "Failed to create event", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        } else {
                            // Postoji već događaj s istim employeeId i dailyWorkhours
                            Toast.makeText(getActivity(), "Event on that day and time period already exists", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        // Greška prilikom dohvaćanja podataka iz baze podataka
                        Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void createNotification(String employeeId, Event2 event) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference notsRef = databaseReference.child("notifications");

        notsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Generiranje novog ID-a
                long lastId = dataSnapshot.getChildrenCount() + 1;
                String notId = String.valueOf(lastId);

                String message = "Event " + event.getName() + " on " + event.getDailyWorkHours().getDay() + " from " +
                        event.getDailyWorkHours().getFrom() + " to " + event.getDailyWorkHours().getTo() + " has been added";

               // Notification not = new Notification(employeeId, message);
                //not.setId(notId);
                //not.setRead(false);
                /*
                // Dodavanje nove obavijesti u bazu podataka
                notsRef.child(notId).setValue(not)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                // Uspješno dodana obavijest
                                Toast.makeText(getActivity(), "Notification sent", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Greška prilikom dodavanja obavijesti
                                Toast.makeText(getActivity(), "Failed to create notification", Toast.LENGTH_SHORT).show();
                            }
                        });
                */
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja iz baze podataka
                Toast.makeText(getActivity(), "Failed to read data from database", Toast.LENGTH_SHORT).show();
            }
        });
    }



    private boolean isEqualByDailyWorkHoursList(DailyWorkHours dw1, DailyWorkHours dw2) {
        return dw1.getDay().equals(dw2.getDay()) &&
                dw1.getFrom().equals(dw2.getFrom()) &&
                dw1.getTo().equals(dw2.getTo());
    }

    private void populatePeriod() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference employeeWorkPeriodsRef = databaseReference.child("employeeWorkPeriod");

        employeeWorkPeriodsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<EmployeeWorkPeriod> employeeWorkPeriodList = new ArrayList<>();

                for (DataSnapshot periodSnapshot : dataSnapshot.getChildren()) {
                    EmployeeWorkPeriod period = periodSnapshot.getValue(EmployeeWorkPeriod.class);
                    employeeWorkPeriodList.add(period);
                    workPeriodId=period.getWeeklyWorkHoursId();
                    //nadji weekly
                    findWeekly(workPeriodId);
                    //poredi selektovani datum koji je dan
                }


                List<String[]> workPeriods = getWorkPeriodsForEmployee(employeeId, employeeWorkPeriodList);

                if (!workPeriods.isEmpty() ){
                    editFrom.setText(workPeriods.get(0)[0]);
                    editTo.setText(workPeriods.get(0)[1]);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private List<String[]> getWorkPeriodsForEmployee(String employeeId, List<EmployeeWorkPeriod> employeeWorkPeriodList) {
        List<String[]> workPeriods = new ArrayList<>();

        // Iteriranje kroz svaki EmployeeWorkPeriod objekat
        for (EmployeeWorkPeriod period : employeeWorkPeriodList) {
            // Ako lista employeesId sadrži employeeId
            if (period.getEmployeesId() != null && period.getEmployeesId().contains(employeeId)) {
                // Dodaj polja 'from' i 'to' u listu radnih perioda
                workPeriods.add(new String[]{period.getFrom(), period.getTo()});
            }
        }

        return workPeriods;
    }

    public String getDayOfWeekName(int dayOfWeek) {
        String dayName = "";

        switch (dayOfWeek) {
            case Calendar.SUNDAY:
                dayName = "Sunday";
                break;
            case Calendar.MONDAY:
                dayName = "Monday";
                break;
            case Calendar.TUESDAY:
                dayName = "Tuesday";
                break;
            case Calendar.WEDNESDAY:
                dayName = "Wednesday";
                break;
            case Calendar.THURSDAY:
                dayName = "Thursday";
                break;
            case Calendar.FRIDAY:
                dayName = "Friday";
                break;
            case Calendar.SATURDAY:
                dayName = "Saturday";
                break;
            default:
                // U slučaju neispravnog dana u nedelji
                dayName = "Unknown";
                break;
        }

        return dayName;
    }

    public void findWeekly(String weeklyId){
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference usersRef = database.getReference("weeklyWorkHours");

        usersRef.child(weeklyId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // Korisnik s proslijeđenim ID-om je pronađen
                     weekly= dataSnapshot.getValue(WeeklyWorkHours.class);


                    // Ovdje možete manipulirati podacima o korisniku
                } else {
                    // Korisnik nije pronađen
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Greška prilikom dohvatanja podataka
                Log.w("TAG", "Greška prilikom dohvatanja korisnika.", databaseError.toException());
            }
        });



    }

    private int convertToMinutes(String time) {
        String[] parts = time.split(":");
        int hours = Integer.parseInt(parts[0]);
        int minutes = Integer.parseInt(parts[1]);
        return hours * 60 + minutes;
    }

}