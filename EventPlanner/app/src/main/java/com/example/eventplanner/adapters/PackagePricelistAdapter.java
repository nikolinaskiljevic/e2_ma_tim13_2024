package com.example.eventplanner.adapters;

import static android.view.View.GONE;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class PackagePricelistAdapter extends RecyclerView.Adapter<PackagePricelistAdapter.ViewHolder> {

    private final Context context;
    private final List<Package> packageList;
    private final LayoutInflater inflater;
    private EditButtonClickListener editListener;

private String role;

    public PackagePricelistAdapter(Context context, List<Package> packageList,String role) {
        this.context = context;
        this.packageList = packageList;
        this.inflater = LayoutInflater.from(context);
this.role = role;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_package_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Package packagee = packageList.get(position);
        holder.bind(packagee);

        holder.buttonEdit.setOnClickListener(v -> {
            if (editListener != null) {
                editListener.onEditButtonClick(packagee);
            }
        });
    }

    @Override
    public int getItemCount() {
        return packageList.size();
    }

    public void setOnEditButtonClickListener(EditButtonClickListener listener) {
        this.editListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final ImageButton buttonEdit,buttonEditDiscount;
        final ImageButton buttonDelete;
        final TextView textViewName;
        final TextView textViewPrice;
        final TextView textViewId;
        final TextView textViewDiscount;
        final TextView textViewDiscountedPrice;
        final EditText editTextDiscount;


        private TextView textViewPackageDescription;
        private TextView textViewPackageImages;
        private TextView textViewPackageVisibility;
        private TextView textViewPackageAvailability;
        private TextView textViewPackageCategory;
        private TextView textViewPackageSelectedProducts;
        private TextView textViewPackageSelectedServices;
        private TextView textViewPackageSubcategories;
        private TextView textViewPackageEventTypes;
        private TextView textViewPackageReservationDeadline;
        private TextView textViewPackageCancellationDeadline;
        private TextView textViewPackageConfirmationMethod;

        ViewHolder(View itemView) {
            super(itemView);
            buttonEdit = itemView.findViewById(R.id.buttonEditPackage);
            buttonEditDiscount = itemView.findViewById(R.id.buttonEditPackageDiscount);

            buttonDelete= itemView.findViewById(R.id.buttonDeletePackage);
            textViewName = itemView.findViewById(R.id.textViewPackageName);
            textViewPrice = itemView.findViewById(R.id.textViewPackagePrice);
            textViewId = itemView.findViewById(R.id.textViewPackageId);
            textViewDiscount = itemView.findViewById(R.id.textViewPackageDiscount);
            textViewDiscountedPrice = itemView.findViewById(R.id.textViewPackageDiscountedPrice);

            //-------------------------------------------------------------

            textViewPackageDescription = itemView.findViewById(R.id.textViewPackageDescription);
            textViewPackageImages = itemView.findViewById(R.id.textViewPackageImages);
            textViewPackageVisibility = itemView.findViewById(R.id.textViewPackageVisibility);
            textViewPackageAvailability = itemView.findViewById(R.id.textViewPackageAvailability);
            textViewPackageCategory = itemView.findViewById(R.id.textViewPackageCategory);
            textViewPackageSelectedProducts = itemView.findViewById(R.id.textViewPackageSelectedProducts);
            textViewPackageSelectedServices = itemView.findViewById(R.id.textViewPackageSelectedServices);
            textViewPackageSubcategories = itemView.findViewById(R.id.textViewPackageSubcategories);
            textViewPackageEventTypes = itemView.findViewById(R.id.textViewPackageEventTypes);
            textViewPackageReservationDeadline = itemView.findViewById(R.id.textViewPackageReservationDeadline);
            textViewPackageCancellationDeadline = itemView.findViewById(R.id.textViewPackageCancellationDeadline);
            textViewPackageConfirmationMethod = itemView.findViewById(R.id.textViewPackageConfirmationMethod);
            editTextDiscount = itemView.findViewById(R.id.editTextDiscount);
            // Set visibility to GONE
            textViewPackageDescription.setVisibility(View.GONE);
            textViewPackageImages.setVisibility(View.GONE);
            textViewPackageVisibility.setVisibility(View.GONE);
            textViewPackageAvailability.setVisibility(View.GONE);
            textViewPackageCategory.setVisibility(View.GONE);
            textViewPackageSelectedProducts.setVisibility(View.GONE);
            textViewPackageSelectedServices.setVisibility(View.GONE);
            textViewPackageSubcategories.setVisibility(View.GONE);
            textViewPackageEventTypes.setVisibility(View.GONE);
            textViewPackageReservationDeadline.setVisibility(View.GONE);
            textViewPackageCancellationDeadline.setVisibility(View.GONE);
            textViewPackageConfirmationMethod.setVisibility(View.GONE);
            buttonDelete.setVisibility(View.GONE);
            buttonEdit.setVisibility(GONE);


            if(!role.equals("pupv")){
                buttonEditDiscount.setVisibility(GONE);
            }




        }

        void bind(Package packagee) {
            textViewName.setText(packagee.getName());
            textViewId.setText(String.valueOf(packagee.getId()));
            textViewPrice.setText(String.valueOf(packagee.getPrice()));
            textViewDiscount.setText(String.valueOf(packagee.getDiscount()));
            textViewDiscountedPrice.setText(String.valueOf(packagee.getPrice() * (100 - packagee.getDiscount()) / 100));

            buttonEditDiscount.setOnClickListener(v -> {
                editTextDiscount.setVisibility(View.VISIBLE);
                editTextDiscount.setText(textViewDiscount.getText());
                textViewDiscount.setVisibility(View.GONE);
                editTextDiscount.requestFocus();
            });


            editTextDiscount.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    textViewDiscount.setText(editTextDiscount.getText());
                    editTextDiscount.setVisibility(View.GONE);
                    textViewDiscount.setVisibility(View.VISIBLE);
                    // Update product discount
                    packagee.setDiscount(Integer.parseInt(editTextDiscount.getText().toString()));
                    savePackageChanges(packagee);
                    // Notify adapter about item change
                    notifyItemChanged(getAdapterPosition());
                    return true;
                }
                return false;
            });
        }
    }

    public interface EditButtonClickListener {
        void onEditButtonClick(Package packagee);
    }




    private void savePackageChanges(Package updatedPackage) {

        // Pristupanje bazi podataka "products" kako bismo pronašli odgovarajući proizvod
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("packages");

        // Dodavanje slušača koji se poziva kada se dohvate podaci iz baze podataka
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Package product = snapshot.getValue(Package.class);

                    //int productid = Integer.parseInt(snapshot.getKey());
                    String productId = snapshot.getKey();// Dohvatanje ključa proizvoda
                    if (product != null && productId.equals(updatedPackage.getId())) {
                        DatabaseReference productRef = databaseRef.child(productId);
                        Log.d("azuriiraan ", product.getName());
                        productRef.setValue(updatedPackage)
                                .addOnSuccessListener(aVoid -> {
                                    // Ažuriranje uspešno završeno
                                })
                                .addOnFailureListener(e -> {
                                    // Greška pri ažuriranju
                                });
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška pri čitanju iz baze podataka
                // Možete ovde izvršiti neku radnju ako dođe do greške
            }
        });

    }




}
