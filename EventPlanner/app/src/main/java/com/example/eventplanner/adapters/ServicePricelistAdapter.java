package com.example.eventplanner.adapters;

import static android.view.View.GONE;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ServicePricelistAdapter extends RecyclerView.Adapter<ServicePricelistAdapter.ViewHolder> {

    private final Context context;
    private final List<Service> serviceList;
    private final LayoutInflater inflater;
    private EditButtonClickListener editListener;
    private FirebaseAuth auth;
    private String userRole;

    public ServicePricelistAdapter(Context context, List<Service> serviceList,String role) {
        this.context = context;
        this.serviceList = serviceList;
        this.inflater = LayoutInflater.from(context);
        this.userRole = role;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.fragment_service_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Service service = serviceList.get(position);



        holder.bind(service);

        holder.buttonEdit.setOnClickListener(v -> {
            if (editListener != null) {
                editListener.onEditButtonClick(service);
            }
        });
    }

    @Override
    public int getItemCount() {
        return serviceList.size();
    }

    public void setOnEditButtonClickListener(EditButtonClickListener listener) {
        this.editListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final ImageButton buttonEdit;
        final ImageButton buttonDelete,buttonEditPrice,buttonEditDiscount;
        final TextView textViewName;
        final TextView textViewPrice;
        final TextView textViewId;
        final TextView textViewDiscount;
        final TextView textViewDiscountedPrice;
        final EditText editTextPrice, editTextDiscount;
        final TextView descriptionTextView;
        //final TextView bookingDeadlineTextView;
        //final TextView cancellationDeadlineTextView;

        ViewHolder(View itemView) {
            super(itemView);
            buttonEdit = itemView.findViewById(R.id.buttonEditService);
            buttonEdit.setVisibility(GONE);
            buttonDelete = itemView.findViewById(R.id.buttonDeleteService);
            buttonDelete.setVisibility(View.GONE);
            buttonEditPrice = itemView.findViewById(R.id.buttonEditServicePrice);
            buttonEditDiscount = itemView.findViewById(R.id.buttonEditServiceDiscount);
            textViewName = itemView.findViewById(R.id.textViewServiceName);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewId = itemView.findViewById(R.id.textViewServiceId);
            textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
            textViewDiscountedPrice = itemView.findViewById(R.id.textViewDiscountedPrice);

            //
            descriptionTextView = itemView.findViewById(R.id.textViewDescription);
            //bookingDeadlineTextView = itemView.findViewById(R.id.textViewBookingDeadline);
            //cancellationDeadlineTextView = itemView.findViewById(R.id.textViewCancellationDeadline);

            descriptionTextView.setVisibility(View.GONE);
            //bookingDeadlineTextView.setVisibility(View.GONE);
            //cancellationDeadlineTextView.setVisibility(View.GONE);
            editTextPrice = itemView.findViewById(R.id.editTextPrice);
            editTextDiscount = itemView.findViewById(R.id.editTextDiscount);

            if(!userRole.equals("pupv")){
                buttonEditPrice.setVisibility(GONE);
                buttonEditDiscount.setVisibility(GONE);
            }




        }

        void bind(Service service) {
            textViewName.setText(service.getServiceName());
            textViewId.setText(String.valueOf(service.getId()));
            textViewPrice.setText(String.valueOf(service.getPrice()));
            textViewDiscount.setText(String.valueOf(service.getDiscount()));
            textViewDiscountedPrice.setText(String.valueOf(service.getPrice() * (100 - service.getDiscount()) / 100));


            buttonEditPrice.setOnClickListener(v -> {
                editTextPrice.setVisibility(View.VISIBLE);
                editTextPrice.setText(textViewPrice.getText());
                textViewPrice.setVisibility(View.GONE);
                editTextPrice.requestFocus();
            });

            buttonEditDiscount.setOnClickListener(v -> {
                editTextDiscount.setVisibility(View.VISIBLE);
                editTextDiscount.setText(textViewDiscount.getText());
                textViewDiscount.setVisibility(View.GONE);
                editTextDiscount.requestFocus();
            });

            editTextPrice.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    textViewPrice.setText(editTextPrice.getText());
                    editTextPrice.setVisibility(View.GONE);
                    textViewPrice.setVisibility(View.VISIBLE);
                    // Update product price
                    service.setPrice(Double.parseDouble(editTextPrice.getText().toString()));
                    // Notify adapter about item change
                    service.setDiscountedPrice(Double.parseDouble(editTextPrice.getText().toString()) * (100 - service.getDiscount()) / 100);
                    saveServiceChanges(service);
                    updateServiceInPackages(service);
                    notifyItemChanged(getAdapterPosition());
                    return true;
                }
                return false;
            });

            editTextDiscount.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    textViewDiscount.setText(editTextDiscount.getText());
                    editTextDiscount.setVisibility(View.GONE);
                    textViewDiscount.setVisibility(View.VISIBLE);
                    service.setDiscount(Double.parseDouble(editTextDiscount.getText().toString()));
                    service.setDiscountedPrice(service.getPrice() * (100 - Double.parseDouble(editTextDiscount.getText().toString())) / 100);
                    saveServiceChanges(service);
                    updateServiceInPackages(service);

                    notifyItemChanged(getAdapterPosition());
                    return true;
                }
                return false;
            });
        }
    }

    public interface EditButtonClickListener {
        void onEditButtonClick(Service service);
    }


    private void saveServiceChanges(Service updatedService) {

        // Pristupanje bazi podataka "products" kako bismo pronašli odgovarajući proizvod
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("services");

        // Pristupanje bazi podataka "products" kako bismo pronašli odgovarajući proizvod

        // Dodavanje slušača koji se poziva kada se dohvate podaci iz baze podataka
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Service product = snapshot.getValue(Service.class);

                    //int productid = Integer.parseInt(snapshot.getKey());
                    String productId = snapshot.getKey();// Dohvatanje ključa proizvoda
                    if (product != null && productId.equals(product.getId())) {
                        DatabaseReference productRef = databaseRef.child(productId);
                        productRef.setValue(updatedService)
                                .addOnSuccessListener(aVoid -> {
                                    updatePackagesWithService(updatedService);
                                    // Ažuriranje uspešno završeno
                                })
                                .addOnFailureListener(e -> {
                                    // Greška pri ažuriranju
                                });
                        break;
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška pri čitanju iz baze podataka
                // Možete ovde izvršiti neku radnju ako dođe do greške
            }
        });

    }



    public void updateServiceInPackages(Service updatedService) {
        // Dobavljanje svih paketa
        List<Package> allPackages = getAllPackages();

        // Prolazak kroz svaki paket
        for (Package pkg : allPackages) {
            // Dobavljanje liste servisa u paketu
            List<Service> selectedServices = pkg.getSelectedServices();

            // Provera da li paket sadrži servis sa datim ID-jem
            for (int i = 0; i < selectedServices.size(); i++) {
                Service service = selectedServices.get(i);
                String serviceId = String.valueOf(service.getId());
                if (serviceId.equals(updatedService.getId())) {
                    // Zamena postojećeg servisa novim servisom
                    selectedServices.set(i, updatedService);
                    // Ako želite da zamenite samo prvi servis koji se poklapa sa ID-jem, možete prekinuti petlju ovde
                    // break;
                }
            }

            // Ako želite da ažurirate listu servisa u paketu nakon zamene
            pkg.setSelectedServices(selectedServices);

            // Nakon što ste završili sa ažuriranjem servisa u paketu, možete ažurirati cenu paketa pozivom odgovarajuće metode
            pkg.updatePrice();
        }
    }



    private List<Package> getAllPackages() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("packages");

        List<Package> packages = new ArrayList<>();
        // Add listener to read data from the Firebase database
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Clear the product list before adding new ones from the database

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Package product = snapshot.getValue(Package.class);
                    if (!product.isDeleted()) { // Check if the product is not marked as deleted
                        packages.add(product);
                    }
                }

                // Notify the adapter that the data has changed
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error if there is a problem reading data from the database
                // Toast.makeText(getContext(), "Error reading products from the database", Toast.LENGTH_SHORT).show();
            }
        });
        return packages;
    }


    private void updatePackagesWithService(Service updatedProduct) {
        DatabaseReference packagesRef = FirebaseDatabase.getInstance().getReference("packages");

        packagesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot packageSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot productSnapshot : packageSnapshot.child("selectedServices").getChildren()) {
                        Service product = productSnapshot.getValue(Service.class);
                        if (product != null && product.getId().equals(updatedProduct.getId())) {
                            productSnapshot.getRef().setValue(updatedProduct);
                            updatePackagePrice(packageSnapshot.getKey());
                            Log.d("updatePackage", "Product with ID " + product.getId() + " updated in package " + packageSnapshot.getKey());
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("updatePackages", "Error updating packages", databaseError.toException());
            }
        });
    }


    private void updatePackagePrice(String packageId) {
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference("packages").child(packageId);

        packageRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                double totalPrice = 0.0;
                for (DataSnapshot productSnapshot : dataSnapshot.child("products").getChildren()) {
                    double productPrice = productSnapshot.child("price").getValue(Double.class);
                    totalPrice += productPrice;
                }
                for (DataSnapshot serviceSnapshot : dataSnapshot.child("services").getChildren()) {
                    double servicePrice = serviceSnapshot.child("price").getValue(Double.class);
                    totalPrice += servicePrice;
                }
                // Update package price
                packageRef.child("price").setValue(totalPrice)
                        .addOnSuccessListener(aVoid -> {
                            // Price updated successfully
                        })
                        .addOnFailureListener(e -> {
                            // Error updating price
                        });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Error reading package data
            }
        });
    }


}
