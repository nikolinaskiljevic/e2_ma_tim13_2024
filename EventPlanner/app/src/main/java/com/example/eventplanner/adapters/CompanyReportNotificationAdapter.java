package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.CompanyReport;
import com.example.eventplanner.model.Notification;

import java.util.List;

public class CompanyReportNotificationAdapter  extends RecyclerView.Adapter<CompanyReportNotificationAdapter.ViewHolder> {


    private List<CompanyReport> notificationList;

    public CompanyReportNotificationAdapter(List<CompanyReport> notificationList) {
        this.notificationList = notificationList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_company_report_notification, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CompanyReport notification = notificationList.get(position);
        holder.titleTextView.setText(notification.getReasons());
        holder.messageTextView.setText(notification.getCompanyId());
    }

    @Override
    public int getItemCount() {
        return notificationList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView titleTextView;
        public TextView messageTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.text_view_reason);
            messageTextView = itemView.findViewById(R.id.text_view_company_name);
        }
    }

}
