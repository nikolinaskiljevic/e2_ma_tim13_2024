package com.example.eventplanner.model;

import java.util.List;

public class WeeklyWorkHours {

    private String id;
    private List<DailyWorkHours> dailyWorkHoursList;

    public WeeklyWorkHours(){}

    public WeeklyWorkHours(String id, List<DailyWorkHours> dailyWorkHoursList) {
        this.id = id;
        this.dailyWorkHoursList = dailyWorkHoursList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DailyWorkHours> getDailyWorkHoursList() {
        return dailyWorkHoursList;
    }

    public void setDailyWorkHoursList(List<DailyWorkHours> dailyWorkHoursList) {
        this.dailyWorkHoursList = dailyWorkHoursList;
    }
}
