package com.example.eventplanner.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Product;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;
public class ProductAdapter extends ArrayAdapter<Product> {

    private final Context context;
    private final List<Product> productList;
    private final LayoutInflater inflater;
    private EditButtonClickListener editListener;
    private DeleteButtonClickListener deleteListener;

    public ProductAdapter(@NonNull Context context, int resource, @NonNull List<Product> productList) {
        super(context, resource, productList);
        this.context = context;
        this.productList = productList;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_product_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Product product = productList.get(position);

        // Bind data to views
        holder.bind(product);

        // Set click listeners
        holder.buttonEdit.setOnClickListener(v -> {
            if (editListener != null) {
                editListener.onEditButtonClick(product);
            }
        });

        holder.buttonDelete.setOnClickListener(v -> {
            if (deleteListener != null) {
                deleteListener.onDeleteButtonClick(product);
            }
        });

        return convertView;
    }

    public void setOnEditButtonClickListener(EditButtonClickListener listener) {
        this.editListener = listener;
    }

    public void setOnDeleteButtonClickListener(DeleteButtonClickListener listener) {
        this.deleteListener = listener;
    }

    static class ViewHolder {
        final ImageButton buttonEdit;
        final ImageButton buttonDelete;
        final TextView textViewCategory, textViewSubcategory, textViewName, textViewDescription,
                textViewPrice, textViewEventTypes, textViewAvailable;

        ViewHolder(View itemView) {
            buttonEdit = itemView.findViewById(R.id.buttonEdit);
            buttonDelete = itemView.findViewById(R.id.buttonDelete);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);
            textViewSubcategory = itemView.findViewById(R.id.textViewSubcategory);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewEventTypes = itemView.findViewById(R.id.textViewEventTypes);
            textViewAvailable = itemView.findViewById(R.id.textViewAvailable);
        }

        void bind(Product product) {
            // Bind data to views
            textViewCategory.setText(product.getCategory());
            textViewSubcategory.setText(product.getSubcategory());
            textViewName.setText(product.getName());
            textViewDescription.setText(product.getDescription());
            textViewPrice.setText(String.valueOf(product.getPrice()));
            textViewEventTypes.setText(product.getEventTypes());
            textViewAvailable.setText(product.isAvailable() ? "Available" : "Not available");
        }
    }

    public interface EditButtonClickListener {
        void onEditButtonClick(Product product);
    }

    public interface DeleteButtonClickListener {
        void onDeleteButtonClick(Product product);
    }

}