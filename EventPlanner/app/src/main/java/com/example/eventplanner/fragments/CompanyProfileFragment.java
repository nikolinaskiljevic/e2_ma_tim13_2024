package com.example.eventplanner.fragments;

import static androidx.core.content.ContextCompat.getSystemService;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.CompanyReport;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationHelper;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CompanyProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CompanyProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "companyId";
    private static final String ARG_PARAM2 = "userRole";

    // TODO: Rename and change types of parameters
    private String companyId;
    private String userRole;
    private Company currentCompany;
    private NotificationRepository notificationRepository;
    private String companyImageURL;
    private List<Company> allCompanies = new ArrayList<>();
    private List<User> allUsers = new ArrayList<>();
    private List<User> allAdmins = new ArrayList<>();
    private List<CompanyGrade> allGrades = new ArrayList<>();
    private List<CompanyGrade> companyGrades = new ArrayList<>();
    private ImageButton buttonReportCompany;
    private EditText editTextReportReason;
    private ImageButton buttonSubmitReport;
    private TextView name, email, address, phone, description, grades;
    private ImageView companyPhoto;
    private Button openChat;

    public CompanyProfileFragment() {
        // Required empty public constructor
        notificationRepository = new NotificationRepository();
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param companyId Parameter 1
     * @param userRole  Parameter 2
     * @return A new instance of fragment CompanyProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CompanyProfileFragment newInstance(String companyId, String userRole) {
        CompanyProfileFragment fragment = new CompanyProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, companyId);
        args.putString(ARG_PARAM2, userRole);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationRepository = new NotificationRepository();
        if (getArguments() != null) {
            companyId = getArguments().getString(ARG_PARAM1);
            userRole = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_company_profile, container, false);
        notificationRepository = new NotificationRepository();
        buttonReportCompany = view.findViewById(R.id.buttonReportCompany);
        openChat = view.findViewById(R.id.buttonOpenChat);
        if (userRole.equals("pupv") || userRole.equals("organizer")) {
            buttonReportCompany.setVisibility(View.VISIBLE);
        } else {
            buttonReportCompany.setVisibility(View.GONE);
        }
        if(userRole.equals("organizer")) {
            openChat.setVisibility(View.VISIBLE);
        }
        else {
            openChat.setVisibility(View.GONE);
        }

        editTextReportReason = view.findViewById(R.id.editTextReportReason);
        buttonSubmitReport = view.findViewById(R.id.buttonSubmitReport);

        getAllCompanies();

        name = view.findViewById(R.id.textViewCompanyName);
        email = view.findViewById(R.id.textViewCompanyEmail);
        address = view.findViewById(R.id.textViewCompanyAddress);
        phone = view.findViewById(R.id.textViewCompanyPhone);
        description = view.findViewById(R.id.textViewCompanyDescription);
        companyPhoto = view.findViewById(R.id.imageViewCompanyPhoto);
        grades = view.findViewById(R.id.textViewCompanyGrades);

        buttonReportCompany.setOnClickListener(v -> {
            editTextReportReason.setVisibility(View.VISIBLE);
            buttonSubmitReport.setVisibility(View.VISIBLE);
        });

        buttonSubmitReport.setOnClickListener(v -> {
            String reportReason = editTextReportReason.getText().toString();
            if (!reportReason.isEmpty()) {
                createReport(reportReason);
                editTextReportReason.setVisibility(View.GONE);
                buttonSubmitReport.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Report submitted", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Please enter a reason", Toast.LENGTH_SHORT).show();
            }
        });

        openChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, ChatFragment.newInstance(currentCompany.getPupvId()));
                transaction.addToBackStack(null); // Dodajte fragment na back stack kako biste se mogli vratiti nazad
                transaction.commit();
            }
        });

        return view;
    }

    private void getAllCompanyGrades() {
        DatabaseReference gradeRef = FirebaseDatabase.getInstance().getReference("companyGrades");

        gradeRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allGrades.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    CompanyGrade c = snapshot.getValue(CompanyGrade.class);
                    if (c != null) {
                        allGrades.add(c);
                    }
                }
                findCompanyGrades();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private void findCompanyGrades() {
        companyGrades.clear();
        for (CompanyGrade g : allGrades) {
            if (g.getCompanyName().equals(currentCompany.getName())) {
                companyGrades.add(g);
            }
        }

        loadCompanyGrades();
    }

    private void loadCompanyGrades() {
        StringBuilder g = new StringBuilder();
        int i = 1;
        for (CompanyGrade grade : companyGrades) {
            if (g.length() == 0) {
                g = new StringBuilder(Integer.toString(i) + ". " + "Comment: " + grade.getComment() + "\n" +
                        "\t\tRating: " + String.format("%.2f", grade.getGrade()) + "\n");
                i++;
            } else {
                g.append(Integer.toString(i)).append(". ").append("Comment: ").append(grade.getComment()).append("\n").append("\t\tRating: ").append(String.format("%.2f", grade.getGrade())).append("\n");
                i++;
            }
        }

        if (companyGrades.isEmpty()) {
            g = new StringBuilder("There is no company grades for this company.");
        }

        grades.setText(g.toString());

        getAllUsers();
    }

    private void getAllUsers() {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allUsers.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    if (u != null) {
                        allUsers.add(u);
                    }
                }
                findAdmins();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private void findAdmins() {
        allAdmins.clear();
        for(User u: allUsers) {
            if(u.getRole().equals("admin")) {
                allAdmins.add(u);
            }
        }
    }

    private void getAllCompanies() {
        DatabaseReference compRef = FirebaseDatabase.getInstance().getReference("Companies");

        compRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allCompanies.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Company c = snapshot.getValue(Company.class);
                    if (c != null) {
                        allCompanies.add(c);
                    }
                }
                findCompany();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private void findCompany() {
        for (Company c : allCompanies) {
            if (c.getId().equals(companyId)) {
                currentCompany = c;
                break;
            }
        }

        loadCompany();
    }

    private void loadCompany() {
        name.setText("Name: " + currentCompany.getName());
        email.setText("Email: " + currentCompany.getEmail());
        address.setText("Address: " + currentCompany.getAddress());
        phone.setText("Phone: " + currentCompany.getPhoneNumber());
        description.setText("Description: " + currentCompany.getDescription());

        if (currentCompany.getImageUrl() != null && !currentCompany.getImageUrl().isEmpty()) {
            companyImageURL = currentCompany.getImageUrl();
            // Use Glide to load and display the image
            Glide.with(requireContext())
                    .load(currentCompany.getImageUrl())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.lavender_border)
                            .error(R.drawable.logo))
                    .into(companyPhoto);
        }

        getAllCompanyGrades();
    }

    private void createReport(String reason) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();

        if (user != null) {
            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("reports");
            String reportId = databaseRef.push().getKey();
            CompanyReport report = new CompanyReport(companyId, reason, user.getUid());
            if (reportId != null) {
                databaseRef.child(reportId).setValue(report).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            for(User u: allAdmins) {
                                Notification notification = new Notification();
                                notification.setStatus(Notification.Status.CREATED);
                                notification.setMessage("Company is reported!\nReason: " + reason);
                                notification.setText("Notification! Company report!");
                                notification.setUserId(u.getUid());
                                notificationRepository.createNotification(notification);
                            }

                            // Prikazivanje obaveštenja kada je izveštaj uspešno sačuvan
                            //NotificationHelper.displayNotification(getContext(), "New Report Available", "Check out the latest company report now.");
                        } else {
                            // Opcionalno: Obrada greške prilikom čuvanja izveštaja
                            Log.e("createReport", "Failed to save report", task.getException());
                        }
                    }
                });
            }
        } else {
            // Opcionalno: Obrada greške kada korisnik nije prijavljen
            Log.e("createReport", "User is not authenticated");
        }
    }

}

