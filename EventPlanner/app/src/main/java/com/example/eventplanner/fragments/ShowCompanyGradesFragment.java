package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CompanyGradeAdapter;
import com.example.eventplanner.adapters.CompanyGradeReportAdapter;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.CompanyGradeReport;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class ShowCompanyGradesFragment extends Fragment {

    private RecyclerView recyclerView;
    private CompanyGradeAdapter adapter;
    private List<CompanyGrade> gradesList;

    private String companyName;

    private Spinner spinnerFilter;

    private boolean isSpinnerInitialized = false;

    public ShowCompanyGradesFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_show_company_grades, container, false);
        recyclerView = view.findViewById(R.id.recyclerViewGrades);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        spinnerFilter = view.findViewById(R.id.spinnerFilter);

        List<String> filterOptions = new ArrayList<>();
        filterOptions.add("Ascending");
        filterOptions.add("Descending");



        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, filterOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFilter.setAdapter(adapter);


        spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (isSpinnerInitialized) {
                    sortGradesByDate(parentView.getItemAtPosition(position).toString());
                }else{
                    isSpinnerInitialized = true;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Do nothing here
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            companyName = bundle.getString("companyName");


        }
        gradesList = new ArrayList<>();
        populateGrades();

        return view;
    }

    private void populateGrades(){

        gradesList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference reportsRef = databaseReference.child("companyGrades");


        reportsRef.addListenerForSingleValueEvent(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                List<CompanyGrade> newGradeList = new ArrayList<>();

                for (DataSnapshot gradeSnapshot : dataSnapshot.getChildren()) {
                    CompanyGrade grade = gradeSnapshot.getValue(CompanyGrade.class);
                    Log.e("TAG","CompanyName"+ companyName);
                    if(grade.getCompanyName().equals(companyName)) {
                        Log.e("TAG","USAO");
                        newGradeList.add(grade);
                    }
                }



                    for (CompanyGrade grade : newGradeList) {
                        if (!gradesList.contains(grade)) {
                            gradesList.add(grade);
                        }
                    }

                Log.e("TAG","gradeslist"+gradesList);
                adapter = new CompanyGradeAdapter(gradesList, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void sortGradesByDate(String sortOrder) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        List<CompanyGrade> sortedList = new ArrayList<>(gradesList);

        for (int i = 0; i < sortedList.size() - 1; i++) {
            for (int j = i + 1; j < sortedList.size(); j++) {
                try {
                    Date date1 = sdf.parse(sortedList.get(i).getDate());
                    Date date2 = sdf.parse(sortedList.get(j).getDate());

                    boolean condition = sortOrder.equals("Ascending") ? date1.after(date2) : date1.before(date2);

                    if (condition) {
                        // Swap elements
                        CompanyGrade temp = sortedList.get(i);
                        sortedList.set(i, sortedList.get(j));
                        sortedList.set(j, temp);
                    }
                } catch (ParseException e) {
                    Log.e("ShowCompanyGradesFragment", "Date parsing error", e);
                }
            }
        }

        // Debagiranje: Proverimo sortiranu listu
        for (CompanyGrade grade : sortedList) {
            Log.d("ShowCompanyGradesFragment", "Sorted Grade Date: " + grade.getDate());
        }

        adapter.updateList(sortedList);
    }

}