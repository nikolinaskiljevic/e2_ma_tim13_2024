package com.example.eventplanner.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Iterator;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyCategories;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PupvProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PupvProfile extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static final int PICK_IMAGE_REQUEST = 1;

    private TextView textViewUserName, textViewUserAddress, textViewUserPhone, textViewUserEmail, textViewCompanyName, textViewCompanyEmail, textViewCompanyAddress, textViewCompanyPhone, textViewCompanyDescription, productsCategories, eventTypes;
    private ImageView imageViewUserPhoto, imageViewCompanyPhoto;
    private String profileImageURL, companyImageURL;
    private EditText newName, newAddress, newPhone, oldPassword, newPassword, confirmPassword, editTextAddress, editTextPhone, editTextDescription;
    private Button saveName, saveAddress, savePhone, savePassword, changePassword, updateCompanyAddress, updateCompanyPhone, updateCompanyDescription, uploadNewCompanyPhoto, updateCompanyPhoto, addEventTypes, removeEventTypes, deactivateButton,buttonShowGrades;
    private LinearLayout eventTypesCheckboxes, existingEventCheckBoxes;
    private List<Company> allCompanies = new ArrayList<>();
    private List<CompanyCategories> allCompanyCategories = new ArrayList<>();
    private List<CompanyCategories> thisCompanyCategories = new ArrayList<>();
    private List<Product> allProducts = new ArrayList<>();
    private List<Product> thisCompanyProducts = new ArrayList<>();
    private List<User> usersList = new ArrayList<>();
    private List<User> pupvEmployees = new ArrayList<>();
    private List<Event> allEvents = new ArrayList<>();
    private List<String> allEventTypes = new ArrayList<>();
    private Company pupvCompany;
    private Uri picURL = null;

    public PupvProfile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PupvProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static PupvProfile newInstance(String param1, String param2) {
        PupvProfile fragment = new PupvProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pupv_profile, container, false);


        textViewCompanyName = view.findViewById(R.id.textViewNameOfCompany);
        textViewCompanyEmail = view.findViewById(R.id.textViewEmailOfCompany);
        textViewCompanyAddress = view.findViewById(R.id.textViewAddressOfCompany);
        textViewCompanyPhone = view.findViewById(R.id.textViewPhoneOfCompany);
        textViewCompanyDescription = view.findViewById(R.id.textViewDescriptionOfCompany);

        editTextAddress = view.findViewById(R.id.editTextAddressOfCompany);
        editTextPhone = view.findViewById(R.id.editTextPhoneOfCompany);
        editTextDescription = view.findViewById(R.id.editTextDescriptionOfCompany);
        imageViewCompanyPhoto = view.findViewById(R.id.imageViewCompany);


        textViewUserName = view.findViewById(R.id.textViewUserName);
        textViewUserAddress = view.findViewById(R.id.textViewUserAddress);
        textViewUserPhone = view.findViewById(R.id.textViewUserPhone);
        textViewUserEmail = view.findViewById(R.id.textViewUserEmail);
        imageViewUserPhoto = view.findViewById(R.id.imageViewProfile);

        newName = view.findViewById(R.id.editTextName);
        newAddress = view.findViewById(R.id.editTextAddress);
        newPhone = view.findViewById(R.id.editTextPhone);
        saveName = view.findViewById(R.id.buttonSaveName);
        saveAddress = view.findViewById(R.id.buttonSaveAddress);
        savePhone = view.findViewById(R.id.buttonSavePhone);


        productsCategories = view.findViewById(R.id.textViewThisCompanyProductCategories);
        eventTypes = view.findViewById(R.id.textViewThisCompanyEventTypes);
        eventTypesCheckboxes = view.findViewById(R.id.eventTypesCheckBoxContainer);
        existingEventCheckBoxes = view.findViewById(R.id.existingEventTypesCheckBoxContainer);

        oldPassword = view.findViewById(R.id.editTextOldPassword);
        newPassword = view.findViewById(R.id.editTextNewPassword);
        confirmPassword = view.findViewById(R.id.editTextConfirmNewPassword);
        savePassword = view.findViewById(R.id.buttonSavePassword);
        changePassword = view.findViewById(R.id.buttonChangePassword);

        updateCompanyAddress = view.findViewById(R.id.buttonSaveAddressOfCompany);
        updateCompanyPhone = view.findViewById(R.id.buttonSavePhoneOfCompany);
        updateCompanyDescription = view.findViewById(R.id.buttonSaveDescriptionOfCompany);
        uploadNewCompanyPhoto = view.findViewById(R.id.buttonUploadPhotoOfCompany);
        updateCompanyPhoto = view.findViewById(R.id.buttonSavePhotoOfCompany);

        addEventTypes = view.findViewById(R.id.buttonAddEventTypes);
        removeEventTypes = view.findViewById(R.id.buttonRemoveEventTypes);
        deactivateButton = view.findViewById(R.id.buttonDeactivate);
        buttonShowGrades=view.findViewById(R.id.buttonShowGrades);

        getCompanies();
        loadUserData();
        fetchAllUsers();

        deactivateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean hasNoServices = true;
                //obraditi provjeru da li korisnik ima rezervisane usluge (pomocu Uid)

                if(hasNoServices) {
                    updateUser(0, "", true);
                    for(User e: pupvEmployees) {
                        deactivateEmployees(e.getUid());
                    }
                    for(Product p: thisCompanyProducts) {
                        deactivateProducts(p);
                    }
                }
                else {
                    Toast.makeText(getActivity(), "You can not deactivate your profile. You have reserved services.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        removeEventTypes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> selectedEventTypes = new ArrayList<>();
                for (int i = 0; i < existingEventCheckBoxes.getChildCount(); i++) {
                    View child = existingEventCheckBoxes.getChildAt(i);
                    if (child instanceof CheckBox) {
                        CheckBox checkBox = (CheckBox) child;
                        if (checkBox.isChecked()) {
                            selectedEventTypes.add(checkBox.getText().toString());
                        }
                    }
                }
                // Obrada odabranih event tipova

                updateEventTypes(selectedEventTypes, false);
            }
        });

        addEventTypes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> selectedEventTypes = new ArrayList<>();
                for (int i = 0; i < eventTypesCheckboxes.getChildCount(); i++) {
                    View child = eventTypesCheckboxes.getChildAt(i);
                    if (child instanceof CheckBox) {
                        CheckBox checkBox = (CheckBox) child;
                        if (checkBox.isChecked()) {
                            selectedEventTypes.add(checkBox.getText().toString());
                        }
                    }
                }
                // Obrada odabranih event tipova

                updateEventTypes(selectedEventTypes, true);
            }
        });

        uploadNewCompanyPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddPhotoDialog();
            }
        });

        updateCompanyPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(picURL != null){
                    updateCompanyPhoto(picURL);
                }
            }
        });

        updateCompanyAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCompany(1, editTextAddress.getText().toString());
            }
        });

        updateCompanyPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCompany(2, editTextPhone.getText().toString());
            }
        });

        updateCompanyDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCompany(3, editTextDescription.getText().toString());
            }
        });


        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPassword.setVisibility(View.VISIBLE);
                newPassword.setVisibility(View.VISIBLE);
                confirmPassword.setVisibility(View.VISIBLE);
                savePassword.setVisibility(View.VISIBLE);
            }
        });

        savePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });

        saveName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //currentUser.setName(newName.getText().toString());
                updateUser(1, newName.getText().toString(), false);
            }
        });

        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser(2, newAddress.getText().toString(), false);
            }
        });

        savePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser(3, newPhone.getText().toString(), false);
            }
        });

        buttonShowGrades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String companyName = pupvCompany.getName();


                ShowCompanyGradesFragment fragment = new ShowCompanyGradesFragment();
                Bundle bundle = new Bundle();
                bundle.putString("companyName", companyName);
                fragment.setArguments(bundle);


                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    private void updateEventTypes(List<String> newTypes, boolean adding) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String userId = currentUser.getUid();
        Company updatedCompany;

        updatedCompany = new Company(userId, textViewCompanyEmail.getText().toString(), textViewCompanyName.getText().toString(), textViewCompanyAddress.getText().toString(), textViewCompanyPhone.getText().toString(), textViewCompanyDescription.getText().toString(), companyImageURL);
        updatedCompany.setId(pupvCompany.getId());
        List<String> updatedTypes = pupvCompany.getEventTypes();

        if(adding) {
            updatedTypes.addAll(newTypes);
        }
        else {
            Iterator<String> iterator = updatedTypes.iterator();
            while (iterator.hasNext()) {
                String s1 = iterator.next();
                if (newTypes.contains(s1)) {
                    iterator.remove();
                }
            }
        }

        updatedCompany.setEventTypes(updatedTypes);

        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference().child("Companies").child(pupvCompany.getId());
        companyRef.setValue(updatedCompany).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Well done!", Toast.LENGTH_SHORT).show();
                    getCompanies();

                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void defineCheckboxList() {
        eventTypesCheckboxes.removeAllViews();
        existingEventCheckBoxes.removeAllViews();

        List<String> unaddedTypes = new ArrayList<>();
        for(String et: allEventTypes) {
            if(!pupvCompany.getEventTypes().contains(et) && !unaddedTypes.contains(et)) {
                unaddedTypes.add(et);
            }
        }

        for (String eventType: unaddedTypes){
            CheckBox checkBox = new CheckBox(requireContext());
            checkBox.setText(eventType);
            eventTypesCheckboxes.addView(checkBox);
        }

        for (String eventType: pupvCompany.getEventTypes()){
            CheckBox checkBox = new CheckBox(requireContext());
            checkBox.setText(eventType);
            existingEventCheckBoxes.addView(checkBox);
        }
    }

    private void getAllCompanyCategories() {
        DatabaseReference compRef = FirebaseDatabase.getInstance().getReference("CompanyCategories");

        compRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allCompanyCategories.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    CompanyCategories cc = snapshot.getValue(CompanyCategories.class);
                    if (cc != null) {
                        allCompanyCategories.add(cc);
                    }
                }
                findCategoriesOfThisCompany();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read categories of company", databaseError.toException());
            }
        });
    }

    private void getAllEvents() {
        DatabaseReference compRef = FirebaseDatabase.getInstance().getReference("events");

        compRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allEvents.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Event e = snapshot.getValue(Event.class);
                    if (e != null) {
                        allEvents.add(e);
                    }
                }
                findAllEventTypesInSystem();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read products", databaseError.toException());
            }
        });
    }

    private void findAllEventTypesInSystem() {
        allEventTypes.clear();
        for(Event e: allEvents) {
            allEventTypes.add(e.getEventType());
        }

        fillEventTypes();
    }

    private void fillEventTypes() {
        StringBuilder et = new StringBuilder();
        int i = 1;
        for(String s: pupvCompany.getEventTypes()){
            if(et.toString().isEmpty()) {
                et = new StringBuilder(Integer.toString(i) + ". " + s + "\n");
                i++;
            }
            else {
                et.append(Integer.toString(i)).append(". ").append(s).append("\n");
                i++;
            }
        }

        if(pupvCompany.getEventTypes().isEmpty()){
            et = new StringBuilder("There is no event types of this company.");
        }

        eventTypes.setText(et);
        defineCheckboxList();
    }

    private void findCategoriesOfThisCompany() {
        thisCompanyCategories.clear();
        for(CompanyCategories cc: allCompanyCategories) {
            if(cc.getCompanyId().equals(pupvCompany.getId())) {
                thisCompanyCategories.add(cc);
            }
        }

        getAllProducts();
    }

    private void getAllProducts() {
        DatabaseReference compRef = FirebaseDatabase.getInstance().getReference("products");

        compRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allProducts.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product p = snapshot.getValue(Product.class);
                    if (p != null) {
                        allProducts.add(p);
                    }
                }
                findProductsOfThisCompany();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read products", databaseError.toException());
            }
        });
    }

    private void findProductsOfThisCompany() {
        thisCompanyProducts.clear();
        List<String> productIds = new ArrayList<>();
        for(CompanyCategories cc: thisCompanyCategories){
            for(String pid: cc.getProductIds()) {
                if(productIds.isEmpty() || !productIds.contains(pid)) {
                    productIds.add(pid);
                }
            }
        }

        for(String pid: productIds) {
            for(Product p: allProducts) {
                if(pid.equals(p.getId())) {
                    thisCompanyProducts.add(p);
                    break;
                }
            }
        }

        fillProductCategoriesTextview();
    }

    private void fillProductCategoriesTextview() {
        StringBuilder textView = new StringBuilder();
        for(Product p: thisCompanyProducts) {
            if(textView.toString().equals("")){
                textView = new StringBuilder(p.getCategory() + "\n");
            }
            else {
                textView.append(p.getCategory()).append("\n");
            }
        }

        if(thisCompanyProducts.isEmpty()){
            textView = new StringBuilder("There is no product categories of this company.");
        }

        productsCategories.setText(textView);
        getAllEvents();
    }

    private void updateCompany(int field, String data) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String userId = currentUser.getUid();
        Company updatedCompany;

        if(field == 1) { //address
            updatedCompany = new Company(userId, textViewCompanyEmail.getText().toString(), textViewCompanyName.getText().toString(), data, textViewCompanyPhone.getText().toString(), textViewCompanyDescription.getText().toString(), companyImageURL);
            updatedCompany.setId(pupvCompany.getId());
        }
        else if(field == 2) { //phone
            updatedCompany = new Company(userId, textViewCompanyEmail.getText().toString(), textViewCompanyName.getText().toString(), textViewCompanyAddress.getText().toString(), data, textViewCompanyDescription.getText().toString(), companyImageURL);
            updatedCompany.setId(pupvCompany.getId());
        }
        else if(field == 3) { //description
            updatedCompany = new Company(userId, textViewCompanyEmail.getText().toString(), textViewCompanyName.getText().toString(), textViewCompanyAddress.getText().toString(), textViewCompanyPhone.getText().toString(), data, companyImageURL);
            updatedCompany.setId(pupvCompany.getId());
        }
        else {
            updatedCompany = new Company(userId, textViewCompanyEmail.getText().toString(), textViewCompanyName.getText().toString(), textViewCompanyAddress.getText().toString(), textViewCompanyPhone.getText().toString(), textViewCompanyDescription.getText().toString(), data);
            updatedCompany.setId(pupvCompany.getId());
            companyImageURL = data;
        }

        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference().child("Companies").child(pupvCompany.getId());
        companyRef.setValue(updatedCompany).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Well done!", Toast.LENGTH_SHORT).show();
                    getCompanies();
                    editTextAddress.setText("");
                    editTextPhone.setText("");
                    editTextDescription.setText("");
                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void openAddPhotoDialog() {
        // Open gallery to select image
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            picURL = data.getData();

            //companyImageURL = picURL.toString();
            imageViewCompanyPhoto.setImageURI(picURL);
        }
    }

    private void updateCompanyPhoto(Uri imageUri) {
        // Generate a random name for the image
        String imageName = "profile_image_" + System.currentTimeMillis() + ".jpg";

        // Create a reference to the Firebase Storage location
        StorageReference imageRef = FirebaseStorage.getInstance().getReference().child("profile_images").child(imageName);

        // Upload the image
        imageRef.putFile(imageUri)
                .addOnSuccessListener(requireActivity(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // Image upload successful, get the download URL
                        imageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                String imageUrl = uri.toString();
                                // Now, you can save this imageUrl along with user data in the database
                                updateCompany(0, imageUrl);
                            }
                        });
                    }
                })
                .addOnFailureListener(requireActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Image upload failed
                        Toast.makeText(requireContext(), "Failed to upload image", Toast.LENGTH_SHORT).show();
                        Log.e("User Creation", "Image upload failed", e);
                    }
                });
    }

    private void loadUserData() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String userId = currentUser.getUid();
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        textViewUserName.setText(user.getFirstName() + " " + user.getLastName());
                        textViewUserAddress.setText(user.getAddress());
                        textViewUserPhone.setText(user.getPhoneNumber());
                        textViewUserEmail.setText(currentUser.getEmail());
                        if (user.getImageURL() != null && !user.getImageURL().isEmpty()) {
                            profileImageURL = user.getImageURL();
                            // Use Glide to load and display the image
                            Glide.with(requireContext())
                                    .load(user.getImageURL())
                                    .apply(new RequestOptions()
                                            .placeholder(R.drawable.lavender_border)
                                            .error(R.drawable.logo))
                                    .into(imageViewUserPhoto);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Handle database error
                }
            });
        }
    }

    private void getCompanies() {
        DatabaseReference compRef = FirebaseDatabase.getInstance().getReference("Companies");

        compRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allCompanies.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Company c = snapshot.getValue(Company.class);
                    if (c != null) {
                        allCompanies.add(c);
                    }
                }
                findCompany();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private void findCompany() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        for(Company c: allCompanies) {
            if(c.getPupvId().equals(currentUser.getUid())) {
                pupvCompany = c;
                break;
            }
        }

        loadCompany();
    }

    private void loadCompany() {
        textViewCompanyName.setText(pupvCompany.getName());
        textViewCompanyAddress.setText(pupvCompany.getAddress());
        textViewCompanyEmail.setText(pupvCompany.getEmail());
        textViewCompanyPhone.setText(pupvCompany.getPhoneNumber());
        textViewCompanyDescription.setText(pupvCompany.getDescription());

        if (pupvCompany.getImageUrl() != null && !pupvCompany.getImageUrl().isEmpty()) {
            companyImageURL = pupvCompany.getImageUrl();
            // Use Glide to load and display the image
            Glide.with(requireContext())
                    .load(pupvCompany.getImageUrl())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.lavender_border)
                            .error(R.drawable.logo))
                    .into(imageViewCompanyPhoto);
        }

        getAllCompanyCategories();
    }

    private void updateUser(int field, String data, boolean isDeactivated) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String userId = currentUser.getUid();
        User updatedUser;

        if(field == 1) {
            String[] parts = data.split(" ");
            updatedUser = new User(userId, "pupv", textViewUserEmail.getText().toString(), parts[0], parts[1], textViewUserAddress.getText().toString(), textViewUserPhone.getText().toString(), isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }
        else if(field == 2) {
            String[] parts = textViewUserName.getText().toString().split(" ");
            updatedUser = new User(userId, "pupv", textViewUserEmail.getText().toString(), parts[0], parts[1], data, textViewUserPhone.getText().toString(), isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }
        else if(field == 3) {
            String[] parts = textViewUserName.getText().toString().split(" ");
            updatedUser = new User(userId, "pupv", textViewUserEmail.getText().toString(), parts[0], parts[1], textViewUserAddress.getText().toString(), data, isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }
        else {
            String[] parts = textViewUserName.getText().toString().split(" ");
            updatedUser = new User(userId, "pupv", textViewUserEmail.getText().toString(), parts[0], parts[1], textViewUserAddress.getText().toString(), textViewUserPhone.getText().toString(), isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
        userRef.setValue(updatedUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Well done!", Toast.LENGTH_SHORT).show();
                    loadUserData();
                    newName.setText("");
                    newAddress.setText("");
                    newPhone.setText("");
                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void deactivateEmployees(String employeeId) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        String[] parts = textViewUserName.getText().toString().split(" ");
        User updatedUser = new User(employeeId, "employee", textViewUserEmail.getText().toString(), parts[0], parts[1], textViewUserAddress.getText().toString(), textViewUserPhone.getText().toString(), true);
        updatedUser.setImageURL(profileImageURL);
        updatedUser.setPupvId(currentUser.getUid());

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(employeeId);
        userRef.setValue(updatedUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Well done!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void deactivateProducts(Product p) {
        p.setVisible(false);

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("products").child(p.getId());
        userRef.setValue(p).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Well done!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void fetchAllUsers() {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("Users");

        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersList.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    if (user != null) {
                        usersList.add(user);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findCompanyEmployees();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private void findCompanyEmployees() {
        pupvEmployees.clear();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        for(User u: usersList) {
            if(u.getPupvId().equals(currentUser.getUid()) && u.getRole().equals("employee")){
                pupvEmployees.add(u);
            }
        }
    }

    private void changePassword() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String oldPass = oldPassword.getText().toString();
        String newPass = newPassword.getText().toString();
        String confirmNewPass = confirmPassword.getText().toString();

        if (!newPass.equals(confirmNewPass)) {
            Toast.makeText(getActivity(), "New passwords do not match", Toast.LENGTH_SHORT).show();
            return;
        }

        if (currentUser != null && currentUser.getEmail() != null) {
            // Reauthenticate the user
            AuthCredential credential = EmailAuthProvider.getCredential(currentUser.getEmail(), oldPass);
            currentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        // Update the password
                        currentUser.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(getActivity(), "Password updated successfully", Toast.LENGTH_SHORT).show();
                                    oldPassword.setVisibility(View.INVISIBLE);
                                    newPassword.setVisibility(View.INVISIBLE);
                                    confirmPassword.setVisibility(View.INVISIBLE);
                                    savePassword.setVisibility(View.INVISIBLE);
                                } else {
                                    Toast.makeText(getActivity(), "Error updating password", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "Authentication failed. Check your old password", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}