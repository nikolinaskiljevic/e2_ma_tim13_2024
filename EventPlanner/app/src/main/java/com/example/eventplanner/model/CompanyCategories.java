package com.example.eventplanner.model;

import java.util.List;

public class CompanyCategories {
    String id;
    String companyId;
    private List<String> eventIds;
    private List<String> productIds;

    public CompanyCategories() {
    }

    public CompanyCategories(String id, String companyId, List<String> eventIds, List<String> productIds) {
        this.id = id;
        this.companyId = companyId;
        this.eventIds = eventIds;
        this.productIds = productIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public List<String> getEventIds() {
        return eventIds;
    }

    public void setEventIds(List<String> eventIds) {
        this.eventIds = eventIds;
    }

    public List<String> getProductIds() {
        return productIds;
    }

    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }
}
