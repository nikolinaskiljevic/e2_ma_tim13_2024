package com.example.eventplanner.fragments;

import android.annotation.SuppressLint;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ServiceAdapter;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditServiceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditServiceFragment extends Fragment implements Parcelable {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    // EditText fields for service details
    private EditText editTextServiceName, editTextDescription, editTextPrice,
            editTextDiscount, editTextEventTypes, editTextBookingDeadline,editTextCancellationDeadline,
            editTextDuration, editTextSpecifics;

    // CheckBoxes for visibility and availability
    Spinner spinnerSubcategory;
    private CheckBox checkBoxVisible, checkBoxAvailable;

    // Button for saving changes
    private Button buttonSave;

    public EditServiceFragment() {
        // Required empty public constructor
    }

    protected EditServiceFragment(Parcel in) {
        mParam1 = in.readString();
        mParam2 = in.readString();
    }

    public static final Creator<EditServiceFragment> CREATOR = new Creator<EditServiceFragment>() {
        @Override
        public EditServiceFragment createFromParcel(Parcel in) {
            return new EditServiceFragment(in);
        }

        @Override
        public EditServiceFragment[] newArray(int size) {
            return new EditServiceFragment[size];
        }
    };

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment EditServiceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditServiceFragment newInstance(Service service) {
        EditServiceFragment fragment = new EditServiceFragment();
        Bundle args = new Bundle();
        args.putParcelable("service",   (Parcelable)service);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_service, container, false);

        // Initialize EditText fields
        editTextServiceName = view.findViewById(R.id.editTextServiceName);
        editTextDescription = view.findViewById(R.id.editTextDescription);
        editTextPrice = view.findViewById(R.id.editTextPrice);
        editTextDiscount = view.findViewById(R.id.editTextDiscount);
        editTextBookingDeadline = view.findViewById(R.id.bookingDeadline);
        editTextCancellationDeadline = view.findViewById(R.id.cancellationDeadLine);
        spinnerSubcategory = view.findViewById(R.id.spinnerServiceSubcategory);
        editTextEventTypes = view.findViewById(R.id.editTextEventTypes);
        editTextDuration = view.findViewById(R.id.editTextDuration);
        editTextSpecifics = view.findViewById(R.id.editSpecifics);

        // Initialize CheckBoxes
        checkBoxVisible = view.findViewById(R.id.visibility);
        checkBoxAvailable = view.findViewById(R.id.availability);


        Bundle bundle = getArguments();
        if (bundle != null) {
            Service service = bundle.getParcelable("service");
            if (service != null) {
                // Popunite polja sa podacima iz Service objekta
                populateFields(service, view);
            }
        }
        String eventTypesText = editTextEventTypes.getText().toString().trim();
        List<String> eventTypesList = new ArrayList<>();
        eventTypesList.add(eventTypesText);




        buttonSave = view.findViewById(R.id.buttonSaveEditing);
        buttonSave.setOnClickListener(v -> {
            // Ažuriranje usluge sa novim vrednostima

            Service updatedService = new Service(
                    editTextServiceName.getText().toString(),
                    editTextDescription.getText().toString(),
                    Double.parseDouble(editTextPrice.getText().toString()),
                    Integer.parseInt(editTextDiscount.getText().toString()),
                    editTextBookingDeadline.getText().toString(),
                    editTextCancellationDeadline.getText().toString(),
                   // spinnerSubcategory.get,
                    eventTypesList,
                    editTextDuration.getText().toString(),
                    editTextSpecifics.getText().toString(),
                    checkBoxVisible.isChecked(),
                    checkBoxAvailable.isChecked()
            );

            // Čuvanje ažurirane usluge
            saveServiceChanges(updatedService);

            // Zatvaranje EditServiceFragmenta i povratak na prethodni fragment
            getParentFragmentManager().popBackStack();
        });



        return view;
    }

    private void saveServiceChanges(Service updatedService) {
        if (mListener != null && getContext() != null) {
            mListener.OnServiceUpdated(updatedService, getContext());

            // Pristupanje Firebase bazi podataka "services" kako bismo pronašli odgovarajuću uslugu
            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("services");

            // Dodavanje slušača koji se poziva kada se dohvate podaci iz baze podataka
            databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Service service = snapshot.getValue(Service.class);
                        if (service != null && service.getId() == updatedService.getId()) {
                            if (updatedService != null) {
                                Log.d("Promjena: ", updatedService.getServiceName() != null ? updatedService.getServiceName() : "ServiceName je null");
                                Log.d("Promjena: " , updatedService.getDescription() != null ? updatedService.getDescription() : "Description je null");
                                Log.d("Promjena: " , updatedService.getDuration() != null ? updatedService.getDuration() : "Duration je null");
                                Log.d("Promjena: " , updatedService.getCategory() != null ? updatedService.getCategory() : "Category je null");
                            } else {
                                Log.d("Promjena: ", "updatedService je null");
                            }

                            DatabaseReference serviceRef = snapshot.getRef();
                            serviceRef.setValue(updatedService)
                                    .addOnSuccessListener(aVoid -> {
                                        // Ažuriranje uspješno završeno
                                        if (getContext() != null) {
                                            Toast.makeText(getContext(), "Service updated successfully", Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .addOnFailureListener(e -> {
                                        // Greška pri ažuriranju
                                        Toast.makeText(getContext(), "Failed to update service: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                                    });
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Greška pri čitanju iz baze podataka
                    // Možete ovde izvršiti neku radnju ako dođe do greške
                    Toast.makeText(getContext(), "Failed to read from database: " + databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }


}


    private void populateFields(Service service, View view) {
        // Pronalaženje svih TextView polja u layout-u fragmenta
        TextView textViewServiceName = view.findViewById(R.id.editTextServiceName);
        TextView textEventTypes = view.findViewById(R.id.editTextEventTypes);
        TextView textViewDescription = view.findViewById(R.id.editTextDescription);
        TextView textViewPrice = view.findViewById(R.id.editTextPrice);
        TextView textViewDiscount = view.findViewById(R.id.editTextDiscount);
        TextView textViewBookingDeadline = view.findViewById(R.id.bookingDeadline);
        TextView textViewCancellationDeadline = view.findViewById(R.id.cancellationDeadLine);
        Spinner subcategorySpinner = view.findViewById(R.id.spinnerServiceSubcategory);
        CheckBox checkBoxVisibility = view.findViewById(R.id.visibility);
        CheckBox checkBoxAvailability = view.findViewById(R.id.availability);
        TextView textViewDuration = view.findViewById(R.id.editTextDuration);
        TextView textViewSpecifics = view.findViewById(R.id.editSpecifics);

        checkBoxVisibility.setChecked(service.isVisible());
        checkBoxAvailability.setChecked(service.isAvailable());

        textViewDuration.setText(service.getDuration());
        textViewSpecifics.setText(service.getSpecifics());

        // Postavljanje teksta u TextView polja na osnovu podataka iz Service objekta
        textViewServiceName.setText(service.getServiceName());
        textViewDescription.setText(service.getDescription());
        textViewPrice.setText(String.valueOf(service.getPrice()));

        textViewBookingDeadline.setText( service.getBookingDeadline());
        textViewCancellationDeadline.setText( service.getCancellationDeadline());
        // Konvertujte listu stringova u jedan string koristeći StringBuilder
        StringBuilder eventTypesBuilder = new StringBuilder();
        for (String eventType : service.getEventTypes()) {
            eventTypesBuilder.append(eventType).append(", "); // Dodajte svaki event type i zarez
        }

        if (eventTypesBuilder.length() > 0) {
                eventTypesBuilder.deleteCharAt(eventTypesBuilder.length() - 2);
        }

        textEventTypes.setText(eventTypesBuilder.toString());
        textViewSpecifics.setText(service.getSpecifics());


    }

    public interface OnServiceUpdateListener {
        void OnServiceUpdated(Service updatedService,Context context);
    }
    private EditServiceFragment.OnServiceUpdateListener mListener;

    // Ostatak koda fragmenta EditProduct


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof EditServiceFragment.OnServiceUpdateListener) {
            mListener = (EditServiceFragment.OnServiceUpdateListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnServiceUpdateListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(mParam1);
        dest.writeString(mParam2);
    }
}