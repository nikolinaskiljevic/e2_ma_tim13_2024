package com.example.eventplanner.activities;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.eventplanner.R;
import com.example.eventplanner.fragments.AboutFragment;
import com.example.eventplanner.fragments.AdminEventCategoriesFragment;
import com.example.eventplanner.fragments.AdminProductsCategoriesFragment;
import com.example.eventplanner.fragments.AdminUsersFragment;
import com.example.eventplanner.fragments.AllUserReportsFragment;
import com.example.eventplanner.fragments.BudgetPlanningFragment;
import com.example.eventplanner.fragments.ChatsReviewFragment;
import com.example.eventplanner.fragments.CompanyReportNotificationsFragment;
import com.example.eventplanner.fragments.CreateEventAgenda;
import com.example.eventplanner.fragments.CompanyProfileFragment;
import com.example.eventplanner.fragments.CreateEventFragment;
import com.example.eventplanner.fragments.CreateGuestList;
import com.example.eventplanner.fragments.DetailsFragment;
import com.example.eventplanner.fragments.EditProduct;
import com.example.eventplanner.fragments.EmployeeManageFragment;
import com.example.eventplanner.fragments.EditServiceFragment;
import com.example.eventplanner.fragments.EmployeeProfile;
import com.example.eventplanner.fragments.FavouritesFragment;
import com.example.eventplanner.fragments.HomeFragment;
import com.example.eventplanner.fragments.ManageEmployeesFragment;
import com.example.eventplanner.fragments.NotificationsFragment;
import com.example.eventplanner.fragments.ODProfile;
import com.example.eventplanner.fragments.PupvProfile;
import com.example.eventplanner.fragments.PackagesFragment;
import com.example.eventplanner.fragments.PracelistFragment;
import com.example.eventplanner.fragments.SearchFragment;
import com.example.eventplanner.fragments.CreateProduct;
import com.example.eventplanner.fragments.CreateService;
import com.example.eventplanner.fragments.CretePackage;
import com.example.eventplanner.fragments.HomeFragment;
import com.example.eventplanner.fragments.ProductFragment;
import com.example.eventplanner.fragments.ServiceFragment;
import com.example.eventplanner.fragments.ShowAllCompaniesFragment;
import com.example.eventplanner.fragments.ShowAllReportsFragment;
import com.example.eventplanner.fragments.ShowPackageReservationsFragment;
import com.example.eventplanner.fragments.ShowServiceReservationsFragment;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationHelper;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.util.Log;

import android.provider.MediaStore;

import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import android.provider.MediaStore;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, EditProduct.OnProductUpdateListener, EditServiceFragment.OnServiceUpdateListener {
    private static final int GALLERY_REQUEST_CODE = 1001;
    public static final String REPORT_CHANNEL_ID = "report_company";
    private DrawerLayout drawerLayout;
    private FirebaseAuth auth;
    public FirebaseUser user;
    private Button logoutButton;

    private User user_class;
    private ImageButton notificationButton;

    private List<User> usersList = new ArrayList<>();
    private String userRole; //admin, pupv, employee, organizer,

    private TextView textViewCategory;
    private TextView textViewSubcategory;
    private TextView textViewName;
    private TextView textViewDescription;
    private TextView textViewPrice;
    private TextView textViewEventType;
    private User userr;
    private MenuItem manageEmployeesItem, employeeItem, adminProductItem, adminUserItem, adminEventItem, createAgenda, guestListManagement, companies, reports, favourites, budgetPlanning,pricelistManagment,allProducts, allServices, alLPackages,createProduct, createService, createPackage;;
    private NotificationRepository notificationRepository;

    private Menu menu;
    private CompanyReportNotificationsFragment notificationFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();
        fetchAllUsers();
        user = auth.getCurrentUser();

        notificationRepository = new NotificationRepository();

        // NotificationHelper.createNotificationChannel(this);
        if (user == null) {
            Toast.makeText(getApplicationContext(), "User Not Authentificated",
                    Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(), AuthActivity.class));
            this.finish();
        } else {
            Log.d("User Authentificated", user.getEmail());
            //getUserById(user.getUid());
        }

        loadUserData();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar); //Ignore red line errors
        setSupportActionBar(toolbar);
        drawerLayout = findViewById(R.id.main);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open_nav,
                R.string.close_nav);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_home);
        }


        menu = navigationView.getMenu();
        manageEmployeesItem = menu.findItem(R.id.manage_employees);
        employeeItem = menu.findItem(R.id.nav_manage_employee);
        adminUserItem = menu.findItem(R.id.admin_users);
        adminProductItem = menu.findItem(R.id.admin_product);
        adminEventItem = menu.findItem(R.id.admin_event);
        createAgenda = menu.findItem(R.id.nav_create_agenda);
        companies = menu.findItem(R.id.nav_company);
        reports = menu.findItem(R.id.nav_reports);
        guestListManagement = menu.findItem(R.id.nav_manage_guest);
        pricelistManagment = menu.findItem(R.id.manage_pracelist);
        allProducts = menu.findItem(R.id.nav_products);
        allServices = menu.findItem(R.id.nav_services);
        alLPackages = menu.findItem(R.id.nav_packages);
        createProduct = menu.findItem(R.id.nav_create_product);
        createService = menu.findItem(R.id.nav_create_service);
        createPackage = menu.findItem(R.id.nav_create_package);


         guestListManagement = menu.findItem(R.id.nav_manage_guest);
         favourites = menu.findItem(R.id.nav_favourites);
         budgetPlanning = menu.findItem(R.id.nav_budget_planning);


        logoutButton = findViewById(R.id.logout_button);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Sign out the user from Firebase
                logoutUser();
            }
        });

        notificationButton = findViewById(R.id.notifications_button);

        // Call method to check for unread notifications and update the button
        checkAndChangeNotificationButton();

        // Set onClickListener for the notification button
        notificationButton.setOnClickListener(v -> {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new NotificationsFragment()).commit();
        });
        // Create notification channel
        createNotificationChannel();

        // Listen for unread notifications
        listenForCreatedNotifications();
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Event Planner Notifications";
            String description = "Channel for Event Planner notifications";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("admins_notification", name, importance);
            channel.setDescription(description);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    // Metoda za slušanje novih kreiranih obaveštenja
    private void listenForCreatedNotifications() {
        notificationRepository.listenForCreatedNotifications(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    Log.d(TAG, "listenForUnreadNotifications: Unread notifications found");

                    // Change the image button when a new notification is found
                    notificationButton.setImageResource(R.drawable.notifications_icon);

                    for (DataSnapshot document : dataSnapshot.getChildren()) {
                        // Retrieve notification content from the document snapshot
                        String title = document.child("title").getValue(String.class);
                        String message = document.child("text").getValue(String.class);

                        // Show the notification
                        showNotification(title, message);

                        // Update the notification as read
                        String notificationId = document.getKey();
                        notificationRepository.setNotificationAsShown(notificationId);
                    }
                } else {
                    Log.d(TAG, "listenForUnreadNotifications: No unread notifications");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "listenForUnreadNotifications: Error listening for notifications", databaseError.toException());
            }
        });
    }

    // Metoda za proveru i promenu dugmeta za obaveštenja
    private void checkAndChangeNotificationButton() {
        notificationRepository.getLoggedUserUnreadNotifications()
                .addOnSuccessListener(notifications -> {
                    if (!notifications.isEmpty()) {
                        notificationButton.setImageResource(R.drawable.notifications_icon);
                    }
                })
                .addOnFailureListener(e -> {
                    // Handle failure to retrieve notifications
                    Log.e(TAG, "checkAndChangeNotificationButton: Error retrieving unread notifications", e);
                });
    }

    // Metoda za prikazivanje obaveštenja
    private void showNotification(String title, String message) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_IMMUTABLE);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "admins_notification")
                .setSmallIcon(R.drawable.notifications_icon)
                .setContentTitle(title)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, builder.build());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        notificationRepository.removeListener();
    }

    private void setupMenuItems() {
        if(userRole == null) return;
        if(!userRole.equals("organizer")){
            createAgenda.setVisible(false);
            guestListManagement.setVisible(false);
            favourites.setVisible(false);
            budgetPlanning.setVisible(false);
        }
        if(!userRole.equals("employee")){
            employeeItem.setVisible(false);
        }
        if(!userRole.equals("pupv")){
            manageEmployeesItem.setVisible(false);
            createPackage.setVisible(false);
            createService.setVisible(false);
            createProduct.setVisible(false);
        }
        if(!userRole.equals("admin")){
            reports.setVisible(false);
        }
        if(!userRole.equals("admin")){
            adminEventItem.setVisible(false);
        }
        if(!userRole.equals("admin")){
            adminProductItem.setVisible(false);
        }
        if(!userRole.equals("admin")){
            adminUserItem.setVisible(false);
        }
        if(!(userRole.equals("employee")|| userRole.equals("pupv"))){
            allProducts.setVisible(false);
            allServices.setVisible(false);
            alLPackages.setVisible(false);
        }
    }

    public void logoutUser() {
        auth.signOut();

                // Send the user to the login activity
                Intent intent = new Intent(MainActivity.this, AuthActivity.class);
                // Clear the activity stack so that the user cannot navigate back to the main activity
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish(); // Finish the current activity
    }


    private void loadUserData() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String userId = currentUser.getUid();
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    user_class = dataSnapshot.getValue(User.class);
                    if(user_class == null)return;
                    if(user_class.getRole() == null) return;
                    Toast.makeText(getApplicationContext(), "User role " + user_class.getRole(),
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Handle database error
                }
            });
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.nav_home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        }
        else if (itemId == R.id.nav_favourites && userRole.equals("organizer")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new FavouritesFragment()).commit();
        }
        else if (itemId == R.id.nav_my_profile && userRole.equals("organizer")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ODProfile()).commit();
        }
        else if (itemId == R.id.nav_my_chats) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ChatsReviewFragment()).commit();
        }
        else if (itemId == R.id.nav_my_profile && userRole.equals("employee")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new EmployeeProfile()).commit();
        }
        else if (itemId == R.id.nav_my_profile && userRole.equals("pupv")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new PupvProfile()).commit();
        }
        else if (itemId == R.id.nav_create_agenda && userRole.equals("organizer")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CreateEventAgenda()).commit();
        }
        else if (itemId == R.id.nav_manage_guest && userRole.equals("organizer")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CreateGuestList()).commit();
        }
        else if (itemId == R.id.nav_info) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AboutFragment()).commit();
        }else if (itemId == R.id.manage_employees && userRole.equals("pupv") ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ManageEmployeesFragment()).commit();
        } else if (itemId == R.id.admin_event && userRole.equals("admin")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AdminEventCategoriesFragment()).commit();
        } else if (itemId == R.id.admin_product && userRole.equals("admin")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AdminProductsCategoriesFragment()).commit();
        } else if (itemId == R.id.admin_users && userRole.equals("admin")) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AdminUsersFragment()).commit();
        }
        else if (itemId == R.id.nav_search) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SearchFragment()).commit();
        }
        else if (itemId == R.id.nav_create_event) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CreateEventFragment()).commit();
        }
        else if (itemId == R.id.nav_budget_planning) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new BudgetPlanningFragment()).commit();
        }
        else if (itemId == R.id.nav_services) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ServiceFragment()).commit();
        } else if (itemId == R.id.nav_products) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProductFragment()).commit();
        } else if (itemId == R.id.nav_create_product) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CreateProduct()).commit();
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE);
        }
          else if (itemId == R.id.nav_create_service) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CreateService()).commit();
        }else if (itemId == R.id.nav_create_package) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CretePackage()).commit();
        }else if (itemId == R.id.nav_packages) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new PackagesFragment()).commit();
        }
        else if (itemId == R.id.nav_manage_employee && userRole.equals("employee") ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new EmployeeManageFragment()).commit();
        }
        else if (itemId == R.id.manage_pracelist  && (userRole.equals("employee")  || userRole.equals("pupv") )) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new PracelistFragment()).commit();
        }
        else if (itemId == R.id.nav_company ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ShowAllCompaniesFragment()).commit();
        }
        else if (itemId == R.id.nav_reports && userRole.equals("admin") ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ShowAllReportsFragment()).commit();
        }

        else if(itemId==R.id.nav_notifications) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new CompanyReportNotificationsFragment()).commit();

        }
        else if (itemId == R.id.nav_reservations ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ShowServiceReservationsFragment()).commit();
        }
        else if (itemId == R.id.nav_packageReservations ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ShowPackageReservationsFragment()).commit();
        }

        else if (itemId == R.id.nav_userReports ) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AllUserReportsFragment()).commit();
        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onProductUpdated(Product updatedProduct) {

    }

    public void getUserById(String userId){
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference usersRef = database.getReference("Users");

        usersRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // Korisnik s proslijeđenim ID-om je pronađen
                    userr = dataSnapshot.getValue(User.class);

                    // Provjera uloge korisnika i postavljanje vidljivosti stavke izbornika
                    if (userr != null && userr.getRole().equals("pupv")) {
                        // Ako je korisnik "pupv", postavi stavku vidljivom
                        manageEmployeesItem.setVisible(true);
                    } else {
                        // Inače, postavi stavku nevidljivom
                        manageEmployeesItem.setVisible(false);

                    }

                    if (userr != null && userr.getRole().equals("admin")) {
                        // Ako je korisnik "pupv", postavi stavku vidljivom
                        adminUserItem.setVisible(true);
                    } else {
                        // Inače, postavi stavku nevidljivom
                        adminUserItem.setVisible(false);

                    }
                    if (userr != null && (userr.getRole().equals("pupv") || userr.getRole().equals("employee")) ) {
                        // Ako je korisnik "pupv", postavi stavku vidljivom
                        pricelistManagment.setVisible(true);
                    } else {
                        // Inače, postavi stavku nevidljivom
                        adminUserItem.setVisible(false);

                    }

                    if (userr != null && userr.getRole().equals("admin")) {
                        // Ako je korisnik "pupv", postavi stavku vidljivom
                        adminProductItem.setVisible(true);
                    } else {
                        // Inače, postavi stavku nevidljivom
                        adminProductItem.setVisible(false);

                    }

                    if (userr != null && userr.getRole().equals("admin")) {
                        // Ako je korisnik "pupv", postavi stavku vidljivom
                        adminEventItem.setVisible(true);
                    } else {
                        // Inače, postavi stavku nevidljivom
                        adminEventItem.setVisible(false);

                    }



                    if (userr != null && userr.getRole().equals("employee")) {

                        Log.w("TAG", "ROLE" + userr.getRole());
                        employeeItem.setVisible(true);
                    } else {

                        Log.w("TAG", "ROLE" + userr.getRole());
                        employeeItem.setVisible(false);
                    }

                } else {
                    // Korisnik nije pronađen
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Greška prilikom dohvatanja podataka
                Log.w("TAG", "Greška prilikom dohvatanja korisnika.", databaseError.toException());
            }
        });




    }

    @Override
    public void OnServiceUpdated(Service updatedService, Context context) {
        saveUpdatedService(updatedService, context);

    }

    private void saveUpdatedService(Service updatedService, Context context) {
        // Implementirajte logiku za čuvanje ažurirane usluge koristeći kontekst koji je proslijeđen
        // na primjer:
        Toast.makeText(context, "Service updated successfully", Toast.LENGTH_SHORT).show();
    }

    private void fetchAllUsers() {
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference("Users");

        usersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersList.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    if (user != null) {
                        usersList.add(user);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findRole();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private void findRole() {
        for(User u: usersList) {
            if(u.getUid().equals(user.getUid())) {
                userRole = u.getRole();
                break;
            }
        }

        setupMenuItems();
    }

}