package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployeeAdapter;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


public class GradeCompanyFragment extends Fragment {


    private EditText editTextCompany;
    private EditText editTextComment;
    private RatingBar ratingBar;
    private Button submitButton;

    private String loggedInId;

    public String companyName;
    public String pupV;

    private NotificationRepository notificationRepository = new NotificationRepository();
    public GradeCompanyFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_grade_company, container, false);

        editTextCompany = view.findViewById(R.id.editTextCompany);
        editTextComment = view.findViewById(R.id.editTextComment);
        ratingBar = view.findViewById(R.id.ratingBar);
        submitButton = view.findViewById(R.id.submitButton);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
             loggedInId = currentUser.getUid();
        }

        Bundle bundle = getArguments();
        if (bundle != null) {
            companyName = bundle.getString("companyName");
            editTextCompany.setText(companyName);

        }

        getCompanyByName();
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String companyName = editTextCompany.getText().toString();
                String comment = editTextComment.getText().toString();
                float rating = ratingBar.getRating();
                Date date= new Date();
                CompanyGrade newGrade= new CompanyGrade(companyName,loggedInId,comment,rating,date.toString(),pupV);
                saveCompanyGrade(newGrade);
                saveMessage("NewCompanyGrade",newGrade.getPupvId(), newGrade.getOdId());
            }
        });

        return view;
    }

    private void getCompanyByName(){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference companiesRef = databaseReference.child("Companies");

        // Dohvaćanje podataka iz čvora "employees"
        companiesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    Company company = userSnapshot.getValue(Company.class);

                   if(company.getName().equals(companyName)){
                       pupV= company.getPupvId();
                   }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void saveCompanyGrade(CompanyGrade grade){
        String id = UUID.randomUUID().toString();
        grade.setId(id);
        // Save the company grade to Firebase
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("companyGrades");
        databaseReference.child(id).setValue(grade);
    }

    private void saveMessage(String content,String recipientId,String senderId) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference().child("messages");
        String messageId = messageRef.push().getKey();

        String date = LocalDateTime.now().toString();
        Message newMessage = new Message(messageId, senderId, recipientId, date, content, "unread");

        messageRef.child(messageId).setValue(newMessage);



        saveNotification(newMessage,senderId);
    }

    private void saveNotification(Message newMessage,String sender) {
        Notification notification = new Notification();
        notification.setStatus(Notification.Status.CREATED);
        notification.setMessage(newMessage.getContent());
        notification.setText("New message from " + sender+  ": " + newMessage.getContent());
        notification.setUserId(newMessage.getRecipientId());
        notificationRepository.createNotification(notification);
    }
}