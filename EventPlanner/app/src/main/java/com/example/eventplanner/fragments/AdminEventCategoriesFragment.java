package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplanner.model.EventCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.example.eventplanner.R;

import android.widget.Toast;

public class AdminEventCategoriesFragment extends Fragment {


    private LinearLayout layoutCategories;
    private EditText editTextCategoryName;
    private EditText editTextCategoryDescription;
    private Button buttonAddCategory;
    private DatabaseReference categoriesRef;

    public AdminEventCategoriesFragment() {
        // Required empty public constructor
    }

    private boolean isEditMode = false;
    private String categoryIdToUpdate;

    private View.OnClickListener addCategoryClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isEditMode) {
                // If in edit mode, update the category
                String updatedCategoryName = editTextCategoryName.getText().toString().trim();
                String updatedCategoryDescription = editTextCategoryDescription.getText().toString().trim();
                if (!updatedCategoryName.isEmpty()) {
                    updateCategory(categoryIdToUpdate, updatedCategoryName, updatedCategoryDescription);
                    editTextCategoryName.setText("");
                    editTextCategoryDescription.setText("");
                    buttonAddCategory.setText("Add Category");
                    isEditMode = false;
                }
            } else {
                // If not in edit mode, add a new category
                String categoryName = editTextCategoryName.getText().toString().trim();
                String categoryDescription = editTextCategoryDescription.getText().toString().trim();
                if (!categoryName.isEmpty()) {
                    String key = saveCategoryToFirebase(categoryName, categoryDescription);
                    addCategory(key, categoryName, categoryDescription);
                    editTextCategoryName.setText("");
                    editTextCategoryDescription.setText("");
                }
            }
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_event_categories, container, false);

        layoutCategories = view.findViewById(R.id.layoutCategories);
        editTextCategoryName = view.findViewById(R.id.editTextEventName);
        editTextCategoryDescription = view.findViewById(R.id.editTextCompanyDescription);
        buttonAddCategory = view.findViewById(R.id.buttonAddCategory);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        categoriesRef = database.getReference("eventCategories");

        buttonAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String categoryName = editTextCategoryName.getText().toString().trim();
                String categoryDescription = editTextCategoryDescription.getText().toString().trim();
                if (!categoryName.isEmpty()) {

                    String key = saveCategoryToFirebase(categoryName, categoryDescription);
                    addCategory(key,categoryName, categoryDescription);
                    editTextCategoryName.setText("");
                    editTextCategoryDescription.setText("");
                }
            }
        });

        // Load existing categories from Firebase
        loadCategoriesFromFirebase();

        return view;
    }

    private void addCategory(String categoryId, String categoryName, String categoryDescription) {
        // Inflate category item layout



        View categoryView = getLayoutInflater().inflate(R.layout.item_events, null);

        categoryView.setTag(categoryId);

        TextView textViewCategoryName = categoryView.findViewById(R.id.textViewCategoryName);
        TextView textViewCategoryInfo = categoryView.findViewById(R.id.textViewAdditionalInfo);
        Button buttonDelete = categoryView.findViewById(R.id.buttonDelete);
        Button buttonEdit = categoryView.findViewById(R.id.buttonEdit);
        Button buttonInfo = categoryView.findViewById(R.id.buttonInfo);

        // Set category name and description
        textViewCategoryName.setText(categoryName);
        textViewCategoryInfo.setText(categoryDescription);

        buttonInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle edit action
                // Navigate to the AdminEventSubcategoriesFragment

                String categoryId = (String) categoryView.getTag();
                FragmentTransaction transaction = getParentFragmentManager().beginTransaction();
                AdminEventSubcategoriesFragment subcategoriesFragment = new AdminEventSubcategoriesFragment();
                Bundle bundle = new Bundle();
                bundle.putString("categoryName", categoryName); // Pass the category name
                bundle.putString("categoryId", categoryId);
                subcategoriesFragment.setArguments(bundle);
                transaction.replace(R.id.fragment_container, subcategoriesFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        // Set click listeners for delete and edit buttons
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle delete action
                String categoryId = (String) categoryView.getTag();
                deleteCategory(categoryId);
                layoutCategories.removeView(categoryView);
            }
        });

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle edit action
                String categoryId = (String) categoryView.getTag();
                editTextCategoryName.setText(categoryName);
                editTextCategoryDescription.setText(categoryDescription);
                buttonAddCategory.setText("Edit Category");

                buttonAddCategory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String updatedCategoryName = editTextCategoryName.getText().toString().trim();
                        String updatedCategoryDescription = editTextCategoryDescription.getText().toString().trim();
                        if (!updatedCategoryName.isEmpty()) {
                            updateCategory(categoryId, updatedCategoryName, updatedCategoryDescription);
                            // Reset EditText fields and button text
                            editTextCategoryName.setText("");
                            editTextCategoryDescription.setText("");
                            buttonAddCategory.setText("Add Category");
                            buttonAddCategory.setOnClickListener(addCategoryClickListener);
                        }
                    }
                });
            }
        });

        // Add category view to the layout
        layoutCategories.addView(categoryView);
    }

    private void deleteCategory(String categoryId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("eventCategories").child(categoryId);

        /*databaseReference.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Category deleted successfully", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "Failed to delete category", Toast.LENGTH_SHORT).show();
                }
            }
        });*/
    }

    private void updateCategory(String categoryId, String updatedCategoryName, String updatedCategoryDescription) {
        DatabaseReference categoryRef = categoriesRef.child(categoryId);
        categoryRef.child("name").setValue(updatedCategoryName);
        categoryRef.child("description").setValue(updatedCategoryDescription)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {

                            Toast.makeText(getActivity(), "Category updated successfully", Toast.LENGTH_SHORT).show();

                            loadCategoriesFromFirebase();
                        } else {
                            Toast.makeText(getActivity(), "Failed to update category", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private void updateUICategory(String categoryId, String updatedCategoryName, String updatedCategoryDescription) {
        // Find the view representing the edited category in your layout
        View categoryView = layoutCategories.findViewWithTag(categoryId);
        if (categoryView != null) {
            // Update TextViews with the new category name and description
            TextView textViewCategoryName = categoryView.findViewById(R.id.textViewCategoryName);
            TextView textViewCategoryInfo = categoryView.findViewById(R.id.textViewAdditionalInfo);
            textViewCategoryName.setText(updatedCategoryName);
            textViewCategoryInfo.setText(updatedCategoryDescription);
        }
    }

    private String saveCategoryToFirebase(String categoryName, String categoryDescription) {
        String key = categoriesRef.push().getKey();
        if (key != null) {
            EventCategory eventCategory = new EventCategory(key, categoryName, categoryDescription);
            categoriesRef.child(key).setValue(eventCategory);
            return key;
        }
        return null;
    }

    private void loadCategoriesFromFirebase() {
        categoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                layoutCategories.removeAllViews();
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    EventCategory eventCategory = categorySnapshot.getValue(EventCategory.class);
                    if (eventCategory != null) {
                        addCategory(eventCategory.getId(), eventCategory.getName(), eventCategory.getDescription());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error
            }
        });
    }
}