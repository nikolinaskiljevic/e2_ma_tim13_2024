package com.example.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.GradeCompanyFragment;
import com.example.eventplanner.fragments.OrganizerProfileFragment;
import com.example.eventplanner.fragments.ReportCompanyGradeFragment;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.PackageReservation;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.ServiceReservation;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.CountDownLatch;

public class PackageReservationAdapter extends RecyclerView.Adapter<PackageReservationAdapter.PackageReservationViewHolder>{

    private List<PackageReservation> reservations;
    private Context context;
    private User user;
    private FirebaseAuth mAuth;
    private User loggedInUser ;

    private FirebaseUser userr;
    private String odFirstName;
    private String odLastName;
    private String odId;
    private String packageName;
    private Package packagee;

    private String email ;
    private String address ;
    private String phoneNumber ;
    private String imageUrl ;

    public PackageReservationAdapter(List<PackageReservation> reservations, Context context) {
        this.reservations = reservations;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }



    @NonNull
    @Override
    public PackageReservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_package_reservation_card, parent, false);
        return new PackageReservationAdapter.PackageReservationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageReservationViewHolder holder, int position) {
        PackageReservation res = reservations.get(position);
        packagee = new Package();
        fetchOD(res.getOdId(), holder);
        findPackageServicesAndProducts(res.getPackageId(), holder);


        loggedInUser = new User();

        userr = mAuth.getCurrentUser();
        if (userr != null) {
            fetchLoggedIn(userr.getUid());
        }
        if (res.getStatus().equals("new") || res.getStatus().equals("accepted")) {
            holder.buttonCancel.setVisibility(View.VISIBLE);
        } else {
            holder.buttonCancel.setVisibility(View.GONE);
        }
        holder.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (loggedInUser.getRole().equals("organizer")) {
                    cancelPackageReservation(res.getId(), "odCanceled");
                    cancelAllServiceReservations(res, "odCanceled");
                }
                if (loggedInUser.getRole().equals("pupv")) {
                    cancelPackageReservation(res.getId(), "pupCanceled");
                    cancelAllServiceReservations(res, "pupCanceled");
                }
            }
        });


        holder.viewProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Pretpostavljamo da imate informacije o organizatoru u nekoj varijabli


                OrganizerProfileFragment fragment = OrganizerProfileFragment.newInstance(odId,email, odFirstName, odLastName, address, phoneNumber, imageUrl);
                FragmentActivity activity = (FragmentActivity) context;
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });





    }



    @Override
    public int getItemCount() {
        return reservations.size();
    }

    public static class PackageReservationViewHolder extends RecyclerView.ViewHolder {

        TextView textViewOdFirstName,textViewOdLastName,textViewPackageName;
        RecyclerView recyclerViewServices,recyclerViewProducts;
        Button buttonCancel;
        Button viewProfileButton;


        public PackageReservationViewHolder(@NonNull View itemView) {
            super(itemView);


            textViewOdFirstName=itemView.findViewById(R.id.textViewOdFirstName);
            textViewOdLastName=itemView.findViewById(R.id.textViewOdLastName);
            textViewPackageName=itemView.findViewById(R.id.textViewPackageName);
            recyclerViewServices=itemView.findViewById(R.id.recyclerViewServices);
            recyclerViewProducts=itemView.findViewById(R.id.recyclerViewProducts);
            buttonCancel=itemView.findViewById(R.id.buttonCancel);
            viewProfileButton = itemView.findViewById(R.id.buttonViewProfile);

        }
    }


    private void findPackageServicesAndProducts(String packageId,PackageReservationViewHolder holder) {
        DatabaseReference serviceRef = FirebaseDatabase.getInstance().getReference("packages");

        serviceRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    Package pac = resSnapshot.getValue(Package.class);
                    if (pac != null && pac.getId().equals(packageId)) {
                        packagee= pac;
                        holder.textViewPackageName.setText(packagee.getName());
                        holder.recyclerViewServices.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        ShowServiceAdapter servicesAdapter = new ShowServiceAdapter(packagee.getSelectedServices(), context);
                        holder.recyclerViewServices.setAdapter(servicesAdapter);

                        holder.recyclerViewProducts.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
                        ShowProductAdapter productAdapter = new ShowProductAdapter(packagee.getSelectedProducts(), context);
                        holder.recyclerViewProducts.setAdapter(productAdapter);

                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("findServiceName", "Failed to read service", databaseError.toException());
            }
        });
    }




    private void fetchOD(String id,PackageReservationViewHolder holder){
        DatabaseReference odRef = FirebaseDatabase.getInstance().getReference("Users").child(id);
        odRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    Log.e("TAG","user"+ user);
                    Log.e("TAG","username"+ user.getFirstName());
                    odFirstName=user.getFirstName();
                    odLastName=user.getLastName();
                    odId = user.getUid();
                    email = user.getEmail();
                    phoneNumber = user.getPhoneNumber();
                    address = user.getAddress();
                    imageUrl = user.getImageURL();
                    holder.textViewOdFirstName.setText(odFirstName);
                    holder.textViewOdLastName.setText(odLastName);


                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }



    private void fetchLoggedIn(String userId) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    loggedInUser=user;

                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }


    public void cancelPackageReservation(String resId,String status){
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference().child("PackageReservations").child(resId);

        packageRef.child("status").setValue(status);


    }
    public void cancelServiceReservation(String resId,String status){
        DatabaseReference reservationsRef = FirebaseDatabase.getInstance().getReference().child("ServiceReservations").child(resId);

        reservationsRef.child("status").setValue(status);


    }

    public void cancelAllServiceReservations(PackageReservation pacRes,String status){
        for(String serviceReservationsId: pacRes.getServiceReservations()){
            cancelServiceReservation(serviceReservationsId,status);
        }
    }


}
