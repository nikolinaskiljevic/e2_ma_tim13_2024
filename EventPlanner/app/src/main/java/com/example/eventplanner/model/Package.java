package com.example.eventplanner.model;

import java.util.List;

public class Package {
    private String id;
    private String name;
    private String description;
    private double price;
    private int discount;
    private List<String> images;
    private boolean isVisible;
    private boolean isAvailable;
    private String category;
    private List<Product> selectedProducts;
    private List<Service> selectedServices;
    private List<String> subcategories;
    private List<String> eventTypes;
    private String reservationDeadline;
    private String cancellationDeadline;
    private String confirmationMethod;
    private boolean deleted;
    private String companyId;

    public Package(String name, String description, double price, int discount, List<String> images, boolean isVisible, boolean isAvailable, String category, List<Product> selectedProducts, List<Service> selectedServices, List<String> subcategories, List<String> eventTypes, String reservationDeadline, String cancellationDeadline, String confirmationMethod) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.images = images;
        this.isVisible = isVisible;
        this.isAvailable = isAvailable;
        this.category = category;
        this.selectedProducts = selectedProducts;
        this.selectedServices = selectedServices;
        this.subcategories = subcategories;
        this.eventTypes = eventTypes;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.confirmationMethod = confirmationMethod;
        this.deleted = false;
    }
    public void updatePrice() {
        double totalPrice = 0;
        if(this.selectedProducts != null) {
            for (Product product : this.selectedProducts) {
                totalPrice += product.getPrice() * (100 - product.getDiscount()) / 100;
            }
        }
        if(this.selectedServices != null){
            for (Service service : selectedServices) {
                totalPrice += service.getPrice() * (100 - service.getDiscount()) / 100;
            }
        }
        this.price = totalPrice;
    }


    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Package() {
this.deleted = false;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {

        double totalPrice = 0;
        if(this.selectedProducts != null) {
            for (Product product : this.selectedProducts) {
                totalPrice += product.getPrice() * (100 - product.getDiscount()) / 100;
            }
        }
        if(this.selectedServices != null){
            for (Service service : selectedServices) {
                totalPrice += service.getPrice() * (100 - service.getDiscount()) / 100;
            }
        }

        return Double.parseDouble(String.valueOf(totalPrice));
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Product> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(List<Product> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    public List<Service> getSelectedServices() {
        return selectedServices;
    }

    public void setSelectedServices(List<Service> selectedServices) {
        this.selectedServices = selectedServices;
    }

    public List<String> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<String> subcategories) {
        this.subcategories = subcategories;
    }

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<String> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public String getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(String reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public String getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public String getConfirmationMethod() {
        return confirmationMethod;
    }

    public void setConfirmationMethod(String confirmationMethod) {
        this.confirmationMethod = confirmationMethod;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
