package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.MessageAdapter;
import com.example.eventplanner.adapters.PackageDetailsAdapter;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventAgenda;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "recipientId";

    // TODO: Rename and change types of parameters
    private String recipientId;
    private MessageAdapter messageAdapter;
    private NotificationRepository notificationRepository = new NotificationRepository();
    private TextView chatTitle;
    private EditText newMessage;
    private ImageButton sendMessageButton;
    private RecyclerView oldMessages;
    private List<Message> allMessages = new ArrayList<>();
    private ArrayList<Message> chatMessages = new ArrayList<>();
    private User recipient;
    private User loggedUser;


    public ChatFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param recipientId Parameter 1.
     * @return A new instance of fragment ChatFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatFragment newInstance(String recipientId) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, recipientId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            recipientId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chat, container, false);

        chatTitle = view.findViewById(R.id.textViewChatTitle);
        oldMessages = view.findViewById(R.id.recyclerViewMessages);
        newMessage = view.findViewById(R.id.editTextNewMessage);
        sendMessageButton = view.findViewById(R.id.buttonSendMessage);

        getAllUsers();

        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = newMessage.getText().toString();

                saveMessage(content);
                newMessage.setText("");
                getAllMessages();
                //getAllUsers();
            }
        });


        return view;
    }

    private void getAllUsers() {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<User> allUsers = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    if (u != null) {
                        allUsers.add(u);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findRecipient(allUsers);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllMessages", "Failed to read messages", databaseError.toException());
            }
        });
    }

    private void findRecipient(List<User> allUsers) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        for(User u: allUsers) {
            if(u.getUid().equals(recipientId)) {
                recipient = u;
                chatTitle.setText(recipient.getRole() + ": " + recipient.getFirstName() + " " + recipient.getLastName());
            }
            if(u.getUid().equals(currentUser.getUid())) {
                loggedUser = u;
            }
        }
        getAllMessages();
    }

    private void getAllMessages() {
        DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference("messages");

        messageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allMessages.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Message m = snapshot.getValue(Message.class);
                    if (m != null) {
                        allMessages.add(m);
                    }
                }
                // Sada usersList sadrži sve korisnike
                getChatMessages();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllMessages", "Failed to read messages", databaseError.toException());
            }
        });
    }

    private void getChatMessages() {
        chatMessages.clear();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        for(Message m: allMessages) {
            if(m.getSenderId().equals(currentUser.getUid()) && m.getRecipientId().equals(recipientId) ||
               m.getSenderId().equals(recipientId) && m.getRecipientId().equals(currentUser.getUid())) {
                chatMessages.add(m);
            }
        }

        sortMessages();
    }

    private void sortMessages() {
        DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

        Collections.sort(chatMessages, new Comparator<Message>() {
            @Override
            public int compare(Message m1, Message m2) {
                LocalDateTime dateTime1 = LocalDateTime.parse(m1.getDate(), formatter);
                LocalDateTime dateTime2 = LocalDateTime.parse(m2.getDate(), formatter);
                return dateTime1.compareTo(dateTime2);
            }
        });

        setMessageAdapter();
    }

    private void setMessageAdapter() {
        //proslijedi listu chatMessages adapteru
        //ovdje setuj adapter za oldMessages
        oldMessages.setHasFixedSize(true);
        oldMessages.setLayoutManager(new LinearLayoutManager(getContext()));

        messageAdapter = new MessageAdapter(getContext(), chatMessages);
        oldMessages.setAdapter(messageAdapter);
    }

    private void saveMessage(String content) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference().child("messages");
        String messageId = messageRef.push().getKey();

        String date = LocalDateTime.now().toString();
        Message newMessage = new Message(messageId, currentUser.getUid(), recipientId, date, content, "unread");

        messageRef.child(messageId).setValue(newMessage);

        Toast.makeText(getContext(), "Message sent successfully!", Toast.LENGTH_SHORT).show();

        saveNotification(newMessage);
    }

    private void saveNotification(Message newMessage) {
        Notification notification = new Notification();
        notification.setStatus(Notification.Status.CREATED);
        notification.setMessage(newMessage.getContent());
        notification.setText("New message from " + loggedUser.getFirstName() + " " + loggedUser.getLastName() + ": " + newMessage.getContent());
        notification.setUserId(newMessage.getRecipientId());
        notificationRepository.createNotification(notification);
    }
}