package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.R;

import java.util.List;

public class SearchResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Product> productList;
    private List<Service> serviceList;
    private List<Package> packageList;
    private boolean isShowingProducts;
    private boolean isShowingPackages;
    private boolean isShowingServices;

    public SearchResultAdapter(List<Product> productList, List<Service> serviceList, List<Package> packageList, boolean isShowingProducts, boolean isShowingPackages, boolean isShowingServices) {
        this.productList = productList;
        this.serviceList = serviceList;
        this.packageList = packageList;
        this.isShowingProducts = isShowingProducts;
        this.isShowingPackages = isShowingPackages;
        this.isShowingServices = isShowingServices;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        if (isShowingProducts) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
            return new ProductViewHolder(view);
        } else if (isShowingPackages) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_package, parent, false);
            return new PackageViewHolder(view);
        } else if (isShowingServices) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false);
            return new ServiceViewHolder(view);
        }
        throw new IllegalArgumentException("No valid view type found");
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (isShowingProducts) {
            ProductViewHolder productViewHolder = (ProductViewHolder) holder;
            productViewHolder.bind(productList.get(position));
        } else if (isShowingPackages) {
            PackageViewHolder packageViewHolder = (PackageViewHolder) holder;
            packageViewHolder.bind(packageList.get(position));
        } else if (isShowingServices) {
            ServiceViewHolder serviceViewHolder = (ServiceViewHolder) holder;
            serviceViewHolder.bind(serviceList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (isShowingProducts) {
            return productList.size();
        } else if (isShowingPackages) {
            return packageList.size();
        } else if (isShowingServices) {
            return serviceList.size();
        }
        return 0;
    }

    // ViewHolder za prikaz proizvoda
    private static class ProductViewHolder extends RecyclerView.ViewHolder {
        private TextView productNameTextView;
        private TextView categoryTextView;
        private TextView subcategoryTextView;
        private TextView descriptionTextView;
        private TextView priceTextView;
        private TextView discountTextView;
        private TextView eventTypesTextView;
        private TextView availableTextView;

        ProductViewHolder(@NonNull View itemView) {
            super(itemView);
            productNameTextView = itemView.findViewById(R.id.tvProductName);
            categoryTextView = itemView.findViewById(R.id.textViewCategory);
            subcategoryTextView = itemView.findViewById(R.id.textViewSubcategory);
            descriptionTextView = itemView.findViewById(R.id.textViewDescription);
            priceTextView = itemView.findViewById(R.id.textViewPrice);
            discountTextView = itemView.findViewById(R.id.textViewDiscount);
            eventTypesTextView = itemView.findViewById(R.id.textViewEventTypes);
            availableTextView = itemView.findViewById(R.id.textViewAvailable);
        }

        void bind(Product product) {
            productNameTextView.setText(product.getName());
            categoryTextView.setText(product.getCategory());
            subcategoryTextView.setText(product.getSubcategory());
            descriptionTextView.setText(product.getDescription());
            priceTextView.setText(String.valueOf(product.getPrice()));
            discountTextView.setText(String.valueOf(product.getDiscount()));
            eventTypesTextView.setText(product.getEventTypes());
            availableTextView.setText(String.valueOf(product.isAvailable()));
        }
    }

    private static class PackageViewHolder extends RecyclerView.ViewHolder {
        private TextView packageNameTextView;
        private TextView descriptionTextView;
        private TextView priceTextView;
        private TextView isVisibleTextView;
        private TextView isAvailableTextView;
        private TextView productsTextView;
        private TextView servicesTextView;
        private TextView categoryTextView;
        private TextView subcategoriesTextView;
        private TextView eventTypesTextView;
        private TextView reservationDeadlineTextView;
        private TextView cancellationDeadlineTextView;
        private TextView confirmationMethodTextView;

        PackageViewHolder(@NonNull View itemView) {
            super(itemView);
            packageNameTextView = itemView.findViewById(R.id.tViewPackageName);
            descriptionTextView = itemView.findViewById(R.id.textViewDescription);
            priceTextView = itemView.findViewById(R.id.textViewPrice);
            isVisibleTextView = itemView.findViewById(R.id.tvIsVisible);
            isAvailableTextView = itemView.findViewById(R.id.tvIsAvailable);
            productsTextView = itemView.findViewById(R.id.tvProducts);
            servicesTextView = itemView.findViewById(R.id.textViewServices);
            categoryTextView = itemView.findViewById(R.id.textViewCategory);
            subcategoriesTextView = itemView.findViewById(R.id.tvSubcategories);
            eventTypesTextView = itemView.findViewById(R.id.textViewEventTypes);
            reservationDeadlineTextView = itemView.findViewById(R.id.tvReservationDeadline);
            cancellationDeadlineTextView = itemView.findViewById(R.id.textViewCancellationDeadline);
            confirmationMethodTextView = itemView.findViewById(R.id.tvConfirmationMethod);
        }

        void bind(Package packageObj) {
            packageNameTextView.setText(packageObj.getName());
            descriptionTextView.setText(packageObj.getDescription());
            priceTextView.setText(String.valueOf(packageObj.getPrice()));
            isVisibleTextView.setText(String.valueOf(packageObj.isVisible()));
            isAvailableTextView.setText(String.valueOf(packageObj.isAvailable()));

            StringBuilder products = new StringBuilder();
            for (Product p : packageObj.getSelectedProducts()) {
                products.append(p.getName()).append(", ");
            }
            if (products.length() > 0) {
                products.deleteCharAt(products.length() - 2); // Remove trailing comma and space
            }
            productsTextView.setText(products.toString());

            StringBuilder services = new StringBuilder();
            for (Service s : packageObj.getSelectedServices()) {
                services.append(s.getServiceName()).append(", ");
            }
            if (services.length() > 0) {
                services.deleteCharAt(services.length() - 2); // Remove trailing comma and space
            }
            servicesTextView.setText(services.toString());

            categoryTextView.setText(packageObj.getCategory());

            StringBuilder subcategories = new StringBuilder();
            for (String subcategory : packageObj.getSubcategories()) {
                subcategories.append(subcategory).append(", ");
            }
            if (subcategories.length() > 0) {
                subcategories.deleteCharAt(subcategories.length() - 2); // Remove trailing comma and space
            }
            subcategoriesTextView.setText(subcategories.toString());

            StringBuilder events = new StringBuilder();
            for (String eventType : packageObj.getEventTypes()) {
                events.append(eventType).append(", ");
            }
            if (events.length() > 0) {
                events.deleteCharAt(events.length() - 2); // Remove trailing comma and space
            }
            eventTypesTextView.setText(events.toString());

            reservationDeadlineTextView.setText(packageObj.getReservationDeadline());
            cancellationDeadlineTextView.setText(packageObj.getCancellationDeadline());
            confirmationMethodTextView.setText(packageObj.getConfirmationMethod());
        }
    }


    // ViewHolder za prikaz usluge
    private static class ServiceViewHolder extends RecyclerView.ViewHolder {
        private TextView serviceNameTextView;
        private TextView categoryTextView;
        private TextView subcategoryTextView;
        private TextView descriptionTextView;
        private TextView specificsTextView;
        private TextView priceTextView;
        private TextView discountTextView;
        private TextView eventTypesTextView;
        private TextView availableTextView;
        private TextView employeesTextView;
        private TextView bookingDeadlineTextView;
        private TextView cancellationDeadlineTextView;
        private TextView reservationConfirmationTextView;

        ServiceViewHolder(@NonNull View itemView) {
            super(itemView);
            serviceNameTextView = itemView.findViewById(R.id.textViewServiceName);
            categoryTextView = itemView.findViewById(R.id.textViewCategory);
            subcategoryTextView = itemView.findViewById(R.id.textViewSubcategory);
            descriptionTextView = itemView.findViewById(R.id.textViewDescription);
            specificsTextView = itemView.findViewById(R.id.textViewSpecifics);
            priceTextView = itemView.findViewById(R.id.textViewPrice);
            discountTextView = itemView.findViewById(R.id.textViewDiscount);
            eventTypesTextView = itemView.findViewById(R.id.textViewEventTypes);
            availableTextView = itemView.findViewById(R.id.textViewAvailable);
            employeesTextView = itemView.findViewById(R.id.textViewEmployees);
            bookingDeadlineTextView = itemView.findViewById(R.id.textViewBookingDeadline);
            cancellationDeadlineTextView = itemView.findViewById(R.id.textViewCancellationDeadline);
            reservationConfirmationTextView = itemView.findViewById(R.id.textViewReservationConfirmation);
        }

        void bind(Service service) {
            serviceNameTextView.setText(service.getServiceName());
            categoryTextView.setText(service.getCategory());
            subcategoryTextView.setText(service.getSubcategory());
            descriptionTextView.setText(service.getDescription());
            specificsTextView.setText(service.getSpecifics());
            priceTextView.setText(String.valueOf(service.getPrice()));
            discountTextView.setText(String.valueOf(service.getDiscount()));
            StringBuilder events = new StringBuilder();
            for (int i = 0; i < service.getEventTypes().size(); i++) {
                events.append(service.getEventTypes().get(i));
                if (i < service.getEventTypes().size() - 1) {
                    events.append(", ");
                }
            }
            eventTypesTextView.setText(events.toString());
            availableTextView.setText(String.valueOf(service.isAvailable()));
            StringBuilder employees = new StringBuilder();
            for (int i = 0; i < service.getEmployees().size(); i++) {
                employees.append(service.getEmployees().get(i).getFirstName()).append(" ").append(service.getEmployees().get(i).getLastName());
                if (i < service.getEventTypes().size() - 1) {
                    events.append(", ");
                }
            }
            employeesTextView.setText(employees.toString());
            bookingDeadlineTextView.setText(String.valueOf(service.getBookingDeadline()));
            cancellationDeadlineTextView.setText(service.getCancellationDeadline());
            reservationConfirmationTextView.setText(String.valueOf(service.getReservationConfirmation()));
        }
    }
}
