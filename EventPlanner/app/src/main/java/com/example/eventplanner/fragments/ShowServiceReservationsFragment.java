package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.Spinner;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.CompanyGradeAdapter;
import com.example.eventplanner.adapters.ServiceReservationAdapter;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.ServiceReservation;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;


public class ShowServiceReservationsFragment extends Fragment {

    private RecyclerView recyclerView;
    private ServiceReservationAdapter adapter;
    private List<ServiceReservation> reservations;
    private FirebaseAuth mAuth;
    private User loggedInUser ;

    private FirebaseUser userr;

    private SearchView searchView;
    private Spinner spinnerFilter;

    private Button buttonSearch;
    private String employeeFirstName;
    private String employeeLastName;

    private String odFirstName;
    private String odLastName;
    private String serviceName;


    public ShowServiceReservationsFragment() {
        // Required empty public constructor

    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_show_service_reservations, container, false);

        recyclerView = view.findViewById(R.id.recyclerViewReservations);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        searchView=view.findViewById(R.id.searchView);
        spinnerFilter=view.findViewById(R.id.spinnerFilter);
        buttonSearch=view.findViewById(R.id.buttonSearch);

       // new,pupCanceled,odCanceled,adminCanceled,accepted,realized
        // Add strings to spinner
        List<String> filterOptions = new ArrayList<>();
        filterOptions.add("all");
        filterOptions.add("new");
        filterOptions.add("pupCanceled");
        filterOptions.add("odCanceled");
        filterOptions.add("adminCanceled");
        filterOptions.add("accepted");
        filterOptions.add("realized");
        // Add more options as needed

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, filterOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFilter.setAdapter(adapter);

        reservations= new ArrayList<>();
        mAuth = FirebaseAuth.getInstance();
        userr = mAuth.getCurrentUser();
        if(userr!=null){
            fetchUserAndPopulateReservations(userr.getUid());
        }


       // fetchEmployee("UMd8ieyupyZZeTFuncnPURh9hC73");

        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String selectedItem = spinnerFilter.getSelectedItem().toString();
                String query = searchView.getQuery().toString();

                if (!query.isEmpty() && !selectedItem.equals("all")) {
                    searchAndFilterReservations(query, selectedItem);
                } else if (!query.isEmpty()) {
                    searchReservations(query);
                } else if (!selectedItem.equals("all")) {
                    filterReservations(selectedItem);
                }else if(query.isEmpty() && selectedItem.equals("all")){
                    cancelSearch();
                }

            }
        });


        return view;
    }


    private void fetchUserAndPopulateReservations(String userId) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                if (user != null) {
                  String userRole=  user.getRole();

                    if(userRole.equals("pupv")){
                    getAllServiceReservations();
                    }
                    if(userRole.equals("organizer")){
                    getServiceReservationsByOD(user.getUid());
                    }
                    if(userRole.equals("employee")){
                    getServiceReservationsByEmployee(user.getUid());
                    }

                    // Možeš ovdje dalje raditi s korisnikom
                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }

    private void getAllServiceReservations() {
        DatabaseReference resRef = FirebaseDatabase.getInstance().getReference("ServiceReservations");
        List<ServiceReservation> newReservationsList = new ArrayList<>();
        Log.e("TAG","USAO"+resRef);
        resRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                List<ServiceReservation> newReservationsList = new ArrayList<>();

                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    ServiceReservation res = resSnapshot.getValue(ServiceReservation.class);
                    Log.e("TAG","resSnapshot"+res);
                    newReservationsList.add(res);

                }
                for (ServiceReservation ress : newReservationsList) {
                    if (!reservations.contains(ress)) {
                        reservations.add(ress);
                    }
                }

                Log.e("TAG","reservations"+reservations);
                adapter = new ServiceReservationAdapter(reservations, getActivity());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllReservations", "Failed to read resevations", databaseError.toException());
            }
        });
    }

    private void getServiceReservationsByOD(String odId) {
        DatabaseReference resRef = FirebaseDatabase.getInstance().getReference("ServiceReservations");
        List<ServiceReservation> newReservationsList = new ArrayList<>();
        resRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                List<ServiceReservation> newReservationsList = new ArrayList<>();

                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    ServiceReservation res = resSnapshot.getValue(ServiceReservation.class);
                    if(res.getOdId().equals(odId)){
                        newReservationsList.add(res);
                    }


                }
                for (ServiceReservation ress : newReservationsList) {
                    if (!reservations.contains(ress)) {
                        reservations.add(ress);
                    }
                }

                Log.e("TAG","reservations"+reservations);
                adapter = new ServiceReservationAdapter(reservations, getActivity());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllReservations", "Failed to read resevations", databaseError.toException());
            }
        });
    }

    private void getServiceReservationsByEmployee(String employeeId) {
        DatabaseReference resRef = FirebaseDatabase.getInstance().getReference("ServiceReservations");
        List<ServiceReservation> newReservationsList = new ArrayList<>();
        resRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                List<ServiceReservation> newReservationsList = new ArrayList<>();

                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    ServiceReservation res = resSnapshot.getValue(ServiceReservation.class);
                    if(res.getEmployeeId().equals(employeeId)){
                        newReservationsList.add(res);
                    }


                }
                for (ServiceReservation ress : newReservationsList) {
                    if (!reservations.contains(ress)) {
                        reservations.add(ress);
                    }
                }

                Log.e("TAG","reservations"+reservations);
                adapter = new ServiceReservationAdapter(reservations, getActivity());
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllReservations", "Failed to read resevations", databaseError.toException());
            }
        });
    }

    private void searchAndFilterReservations(String query, String selectedItem){
        List<ServiceReservation> newReservationsList= new ArrayList<>();
        List<ServiceReservation> filteredList= new ArrayList<>();
        for(ServiceReservation res:reservations){


            fetchEmployee(res.getEmployeeId(), () -> {
                fetchOD(res.getOdId(), () -> {
                    findServiceName(res.getServiceId(), () -> {
                        if (res.getStatus().equals(selectedItem) && equalsQuery(query, res)) {
                            newReservationsList.add(res);
                            for(ServiceReservation ress: newReservationsList){
                                if(!filteredList.contains(ress)){
                                    filteredList.add(ress);
                                }
                                adapter = new ServiceReservationAdapter(filteredList, getActivity());
                                recyclerView.setAdapter(adapter);
                            }
                        }
                    });
                });
            });
            /*
            fetchEmployee(res.getEmployeeId());
            fetchOD(res.getOdId());
            findServiceName(res.getServiceId());
           if (res.getStatus().equals(selectedItem) && equalsQuery(query,res)){
                newReservationsList.add(res);
           }
           /
             */
        }



    }

    private void searchReservations(String query){
        Log.e("TAG","SEARCH");
        List<ServiceReservation> newReservationsList= new ArrayList<>();
        List<ServiceReservation> filteredList= new ArrayList<>();
        for(ServiceReservation res:reservations){
            /*
            fetchEmployee(res.getEmployeeId());
            fetchOD(res.getOdId());
            findServiceName(res.getServiceId());
            if (equalsQuery(query,res)){
                newReservationsList.add(res);
            }

             */
            fetchEmployee(res.getEmployeeId(), () -> {
                fetchOD(res.getOdId(), () -> {
                    findServiceName(res.getServiceId(), () -> {
                        if (equalsQuery(query,res)) {
                            Log.e("TAG", "DODAO u NEW" + res);
                            newReservationsList.add(res);

                        }
                    });
                });
            });



              //  adapter = new ServiceReservationAdapter(filteredList, getActivity());
              //  recyclerView.setAdapter(adapter);

        }
        for(ServiceReservation ress: newReservationsList){
            if(!filteredList.contains(ress)){
                Log.e("TAG","DODAO u FILTERED");
                filteredList.add(ress);

            }
        }

        adapter = new ServiceReservationAdapter(filteredList, getActivity());
        recyclerView.setAdapter(adapter);

    }

    private void filterReservations(String selectedItem){
        List<ServiceReservation> newReservationsList= new ArrayList<>();
        List<ServiceReservation> filteredList= new ArrayList<>();
        for(ServiceReservation res:reservations){


            if (res.getStatus().equals(selectedItem)){
                newReservationsList.add(res);
            }
        }

        for(ServiceReservation res: newReservationsList){
            if(!filteredList.contains(res)){
                filteredList.add(res);
            }
        }
        adapter = new ServiceReservationAdapter(filteredList, getActivity());
        recyclerView.setAdapter(adapter);
    }




    private void findServiceName(String serviceId,DataCallback callback) {
        DatabaseReference serviceRef = FirebaseDatabase.getInstance().getReference("services");

        serviceRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    Service service = resSnapshot.getValue(Service.class);
                    if (service != null && service.getId().equals(serviceId)) {
                        serviceName=service.getServiceName();
                        Log.e("TAG","SERVICENAME"+ serviceName);
                    }


                }
                callback.onDataReceived();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("findServiceName", "Failed to read service", databaseError.toException());
                callback.onDataReceived();
            }
        });
    }


        private boolean equalsQuery(String query,ServiceReservation res){


            Log.e("TAG","U METODI"+ employeeFirstName+employeeLastName+odFirstName+odLastName+serviceName);
            return employeeFirstName.equals(query)|| employeeLastName.equals(query) || odLastName.equals(query)|| odFirstName.equals(query)|| serviceName.equals(query) ;
        }


    private void fetchEmployee(String id,DataCallback callback){
        Log.e("TAG","ID"+ id);
        DatabaseReference userrRef = FirebaseDatabase.getInstance().getReference("Users").child(id);
        Log.e("TAG","fetchovan"+ userrRef);
        userrRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    Log.e("TAG","user"+ user);
                    Log.e("TAG","username"+ user.getFirstName());
                    employeeFirstName=user.getFirstName();
                    employeeLastName=user.getLastName();


                    // Možeš ovdje dalje raditi s korisnikom
                }
                callback.onDataReceived();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
                callback.onDataReceived();
            }
        });
    }
    private void fetchOD(String id,DataCallback callback){
        Log.e("TAG","ID"+ id);
        DatabaseReference odRef = FirebaseDatabase.getInstance().getReference("Users").child(id);
        Log.e("TAG","fetchovan"+ odRef);
        odRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    Log.e("TAG","user"+ user);
                    Log.e("TAG","username"+ user.getFirstName());
                    odFirstName=user.getFirstName();
                    odLastName=user.getLastName();


                    // Možeš ovdje dalje raditi s korisnikom
                }
                callback.onDataReceived();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
                callback.onDataReceived();
            }
        });
    }

    public void cancelSearch(){
        adapter = new ServiceReservationAdapter(reservations, getActivity());
        recyclerView.setAdapter(adapter);
    }

    public interface DataCallback {
        void onDataReceived();
    }

}