package com.example.eventplanner.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;

public class EmptyAdapter extends RecyclerView.Adapter<EmptyAdapter.EmptyViewHolder> {

    private int numberOfItems;

    public EmptyAdapter(int numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    @NonNull
    @Override
    public EmptyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Kreirajte prazan ViewHolder
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_employee_card, parent, false);
        return new EmptyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull EmptyViewHolder holder, int position) {
        // Ne radite ništa u onBindViewHolder metodi
    }

    @Override
    public int getItemCount() {
        return numberOfItems;
    }

    static class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
