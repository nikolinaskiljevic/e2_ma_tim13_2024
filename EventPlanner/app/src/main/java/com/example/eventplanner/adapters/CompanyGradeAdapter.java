package com.example.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.GradeCompanyFragment;
import com.example.eventplanner.fragments.ReportCompanyGradeFragment;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CompanyGradeAdapter extends RecyclerView.Adapter<CompanyGradeAdapter.CompanyGradeViewHolder>{

    private List<CompanyGrade> gradesList;
    private Context context;
    private User user;
    private FirebaseAuth mAuth;
    private User loggedInUser ;

    private FirebaseUser userr;
    private String userRole;

    private boolean exists=false;

    public CompanyGradeAdapter(List<CompanyGrade> gradesList, Context context) {
        this.gradesList = gradesList;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }



    @NonNull
    @Override
    public CompanyGradeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_company_grade_card, parent, false);
        return new CompanyGradeAdapter.CompanyGradeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyGradeViewHolder holder, int position) {
        CompanyGrade grade = gradesList.get(position);

        // Postavljanje podataka o zaposleniku u ViewHolder

        holder.textViewDate.setText(grade.getDate().toString());
        holder.textViewComment.setText(grade.getComment());
        holder.ratingbar.setRating(grade.getGrade());

        loggedInUser= new User();
        mAuth = FirebaseAuth.getInstance();


        checkIfGradeExists(grade.getId(), exists -> {
            if (exists) {
                holder.buttonReport.setVisibility(View.GONE);
            } else {
                holder.buttonReport.setVisibility(View.VISIBLE);
            }
        });

        userr = mAuth.getCurrentUser();
        if (userr != null) {
            fetchUserById(userr.getUid(), new CompanyAdapter.UserFetchCallback() {
                @Override
                public void onUserFetched(User user) {
                    if ( !userRole.equals("pupv") || exists ) {
                        holder.buttonReport.setVisibility(View.GONE);
                    }else{
                        checkIfGradeExists(grade.getId(), exists -> {
                            if (exists) {
                                holder.buttonReport.setVisibility(View.GONE);
                            } else {
                                holder.buttonReport.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }
            });
        }

        holder.buttonReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String companyGradeId = gradesList.get(position).getId();


                ReportCompanyGradeFragment fragment = new ReportCompanyGradeFragment();
                Bundle bundle = new Bundle();
                bundle.putString("companyGradeId", companyGradeId);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


    }

    @Override
    public int getItemCount() {
        return gradesList.size();
    }

    public static class CompanyGradeViewHolder extends RecyclerView.ViewHolder {

        TextView textViewDate,textViewComment;
        RatingBar  ratingbar;
        Button buttonReport;
        public CompanyGradeViewHolder(@NonNull View itemView) {
            super(itemView);


            textViewDate=itemView.findViewById(R.id.textViewDate);
            textViewComment=itemView.findViewById(R.id.textViewComment);
            ratingbar = itemView.findViewById(R.id.ratingbar);
            buttonReport= itemView.findViewById(R.id.buttonReport);
        }
    }

    private void fetchUserById(String userId, final CompanyAdapter.UserFetchCallback callback) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    userRole=user.getRole();

                    Log.d("fetchUserById", "User found: " + user.toString());

                    // Možeš ovdje dalje raditi s korisnikom
                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }
                callback.onUserFetched(user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
                callback.onUserFetched(null);
            }
        });
    }

    public interface UserFetchCallback {
        void onUserFetched(User user);
    }
    public void updateList(List<CompanyGrade> newGradesList) {
        gradesList.clear();
        gradesList.addAll(newGradesList);
        notifyDataSetChanged();
    }

    private void checkIfGradeExists(String companyGradeId, CheckGradeExistsCallback callback) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference reportsRef = databaseReference.child("CompanyGradeReports");

        reportsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean exists = false;
                for (DataSnapshot reportSnapshot : dataSnapshot.getChildren()) {
                    String gradeId = reportSnapshot.child("companyGradeId").getValue(String.class);
                    if (companyGradeId.equals(gradeId)) {
                        exists = true;
                        break;
                    }
                }
                callback.onGradeExists(exists);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Handle cancelled event if needed
            }
        });
    }
    public interface CheckGradeExistsCallback {
        void onGradeExists(boolean exists);
    }


}
