package com.example.eventplanner.fragments;

import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventAgenda;
import com.example.eventplanner.model.EventGuest;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateGuestList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateGuestList extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private Spinner organizerEvents, spinnerAllGuests;
    private EditText guestName, guestSurname;
    private RadioButton radioButtonVegan2, radioButtonVegetarian2, button03, button310, button1018, button1830, button3050, button5070, buttonInvited, buttonAccepted, buttonVegan, buttonVegetarian, radioButtonInvite2, radioButtonAccept2;
    private Button buttonDownloadPdf, saveGuest, addGuest, removeGuest, buttonUpdateGuestInvite, buttonUpdateGuestAccept, buttonUpdateGuestSpecialRequests;
    private List<Event> allEvents = new ArrayList<>();
    private List<Event> myEvents = new ArrayList<>();
    private List<EventGuest> allGuests = new ArrayList<>();
    private List<EventGuest> selectedEventGuests = new ArrayList<>();
    private Event selectedEvent;
    private EventGuest selectedGuest;
    private TextView textViewSelectedEventGuests, textViewGuestInvite, textViewGuestAccept, textViewGuestSpecialRequest;
    private LinearLayout eventGuestsCheckboxes, existingEventGuestsCheckboxes;

    public CreateGuestList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateGuestList.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateGuestList newInstance(String param1, String param2) {
        CreateGuestList fragment = new CreateGuestList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_guest_list, container, false);

        organizerEvents = view.findViewById(R.id.spinnerOrganizerEvents);
        guestName = view.findViewById(R.id.editTextGuestName);
        guestSurname = view.findViewById(R.id.editTextGuestSurname);

        button03 = view.findViewById(R.id.radioButton03);
        button310 = view.findViewById(R.id.radioButton310);
        button1018 = view.findViewById(R.id.radioButton1018);
        button1830 = view.findViewById(R.id.radioButton1830);
        button3050 = view.findViewById(R.id.radioButton3050);
        button5070 = view.findViewById(R.id.radioButton5070);

        buttonInvited = view.findViewById(R.id.radioButtonInvite);

        buttonAccepted = view.findViewById(R.id.radioButtonAccept);

        buttonVegan = view.findViewById(R.id.radioButtonVegan);
        buttonVegetarian = view.findViewById(R.id.radioButtonVegetarian);

        saveGuest = view.findViewById(R.id.buttonSaveGuest);
        addGuest = view.findViewById(R.id.buttonAddGuest);
        removeGuest = view.findViewById(R.id.buttonRemoveGuest);

        textViewSelectedEventGuests = view.findViewById(R.id.textViewExistingSelectedEventGuests);
        eventGuestsCheckboxes = view.findViewById(R.id.eventGuestsCheckboxes);
        existingEventGuestsCheckboxes = view.findViewById(R.id.existingEventGuestsCheckboxes);

        spinnerAllGuests = view.findViewById(R.id.spinnerAllGuests);
        textViewGuestInvite = view.findViewById(R.id.textViewGuestInvite);
        textViewGuestAccept = view.findViewById(R.id.textViewGuestAccept);
        textViewGuestSpecialRequest = view.findViewById(R.id.textViewGuestSpecialRequest);
        radioButtonInvite2 = view.findViewById(R.id.radioButtonInvite2);
        radioButtonAccept2 = view.findViewById(R.id.radioButtonAccept2);
        radioButtonVegan2 = view.findViewById(R.id.radioButtonVegan2);
        radioButtonVegetarian2 = view.findViewById(R.id.radioButtonVegetarian2);
        buttonUpdateGuestInvite = view.findViewById(R.id.buttonUpdateGuestInvite);
        buttonUpdateGuestAccept = view.findViewById(R.id.buttonUpdateGuestAccept);
        buttonUpdateGuestSpecialRequests = view.findViewById(R.id.buttonUpdateGuestSpecialRequests);
        buttonDownloadPdf = view.findViewById(R.id.buttonDownloadPdf);

        getAllEvents();

        // Dodavanje TextWatcher-a na sva polja
        guestName.addTextChangedListener(watcher);
        guestSurname.addTextChangedListener(watcher);
        // Postavljanje početnog stanja dugmeta "Add guest"
        saveGuest.setEnabled(false);

        buttonDownloadPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePdf();
            }
        });

        buttonUpdateGuestAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean accepted = radioButtonAccept2.isChecked();
                selectedGuest.setAcceptedInvite(accepted);
                updateSelectedGuest(selectedGuest);
            }
        });

        buttonUpdateGuestSpecialRequests.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean vegan = radioButtonVegan2.isChecked();
                boolean vegetarian = radioButtonVegetarian2.isChecked();
                if(vegan) {
                    selectedGuest.setVegan(true);
                    selectedGuest.setVegetarian(false);
                }
                else if(vegetarian) {
                    selectedGuest.setVegetarian(true);
                    selectedGuest.setVegan(false);
                }
                else {
                    selectedGuest.setVegan(false);
                    selectedGuest.setVegetarian(false);
                }

                updateSelectedGuest(selectedGuest);
            }
        });

        buttonUpdateGuestInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean invited = radioButtonInvite2.isChecked();
                selectedGuest.setInvited(invited);
                updateSelectedGuest(selectedGuest);
            }
        });

        spinnerAllGuests.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                getSelectedGuestData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Ne radimo ništa kada nije odabrana stavka
            }
        });

        removeGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> selectedEventTypes = new ArrayList<>();
                for (int i = 0; i < existingEventGuestsCheckboxes.getChildCount(); i++) {
                    View child = existingEventGuestsCheckboxes.getChildAt(i);
                    if (child instanceof CheckBox) {
                        CheckBox checkBox = (CheckBox) child;
                        if (checkBox.isChecked()) {
                            selectedEventTypes.add(checkBox.getText().toString());
                        }
                    }
                }
                // Obrada odabranih event tipova

                updateSelectedEventGuestList(selectedEventTypes, false);
            }
        });

        addGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> selectedGuests = new ArrayList<>();
                for (int i = 0; i < eventGuestsCheckboxes.getChildCount(); i++) {
                    View child = eventGuestsCheckboxes.getChildAt(i);
                    if (child instanceof CheckBox) {
                        CheckBox checkBox = (CheckBox) child;
                        if (checkBox.isChecked()) {
                            selectedGuests.add(checkBox.getText().toString());
                        }
                    }
                }

                updateSelectedEventGuestList(selectedGuests, true);
            }
        });

        organizerEvents.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                findSelectedEvent();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Ne radimo ništa kada nije odabrana stavka
            }
        });

        saveGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

                String name = guestName.getText().toString();
                String surname = guestSurname.getText().toString();
                String ageRange = getAgeRange();
                boolean invited = buttonInvited.isChecked();
                boolean acceptedInvite = buttonAccepted.isChecked();
                boolean vegan = buttonVegan.isChecked();
                boolean vegetarian = buttonVegetarian.isChecked();

                EventGuest guest = new EventGuest(name, surname, ageRange, invited, acceptedInvite, vegan, vegetarian);

                saveGuestToDatabase(guest);
                guestName.setText("");
                guestSurname.setText("");
            }
        });

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                generatePdf();
            } else {
                Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void generatePdf() {
        getAllGuests();

        String pdfPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        String pdfName = selectedEvent.getEventName() + "_guests.pdf";
        File pdfFile = new File(pdfPath, pdfName);

        try {
            FileOutputStream fos = new FileOutputStream(pdfFile);
            PdfWriter writer = new PdfWriter(fos);
            PdfDocument pdfDocument = new PdfDocument(writer);
            Document document = new Document(pdfDocument);

            // Dodajte naslov
            String title = "List of guests for event: " + selectedEvent.getEventName();
            document.add(new Paragraph(title).setBold().setFontSize(18).setTextAlignment(TextAlignment.CENTER));

            // Kreirajte tabelu sa odgovarajućim brojem kolona
            float[] columnWidths = {2, 2, 2, 2, 2, 2};
            Table table = new Table(columnWidths);
            table.setHorizontalAlignment(HorizontalAlignment.CENTER);

            // Dodajte zaglavlja kolona
            table.addHeaderCell(new Cell().add(new Paragraph("Name")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("Surname")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("Age range")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("Invited")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("Accepted invitation")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("Special request")).setBold().setTextAlignment(TextAlignment.CENTER));

            // Dodajte podatke iz liste selectedEventAgendas
            for (EventGuest guest : selectedEventGuests) {
                table.addCell(new Cell().add(new Paragraph(guest.getName())).setTextAlignment(TextAlignment.CENTER));
                table.addCell(new Cell().add(new Paragraph(guest.getSurname())).setTextAlignment(TextAlignment.CENTER));
                table.addCell(new Cell().add(new Paragraph(guest.getAgeRange())).setTextAlignment(TextAlignment.CENTER));
                if(guest.isInvited()) {
                    table.addCell(new Cell().add(new Paragraph("Yes")).setTextAlignment(TextAlignment.CENTER));
                }
                else {
                    table.addCell(new Cell().add(new Paragraph("No")).setTextAlignment(TextAlignment.CENTER));
                }
                if(guest.isAcceptedInvite()) {
                    table.addCell(new Cell().add(new Paragraph("Yes")).setTextAlignment(TextAlignment.CENTER));
                }
                else {
                    table.addCell(new Cell().add(new Paragraph("No")).setTextAlignment(TextAlignment.CENTER));
                }
                if(guest.isVegan()) {
                    table.addCell(new Cell().add(new Paragraph("Vegan")).setTextAlignment(TextAlignment.CENTER));
                }
                else if(guest.isVegetarian()){
                    table.addCell(new Cell().add(new Paragraph("Vegetarian")).setTextAlignment(TextAlignment.CENTER));
                }
                else {
                    table.addCell(new Cell().add(new Paragraph("Nothing")).setTextAlignment(TextAlignment.CENTER));
                }
            }

            document.add(table);
            document.close();

            Toast.makeText(getContext(), "PDF generated successfully", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Error generating PDF: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void updateSelectedGuest(EventGuest guest) {
        DatabaseReference guestRef = FirebaseDatabase.getInstance().getReference().child("eventGuests").child(guest.getId());
        guestRef.setValue(guest).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Well done!", Toast.LENGTH_SHORT).show();
                    getAllGuests();
                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getSelectedGuestData() {
        String selected = "";
        if(spinnerAllGuests.getSelectedItem() != null) {
            selected = spinnerAllGuests.getSelectedItem().toString();
        }

        // Izvlačimo deo nakon "Selected guest: "
        String prefix = "Selected guest: ";
        String extractedPart = selected.substring(selected.indexOf(prefix) + prefix.length());

        // Razdvajamo ime i prezime
        String[] nameParts = extractedPart.split(" ");
        String name = nameParts[0];
        String surname = nameParts[1];

        for(EventGuest g: allGuests) {
            if(g.getName().equals(name) && g.getSurname().equals(surname)) {
                selectedGuest = g;
            }
        }

        fillSelectedGuestInfo();
    }

    private void fillSelectedGuestInfo() {
        if(selectedGuest.isInvited()) {
            textViewGuestInvite.setText("Guest is invited.");
        }
        else {
            textViewGuestInvite.setText("Guest is not invited.");
        }

        if(selectedGuest.isAcceptedInvite()) {
            textViewGuestAccept.setText("Guest is accepted the invitation.");
        }
        else {
            textViewGuestAccept.setText("Guest is not accepted the invitation.");
        }

        if(selectedGuest.isVegan()) {
            textViewGuestSpecialRequest.setText("Guest is vegan.");
        }
        else if(selectedGuest.isVegetarian()){
            textViewGuestSpecialRequest.setText("Guest is vegetarian.");
        }
        else {
            textViewGuestSpecialRequest.setText("Guest do not have special requests.");
        }

    }

    private void findSelectedEvent() {
        String selectedEv = "";
        if(organizerEvents.getSelectedItem() != null) {
            selectedEv = organizerEvents.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selectedEv);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            selectedEvent = findEvent(eventName);
        } else {
            System.out.println("Event name not found in the string.");
        }

        getAllGuests();
    }

    private void getAllGuests() {
        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference("eventGuests");

        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allGuests.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EventGuest g = snapshot.getValue(EventGuest.class);
                    if (g != null) {
                        allGuests.add(g);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findSelectedEventGuests();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllEvents", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void findSelectedEventGuests() {
        selectedEventGuests.clear();

        for(String s: selectedEvent.getGuestIds()){
            for(EventGuest g: allGuests) {
                if(s.equals(g.getId()) && !s.isEmpty()) {
                    selectedEventGuests.add(g);
                    break;
                }
            }
        }

        fillSelectedEventGuestList();
    }

    private void fillSelectedEventGuestList() {
        StringBuilder et = new StringBuilder();
        int i = 1;
        for(EventGuest g: selectedEventGuests){
            if(et.toString().isEmpty()) {
                et = new StringBuilder(Integer.toString(i) + ". " + g.getName() + " " + g.getSurname() + "\n");
                i++;
            }
            else {
                et.append(Integer.toString(i)).append(". ").append(g.getName()).append(" ").append(g.getSurname()).append("\n");
                i++;
            }
        }

        if(selectedEventGuests.isEmpty()){
            et = new StringBuilder("There is no guests of selected event.");
        }

        textViewSelectedEventGuests.setText(et);
        defineCheckboxList();
    }

    private void defineCheckboxList() {
        eventGuestsCheckboxes.removeAllViews();
        existingEventGuestsCheckboxes.removeAllViews();

        List<String> unaddedGuests = new ArrayList<>();
        for(EventGuest g: allGuests) {
            if(!selectedEvent.getGuestIds().contains(g.getId()) && !unaddedGuests.contains(g.getName() + " " + g.getSurname())) {
                unaddedGuests.add(g.getName() + " " + g.getSurname());
            }
        }

        for (String eventType: unaddedGuests){
            CheckBox checkBox = new CheckBox(requireContext());
            checkBox.setText(eventType);
            eventGuestsCheckboxes.addView(checkBox);
        }

        for (EventGuest g: selectedEventGuests){
            CheckBox checkBox = new CheckBox(requireContext());
            String s = g.getName() + " " + g.getSurname();
            checkBox.setText(s);
            existingEventGuestsCheckboxes.addView(checkBox);
        }

        setupAllGuestsSpinner();
    }

    private void saveGuestToDatabase(EventGuest guest) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("eventGuests");
        String guestId = databaseRef.push().getKey();
        guest.setId(guestId);
        databaseRef.child(guestId).setValue(guest);

        Toast.makeText(getContext(), "Guest added successfully!", Toast.LENGTH_SHORT).show();
    }

    private void updateSelectedEventGuestList(List<String> guestsNames, boolean adding) {
        List<String> existingGuests = selectedEvent.getGuestIds();
        List<String> guestsIds = getSelectedGuestsIds(guestsNames);

        if(adding) {
            existingGuests.addAll(guestsIds);
            selectedEvent.setGuestIds(existingGuests);
        }
        else {
            Iterator<String> iterator = selectedEvent.getGuestIds().iterator();
            while (iterator.hasNext()) {
                String s1 = iterator.next();
                if (guestsIds.contains(s1)) {
                    iterator.remove();
                }
            }
        }

        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference().child("events").child(selectedEvent.getId());
        eventRef.setValue(selectedEvent).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Guest list updated!", Toast.LENGTH_SHORT).show();
                    getAllEvents();
                    guestName.setText("");
                    guestSurname.setText("");
                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private List<String> getSelectedGuestsIds(List<String> newGuestsNames) {
        List<String> retVal = new ArrayList<>();

        for(String n: newGuestsNames) {
            for(EventGuest g: allGuests) {
                String[] nameAndSurname = n.split(" ");
                if(nameAndSurname[0].equals(g.getName()) && nameAndSurname[1].equals(g.getSurname())) {
                    retVal.add(g.getId());
                    break;
                }
            }
        }

        return retVal;
    }

    private Event findEvent(String eventName) {
        for(Event e: myEvents) {
            if(e.getEventName().equals(eventName)) {
                selectedEvent = e;
                return e;
            }
        }

        return null;
    }

    private String getAgeRange() {

        if(button03.isChecked()){
            return "0 - 3";
        }
        else if(button310.isChecked()) {
            return "3 - 10";
        }
        else if(button1018.isChecked()) {
            return "10 - 18";
        }
        else if(button1830.isChecked()) {
            return "18 - 30";
        }
        else if(button3050.isChecked()) {
            return "30 - 50";
        }
        else if(button5070.isChecked()) {
            return "50 - 70";
        }
        else {
            return "70+";
        }

    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            // Provera da li su sva polja popunjena
            boolean allFieldsFilled = !TextUtils.isEmpty(guestName.getText().toString().trim()) &&
                    !TextUtils.isEmpty(guestSurname.getText().toString().trim());

            // Omogućavanje ili onemogućavanje dugmeta na osnovu toga da li su sva polja popunjena
            saveGuest.setEnabled(allFieldsFilled);
        }
    };

    private void getAllEvents() {
        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference("events");

        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allEvents.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Event e = snapshot.getValue(Event.class);
                    if (e != null) {
                        allEvents.add(e);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findOrganizerEvents();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllEvents", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void findOrganizerEvents() {
        myEvents.clear();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        for(Event e: allEvents) {
            if(e.getOrganizerId().equals(currentUser.getUid())) {
                myEvents.add(e);
            }
        }

        setupOrganizerEventsSpinner();
    }

    private void setupOrganizerEventsSpinner() {
        // Kreiramo listu imena dogadjaja
        List<String> events = new ArrayList<>();
        for (Event e : allEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        organizerEvents.setAdapter(spinnerAdapter);
    }

    private void setupAllGuestsSpinner() {
        // Kreiramo listu imena dogadjaja
        List<String> guests = new ArrayList<>();
        for (EventGuest g : allGuests) {
            String var = "Selected guest: " + g.getName() + " " + g.getSurname();
            guests.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, guests);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerAllGuests.setAdapter(spinnerAdapter);
    }

}