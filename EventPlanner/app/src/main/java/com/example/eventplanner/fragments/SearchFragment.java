package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.example.eventplanner.R;
import com.example.eventplanner.dto.SearchParametersDTO;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventCategory;
import com.example.eventplanner.model.EventSubcategory;
import com.example.eventplanner.model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Employee> allEmployees = new ArrayList<>();
    private List<EventCategory> allCategories = new ArrayList<>();
    private List<EventSubcategory> appropriateSubcategories = new ArrayList<>();

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        Spinner spinnerPFP = rootView.findViewById(R.id.spinnerPackageFavorProduct);
        Spinner spinnerType = rootView.findViewById(R.id.spinnerEventType);
        EditText etName = rootView.findViewById(R.id.editTextSearchByName);
        EditText etLocation = rootView.findViewById(R.id.editTextSearchByLocation);

        Spinner spinnerCategory = rootView.findViewById(R.id.spinnerCategory);
        getAllCategoriesFromDatabase(spinnerCategory);
        Spinner spinnerFoodSubcategory = rootView.findViewById(R.id.spinnerSubcategoryFood);
        //Spinner spinnerPhotoSubcategory = rootView.findViewById(R.id.spinnerSubcategoryPhoto);

        Spinner spinnerSP = rootView.findViewById(R.id.spinnerServiceProvider);
        getAllEmployeesFromDatabase(spinnerSP);
        EditText etMinPrice = rootView.findViewById(R.id.editTextMinPrice);
        EditText etMaxPrice = rootView.findViewById(R.id.editTextMaxPrice);
        DatePicker firstDP = rootView.findViewById(R.id.datePickerFrom);
        DatePicker secondDP = rootView.findViewById(R.id.datePickerTo);
        //LinearLayout datePickers = rootView.findViewById(R.id.layoutDatePickers);
        Spinner spinnerAvailability = rootView.findViewById(R.id.spinnerAvailability);
        Button searchButton = rootView.findViewById(R.id.buttonSearch);

        // Postavljanje osluškivača događaja za spinere
        spinnerPFP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                updateVisibility(rootView, spinnerFoodSubcategory);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Ne radimo ništa kada nije odabrana stavka
            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                updateVisibility(rootView, spinnerFoodSubcategory);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Ne radimo ništa kada nije odabrana stavka
            }
        });

        // Pozivamo metodu za ažuriranje vidljivosti kako bismo postavili početne vrednosti
        updateVisibility(rootView, spinnerFoodSubcategory);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Čitanje unetih podataka
                String selectedFilterOption = spinnerPFP.getSelectedItem().toString();
                String selectedFilter = selectedFilterOption.substring(selectedFilterOption.indexOf(":") + 2);
                //Odsecite "Event type:" sa izabrane opcije
                String selectedOptionType = spinnerType.getSelectedItem().toString();
                String selectedType = selectedOptionType.substring(selectedOptionType.indexOf(":") + 2);
                String enteredName = etName.getText().toString();
                String enteredLocation = etLocation.getText().toString();
                String selectedOptionCategory = spinnerCategory.getSelectedItem().toString();
                String selectedCategory = selectedOptionCategory.substring(selectedOptionCategory.indexOf(":") + 2);
                String selectedOptionSubcategory = spinnerFoodSubcategory.getSelectedItem().toString();
                String selectedSubcategory = selectedOptionSubcategory.substring(selectedOptionSubcategory.indexOf(":") + 2);
                /*if ("Food and catering".equals(selectedCategory)) { //ili " Food and catering"
                    String selectedOptionFoodSubcategory = spinnerFoodSubcategory.getSelectedItem().toString();
                    selectedCategory = selectedOptionFoodSubcategory.substring(selectedOptionFoodSubcategory.indexOf(":") + 2);
                }
                else if ("Photo and video".equals(selectedCategory)){
                    String selectedOptionPhotoSubcategory = spinnerPhotoSubcategory.getSelectedItem().toString();
                    selectedCategory = selectedOptionPhotoSubcategory.substring(selectedOptionPhotoSubcategory.indexOf(":") + 2);
                }
                else {
                    selectedSubcategory = "None";
                }*/
                String selectedOptionProvider = spinnerSP.getSelectedItem().toString();
                String selectedProvider = selectedOptionProvider.substring(selectedOptionProvider.indexOf(":") + 2);
                int enteredMinPrice;
                int enteredMaxPrice;

                if(etMinPrice.getText().toString().isEmpty() && etMaxPrice.getText().toString().isEmpty()) {
                    enteredMinPrice = 0;
                    enteredMaxPrice = 0;
                }
                else if(!etMinPrice.getText().toString().isEmpty() && etMaxPrice.getText().toString().isEmpty()) {
                    enteredMinPrice = Integer.parseInt(etMinPrice.getText().toString());
                    enteredMaxPrice = 0;
                }
                else if(etMinPrice.getText().toString().isEmpty() && !etMaxPrice.getText().toString().isEmpty()) {
                    enteredMinPrice = 0;
                    enteredMaxPrice = Integer.parseInt(etMaxPrice.getText().toString());
                }
                else {
                    enteredMinPrice = Integer.parseInt(etMinPrice.getText().toString());
                    enteredMaxPrice = Integer.parseInt(etMaxPrice.getText().toString());
                }

                int firstYear = firstDP.getYear();
                int firstMonth = firstDP.getMonth() + 1;
                int firstDayOfMonth = firstDP.getDayOfMonth();
                LocalDate firstDate = LocalDate.of(firstYear, firstMonth, firstDayOfMonth);
                int secondYear = secondDP.getYear();
                int secondMonth = secondDP.getMonth() + 1;
                int secondDayOfMonth = secondDP.getDayOfMonth();
                LocalDate secondDate = LocalDate.of(secondYear, secondMonth, secondDayOfMonth);
                String selectedOptionAvailability = spinnerAvailability.getSelectedItem().toString();
                String selectedAvailabilityOption = selectedOptionAvailability.substring(selectedOptionAvailability.indexOf(":") + 2);

                SearchParametersDTO searchParams = new SearchParametersDTO(selectedFilter, selectedType, enteredName, enteredLocation, selectedCategory, selectedSubcategory, selectedProvider, enteredMinPrice, enteredMaxPrice, firstDate, secondDate, selectedAvailabilityOption);

                //trebace if-else da bi se znalo da li da se kopa po cvoru usluga/paketa/proizvoda
                //onda ide poziv metode za dobavljanje iz baze? (ili u novom fragmentu)
                //klikom na dugme search, bi se taj DTO prenosio u novi fragment gdje ce biti prikazani pretrage

                //saveEventToDatabase(event);
                // Kreirajte novi fragment SearchResultFragment
                SearchResultFragment fragment = SearchResultFragment.newInstance(searchParams);

                // Dobijte menadžer fragmenta
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();

                // Pokrenite transakciju za zamenu trenutnog fragmenta sa novim fragmentom
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.fragment_container, fragment); // R.id.fragment_container je ID vašeg FrameLayout-a u aktivnosti gde želite da zamenite fragmente
                transaction.addToBackStack(null); // Dodajte transakciju na back stack, tako da se možete vratiti na prethodni fragment pritiskom na dugme "Back"
                transaction.commit(); // Potvrdite transakciju

            }
        });

        return rootView;
    }

    private void updateVisibility(View rootView, Spinner spinnerSubcat) {
        Spinner spinnerPFP = rootView.findViewById(R.id.spinnerPackageFavorProduct);
        String selectedFilter = spinnerPFP.getSelectedItem().toString();

        Spinner spinnerCategory = rootView.findViewById(R.id.spinnerCategory);
        String selectedOptionCategory = spinnerCategory.getSelectedItem().toString();
        String selectedCategory = selectedOptionCategory.substring(selectedOptionCategory.indexOf(":") + 2);

        EditText textLocation = rootView.findViewById(R.id.editTextSearchByLocation);
        Spinner spinnerFoodSubcategory = rootView.findViewById(R.id.spinnerSubcategoryFood);
        //Spinner spinnerPhotoSubcategory = rootView.findViewById(R.id.spinnerSubcategoryPhoto);
        Spinner spinnerSP = rootView.findViewById(R.id.spinnerServiceProvider);
        LinearLayout datePickers = rootView.findViewById(R.id.layoutDatePickers);

        switch (selectedFilter) {
            case "Filter: Service":
                textLocation.setVisibility(View.VISIBLE);
                /*if ("Category: Food and catering".equals(selectedCategory)) {
                    spinnerFoodSubcategory.setVisibility(View.VISIBLE);
                    spinnerPhotoSubcategory.setVisibility(View.INVISIBLE);
                }
                else if ("Category: Photo and video".equals(selectedCategory)){
                    spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                    spinnerPhotoSubcategory.setVisibility(View.VISIBLE);
                }
                else {
                    spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                    spinnerPhotoSubcategory.setVisibility(View.INVISIBLE);
                }*/
                for(EventCategory e: allCategories) {
                    if(e.getName().equals(selectedCategory)) {
                        getAllSubcategoriesFromDatabase(e.getId(), spinnerSubcat);
                    }
                }
                if ("None".equals(selectedCategory)){
                    spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                }
                else {
                    spinnerFoodSubcategory.setVisibility(View.VISIBLE);
                }
                spinnerSP.setVisibility(View.VISIBLE);
                datePickers.setVisibility(View.VISIBLE);
                break;
            case "Filter: Product":
                textLocation.setVisibility(View.INVISIBLE);
                /*if ("Category: Food and catering".equals(selectedCategory)){
                    spinnerFoodSubcategory.setVisibility(View.VISIBLE);
                    spinnerPhotoSubcategory.setVisibility(View.INVISIBLE);
                }
                else if ("Category: Photo and video".equals(selectedCategory)){
                    spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                    spinnerPhotoSubcategory.setVisibility(View.VISIBLE);
                }
                else {
                    spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                    spinnerPhotoSubcategory.setVisibility(View.INVISIBLE);
                }*/
                for(EventCategory e: allCategories) {
                    if(e.getName().equals(selectedCategory)) {
                        getAllSubcategoriesFromDatabase(e.getId(), spinnerSubcat);
                    }
                }
                if ("None".equals(selectedCategory)){
                    spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                }
                else {
                    spinnerFoodSubcategory.setVisibility(View.VISIBLE);
                }
                spinnerSP.setVisibility(View.INVISIBLE);
                datePickers.setVisibility(View.INVISIBLE);
                break;
            default:
                textLocation.setVisibility(View.INVISIBLE);
                spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                //spinnerPhotoSubcategory.setVisibility(View.INVISIBLE);
                spinnerSP.setVisibility(View.INVISIBLE);
                datePickers.setVisibility(View.INVISIBLE);
                break;
        }
    }

    private void getAllEmployeesFromDatabase(Spinner spinnerSP) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("employees");
        allEmployees.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Employee e = snapshot.getValue(Employee.class);
                    if (e != null) {
                        allEmployees.add(e);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                setupEmployeesSpinner(spinnerSP);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }

    private void getAllCategoriesFromDatabase(Spinner spinnerCat) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("eventCategories");
        allCategories.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EventCategory ec = snapshot.getValue(EventCategory.class);
                    if (ec != null) {
                        allCategories.add(ec);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                setupCategorySpinner(spinnerCat);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }

    private void setupEmployeesSpinner(Spinner spinnerSP) {
        // Kreiramo listu imena zaposlenih
        List<String> employeeNames = new ArrayList<>();
        for (Employee employee : allEmployees) {
            String var = "Service provider: " + employee.getFirstName() + " " + employee.getLastName();
            employeeNames.add(var);
        }

        String noneVar = "Service provider: None";
        employeeNames.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, employeeNames);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSP.setAdapter(spinnerAdapter);
    }

    private void setupCategorySpinner(Spinner spinnerSP) {
        // Kreiramo listu imena zaposlenih
        List<String> categories = new ArrayList<>();
        for (EventCategory e : allCategories) {
            String var = "Category: " + e.getName();
            categories.add(var);
        }

        String noneVar = "Category: None";
        categories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSP.setAdapter(spinnerAdapter);
    }

    private void getAllSubcategoriesFromDatabase(String categoryId, Spinner spinner) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("eventSubcategories");
        appropriateSubcategories.clear();
        databaseRef.orderByChild("categoryId").equalTo(categoryId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EventSubcategory esc = snapshot.getValue(EventSubcategory.class);
                    if (esc != null) {
                        appropriateSubcategories.add(esc);
                    }
                }
                // Ovdje možete izvršiti željene akcije s listom podataka podkategorija
                // Na primjer, možete pozvati metod koji postavlja Spinner za podkategorije
                // setupSubcategorySpinner();
                setupSubcategorySpinner(spinner);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Tretirajte grešku prilikom čitanja podataka iz baze
            }
        });
    }

    private void setupSubcategorySpinner(Spinner spinnerSubcat) {
        // Kreiramo listu imena zaposlenih
        List<String> subcategories = new ArrayList<>();
        for (EventSubcategory s : appropriateSubcategories) {
            String var = "Subcategory: " + s.getName();
            subcategories.add(var);
        }

        String noneVar = "Subcategory: None";
        subcategories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSubcat.setAdapter(spinnerAdapter);
    }

}
