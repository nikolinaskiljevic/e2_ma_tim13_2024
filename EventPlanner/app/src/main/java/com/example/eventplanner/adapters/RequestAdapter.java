package com.example.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.PUPVAccountRequest;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RequestAdapter extends BaseAdapter {
    private Context context;
    private List<PUPVAccountRequest> requestList;
    private List<PUPVAccountRequest> originalRequestList;
    private LayoutInflater inflater;

    private User user;
    private FirebaseAuth mAuth;
    private Map<String, String> userNameMap;

    public RequestAdapter(Context context, List<PUPVAccountRequest> requestList) {
        this.context = context;
        this.requestList = requestList;
        this.originalRequestList = new ArrayList<>(requestList); // Keep the original list
        this.inflater = LayoutInflater.from(context);
        mAuth = FirebaseAuth.getInstance();
        this.userNameMap = new HashMap<>();
        loadUserNames();
    }

    private void loadUserNames() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Users");

        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    User user = userSnapshot.getValue(User.class);
                    if (user != null) {
                        String fullName = user.getFirstName() + " " + user.getLastName();
                        userNameMap.put(userSnapshot.getKey(), fullName);
                    }
                }
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("TAG", "Error fetching users.", databaseError.toException());
            }
        });
    }

    @Override
    public int getCount() {
        return requestList.size();
    }

    @Override
    public Object getItem(int position) {
        return requestList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_request, parent, false);
        }

        TextView userNameTextView = convertView.findViewById(R.id.user_name);
        TextView creationTimeTextView = convertView.findViewById(R.id.creation_time);
        TextView companyNameTextView = convertView.findViewById(R.id.company_name);

        PUPVAccountRequest request = requestList.get(position);

        String userName = userNameMap.get(request.getUserPUPVId());
        if (userName != null) {
            userNameTextView.setText(userName);
        } else {
            userNameTextView.setText("Error fetching name");
        }

        Instant instant = Instant.parse(request.getCreationTime());
        ZonedDateTime zonedDateTime = instant.atZone(ZoneId.systemDefault());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy, hh:mm a");
        creationTimeTextView.setText(formatter.format(zonedDateTime));
        companyNameTextView.setText(request.getCompany().getName());

        return convertView;
    }

    public void updateData(List<PUPVAccountRequest> newList) {
        requestList.clear();
        requestList.addAll(newList);
        originalRequestList.clear();
        originalRequestList.addAll(newList);
        notifyDataSetChanged();
    }

    public void filterRequests(String query) {
        List<PUPVAccountRequest> filteredList = new ArrayList<>();
        query = query.toLowerCase();

        for (PUPVAccountRequest request : originalRequestList) {
            boolean matchesQuery = request.getCompany().getName().toLowerCase().contains(query) ||
                    request.getCompany().getEmail().toLowerCase().contains(query) ||
                    userNameMap.get(request.getUserPUPVId()).toLowerCase().contains(query);

            if (matchesQuery) {
                filteredList.add(request);
            }
        }

        requestList.clear();
        requestList.addAll(filteredList);
        notifyDataSetChanged();
    }



    public void getUserById(String userId, TextView text) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference usersRef = database.getReference("Users");

        usersRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    user = dataSnapshot.getValue(User.class);
                    text.setText(user.getFirstName() + " " + user.getLastName());
                } else {
                    text.setText("Error fetching name");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("TAG", "Error fetching user.", databaseError.toException());
            }
        });
    }
}
