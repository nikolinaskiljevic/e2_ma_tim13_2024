package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.AllUserReportsAdapter;
import com.example.eventplanner.adapters.ProductAdapter;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.UsersReport;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class AllUserReportsFragment extends Fragment {

    private ListView listView;
    private AllUserReportsAdapter reportAdapter;
    private List<UsersReport> reports;

    public AllUserReportsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_user_reports, container, false);

        // Initialize ListView
        listView = view.findViewById(R.id.listView);

        // Initialize product list
        reports = new ArrayList<>();

        // Initialize adapter
        reportAdapter = new AllUserReportsAdapter(getContext(), R.layout.fragment_user_report_card, reports);
        listView.setAdapter(reportAdapter);



        // Fetch products from the Firebase database
        fetchProductsFromDatabase();

        // Set item click listener for ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Handle item click if needed
            }
        });

        return view;
    }

    private void fetchProductsFromDatabase() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("UserReports");

        // Add listener to read data from the Firebase database
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                reports.clear(); // Clear the product list before adding new ones from the database

                // Iterate through all products in the "products" collection
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    UsersReport product = snapshot.getValue(UsersReport.class);
                        reports.add(product);

                }

                // Notify the adapter that the data has changed
                reportAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error if there is a problem reading data from the database
                Toast.makeText(getContext(), "Error reading products from the database", Toast.LENGTH_SHORT).show();
            }
        });
    }




}