package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.ChatFragment;
import com.example.eventplanner.fragments.CompanyProfileFragment;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChatsReviewAdapter extends RecyclerView.Adapter<ChatsReviewAdapter.ViewHolder> {
    private Map<String, ArrayList<Message>> chats;
    private ArrayList<User> allUsers;
    private List<String> personsIds;
    private Context context;

    public ChatsReviewAdapter(Context context, Map<String, ArrayList<Message>> chats, ArrayList<User> users) {
        this.context = context;
        this.chats = chats;
        this.allUsers = users;
        this.personsIds = new ArrayList<>(chats.keySet()); // Pravimo listu ključeva
    }

    @NonNull
    @Override
    public ChatsReviewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chats_review_card, parent, false);
        return new ChatsReviewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatsReviewAdapter.ViewHolder holder, int position) {
        String personId = personsIds.get(position); // Dobijamo ID korisnika za trenutnu poziciju
        List<Message> messages = chats.get(personId);

        for(User u: allUsers) {
            if(personId.equals(u.getUid())) {
                holder.textViewChatName.setText("Chat with: " + u.getFirstName() + " " + u.getLastName());
            }
        }


        holder.buttonYourOpenChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChatFragment chatFragment = ChatFragment.newInstance(personId);
                FragmentActivity activity = (FragmentActivity) context;
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, chatFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return personsIds.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewChatName;
        public Button buttonYourOpenChat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewChatName = itemView.findViewById(R.id.textViewChatName);
            buttonYourOpenChat = itemView.findViewById(R.id.buttonYourOpenChat);
        }
    }
}
