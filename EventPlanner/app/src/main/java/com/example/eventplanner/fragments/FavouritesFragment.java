package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.FavouritesPackagesAdapter;
import com.example.eventplanner.adapters.FavouritesProductsAdapter;
import com.example.eventplanner.adapters.FavouritesServicesAdapter;
import com.example.eventplanner.adapters.PackageDetailsAdapter;
import com.example.eventplanner.adapters.ProductDetailsAdapter;
import com.example.eventplanner.adapters.ServiceDetailsAdapter;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FavouritesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavouritesFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RadioGroup radioGroup;
    private RadioButton radioButtonService, radioButtonProduct, radioButtonPackage;
    private RecyclerView recyclerViewProducts, recyclerViewServices, recyclerViewPackages;
    private User currentUser;
    private ArrayList<Service> allServices = new ArrayList<>();
    private ArrayList<Product> allProducts = new ArrayList<>();
    private ArrayList<Package> allPackages = new ArrayList<>();
    private ArrayList<Service> favouritesServices = new ArrayList<>();
    private ArrayList<Product> favouritesProducts = new ArrayList<>();
    private ArrayList<Package> favouritesPackages = new ArrayList<>();
    private ArrayList<Event> allEvents = new ArrayList<>();
    private ArrayList<Event> organizerEvents = new ArrayList<>();
    private FavouritesServicesAdapter favouritesServicesAdapter;
    private FavouritesProductsAdapter favouritesProductsAdapter;
    private FavouritesPackagesAdapter favouritesPackagesAdapter;

    public FavouritesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavouritesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavouritesFragment newInstance(String param1, String param2) {
        FavouritesFragment fragment = new FavouritesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favourites, container, false);

        radioGroup = view.findViewById(R.id.radioGroupFavourites);
        radioButtonService = view.findViewById(R.id.radioButtonFavouritesServices);
        int radioButtonServiceId = radioButtonService.getId();
        radioButtonProduct = view.findViewById(R.id.radioButtonFavouritesProducts);
        int radioButtonProductId = radioButtonProduct.getId();
        radioButtonPackage = view.findViewById(R.id.radioButtonFavouritesPackages);

        recyclerViewServices = view.findViewById(R.id.recyclerViewFavouritesServices);
        recyclerViewProducts = view.findViewById(R.id.recyclerViewFavouritesProducts);
        recyclerViewPackages = view.findViewById(R.id.recyclerViewFavouritesPackages);

        getAllServices();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == radioButtonServiceId) {
                    recyclerViewServices.setVisibility(View.VISIBLE);
                    recyclerViewProducts.setVisibility(View.INVISIBLE);
                    recyclerViewPackages.setVisibility(View.INVISIBLE);
                }
                else if(checkedId == radioButtonProductId){
                    recyclerViewServices.setVisibility(View.GONE);
                    recyclerViewProducts.setVisibility(View.VISIBLE);
                    recyclerViewPackages.setVisibility(View.INVISIBLE);
                }
                else {
                    recyclerViewServices.setVisibility(View.GONE);
                    recyclerViewProducts.setVisibility(View.GONE);
                    recyclerViewPackages.setVisibility(View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void getAllServices() {
        DatabaseReference serviceRef = FirebaseDatabase.getInstance().getReference("services");

        serviceRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allServices.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Service s = snapshot.getValue(Service.class);
                    if (s != null) {
                        allServices.add(s);
                    }
                }
                // Sada usersList sadrži sve korisnike
                getAllProducts();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllServices", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void getAllProducts() {
        DatabaseReference productRef = FirebaseDatabase.getInstance().getReference("products");

        productRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allProducts.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product p = snapshot.getValue(Product.class);
                    if (p != null) {
                        allProducts.add(p);
                    }
                }
                // Sada usersList sadrži sve korisnike
                getAllPackages();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllEvents", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void getAllPackages() {
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference("packages");

        packageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allPackages.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Package p = snapshot.getValue(Package.class);
                    if (p != null) {
                        allPackages.add(p);
                    }
                }
                // Sada usersList sadrži sve korisnike
                getAllUsers();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllPackages", "Failed to read packages", databaseError.toException());
            }
        });
    }

    private void getAllUsers() {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<User> allUsers = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    if (u != null) {
                        allUsers.add(u);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findCurrentUser(allUsers);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllPackages", "Failed to read packages", databaseError.toException());
            }
        });
    }

    private void findCurrentUser(List<User> allUsers) {
        FirebaseUser curr = FirebaseAuth.getInstance().getCurrentUser();
        for(User u: allUsers) {
            if(u.getUid().equals(curr.getUid())) {
                currentUser = u;
                break;
            }
        }

        findFavouritesServices();
    }

    private void findFavouritesServices() {
        favouritesServices.clear();
        for(Service s: allServices) {
           if(currentUser.getFavouritesServicesIds().contains(s.getId())) {
               favouritesServices.add(s);
           }
        }

        findFavouritesProducts();
    }

    private void findFavouritesProducts() {
        favouritesProducts.clear();
        for(Product p: allProducts) {
            if(currentUser.getFavouritesProductsIds().contains(p.getId())) {
                favouritesProducts.add(p);
            }
        }

        findFavouritesPackages();
    }

    private void findFavouritesPackages() {
        favouritesPackages.clear();
        for(Package p: allPackages) {
            if(currentUser.getFavouritesPackagesIds().contains(p.getId())) {
                favouritesPackages.add(p);
            }
        }

        getAllEvents();
    }

    private void getAllEvents() {
        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference("events");

        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allEvents.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Event e = snapshot.getValue(Event.class);
                    if (e != null) {
                        allEvents.add(e);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findOrganizerEvents();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllEvents", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void findOrganizerEvents() {
        organizerEvents.clear();
        for(Event e: allEvents) {
            if(e.getOrganizerId().equals(currentUser.getUid())) {
                organizerEvents.add(e);
            }
        }

        setServicesAdapter();
    }

    private void setServicesAdapter() {
        recyclerViewServices.setHasFixedSize(true);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(getContext()));

        favouritesServicesAdapter = new FavouritesServicesAdapter(getContext(), favouritesServices, currentUser, organizerEvents);
        recyclerViewServices.setAdapter(favouritesServicesAdapter);

        setProductsAdapter();
    }

    private void setProductsAdapter() {
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getContext()));

        favouritesProductsAdapter = new FavouritesProductsAdapter(getContext(), favouritesProducts, currentUser, organizerEvents);
        recyclerViewProducts.setAdapter(favouritesProductsAdapter);

        setPackagesAdapter();
    }

    private void setPackagesAdapter() {
        recyclerViewPackages.setHasFixedSize(true);
        recyclerViewPackages.setLayoutManager(new LinearLayoutManager(getContext()));

        favouritesPackagesAdapter = new FavouritesPackagesAdapter(getContext(), favouritesPackages, currentUser, organizerEvents);
        recyclerViewPackages.setAdapter(favouritesPackagesAdapter);
    }
}