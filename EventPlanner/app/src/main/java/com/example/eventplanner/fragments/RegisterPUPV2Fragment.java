package com.example.eventplanner.fragments;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.eventplanner.R;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyCategories;
import com.example.eventplanner.model.CompanyWorkHours;
import com.example.eventplanner.model.DailyWorkHours;
import com.example.eventplanner.model.EventCategory;
import com.example.eventplanner.model.PUPVAccountRequest;
import com.example.eventplanner.model.ProductCategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterPUPV2Fragment extends Fragment {

    // Counters for each day
    private int mondayCounter = 0;
    private int tuesdayCounter = 0;
    private int wednesdayCounter = 0;
    private int thursdayCounter = 0;
    private int fridayCounter = 0;
    private int saturdayCounter = 0;
    private int sundayCounter = 0;

    private EditText editTextEmail;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private EditText editTextFirstName;
    private EditText editTextDescription;
    private EditText editTextAddress;
    private EditText editTextPhoneNumber;
    private ProgressBar progressBar;
    private Button buttonRegisterOD;

    private Map<String, DailyWorkHours> selectedWorkHoursMap = new HashMap<>();

    private List<EventCategory> eventCategories = new ArrayList<>();
    private int currentPageCategories = 0;
    private int totalPagesCategories;
    private LinearLayout categoriesLayout;


    private List<ProductCategory> productCategories = new ArrayList<>();
    private int currentPageProducts = 0;
    private int totalPagesProducts;
    private LinearLayout productsLayout;

    private SharedPreferences sharedPreferencesEventCategory;
    private SharedPreferences sharedPreferencesProductCategory;


    private ImageView imageViewSelectedPhoto;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri picURL = null;

    private FirebaseAuth mAuth;
    public FirebaseUser user;


    public RegisterPUPV2Fragment() {
        // Required empty public constructor
    }

    public static RegisterPUPV2Fragment newInstance(String param1, String param2) {
        RegisterPUPV2Fragment fragment = new RegisterPUPV2Fragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
        user = mAuth.getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_pupv2, container, false);
        Button buttonRegisterOD = view.findViewById(R.id.buttonRegisterOD);
        TextView textViewSelectedHours = view.findViewById(R.id.textViewSelectedHours);

        editTextEmail = view.findViewById(R.id.editTextCompanyEmail);
        editTextFirstName = view.findViewById(R.id.editTextCompanyName);
        editTextAddress = view.findViewById(R.id.editTextCompanyAddress);
        editTextPhoneNumber = view.findViewById(R.id.editTextCompanyPhoneNumber);
        editTextDescription = view.findViewById(R.id.editTextCompanyDescription);
        imageViewSelectedPhoto = view.findViewById(R.id.imageViewSelectedPhoto);

        categoriesLayout = view.findViewById(R.id.categoriesLayout);
        productsLayout = view.findViewById(R.id.categoriesLayout1);

        sharedPreferencesEventCategory = getActivity().getSharedPreferences("CategoryPreferences", Context.MODE_PRIVATE);
        sharedPreferencesProductCategory = getActivity().getSharedPreferences("EventPreferences", Context.MODE_PRIVATE);


        buttonRegisterOD = view.findViewById(R.id.buttonRegisterOD);
        Button buttonAddProfilePic = view.findViewById(R.id.buttonAddCompanyPic);
        progressBar =  view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        DatabaseReference eventCategoriesRef = FirebaseDatabase.getInstance().getReference().child("eventCategories");
        DatabaseReference productCategoriesRef = FirebaseDatabase.getInstance().getReference().child("productCategories");



        Button buttonPrevious = view.findViewById(R.id.buttonPrevious);
        buttonPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPageCategories > 0) {
                    currentPageCategories--;
                    displayCategoriesForPage(currentPageCategories);
                }
            }
        });

        Button buttonPrevious1 = view.findViewById(R.id.buttonPrevious1);
        buttonPrevious1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPageProducts > 0) {
                    currentPageProducts--;
                    displayCategoriesForPage1(currentPageProducts);
                }
            }
        });

// Next button click listener
        Button buttonNext = view.findViewById(R.id.buttonNext);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPageCategories < totalPagesCategories - 1) {
                    currentPageCategories++;
                    displayCategoriesForPage(currentPageCategories);
                }
            }
        });

        Button buttonNext1 = view.findViewById(R.id.buttonNext1);
        buttonNext1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentPageProducts < totalPagesProducts - 1) {
                    currentPageProducts++;
                    displayCategoriesForPage1(currentPageProducts);
                }
            }
        });

        eventCategoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    EventCategory category = categorySnapshot.getValue(EventCategory.class);
                    eventCategories.add(category);
                }
                totalPagesCategories = (int) Math.ceil(eventCategories.size() / 5.0);
                displayCategoriesForPage(currentPageCategories);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle errors
            }
        });

        productCategoriesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    ProductCategory category = categorySnapshot.getValue(ProductCategory.class);
                    productCategories.add(category);
                }
                totalPagesProducts = (int) Math.ceil(productCategories.size() / 5.0);
                displayCategoriesForPage1(currentPageProducts);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle errors
            }
        });



        buttonRegisterOD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Retrieve entered data
                String email = editTextEmail.getText().toString();
                String name = editTextFirstName.getText().toString();
                String address = editTextAddress.getText().toString();
                String phoneNumber = editTextPhoneNumber.getText().toString();
                String description = editTextDescription.getText().toString();


                FirebaseUser firebaseUser = mAuth.getCurrentUser();
                //Company company = new Company(firebaseUser.getUid(), email, name, address, phoneNumber, description, null); // Pass null for imageUrl for now

                // Validate the fields
                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(name) ||
                        TextUtils.isEmpty(address) || TextUtils.isEmpty(phoneNumber) ||
                        TextUtils.isEmpty(description) || sharedPreferencesProductCategory.getAll().values().isEmpty()) {
                    // Show error message if any field is empty
                    Toast.makeText(getActivity(), "Please fill in all fields", Toast.LENGTH_SHORT).show();
                    return;
                }


                Company company = new Company(firebaseUser.getUid(), email, name, address, phoneNumber, description, null);
                //Company company = new Company(email, name, address, phoneNumber, description, null); // Pass null for imageUrl for now

                // Create the Company object
                //Company company = new Company(user.getUid(), email, name, address, phoneNumber, description, null);


                // Save the company information to Firebase
                DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference().child("Companies");
                String companyId = companyRef.push().getKey();
                company.setId(companyId);
                /*
                companyRef.child(companyId).setValue(company)
                //companyRef.push().setValue(company)
                //String key = companyRef.push().getKey();
                //companyRef.child(key).setValue(company)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    // Company information saved successfully
                                    Toast.makeText(getActivity(), "Company registered successfully", Toast.LENGTH_SHORT).show();
                                    Map<String, ?> allEventCategoryValues = sharedPreferencesEventCategory.getAll();

                                    List<String> eventIds = new ArrayList<>();

                                    for (Map.Entry<String, ?> entry : allEventCategoryValues.entrySet()) {
                                        // Cast the value to String and add it to the list
                                        String eventId = (String) entry.getKey(); // Assuming the values are Strings
                                        eventIds.add(eventId);
                                    }
                                    Map<String, ?> allProductCategoryValues = sharedPreferencesProductCategory.getAll();


                                    List<String> productIds = new ArrayList<>();


                                    for (Map.Entry<String, ?> entry : allProductCategoryValues.entrySet()) {
                                        // Cast the value to String and add it to the list
                                        String productId = (String) entry.getKey(); // Assuming the values are Strings
                                        productIds.add(productId);
                                    }
                                    // Create a new CompanyCategories object
                                    CompanyCategories companyCategories = new CompanyCategories();
                                    companyCategories.setCompanyId(companyId); // Assuming companyId is already defined

                                    // Set the eventIds and productIds
                                    companyCategories.setEventIds(eventIds);
                                    companyCategories.setProductIds(productIds);

                                    // Save the CompanyCategories object to Firebase
                                    DatabaseReference companyCategoriesRef = FirebaseDatabase.getInstance().getReference().child("CompanyCategories");
                                    companyCategoriesRef.push().setValue(companyCategories)
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        // Company categories saved successfully
                                                        Toast.makeText(getActivity(), "Company categories saved successfully", Toast.LENGTH_SHORT).show();
                                                        SharedPreferences.Editor editor = sharedPreferencesEventCategory.edit();
                                                        editor.clear(); // Remove all data
                                                        editor.apply();

                                                        editor = sharedPreferencesProductCategory.edit();
                                                        editor.clear(); // Remove all data
                                                        editor.apply();

                                                        // Replace this with the actual company ID
                                                        CompanyWorkHours companyWorkHours = new CompanyWorkHours();
                                                        companyWorkHours.setCompanyId(companyId);
                                                        companyWorkHours.setDailyWorkHoursList(new ArrayList<>(selectedWorkHoursMap.values()));

                                                        // Save the company work hours to Firebase
                                                        DatabaseReference companyWorkHoursRef = FirebaseDatabase.getInstance().getReference().child("CompanyWorkHours");
                                                        companyWorkHoursRef.push().setValue(companyWorkHours)
                                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        if (task.isSuccessful()) {
                                                                            // Company work hours saved successfully
                                                                            Toast.makeText(getActivity(), "Company work hours saved successfully", Toast.LENGTH_SHORT).show();
                                                                            // Clear the selected work hours map for the next entry
                                                                            selectedWorkHoursMap.clear();
                                                                            startActivity(new Intent(requireActivity(), MainActivity.class));
                                                                            requireActivity().finish();
                                                                        } else {
                                                                            // Failed to save company work hours
                                                                            Toast.makeText(getActivity(), "Failed to save company work hours", Toast.LENGTH_SHORT).show();
                                                                        }
                                                                    }
                                                                });


                                                    } else {
                                                        // Failed to save company categories
                                                        Toast.makeText(getActivity(), "Failed to save company categories", Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                } else {
                                    // Failed to save company information
                                    Toast.makeText(getActivity(), "Failed to register company", Toast.LENGTH_SHORT).show();
                                }
                */

                companyRef.child(companyId).setValue(company).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            // Retrieve and save event and product categories
                            Map<String, ?> allEventCategoryValues = sharedPreferencesEventCategory.getAll();
                            List<String> eventIds = new ArrayList<>();
                            for (Map.Entry<String, ?> entry : allEventCategoryValues.entrySet()) {
                                eventIds.add(entry.getKey());

                            }

                            Map<String, ?> allProductCategoryValues = sharedPreferencesProductCategory.getAll();
                            List<String> productIds = new ArrayList<>();
                            for (Map.Entry<String, ?> entry : allProductCategoryValues.entrySet()) {
                                productIds.add(entry.getKey());
                            }

                            // Create and save CompanyCategories
                            CompanyCategories companyCategories = new CompanyCategories();
                            companyCategories.setCompanyId(companyId);
                            companyCategories.setEventIds(eventIds);
                            companyCategories.setProductIds(productIds);

                            DatabaseReference companyCategoriesRef = FirebaseDatabase.getInstance().getReference().child("CompanyCategories");
                            companyCategoriesRef.push().setValue(companyCategories).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        // Clear SharedPreferences
                                        SharedPreferences.Editor editor = sharedPreferencesEventCategory.edit();
                                        editor.clear();
                                        editor.apply();

                                        editor = sharedPreferencesProductCategory.edit();
                                        editor.clear();
                                        editor.apply();

                                        // Create and save CompanyWorkHours
                                        CompanyWorkHours companyWorkHours = new CompanyWorkHours();
                                        companyWorkHours.setCompanyId(companyId);
                                        companyWorkHours.setDailyWorkHoursList(new ArrayList<>(selectedWorkHoursMap.values()));

                                        DatabaseReference companyWorkHoursRef = FirebaseDatabase.getInstance().getReference().child("CompanyWorkHours");
                                        companyWorkHoursRef.push().setValue(companyWorkHours).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    // Clear selected work hours map
                                                    selectedWorkHoursMap.clear();

                                                    // Create and save PUPVAccountRequest
                                                    PUPVAccountRequest accountRequest = new PUPVAccountRequest();
                                                    accountRequest.setId(companyId);
                                                    accountRequest.setCompany(company);
                                                    accountRequest.setCompanyWorkHours(companyWorkHours);
                                                    accountRequest.setCompanyCategories(companyCategories);
                                                    accountRequest.setUserPUPVId(user.getUid()); // Assuming 'user' is the current user


                                                    DatabaseReference requestRef = FirebaseDatabase.getInstance().getReference().child("PUPVAccountRequests");
                                                    requestRef.child(companyId).setValue(accountRequest).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if (task.isSuccessful()) {
                                                                // All data saved successfully
                                                                Toast.makeText(getActivity(), "Company registered successfully", Toast.LENGTH_SHORT).show();

                                                                startActivity(new Intent(requireActivity(), MainActivity.class));
                                                                requireActivity().finish();
                                                            } else {
                                                                // Failed to save PUPVAccountRequest
                                                                Toast.makeText(getActivity(), "Failed to save account request", Toast.LENGTH_SHORT).show();
                                                            }
                                                        }
                                                    });
                                                } else {
                                                    // Failed to save CompanyWorkHours
                                                    Toast.makeText(getActivity(), "Failed to save company work hours", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        });
                                    } else {
                                        // Failed to save CompanyCategories
                                        Toast.makeText(getActivity(), "Failed to save company categories", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        } else {
                            // Failed to save Company
                            Toast.makeText(getActivity(), "Failed to register company", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        // Set onClickListener for each day button to show time picker dialog
        Button mondayButton = view.findViewById(R.id.buttonMonday);
        Button tuesdayButton = view.findViewById(R.id.buttonTuesday);
        Button wednesdayButton = view.findViewById(R.id.buttonWednesday);
        Button thursdayButton = view.findViewById(R.id.buttonThursday);
        Button fridayButton = view.findViewById(R.id.buttonFriday);
        Button saturdayButton = view.findViewById(R.id.buttonSaturday);
        Button sundayButton = view.findViewById(R.id.buttonSunday);

        mondayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mondayCounter < 2) {
                    showTimePickerDialog("Monday", textViewSelectedHours);
                    mondayCounter++;
                } else {
                    mondayButton.setEnabled(false);
                }
            }
        });

        tuesdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tuesdayCounter < 2) {
                    showTimePickerDialog("Tuesday", textViewSelectedHours);
                    tuesdayCounter++;
                } else {
                    tuesdayButton.setEnabled(false);
                }
            }
        });

        wednesdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wednesdayCounter < 2) {
                    showTimePickerDialog("Wednesday", textViewSelectedHours);
                    wednesdayCounter++;
                } else {
                    wednesdayButton.setEnabled(false);
                }
            }
        });

        thursdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (thursdayCounter < 2) {
                    showTimePickerDialog("Thursday", textViewSelectedHours);
                    thursdayCounter++;
                } else {
                    thursdayButton.setEnabled(false);
                }
            }
        });

        fridayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fridayCounter < 2) {
                    showTimePickerDialog("Friday", textViewSelectedHours);
                    fridayCounter++;
                } else {
                    fridayButton.setEnabled(false);
                }
            }
        });

        saturdayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saturdayCounter < 2) {
                    showTimePickerDialog("Saturday", textViewSelectedHours);
                    saturdayCounter++;
                } else {
                    saturdayButton.setEnabled(false);
                }
            }
        });

        sundayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sundayCounter < 2) {
                    showTimePickerDialog("Sunday", textViewSelectedHours);
                    sundayCounter++;
                } else {
                    sundayButton.setEnabled(false);
                }
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    // Function to show time picker dialog as a popup
    private void showTimePickerDialog(String day, TextView textViewSelectedHours) {
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        TimePickerDialog timePickerDialog = new TimePickerDialog(
                getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        if (hourOfDay == -1 && minute == -1) {
                            // User selected this day as a non-working day
                            updateSelectedHours(day, "Non-working", textViewSelectedHours);
                        } else {
                            // Handle time selection
                            String selectedTime = hourOfDay + ":" + minute;
                            // Update UI with selected time
                            updateSelectedHours(day, selectedTime, textViewSelectedHours);
                            // Add selected work hours to the map
                            selectedWorkHoursMap.put(day, new DailyWorkHours(day, selectedTime, selectedTime));
                        }
                    }
                },
                hour,
                minute,
                true
        );

        // Add an option for non-working day
        timePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Non-working", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_NEGATIVE) {
                    // User selected this day as a non-working day
                    updateSelectedHours(day, "Non-working", textViewSelectedHours);
                    // Add selected work hours to the map
                    selectedWorkHoursMap.put(day, new DailyWorkHours(day, "Non-working", "Non-working"));
                }
            }
        });

        timePickerDialog.show();
    }

    // Function to update the UI with selected working hours for each day
    private void updateSelectedHours(String day, String time, TextView textViewSelectedHours) {
        String currentText = textViewSelectedHours.getText().toString();
        String newText = currentText + "\n" + day + ": " + time;
        textViewSelectedHours.setText(newText);
    }

    private void displayCategoriesForPage(int page) {
        int startIndex = page * 5;
        int endIndex = Math.min(startIndex + 5, eventCategories.size());

        List<CheckBox> checkBoxes;
        List<EventCategory> temp = new ArrayList<>();

        for (int i = startIndex; i < endIndex; i++) {
            temp.add(eventCategories.get(i));
        }

        checkBoxes = populateCategories(temp);


    }

    private List<CheckBox> populateCategories(List<EventCategory> categories) {
        List<CheckBox> ret = new ArrayList<>();
        categoriesLayout.removeAllViews();

        // Iterate through the categories list
        for (EventCategory category : categories) {
            // Create a CheckBox for each category
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setText(category.getName());

            // Retrieve the saved state for this category
            boolean isChecked = sharedPreferencesEventCategory.getBoolean(category.getId(), false);
            checkBox.setChecked(isChecked);

            // Add an OnCheckedChangeListener to update the SharedPreferences when checkbox state changes
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // Save the checkbox state to SharedPreferences
                    SharedPreferences.Editor editor = sharedPreferencesEventCategory.edit();
                    editor.putBoolean(category.getId(), isChecked);
                    editor.apply();
                }
            });

            // Add the CheckBox to the LinearLayout
            categoriesLayout.addView(checkBox);
            ret.add(checkBox);
        }
        return ret;
    }


    private List<CheckBox> populateCategories1(List<ProductCategory> categories) {
        List<CheckBox> ret = new ArrayList<>();
        productsLayout.removeAllViews();

        // Iterate through the categories list
        for (ProductCategory category : categories) {
            // Create a CheckBox for each category
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setText(category.getName());

            // Retrieve the saved state for this category
            boolean isChecked = sharedPreferencesProductCategory.getBoolean(category.getId(), false);
            checkBox.setChecked(isChecked);

            // Add an OnCheckedChangeListener to update the SharedPreferences when checkbox state changes
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // Save the checkbox state to SharedPreferences
                    SharedPreferences.Editor editor = sharedPreferencesProductCategory.edit();
                    editor.putBoolean(category.getId(), isChecked);
                    editor.apply();
                }
            });

            // Add the CheckBox to the LinearLayout
            productsLayout.addView(checkBox);
            ret.add(checkBox);
        }
        return ret;
    }

    private void displayCategoriesForPage1(int page) {
        int startIndex = page * 5;
        int endIndex = Math.min(startIndex + 5, productCategories.size());

        List<CheckBox> checkBoxes;
        List<ProductCategory> temp = new ArrayList<>();

        for (int i = startIndex; i < endIndex; i++) {
            temp.add(productCategories.get(i));
        }

        checkBoxes = populateCategories1(temp);


    }



}
