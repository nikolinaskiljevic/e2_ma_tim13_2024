package com.example.eventplanner.fragments;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CretePackage#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CretePackage extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private DatabaseReference productsRef;
    private DatabaseReference servicesRef;
    private Spinner categorySpinner;
    private Button buttonSelectProductsAndServices;
    private List<Product> productList = new ArrayList<>();
    private List<Service> serviceList= new ArrayList<>();
    private Package createdPackage = new Package();
    List<Product> selectedProducts = new ArrayList<>();
    List<Service> selectedServices = new ArrayList<>();
    List<String> images = new ArrayList<>();
    List<String> subcategories = new ArrayList<>();
    List<String> eventTypes = new ArrayList<>();
    private Company currentCompany;
    private List<Company> allCompanies = new ArrayList<>();

    private double totalPrice;

    public CretePackage() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CretePackage.
     */
    // TODO: Rename and change types and number of parameters
    public static CretePackage newInstance(String param1, String param2) {
        CretePackage fragment = new CretePackage();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inicijalizujemo DatabaseReference objekte za kolekcije "products" i "services"
        productsRef = FirebaseDatabase.getInstance().getReference("products");
        servicesRef = FirebaseDatabase.getInstance().getReference("services");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_crete_package, container, false);
        Spinner spinnerCategory = view.findViewById(R.id.category);

        getAllCompanies();

        // Inicijalizacija polja
        EditText editTextPackageName = view.findViewById(R.id.editTextPackageName);
        EditText editTextDescription = view.findViewById(R.id.editTextDescription);
        EditText editTextDiscount = view.findViewById(R.id.editTextDiscount);
        CheckBox checkBoxVisibility = view.findViewById(R.id.checkBoxVisibility);
        CheckBox checkBoxAvailability = view.findViewById(R.id.checkBoxAvailability);
        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(requireContext(),
                R.array.category_options, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(categoryAdapter);

        // Čitanje podataka o proizvodima iz Firebase baze podataka
        productsRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                productList.clear(); // Clear the product list before adding new ones from the database

                // Iterate through all products in the "products" collection
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product product = snapshot.getValue(Product.class);
                    productList.add(product);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error if there is a problem reading data from the database
                Toast.makeText(getContext(), "Error reading products from the database", Toast.LENGTH_SHORT).show();
            }
        });

        // Čitanje podataka o uslugama iz Firebase baze podataka
        servicesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                serviceList.clear(); // Očistimo prethodne podatke
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Service service = snapshot.getValue(Service.class);
                    serviceList.add(service); // Dodamo uslugu u listu
                }
                // Sada imamo sve usluge u listi services
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka
                Toast.makeText(getContext(), "Greška prilikom čitanja usluga iz baze podataka!", Toast.LENGTH_SHORT).show();
            }
        });

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCategory = parent.getItemAtPosition(position).toString();
                ArrayAdapter<CharSequence> subcategoryAdapter;

                switch (selectedCategory) {
                    case "Category: Food and catering":
                        subcategoryAdapter = ArrayAdapter.createFromResource(requireContext(),
                                R.array.subcategory_food_options, android.R.layout.simple_spinner_item);
                        break;
                    case "Photo and video":
                        subcategoryAdapter = ArrayAdapter.createFromResource(requireContext(),
                                R.array.subcategory_photo_options, android.R.layout.simple_spinner_item);
                        break;
                    default:
                        // Ukoliko nema odgovarajuće kategorije, koristimo praznu listu
                        subcategoryAdapter = new ArrayAdapter<>(requireContext(),
                                android.R.layout.simple_spinner_item);
                        break;
                }


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

            });

        buttonSelectProductsAndServices = view.findViewById(R.id.buttonSelectProductsAndServices);
        buttonSelectProductsAndServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                

                showSelectionDialog();
            }
        });


        for (Product product : selectedProducts) {

            totalPrice += product.getPrice();
        }

        for (Service service : selectedServices) {
            totalPrice += service.getPrice();
            Log.d("CIjena servica: ",String.valueOf(service.getPrice()));

        }


        createdPackage.setPrice(totalPrice);



        for (Product product : selectedProducts) {
            if(product.getImagePaths() != null){
                for(String image : product.getImagePaths()){
                    images.add(image);
                }
            }
        }
        for (Service product : selectedServices) {
            if(product.getImages() != null){
                for(String image : product.getImages()){
                    images.add(image);
                }
            }
        }
        createdPackage.setImages(images);


        for (Product product : selectedProducts) {
            subcategories.add(product.getSubcategory());
        }

// Prolazak kroz izabrane usluge i dodavanje njihovih potkategorija
        for (Service service : selectedServices) {
            subcategories.add(service.getSubcategory());
        }

// Postavljanje liste potkategorija u paket
        createdPackage.setSubcategories(subcategories);


        // Prolazak kroz izabrane proizvode i dodavanje njihovih tipova događaja
        for (Product product : selectedProducts) {
            eventTypes.add(product.getEventTypes());
        }

// Prolazak kroz izabrane usluge i dodavanje njihovih tipova događaja
        for (Service service : selectedServices) {
            eventTypes.addAll(service.getEventTypes());
        }

// Postavljanje liste tipova događaja u paket
        createdPackage.setEventTypes(eventTypes);

        //--------------------------------------------[
        String minBookingDeadlineStr = "1 dana pred"; // Postavite na visoku inicijalnu vrijednost
        int minBookingDeadline = Integer.MAX_VALUE;

// Prolazak kroz izabrane usluge i pronalaženje najmanjeg broja dana
        for (Service service : selectedServices) {
            String bookingDeadlineStr = service.getBookingDeadline();
            int daysBefore = Integer.parseInt(bookingDeadlineStr.split(" ")[0]);
            if (daysBefore < minBookingDeadline) {
                minBookingDeadline = daysBefore;
                minBookingDeadlineStr = bookingDeadlineStr;
            }
        }

// Postavljanje najkraćeg roka za rezervaciju u paket
        createdPackage.setReservationDeadline(minBookingDeadlineStr);
        String minCancellationDeadlineStr = "1 dana pred"; // Postavite na visoku inicijalnu vrijednost
        int minCancellationDeadline = Integer.MAX_VALUE;

// Prolazak kroz izabrane usluge i pronalaženje najmanjeg broja dana
        for (Service service : selectedServices) {
            String cancellationDeadlineStr = service.getCancellationDeadline();
            int daysBefore = Integer.parseInt(cancellationDeadlineStr.split(" ")[0]);
            if (daysBefore < minCancellationDeadline) {
                minCancellationDeadline = daysBefore;
                minCancellationDeadlineStr = cancellationDeadlineStr;
            }
        }

// Postavljanje najkraćeg roka za otkazivanje rezervacije u paket
        createdPackage.setCancellationDeadline(minCancellationDeadlineStr);


        boolean hasReservationService = false;

        for (Service service : selectedServices) {
            if (service.getReservationConfirmation().equals("Manual")) {
                hasReservationService = true;
                break; // Ako je pronađena usluga koja se rezerviše ručno, prekinite petlju
            }
        }
        RadioGroup radioGroupConfirmation = view.findViewById(R.id.radioGroupConfirmation);
        RadioButton radioButtonManual = view.findViewById(R.id.radioButtonManual);
        RadioButton radioButtonAutomatic = view.findViewById(R.id.radioButtonAutomatic);

        if (hasReservationService) {
            // Ako postoji usluga koja se rezerviše ručno, selektujte manualni RadioButton
            radioButtonManual.setChecked(true);
        } else {
            // Inače selektujte automatski RadioButton
            radioButtonAutomatic.setChecked(true);
        }
// Pronalaženje dugmeta Submit u vašem fragmentu
        Button submitButton = view.findViewById(R.id.buttonSubmit);

// Postavljanje OnClickListener-a za dugme Submit
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ovde ide kod za kreiranje i čuvanje paketa u bazi podataka

                // Referenca na tabelu packages u bazi podataka
                DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("packages");

                // Kreiranje novog objekta Package sa svim potrebnim informacijama

                String packageId = databaseRef.push().getKey();
                createdPackage.setId(packageId);
                createdPackage.setName(editTextPackageName.getText().toString().trim());
                createdPackage.setDescription(editTextDescription.getText().toString().trim());
                createdPackage.setCompanyId(currentCompany.getId());
                setPriceForPackage();
                setSubcategoriesForPakage();
                // Postavite ostale informacije o paketu...

                // Dodavanje izabranih proizvoda i usluga u paket

                // Čuvanje novog paketa u bazi podataka
                databaseRef.child(packageId).setValue(createdPackage)
                        .addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Toast.makeText(getContext(), "Package created successfully!", Toast.LENGTH_SHORT).show();
                                // Navigacija nazad ili na drugi ekran ako je potrebno
                            } else {
                                Toast.makeText(getContext(), "Failed to create package!", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });


        return view;
    }

    private void getAllCompanies() {
        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference("Companies");

        companyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allCompanies.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Company c = snapshot.getValue(Company.class);
                    if (c != null) {
                        allCompanies.add(c);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findCompany();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllCompanies", "Failed to read companies", databaseError.toException());
            }
        });
    }

    private void findCompany() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        for(Company c: allCompanies) {
            if(c.getPupvId().equals(currentUser.getUid())) {
                currentCompany = c;
                break;
            }
        }
    }

    private void showSelectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setTitle("Select Products and Services");

        // Pretvorite productList i serviceList u niz stringova kako biste ih prikazali u dijalogu sa više izbora
        CharSequence[] productNames = getProductNames(productList);
        CharSequence[] serviceNames = getServiceNames(serviceList);


        // Kreiranje nove liste koja će sadržati sve elemente iz obe liste
        List<CharSequence> combinedList = new ArrayList<>();

// Dodavanje elemenata iz prve liste (proizvodi)
        for (CharSequence productName : productNames) {
            combinedList.add(productName);
        }

// Dodavanje elemenata iz druge liste (usluge)
        for (CharSequence serviceName : serviceNames) {
            combinedList.add(serviceName);
        }

// Kreiranje niza boolean vrednosti za označavanje elemenata
        boolean[] selectedItems = new boolean[combinedList.size()];


        // Implementacija logike za dodavanje izabranih proizvoda i usluga u listu proizvoda i usluga paketa
        builder.setMultiChoiceItems(combinedList.toArray(new CharSequence[0]), selectedItems, (dialog, which, isChecked) -> {
            // Provera da li je izabrana stavka proizvod
            if (which < productNames.length) {
                // Indeks izabrane stavke u nizu productNames
                int productIndex = which;
                // Dodavanje izabranog proizvoda u listu selectedProducts klase Package
                if (isChecked) {
                    selectedProducts.add(productList.get(productIndex));
                } else {
                    selectedProducts.remove(productList.get(productIndex));
                }
            } else {
                // Indeks izabrane stavke u nizu serviceNames
                int serviceIndex = which - productNames.length;
                // Dodavanje izabrane usluge u listu selectedServices klase Package
                if (isChecked) {
                    selectedServices.add(serviceList.get(serviceIndex));
                } else {
                    selectedServices.remove(serviceList.get(serviceIndex));
                }
            }
        });



        builder.setPositiveButton("OK", (dialog, which) -> {
            // Ovde dodajte logiku za obradu odabranih proizvoda i usluga
            createdPackage.setSelectedProducts(selectedProducts);
            createdPackage.setSelectedServices(selectedServices);
        });


        for (Product product : selectedProducts) {
            subcategories.add(product.getSubcategory());
        }

// Prolazak kroz izabrane usluge i dodavanje njihovih potkategorija
        for (Service service : selectedServices) {
            subcategories.add(service.getSubcategory());
        }

// Postavljanje liste potkategorija u paket
        createdPackage.setSubcategories(subcategories);


        // Prolazak kroz izabrane proizvode i dodavanje njihovih tipova događaja
        for (Product product : selectedProducts) {
            eventTypes.add(product.getEventTypes());
        }

// Prolazak kroz izabrane usluge i dodavanje njihovih tipova događaja
        for (Service service : selectedServices) {
            eventTypes.addAll(service.getEventTypes());
        }

// Postavljanje liste tipova događaja u paket
        createdPackage.setEventTypes(eventTypes);

        //--------------------------------------------[
        String minBookingDeadlineStr = "9999 dana pred"; // Postavite na visoku inicijalnu vrijednost
        int minBookingDeadline = Integer.MAX_VALUE;

// Prolazak kroz izabrane usluge i pronalaženje najmanjeg broja dana
        for (Service service : selectedServices) {
            String bookingDeadlineStr = service.getBookingDeadline();
            int daysBefore = Integer.parseInt(bookingDeadlineStr.split(" ")[0]);
            if (daysBefore < minBookingDeadline) {
                minBookingDeadline = daysBefore;
                minBookingDeadlineStr = bookingDeadlineStr;
            }
        }

// Postavljanje najkraćeg roka za rezervaciju u paket
        createdPackage.setReservationDeadline(minBookingDeadlineStr);
        String minCancellationDeadlineStr = "9999 dana pred"; // Postavite na visoku inicijalnu vrijednost
        int minCancellationDeadline = Integer.MAX_VALUE;

// Prolazak kroz izabrane usluge i pronalaženje najmanjeg broja dana
        for (Service service : selectedServices) {
            String cancellationDeadlineStr = service.getCancellationDeadline();
            int daysBefore = Integer.parseInt(cancellationDeadlineStr.split(" ")[0]);
            if (daysBefore < minCancellationDeadline) {
                minCancellationDeadline = daysBefore;
                minCancellationDeadlineStr = cancellationDeadlineStr;
            }
        }

// Postavljanje najkraćeg roka za otkazivanje rezervacije u paket
        createdPackage.setCancellationDeadline(minCancellationDeadlineStr);


        boolean hasReservationService = false;

        for (Service service : selectedServices) {
            if (service.getReservationConfirmation().equals("Manual")) {
                hasReservationService = true;
                break; // Ako je pronađena usluga koja se rezerviše ručno, prekinite petlju
            }
        }
        builder.setNegativeButton("Cancel", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // Metoda za pretvaranje liste proizvoda u niz stringova sa imenima proizvoda
    private CharSequence[] getProductNames(List<Product> productList) {
        List<CharSequence> names = new ArrayList<>();
        for (Product product : productList) {
            names.add(product.getName());
            Log.d("Nadjem: " , product.getName());
            Log.d("Nadjem: " , product.getName());
            Log.d("Nadjem: " , product.getName());
        }
        return names.toArray(new CharSequence[0]);
    }

    // Metoda za pretvaranje liste usluga u niz stringova sa imenima usluga
    private CharSequence[] getServiceNames(List<Service> serviceList) {
        List<CharSequence> names = new ArrayList<>();
        for (Service service : serviceList) {
            names.add(service.getServiceName());
        }
        return names.toArray(new CharSequence[0]);
    }

public void setPriceForPackage(){
    // Prolazak kroz izabrane proizvode i dodavanje njihovih cijena
    for (Product product : selectedProducts) {
        totalPrice += product.getPrice();
    }

// Prolazak kroz izabrane usluge i dodavanje njihovih cijena
    for (Service service : selectedServices) {
        totalPrice += service.getPrice();
    }


    createdPackage.setPrice(totalPrice);
}


public void setSubcategoriesForPakage(){

    for (Product product : selectedProducts) {
        if(product.getImagePaths() != null){
            for(String image : product.getImagePaths()){
                images.add(image);
            }
        }
    }
    for (Service product : selectedServices) {
        if(product.getImages() != null){
            for(String image : product.getImages()){
                images.add(image);
            }
        }
    }
    createdPackage.setImages(images);


    for (Product product : selectedProducts) {
        subcategories.add(product.getSubcategory());
    }

// Prolazak kroz izabrane usluge i dodavanje njihovih potkategorija
    for (Service service : selectedServices) {
        subcategories.add(service.getSubcategory());
    }

// Postavljanje liste potkategorija u paket
    createdPackage.setSubcategories(subcategories);


    // Prolazak kroz izabrane proizvode i dodavanje njihovih tipova događaja
    for (Product product : selectedProducts) {
        eventTypes.add(product.getEventTypes());
    }

// Prolazak kroz izabrane usluge i dodavanje njihovih tipova događaja
    for (Service service : selectedServices) {
        eventTypes.addAll(service.getEventTypes());
    }

// Postavljanje liste tipova događaja u paket
    createdPackage.setEventTypes(eventTypes);

    //--------------------------------------------[
    String minBookingDeadlineStr = "9999 dana pred"; // Postavite na visoku inicijalnu vrijednost
    int minBookingDeadline = Integer.MAX_VALUE;
/*
// Prolazak kroz izabrane usluge i pronalaženje najmanjeg broja dana
    for (Service service : selectedServices) {
        String bookingDeadlineStr = service.getBookingDeadline();
        int daysBefore = Integer.parseInt(bookingDeadlineStr.split(" ")[0]);
        if (daysBefore < minBookingDeadline) {
            minBookingDeadline = daysBefore;
            minBookingDeadlineStr = bookingDeadlineStr;
        }
    }

// Postavljanje najkraćeg roka za rezervaciju u paket
    createdPackage.setReservationDeadline(minBookingDeadlineStr);
    String minCancellationDeadlineStr = "9999 dana pred"; // Postavite na visoku inicijalnu vrijednost
    int minCancellationDeadline = Integer.MAX_VALUE;

// Prolazak kroz izabrane usluge i pronalaženje najmanjeg broja dana
    for (Service service : selectedServices) {
        String cancellationDeadlineStr = service.getCancellationDeadline();
        int daysBefore = Integer.parseInt(cancellationDeadlineStr.split(" ")[0]);
        if (daysBefore < minCancellationDeadline) {
            minCancellationDeadline = daysBefore;
            minCancellationDeadlineStr = cancellationDeadlineStr;
        }
    }

// Postavljanje najkraćeg roka za otkazivanje rezervacije u paket
    createdPackage.setCancellationDeadline(minCancellationDeadlineStr);

*/
    boolean hasReservationService = false;

    for (Service service : selectedServices) {
        if (service.getReservationConfirmation().equals("Manual")) {
            hasReservationService = true;
            break; // Ako je pronađena usluga koja se rezerviše ručno, prekinite petlju
        }
    }


// Postavljanje OnClickListener-a za dugme Submit
}
}