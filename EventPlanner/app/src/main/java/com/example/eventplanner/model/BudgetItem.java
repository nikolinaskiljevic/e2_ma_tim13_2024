package com.example.eventplanner.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BudgetItem implements Serializable {
    private String id;
    private String eventId;
    private String organizerId;
    private String category;
    private String subcategory;
    private double plannedAmount;
    private double spentAmount;
    private List<String> reservedServicesNames;
    private List<String> reservedProductsNames;
    private String type;

    public BudgetItem() {
    }

    public BudgetItem(String eventId, String category, String subcategory, double plannedAmount, String type) {
        this.eventId = eventId;
        this.category = category;
        this.subcategory = subcategory;
        this.plannedAmount = plannedAmount;
        this.type = type;
        this.reservedServicesNames = new ArrayList<>();
        this.reservedServicesNames.add(""); //da bi ga sacuvalo u bazi jer je null
        this.reservedProductsNames = new ArrayList<>();
        this.reservedProductsNames.add("");
    }

    public double getPlannedAmount() {
        return plannedAmount;
    }

    public void setPlannedAmount(double plannedAmount) {
        this.plannedAmount = plannedAmount;
    }

    public double getSpentAmount() {
        return spentAmount;
    }

    public void setSpentAmount(double spentAmount) {
        this.spentAmount = spentAmount;
    }

    public List<String> getReservedServicesNames() {
        return reservedServicesNames;
    }

    public void setReservedServicesNames(List<String> reservedServicesNames) {
        this.reservedServicesNames = reservedServicesNames;
    }

    public List<String> getReservedProductsNames() {
        return reservedProductsNames;
    }

    public void setReservedProductsNames(List<String> reservedProductsNames) {
        this.reservedProductsNames = reservedProductsNames;
    }

    public String getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(String organizerId) {
        this.organizerId = organizerId;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}

