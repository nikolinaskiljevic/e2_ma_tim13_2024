package com.example.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.ChatFragment;
import com.example.eventplanner.fragments.CompanyProfileFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductReservation;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FavouritesProductsAdapter extends RecyclerView.Adapter<FavouritesProductsAdapter.ViewHolder> {

    private ArrayList<Product> allProducts;
    private ArrayList<Event> organizerEvents;
    private User currentUser;
    private Context context;

    public FavouritesProductsAdapter(Context context, ArrayList<Product> products, User user, ArrayList<Event> events) {
        this.context = context;
        this.allProducts = products;
        this.currentUser = user;
        this.organizerEvents = events;
    }

    @NonNull
    @Override
    public FavouritesProductsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.favourites_product_card, parent, false);
        return new FavouritesProductsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavouritesProductsAdapter.ViewHolder holder, int position) {
        Product p = allProducts.get(position);
        holder.tvProductName.setText(p.getName());
        holder.textViewCategory.setText(p.getCategory());
        holder.textViewSubcategory.setText(p.getSubcategory());
        holder.textViewDescription.setText("Description: " + p.getDescription());
        holder.textViewPrice.setText("Price: " + String.format("%.2f", p.getPrice()) + "$");
        holder.textViewDiscount.setText("Discount: " + String.format("%.2f", p.getDiscount()) + "%");
        holder.textViewEventTypes.setText("Event types: " + p.getEventTypes());
        holder.textViewAvailable.setText("Availability: " + (p.isAvailable() ? "Available" : "Unavailable"));
        holder.buttonBuyProduct.setText("Buy " + p.getName());
        setupSpinner(holder.spinnerOrganizerEvents);
        if(p.isAvailable()) {
            holder.buttonBuyProduct.setEnabled(true);
        }
        if(!currentUser.getFavouritesProductsIds().contains(p.getId())) {
            holder.buttonRemoveFromFavourites.setEnabled(false);
        }

        // Postavljanje slušača klika na dugme za prikaz detalja
        holder.buttonBuyProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event selectedEvent = findSelectedEvent(holder.spinnerOrganizerEvents);
                reserveProduct(p,selectedEvent);
            }
        });

        holder.buttonRemoveFromFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Product p = allProducts.get(position);
                    // Ovde radite šta je potrebno sa objektom product, na primer dodavanje u omiljene
                    removeFromFavourites(p);
                }
            }
        });

    }

    private void removeFromFavourites(Product p) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());
        List<String> favProducts = currentUser.getFavouritesProductsIds();
        favProducts.removeIf(prod -> prod.equals(p.getId()));
        currentUser.setFavouritesProductsIds(favProducts);

        userRef.setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText((FragmentActivity) context, "Removed!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText((FragmentActivity) context, "Removing error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void reserveProduct(Product product, Event event) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference reservationsRef = database.getReference("productReservations");

        String reservationId = reservationsRef.push().getKey();
        if (reservationId != null) {
            ProductReservation reservation = new ProductReservation(
                    reservationId,
                    product.getId(),
                    event.getId(), // Use selected event ID
                    currentUser.getUid(),
                    "pending" // Set initial status
            );

            reservationsRef.child(reservationId).setValue(reservation)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(context, "Service reserved successfully!", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Failed to reserve service. Please try again.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        } else {
            Toast.makeText(context, "Error generating reservation ID. Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    private Event findSelectedEvent(Spinner spinner) {
        String selected = "";
        if(spinner.getSelectedItem() != null) {
            selected = spinner.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selected);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            return findEvent(eventName);
        } else {
            System.out.println("Not founded event");
            return null;
        }
    }

    private Event findEvent(String eventName) {
        for(Event e: organizerEvents) {
            if(e.getEventName().equals(eventName)) {
                Toast.makeText((FragmentActivity) context, "Selected event: " + e.getEventName(), Toast.LENGTH_SHORT).show();
                return e;
            }
        }

        return null;
    }

    private void setupSpinner(Spinner spinner) {
        // Kreiramo listu imena dogadjaja
        List<String> events = new ArrayList<>();
        for (Event e : organizerEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>((FragmentActivity) context, android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinner.setAdapter(spinnerAdapter);
    }

    @Override
    public int getItemCount() {
        return allProducts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvProductName;
        public TextView textViewCategory;
        public TextView textViewSubcategory;
        public TextView textViewDescription;
        public TextView textViewPrice;
        public TextView textViewDiscount;
        public TextView textViewEventTypes;
        public TextView textViewAvailable;
        public Button buttonBuyProduct;
        public Button buttonRemoveFromFavourites;
        public Spinner spinnerOrganizerEvents;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvProductName = itemView.findViewById(R.id.tvProductName);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);
            textViewSubcategory = itemView.findViewById(R.id.textViewSubcategory);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
            textViewEventTypes = itemView.findViewById(R.id.textViewEventTypes);
            textViewAvailable = itemView.findViewById(R.id.textViewAvailable);
            buttonBuyProduct = itemView.findViewById(R.id.buttonBuyProduct);
            buttonRemoveFromFavourites = itemView.findViewById(R.id.buttonRemoveFromFavourites);
            spinnerOrganizerEvents = itemView.findViewById(R.id.spinnerOrganizerEvents);
        }
    }
}
