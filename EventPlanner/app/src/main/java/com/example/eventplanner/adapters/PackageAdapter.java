package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Package;

import java.util.List;

public class PackageAdapter  extends ArrayAdapter<Package> {
    private final Context context;
    private final List<Package> packageList;
    private final LayoutInflater inflater;
    private PackageAdapter.EditButtonClickListener editListener;
    private PackageAdapter.DeleteButtonClickListener deleteListener;



    public PackageAdapter(@NonNull Context context, int resource, @NonNull List<Package> productList) {
        super(context, resource, productList);
        this.context = context;
        this.packageList = productList;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        PackageAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_package_card, parent, false);
            holder = new PackageAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (PackageAdapter.ViewHolder) convertView.getTag();
        }

        Package packagee = packageList.get(position);

        // Bind data to views
        holder.bind(packagee);

        // Set click listeners
        holder.buttonEdit.setOnClickListener(v -> {
            if (editListener != null) {
                editListener.onEditButtonClick(packagee);
            }
        });

        holder.buttonDelete.setOnClickListener(v -> {
            if (deleteListener != null) {
                deleteListener.onDeleteButtonClick(packagee);
            }
        });

        return convertView;
    }

    public void setOnEditButtonClickListener(PackageAdapter.EditButtonClickListener listener) {
        this.editListener = listener;
    }

    public void setOnDeleteButtonClickListener(PackageAdapter.DeleteButtonClickListener listener) {
        this.deleteListener = listener;
    }

    static class ViewHolder {
        private ImageButton buttonEdit;
        private ImageButton buttonDelete;
        private TextView textViewPackageName;
        private TextView textViewPackageDescription;
        private TextView textViewPackagePrice;
        private TextView textViewPackageDiscount;
        private TextView textViewPackageImages;
        private TextView textViewPackageVisibility;
        private TextView textViewPackageAvailability;
        private TextView textViewPackageCategory;
        private TextView textViewPackageSelectedProducts;
        private TextView textViewPackageSelectedServices;
        private TextView textViewPackageSubcategories;
        private TextView textViewPackageEventTypes;
        private TextView textViewPackageReservationDeadline;
        private TextView textViewPackageCancellationDeadline;
        private TextView textViewPackageConfirmationMethod;
        ViewHolder(View itemView) {
            buttonEdit = itemView.findViewById(R.id.buttonEditPackage);
            buttonDelete = itemView.findViewById(R.id.buttonDeletePackage);
            textViewPackageName = itemView.findViewById(R.id.textViewPackageName);
            textViewPackageDescription = itemView.findViewById(R.id.textViewPackageDescription);
            textViewPackagePrice = itemView.findViewById(R.id.textViewPackagePrice);
            textViewPackageDiscount = itemView.findViewById(R.id.textViewPackageDiscount);
            textViewPackageImages = itemView.findViewById(R.id.textViewPackageImages);
            textViewPackageVisibility = itemView.findViewById(R.id.textViewPackageVisibility);
            textViewPackageAvailability = itemView.findViewById(R.id.textViewPackageAvailability);
            textViewPackageCategory = itemView.findViewById(R.id.textViewPackageCategory);
            textViewPackageSelectedProducts = itemView.findViewById(R.id.textViewPackageSelectedProducts);
            textViewPackageSelectedServices = itemView.findViewById(R.id.textViewPackageSelectedServices);
            textViewPackageSubcategories = itemView.findViewById(R.id.textViewPackageSubcategories);
            textViewPackageEventTypes = itemView.findViewById(R.id.textViewPackageEventTypes);
            textViewPackageReservationDeadline = itemView.findViewById(R.id.textViewPackageReservationDeadline);
            textViewPackageCancellationDeadline = itemView.findViewById(R.id.textViewPackageCancellationDeadline);
            textViewPackageConfirmationMethod = itemView.findViewById(R.id.textViewPackageConfirmationMethod);
        }

        void bind(Package p) {
            // Bind data to views
            textViewPackageName.setText(p.getName());
            textViewPackageDescription.setText(p.getDescription());
            textViewPackagePrice.setText(String.valueOf(p.getPrice()));
            textViewPackageDiscount.setText(String.valueOf(p.getDiscount()));
            //textViewPackageImages.setText(p.getImages().toString());
            textViewPackageVisibility.setText(String.valueOf(p.isVisible()));
            textViewPackageAvailability.setText(String.valueOf(p.isAvailable()));
            textViewPackageCategory.setText(p.getCategory());
            //textViewPackageSelectedProducts.setText(p.getSelectedProducts().toString());
            //textViewPackageSelectedServices.setText(p.getSelectedServices().toString());
            //textViewPackageSubcategories.setText(p.getSubcategories().toString());
            //textViewPackageEventTypes.setText(p.getEventTypes().toString());
            textViewPackageReservationDeadline.setText(p.getReservationDeadline());
            textViewPackageCancellationDeadline.setText(p.getCancellationDeadline());
            textViewPackageConfirmationMethod.setText(p.getConfirmationMethod());
        }
    }

    public interface EditButtonClickListener {
        void onEditButtonClick(Package pa);
    }

    public interface DeleteButtonClickListener {
        void onDeleteButtonClick(Package product);
    }

}
