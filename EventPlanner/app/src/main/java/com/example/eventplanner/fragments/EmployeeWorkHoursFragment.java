package com.example.eventplanner.fragments;

import static com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.DailyWorkHours;
import com.example.eventplanner.model.EmployeeWorkPeriod;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WeeklyWorkHours;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import android.util.TypedValue;

public class EmployeeWorkHoursFragment extends Fragment {

    private List<EmployeeWorkPeriod> employeeWorkPeriodList = new ArrayList<>();
    private TableLayout tableLayout;
    private TableLayout table2Layout;
    private List<String> workhoursList;

    private  String employeeId;

    private String fullName;

    private List<WeeklyWorkHours> weeklyWorkHoursList;

    private User user;

    public EmployeeWorkHoursFragment() {
        // Required empty public constructor
    }

    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_employee_work_hours, container, false);


        Bundle bundle = getArguments();
        if (bundle != null) {
             employeeId = bundle.getString("employeeId");

        }

        user=new User();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        tableLayout = view.findViewById(R.id.tableLayout);
        table2Layout = view.findViewById(R.id.table2Layout);
        workhoursList=new ArrayList<>();
        weeklyWorkHoursList= new ArrayList<>();
        //LOGOVANI
        getEmployeePeriod(employeeId);


        return view;
    }

    public void getEmployeePeriod(String employeeId){
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("employeeWorkPeriod");
        Log.e(TAG, "USAOO");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                employeeWorkPeriodList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EmployeeWorkPeriod employeeWorkPeriod = snapshot.getValue(EmployeeWorkPeriod.class);
                    if (employeeWorkPeriod != null) {
                        employeeWorkPeriodList.add(employeeWorkPeriod);
                        Log.e(TAG, "USAOO2" + employeeWorkPeriodList);
                    }
                }
                // Ovdje možete ažurirati UI s novim podacima
                updateTableUI();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error
                Log.e(TAG, "Error fetching data", databaseError.toException());
            }
        });
    }

    private void updateTableUI() {
        Log.e(TAG, "U metodi update " + employeeWorkPeriodList);
        for (EmployeeWorkPeriod period : employeeWorkPeriodList) {
            TableRow row = new TableRow(requireContext());

            // Dodavanje TextView za svaki atribut u tabeli

            TextView fromTextView = createTextView(period.getFrom());
            TextView toTextView = createTextView(period.getTo());
            TextView employeeTextView = createTextView(getEmployeeNameById(period.getEmployeesId().get(0))); // Prvi zaposlenik u periodu
            TextView whIdTextView = createTextView(period.getWeeklyWorkHoursId());

            if (!workhoursList.contains(period.getWeeklyWorkHoursId())) {
                workhoursList.add(period.getWeeklyWorkHoursId());
            }


            // Dodavanje TextView-ova u TableRow

            row.addView(fromTextView);
            row.addView(toTextView);
            row.addView(employeeTextView);
            row.addView(whIdTextView);

            // Dodavanje TableRow u TableLayout
            tableLayout.addView(row);
        }

        Log.e(TAG, "LISTA ID" + workhoursList);
        getWorkHoursFromDatabase();

    }

    private TextView createTextView(String text) {
        TextView textView = new TextView(requireContext());
        textView.setText(text);
        textView.setPadding(8, 8, 8, 8);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
        return textView;
    }

    private String getEmployeeNameById(String employeeId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersRef = databaseReference.child("Users").child(employeeId);


        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String firstName = dataSnapshot.child("firstName").getValue(String.class);
                String lastName = dataSnapshot.child("lastName").getValue(String.class);

                 fullName = firstName + " " + lastName;


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        return fullName;
    }


    private void getWorkHoursFromDatabase() {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("weeklyWorkHours");
        AtomicInteger counter = new AtomicInteger(0);

        for (String workHourId : workhoursList) {
            Query query = databaseReference.orderByChild("id").equalTo(workHourId);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        WeeklyWorkHours weeklyWorkHours = snapshot.getValue(WeeklyWorkHours.class);
                        if (weeklyWorkHours != null) {
                            Log.e(TAG, "U METODI");
                            weeklyWorkHoursList.add(weeklyWorkHours);
                            Log.e(TAG, "LISTAAA" + weeklyWorkHoursList);
                        }
                    }

                    int count = counter.incrementAndGet(); // Povećaj brojač i dohvati trenutnu vrijednost

                    // Provjeri da li su svi podaci dohvaćeni
                    if (count == workhoursList.size()) {
                        // Svi podaci su dohvaćeni, pozovi updateUI
                        updateTable2UI();
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Handle error
                    Log.e(TAG, "Error fetching data", databaseError.toException());
                }
            });
        }


        updateTable2UI();
    }

    private void updateTable2UI() {

        for (WeeklyWorkHours weekly : weeklyWorkHoursList) {
            TableRow row = new TableRow(requireContext());
            TextView id = create2TextView(weekly.getId());
            TextView monTextView = create2TextView(weekly.getDailyWorkHoursList().get(0).getFrom()+ "-"+ weekly.getDailyWorkHoursList().get(0).getTo());
            TextView tueTextView = create2TextView(weekly.getDailyWorkHoursList().get(1).getFrom()+"-"+weekly.getDailyWorkHoursList().get(1).getTo());
            TextView wedTextView = create2TextView(weekly.getDailyWorkHoursList().get(2).getFrom()+"-"+weekly.getDailyWorkHoursList().get(2).getTo());
            TextView thuTextView = create2TextView(weekly.getDailyWorkHoursList().get(3).getFrom()+"-"+weekly.getDailyWorkHoursList().get(3).getTo());
            TextView friTextView = create2TextView(weekly.getDailyWorkHoursList().get(4).getFrom()+"-"+weekly.getDailyWorkHoursList().get(4).getTo());

            row.addView(id);
            row.addView(monTextView);
            row.addView(tueTextView);
            row.addView(wedTextView);
            row.addView(thuTextView);
            row.addView(friTextView);

            // Dodavanje TableRow u TableLayout
            table2Layout.addView(row);
        }
    }

    private TextView create2TextView(String text) {
        TextView textView = new TextView(requireContext());
        textView.setText(text);
        textView.setPadding(8, 8, 8, 8);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 9); // Postavljanje font size na 14sp
        return textView;
    }


}
