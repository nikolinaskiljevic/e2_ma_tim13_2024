package com.example.eventplanner.model;

import java.util.Date;

public class CompanyGradeReport {
    public String report;

    public String companyGradeId;
    public Date date;
    public String pupv;
    public String reason;



    //REPORTED,ACCEPTED,REJECTED
    public String status;

    public CompanyGradeReport(){}

    public CompanyGradeReport(String companyGradeId, Date date, String pupv, String reason, String status) {
        this.companyGradeId = companyGradeId;
        this.date = date;
        this.pupv = pupv;
        this.reason = reason;
        this.status = status;
    }

    public String getCompanyGradeId() {
        return companyGradeId;
    }

    public void setCompanyGradeId(String companyGradeId) {
        this.companyGradeId = companyGradeId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPupv() {
        return pupv;
    }

    public void setPupv(String pupv) {
        this.pupv = pupv;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String Report) {
        report = Report;
    }
}
