package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackageDetailsAdapter;
import com.example.eventplanner.adapters.ProductDetailsAdapter;
import com.example.eventplanner.adapters.ServiceDetailsAdapter;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ProductDetailsAdapter productDetailsAdapter;
    private ServiceDetailsAdapter serviceDetailsAdapter;
    private PackageDetailsAdapter packageDetailsAdapter;
    private RadioGroup radioGroup;
    private RadioButton radioButtonService, radioButtonProduct, radioButtonPackage;
    private RecyclerView recyclerViewProducts, recyclerViewServices, recyclerViewPackages;
    private ArrayList<Service> allServices = new ArrayList<>();
    private ArrayList<Product> allProducts = new ArrayList<>();
    private ArrayList<Package> allPackages = new ArrayList<>();
    private ArrayList<User> allUsers = new ArrayList<>();
    private ArrayList<Event> allEvents = new ArrayList<>();
    private ArrayList<Event> organizerEvents = new ArrayList<>();
    private String userRole;
    private User loggedUser;

    public DetailsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsFragment newInstance(String param1, String param2) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        radioGroup = view.findViewById(R.id.radioGroupDisplayOptions);
        radioButtonService = view.findViewById(R.id.radioButtonDisplayServices);
        int radioButtonServiceId = radioButtonService.getId();
        radioButtonProduct = view.findViewById(R.id.radioButtonDisplayProducts);
        int radioButtonProductId = radioButtonProduct.getId();
        radioButtonPackage = view.findViewById(R.id.radioButtonDisplayPackages);

        recyclerViewServices = view.findViewById(R.id.recyclerViewServices);
        recyclerViewProducts = view.findViewById(R.id.recyclerViewProducts);
        recyclerViewPackages = view.findViewById(R.id.recyclerViewPackages);

        getAllUsers();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == radioButtonServiceId) {
                    recyclerViewServices.setVisibility(View.VISIBLE);
                    recyclerViewProducts.setVisibility(View.INVISIBLE);
                    recyclerViewPackages.setVisibility(View.INVISIBLE);
                }
                else if(checkedId == radioButtonProductId){
                    recyclerViewServices.setVisibility(View.GONE);
                    recyclerViewProducts.setVisibility(View.VISIBLE);
                    recyclerViewPackages.setVisibility(View.INVISIBLE);
                }
                else {
                    recyclerViewServices.setVisibility(View.GONE);
                    recyclerViewProducts.setVisibility(View.GONE);
                    recyclerViewPackages.setVisibility(View.VISIBLE);
                }

            }
        });

        return view;
    }

    private void getAllEvents() {
        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference("events");

        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allEvents.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Event e = snapshot.getValue(Event.class);
                    if (e != null) {
                        allEvents.add(e);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findOrganizerEvents();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllEvents", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void findOrganizerEvents() {
        organizerEvents.clear();
        for(Event e: allEvents) {
            if(e.getOrganizerId().equals(loggedUser.getUid())) {
                organizerEvents.add(e);
            }
        }

        getAllServices();
    }

    private void getAllUsers() {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allUsers.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    if (u != null) {
                        allUsers.add(u);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findUserRole();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllPackages", "Failed to read packages", databaseError.toException());
            }
        });
    }

    private void findUserRole() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(currentUser == null) {
            userRole = "nk";
        }
        else {
            for(User u: allUsers) {
                if(currentUser.getUid().equals(u.getUid())) {
                    loggedUser = u;
                    userRole = u.getRole();
                    break;
                }
            }
        }

        if(userRole.equals("organizer")) {
            getAllEvents();
        }
        else {
            getAllServices();
        }
    }

    private void getAllPackages() {
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference("packages");

        packageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allPackages.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Package p = snapshot.getValue(Package.class);
                    if (p != null) {
                        allPackages.add(p);
                    }
                }
                // Sada usersList sadrži sve korisnike
                filterPackageList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllPackages", "Failed to read packages", databaseError.toException());
            }
        });
    }

    private void filterPackageList() {
        if(userRole.equals("nk") || userRole.equals("organizer")) {
            allPackages.removeIf(p -> !p.isVisible());
        }
        setPackagesAdapter();
    }

    private void setPackagesAdapter() {
        recyclerViewPackages.setHasFixedSize(true);
        recyclerViewPackages.setLayoutManager(new LinearLayoutManager(getContext()));

        packageDetailsAdapter = new PackageDetailsAdapter(getContext(), allPackages, loggedUser, userRole, organizerEvents);
        recyclerViewPackages.setAdapter(packageDetailsAdapter);
    }

    private void getAllServices() {
        DatabaseReference serviceRef = FirebaseDatabase.getInstance().getReference("services");

        serviceRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allServices.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Service s = snapshot.getValue(Service.class);
                    if (s != null) {
                        allServices.add(s);
                    }
                }
                // Sada usersList sadrži sve korisnike
                filterServiceList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllServices", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void filterServiceList() {
        if(userRole.equals("nk") || userRole.equals("organizer")) {
            allServices.removeIf(s -> !s.isVisible());
        }
        setServicesAdapter();
    }

    private void setServicesAdapter() {
        recyclerViewServices.setHasFixedSize(true);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(getContext()));

        serviceDetailsAdapter = new ServiceDetailsAdapter(getContext(), allServices, loggedUser, userRole, organizerEvents);
        recyclerViewServices.setAdapter(serviceDetailsAdapter);

        getAllProducts();
    }

    private void getAllProducts() {
        DatabaseReference productRef = FirebaseDatabase.getInstance().getReference("products");

        productRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allProducts.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product p = snapshot.getValue(Product.class);
                    if (p != null) {
                        allProducts.add(p);
                    }
                }
                // Sada usersList sadrži sve korisnike
                filterProductList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllEvents", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void filterProductList() {
        if(userRole.equals("nk") || userRole.equals("organizer")) {
            allProducts.removeIf(p -> !p.isVisible());
        }
        setProductsAdapter();
    }

    private void setProductsAdapter() {
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getContext()));

        productDetailsAdapter = new ProductDetailsAdapter(getContext(), allProducts, loggedUser, userRole, organizerEvents);
        recyclerViewProducts.setAdapter(productDetailsAdapter);

        getAllPackages();
    }


}