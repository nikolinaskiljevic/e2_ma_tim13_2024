package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.eventplanner.R;


public class AdminProductsRequestsFragment extends Fragment {


    private LinearLayout layoutCategories;
    private EditText editTextCategoryName;
    private Button buttonAddCategory;

    public AdminProductsRequestsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_products_requests, container, false);

        layoutCategories = view.findViewById(R.id.layoutCategories);
        buttonAddCategory = view.findViewById(R.id.buttonAddCategory);

        buttonAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        // Add some example categories
        addCategory("Rođenja","Privatni događaji");
        addCategory("Networking","Korporativni događaji");

        return view;
    }

    private void addCategory(String categoryName,String info) {
        // Inflate category item layout
        View categoryView = getLayoutInflater().inflate(R.layout.item_category_request, null);
        TextView textViewCategoryName = categoryView.findViewById(R.id.textViewCategoryName);
        TextView textViewAdditionalInfo = categoryView.findViewById(R.id.textViewAdditionalInfo);

        Button buttonDelete = categoryView.findViewById(R.id.buttonDelete);

        // Set category name
        textViewCategoryName.setText(categoryName);
        textViewAdditionalInfo.setText(info);


        // Set padding between items
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 16); // Adjust the padding as needed
        categoryView.setLayoutParams(layoutParams);

        // Set click listeners for delete and info buttons
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle delete action
                layoutCategories.removeView(categoryView);
            }
        });



        // Add category view to the layout
        layoutCategories.addView(categoryView);
    }


}