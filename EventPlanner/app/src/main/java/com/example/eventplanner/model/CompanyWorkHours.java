package com.example.eventplanner.model;

import java.util.List;

public class CompanyWorkHours {
    private String id;

    private String companyId;
    private List<DailyWorkHours> dailyWorkHoursList;

    public CompanyWorkHours(String id, String companyId, List<DailyWorkHours> dailyWorkHoursList) {
        this.id = id;
        this.companyId = companyId;
        this.dailyWorkHoursList = dailyWorkHoursList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<DailyWorkHours> getDailyWorkHoursList() {
        return dailyWorkHoursList;
    }

    public void setDailyWorkHoursList(List<DailyWorkHours> dailyWorkHoursList) {
        this.dailyWorkHoursList = dailyWorkHoursList;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public CompanyWorkHours(String id, List<DailyWorkHours> dailyWorkHoursList) {
        this.id = id;
        this.dailyWorkHoursList = dailyWorkHoursList;
    }

    public CompanyWorkHours() {
    }
}
