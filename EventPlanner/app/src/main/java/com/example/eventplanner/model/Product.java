package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;

public class Product implements Parcelable {
    private String id;
    private String category;
    private String subcategory;
    private String name;
    private String description;
    private double price;
    private double discount;
    private double discountedPrice;
    private List<String> imagePaths;
    private String eventTypes;
    private boolean available;
    private boolean visible;
    private boolean deleted;
    private String companyId;

    public Product(){
        this.deleted = false;

    }

    // Constructor
    public Product(String category, String subcategory, String name, String description, double price, double discount, double discountedPrice, String gallery, String eventTypes, boolean available, boolean visible) {
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.discountedPrice = price-((100-discount)/100);
        this.eventTypes = eventTypes;
        this.available = available;
        this.visible = visible;
        this.deleted = false;
    }

    protected Product(Parcel in) {
        id = in.readString();
        category = in.readString();
        subcategory = in.readString();
        name = in.readString();
        description = in.readString();
        price = in.readDouble();
        discount = in.readInt();
        discountedPrice = in.readDouble();
        imagePaths = in.createStringArrayList();
        eventTypes = in.readString();
        available = in.readByte() != 0;
        visible = in.readByte() != 0;
        this.deleted = false;

    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public List<String> getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(List<String> imagePaths) {
        this.imagePaths = imagePaths;
    }

    public Product(String category, String subcategory, String name, String description, double price, int discount, String eventTypes, boolean available, boolean visible, List<String> imagePaths) {
        this.category = category;
        this.subcategory = subcategory;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.eventTypes = eventTypes;
        this.available = available;
        this.visible = visible;
        this.imagePaths = imagePaths;
        this.deleted = false;

    }

    // Getters and Setters


    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }



    public void setDiscountedPrice(double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public double getDiscountedPrice() {
        return  this.price-((100-this.discount)/100);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }







    public String getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(String eventTypes) {
        this.eventTypes = eventTypes;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(subcategory);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeDouble(discountedPrice);
        dest.writeStringList(imagePaths);
        dest.writeString(eventTypes);
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeByte((byte) (visible ? 1 : 0));
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}

