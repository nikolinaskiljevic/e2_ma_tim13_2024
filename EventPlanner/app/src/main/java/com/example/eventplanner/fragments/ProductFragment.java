package com.example.eventplanner.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.eventplanner.adapters.ProductAdapter;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ProductFragment extends Fragment implements EditProduct.OnProductUpdateListener {
    private ListView listView;
    private ProductAdapter productAdapter;
    private List<Product> products;

    public ProductFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);

        // Initialize ListView
        listView = view.findViewById(R.id.listView);

        // Initialize product list
        products = new ArrayList<>();

        // Initialize adapter
        productAdapter = new ProductAdapter(getContext(), R.layout.fragment_product_item, products);
        listView.setAdapter(productAdapter);

        productAdapter.setOnEditButtonClickListener(new ProductAdapter.EditButtonClickListener() {
            @Override
            public void onEditButtonClick(Product product) {
                // Handle edit button click here
                FragmentManager fragmentManager = getParentFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, EditProduct.newInstance(product));
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();


            }
        });

        productAdapter.setOnDeleteButtonClickListener(new ProductAdapter.DeleteButtonClickListener() {
            @Override
            public void onDeleteButtonClick(Product product) {

                deleteProduct(product);

            }
        });
        // Fetch products from the Firebase database
        fetchProductsFromDatabase();

        // Set item click listener for ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Handle item click if needed
            }
        });

        return view;
    }

    private void fetchProductsFromDatabase() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products");

        // Add listener to read data from the Firebase database
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                products.clear(); // Clear the product list before adding new ones from the database

                // Iterate through all products in the "products" collection
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product product = snapshot.getValue(Product.class);
                    if (!product.isDeleted()) { // Check if the product is not marked as deleted
                        products.add(product);
                    }
                }

                // Notify the adapter that the data has changed
                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Handle error if there is a problem reading data from the database
                Toast.makeText(getContext(), "Error reading products from the database", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onProductUpdated(Product updatedProduct) {
        // Implement this method if necessary to react to product updates
    }

    private void deleteProduct(Product product) {
        // Mark the product as deleted (logical deletion)
        product.setDeleted(true);

        // Notify database about the change
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products").child(String.valueOf(product.getId()));
        databaseRef.setValue(product);
    }

}
