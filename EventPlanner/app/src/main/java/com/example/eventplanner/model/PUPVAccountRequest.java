package com.example.eventplanner.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class PUPVAccountRequest {
    private String id;
    private Company company;
    private CompanyWorkHours companyWorkHours;
    private CompanyCategories companyCategories;
    private String userPUPVId;
    private String creationTime;

    // DateTimeFormatter for formatting the Instant to a string
    private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_INSTANT;

    // Constructor
    public PUPVAccountRequest() {
        // Initialize creationTime with the current time as a formatted string
        this.creationTime = formatter.format(Instant.now());
    }

    // Getter and Setter for creationTime
    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    // Other getters and setters for existing fields
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public CompanyWorkHours getCompanyWorkHours() {
        return companyWorkHours;
    }

    public void setCompanyWorkHours(CompanyWorkHours companyWorkHours) {
        this.companyWorkHours = companyWorkHours;
    }

    public CompanyCategories getCompanyCategories() {
        return companyCategories;
    }

    public void setCompanyCategories(CompanyCategories companyCategories) {
        this.companyCategories = companyCategories;
    }

    public String getUserPUPVId() {
        return userPUPVId;
    }

    public void setUserPUPVId(String userPUPVId) {
        this.userPUPVId = userPUPVId;
    }

    public long getCreationTimeMillis() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date date = sdf.parse(creationTime);
            return date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
            return 0; // Default to 0 if parsing fails
        }
    }
}
