package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Service;

import java.util.ArrayList;
import java.util.List;

public class ServiceAdapter extends ArrayAdapter<Service> {

    private final Context context;
    private final List<Service> serviceList;
    private final LayoutInflater inflater;
    private EditButtonClickListener editListener;
    private DeleteButtonClickListener deleteListener;

    public ServiceAdapter(@NonNull Context context) {
        super(context, R.layout.fragment_service_card);
        this.context = context;
        this.serviceList = new ArrayList<>();
        this.inflater = LayoutInflater.from(context);
    }
    public ServiceAdapter(@NonNull Context context, int resource, @NonNull List<Service> serviceList) {
        super(context, resource, serviceList);
        this.context = context;
        this.serviceList = serviceList;
        this.inflater = LayoutInflater.from(context);
    }


    public void setServiceList(List<Service> serviceList) {
        this.serviceList.clear();
        this.serviceList.addAll(serviceList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_service_card, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Service service = serviceList.get(position);

        // Bind data to views
        holder.bind(service);

        // Set click listeners
        holder.editServiceButton.setOnClickListener(v -> {
            if (editListener != null) {
                editListener.onEditButtonClick(service);
            }
        });

        holder.deleteServiceButton.setOnClickListener(v -> {
            if (deleteListener != null) {
                deleteListener.onDeleteButtonClick(service);
            }
        });

        return convertView;
    }

    public void setOnEditButtonClickListener(EditButtonClickListener listener) {
        this.editListener = listener;
    }

    public void setOnDeleteButtonClickListener(DeleteButtonClickListener listener) {
        this.deleteListener = listener;
    }

    static class ViewHolder {
        final TextView serviceNameTextView;
        final TextView descriptionTextView;
        final TextView priceTextView;
        final TextView discountTextView;
        final TextView bookingDeadlineTextView;
        final TextView cancellationDeadlineTextView;
        final ImageButton editServiceButton;
        final ImageButton deleteServiceButton;

        ViewHolder(View itemView) {
            serviceNameTextView = itemView.findViewById(R.id.textViewServiceName);
            descriptionTextView = itemView.findViewById(R.id.textViewDescription);
            priceTextView = itemView.findViewById(R.id.textViewPrice);
            discountTextView = itemView.findViewById(R.id.textViewDiscount);
            bookingDeadlineTextView = itemView.findViewById(R.id.textViewBookingDeadline);
            cancellationDeadlineTextView = itemView.findViewById(R.id.textViewCancellationDeadline);
            editServiceButton = itemView.findViewById(R.id.buttonEditService);
            deleteServiceButton = itemView.findViewById(R.id.buttonDeleteService);

        }

        void bind(Service service) {
            // Bind data to views
            serviceNameTextView.setText(service.getServiceName());
            descriptionTextView.setText(service.getDescription());
            priceTextView.setText(String.valueOf(service.getPrice()));
            discountTextView.setText(String.valueOf(service.getDiscount()));
            //bookingDeadlineTextView.setText("Booking Deadline: " + service.getBookingDeadline());
            //cancellationDeadlineTextView.setText("Cancellation Deadline: " + service.getCancellationDeadline());


        }
    }

    public interface EditButtonClickListener {
        void onEditButtonClick(Service service);
    }

    public interface DeleteButtonClickListener {
        void onDeleteButtonClick(Service service);
    }


}
