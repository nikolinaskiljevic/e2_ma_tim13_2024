package com.example.eventplanner.model;

import java.util.ArrayList;
import java.util.List;

public class Company {

        private String id;
        private String pupvId;
        private String email;
        private String name;
        private String address;
        private String phoneNumber;
        private String description;
        private String imageUrl; // URL of the company picture if needed
        private List<String> eventTypes;

        // Default constructor (required for Firebase)
        public Company() {
        }

        // Constructor with parameters
        public Company(String pupvId, String email, String name, String address, String phoneNumber, String description, String imageUrl) {
            this.pupvId = pupvId;
            this.email = email;
            this.name = name;
            this.address = address;
            this.phoneNumber = phoneNumber;
            this.description = description;
            this.imageUrl = imageUrl;
            this.eventTypes = new ArrayList<>();
            this.eventTypes.add("");
        }

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<String> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPupvId() {
            return pupvId;
        }

        public void setPupvId(String pupvId) {
            this.pupvId = pupvId;
        }

        // Getters and setters for all fields
        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
}


