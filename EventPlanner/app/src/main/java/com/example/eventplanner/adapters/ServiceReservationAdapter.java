package com.example.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.GradeCompanyFragment;
import com.example.eventplanner.fragments.ReportCompanyGradeFragment;
import com.example.eventplanner.model.CompanyGrade;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.DailyWorkHours;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Event2;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.ServiceReservation;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class ServiceReservationAdapter extends RecyclerView.Adapter<ServiceReservationAdapter.ServiceReservationViewHolder>{

    private List<ServiceReservation> reservations;
    private Context context;
    private User user;
    private FirebaseAuth mAuth;
    private User loggedInUser ;

    private FirebaseUser userr;
    private String odFirstName;
    private String odLastName;
    private String employeeFirstName;
    private String employeeLastName;
    private String userRole;
    private String serviceName;
    private NotificationRepository notificationRepository = new NotificationRepository();

    public ServiceReservationAdapter(List<ServiceReservation> reservations, Context context) {
        this.reservations = reservations;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }



    @NonNull
    @Override
    public ServiceReservationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_service_reservation_card, parent, false);
        return new ServiceReservationAdapter.ServiceReservationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceReservationViewHolder holder, int position) {
        ServiceReservation res = reservations.get(position);



        fetchUserById(res.getEmployeeId(),holder);
        fetchUserById(res.getOdId(),holder);
        findServiceName(res.getServiceId(),holder);

        mAuth = FirebaseAuth.getInstance();


        loggedInUser= new User();
        userr = mAuth.getCurrentUser();
        if (userr != null) {
           fetchLoggedIn(userr.getUid(), holder,res.getStatus());
        }


        holder.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(loggedInUser.getRole().equals("organizer")){
                    cancelReservation(res.getId(),"odCanceled");
                    saveMessage("Your reservation has been canceled",res.getEmployeeId(),res.getOdId());
                    cancelFromSamePackage(res.getPackageId(),"odCanceled");
                    updateEvent(res.getEmployeeId(),"FREE");

                }
                if(loggedInUser.getRole().equals("pupv") || loggedInUser.getRole().equals("employee")){
                    cancelReservation(res.getId(),"pupCanceled");
                    saveMessage("Reservation canceled, you can grade Company",res.getOdId(), res.getEmployeeId());
                    Log.e("TAG","ISFROMPACK"+ res.isFromPackage());

                    cancelFromSamePackage(res.getPackageId(),"pupCanceled");

                }
            }
        });

        holder.buttonAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               acceptReservation(res.getId(),"accepted");
                DailyWorkHours daily = new DailyWorkHours("Monday","09:00","20:00");
               Event2 event = new Event2("aaa",res.getEmployeeId(),daily,"TAKEN","8.8.2024");
               createNewEvent(event);
            }
        });


    }

    @Override
    public int getItemCount() {
        return reservations.size();
    }

    public static class ServiceReservationViewHolder extends RecyclerView.ViewHolder {

        TextView textViewOdFirstName,textViewOdLastName,textViewEmployeeFirstName,textViewEmployeeLastName,textViewServiceName;

        Button buttonCancel,buttonAccept;
        public ServiceReservationViewHolder(@NonNull View itemView) {
            super(itemView);


            textViewOdFirstName=itemView.findViewById(R.id.textViewOdFirstName);
            textViewOdLastName=itemView.findViewById(R.id.textViewOdLastName);
            textViewEmployeeFirstName=itemView.findViewById(R.id.textViewEmployeeFirstName);
            textViewEmployeeLastName=itemView.findViewById(R.id.textViewEmployeeLastName);
            textViewServiceName=itemView.findViewById(R.id.textViewServiceName);
            buttonCancel= itemView.findViewById(R.id.buttonCancel);
            buttonAccept= itemView.findViewById(R.id.buttonAccept);

        }
    }

    private void fetchUserById(String userId,ServiceReservationViewHolder holder) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                    userRole=user.getRole();

                    if(userRole.equals("employee")){
                        employeeFirstName=user.getFirstName();
                        employeeLastName=user.getLastName();

                        holder.textViewEmployeeFirstName.setText(employeeFirstName);
                        holder.textViewEmployeeLastName.setText(employeeLastName);


                    }
                    if(userRole.equals("organizer")){
                        odFirstName=user.getFirstName();
                        odLastName=user.getLastName();
                        holder.textViewOdFirstName.setText(odFirstName);
                        holder.textViewOdLastName.setText(odLastName);
                    }

                    // Možeš ovdje dalje raditi s korisnikom
                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }

    private void findServiceName(String serviceId,ServiceReservationViewHolder holder) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("services");

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    Service service = resSnapshot.getValue(Service.class);
                    if (service != null && service.getId().equals(serviceId)) {
                        serviceName=service.getServiceName();
                        holder.textViewServiceName.setText(serviceName);
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("findServiceName", "Failed to read service", databaseError.toException());
            }
        });
    }

    private void fetchLoggedIn(String userId,ServiceReservationViewHolder holder,String status) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                   loggedInUser=user;
                    if(loggedInUser.getRole().equals("organizer")){
                        if(status.equals("new") || status.equals("accepted")){
                            holder.buttonCancel.setVisibility(View.VISIBLE);
                        }else{
                            holder.buttonCancel.setVisibility(View.GONE);
                        }
                    }

                    if(loggedInUser.getRole().equals("pupv") || loggedInUser.getRole().equals("employee")){
                        if(!status.equals("realized") ){
                            holder.buttonCancel.setVisibility(View.VISIBLE);
                        }else{
                            holder.buttonCancel.setVisibility(View.GONE);
                        }

                    }
                    if(loggedInUser.getRole().equals("employee")){
                        if(status.equals("new") ) {
                            holder.buttonAccept.setVisibility(View.VISIBLE);
                        }else{
                            holder.buttonAccept.setVisibility(View.GONE);
                        }
                    }else{
                        holder.buttonAccept.setVisibility(View.GONE);
                    }

                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }


    public void cancelReservation(String resId,String status){
            DatabaseReference reservationsRef = FirebaseDatabase.getInstance().getReference().child("ServiceReservations").child(resId);

        reservationsRef.child("status").setValue(status);

    }

    public void acceptReservation(String resId,String status){
        DatabaseReference reservationsRef = FirebaseDatabase.getInstance().getReference().child("ServiceReservations").child(resId);

        reservationsRef.child("status").setValue(status);

    }

    private void cancelFromSamePackage(String packageId,String status) {
        DatabaseReference reservationsRef = FirebaseDatabase.getInstance().getReference("ServiceReservations");


        reservationsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    ServiceReservation res = resSnapshot.getValue(ServiceReservation.class);
                    Log.e("TAG","PACKID"+ res.getPackageId()+packageId);
                    if(res.getPackageId().equals(packageId) ){
                        Log.e("TAG","SAME");
                        cancelReservation(res.getId(),status
                        );
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllReservations", "Failed to read resevations", databaseError.toException());
            }
        });
    }

    private void saveMessage(String content,String recipientId,String senderId) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference().child("messages");
        String messageId = messageRef.push().getKey();

        String date = LocalDateTime.now().toString();
        Message newMessage = new Message(messageId, senderId, recipientId, date, content, "unread");

        messageRef.child(messageId).setValue(newMessage);



        saveNotification(newMessage,senderId);
    }

    private void saveNotification(Message newMessage,String sender) {
        Notification notification = new Notification();
        notification.setStatus(Notification.Status.CREATED);
        notification.setMessage(newMessage.getContent());
        notification.setText("New message from " + sender+  ": " + newMessage.getContent());
        notification.setUserId(newMessage.getRecipientId());
        notificationRepository.createNotification(notification);
    }
    public void updateEvent(String employeeId, String status) {
        DatabaseReference eventsRef = FirebaseDatabase.getInstance().getReference().child("eventss");

        eventsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot eventSnapshot : dataSnapshot.getChildren()) {
                    String currentEmployeeId = eventSnapshot.child("employeeId").getValue(String.class);
                    Log.e("TAG","UPDATE EVENT for empl"+ employeeId);
                    if (employeeId.equals(currentEmployeeId)) {
                        eventSnapshot.getRef().child("eventType").setValue(status);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("updateEvent", "Failed to update event", databaseError.toException());
            }
        });
    }

    public void createNewEvent(Event2 event) {
        DatabaseReference eventsRef = FirebaseDatabase.getInstance().getReference().child("eventss");

        String eventId = eventsRef.push().getKey();
        event.setId(eventId);
        // Kreirajte novi Event objekat

        // Sačuvajte događaj u Firebase pod jedinstvenim ID-em
        eventsRef.child(eventId).setValue(event).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    // Događaj uspešno sačuvan
                } else {
                    // Greška pri čuvanju događaja
                }
            }
        });
    }

}
