package com.example.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Date;
import java.util.List;

public class Service  implements Parcelable {

    private String id;
    private String category;
    private String subcategory;
    private String serviceName;
    private String description;
    private String specifics;
    private double price;
    private double discount;
    private double discountedPrice;
    private List<String> images;
    private List<String> eventTypes;
    private boolean visible;
    private boolean available;
    private List<User> employees;
    private String duration; // Or minMaxDuration
    private String bookingDeadline;
    private String cancellationDeadline;
    private String reservationConfirmation;
    private String companyId;
    private boolean deleted;

    public Service(){
        this.deleted=false;
    }
    public Service(String id, String category, String subcategory, String serviceName, String description, String specifics, double price, double discount, List<String> images, List<String> eventTypes, boolean visible, boolean available, List<User> employees, String duration, String bookingDeadline, String cancellationDeadline, String reservationConfirmation) {
        this.id = id;
        this.category = category;
        this.subcategory = subcategory;
        this.serviceName = serviceName;
        this.description = description;
        this.specifics = specifics;
        this.price = price;
        this.discount = discount;
        this.images = images;
        this.eventTypes = eventTypes;
        this.visible = visible;
        this.available = available;
        this.employees = employees;
        this.duration = duration;
        this.bookingDeadline = bookingDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.reservationConfirmation = reservationConfirmation;
        this.deleted = false;
        this.discountedPrice =  price-((100-discount)/100);
    }

    public Service(String serviceName, String description, double price, double discount, String bookingDeadline, String cancellationDeadline, String category, String subcategory,
                   List<String> eventTypes, String duration, String specifics, boolean visible, boolean available) {
            this.serviceName = serviceName;
            this.description = description;
            this.price = price;
            this.discount = discount;
            this.bookingDeadline = bookingDeadline;
            this.cancellationDeadline = cancellationDeadline;
            this.category = category;
            this.subcategory = subcategory;
            this.eventTypes = eventTypes;
            this.duration = duration;
            this.specifics = specifics;
            this.visible = visible;
            this.available = available;
        this.discountedPrice =  price-((100-discount)/100);
this.deleted=false;

    }

    public Service(String serviceName, String description, double price, double discount, String bookingDeadline,
                   String cancellationDeadline, List<String> eventTypes, String duration, String specifics,
                   boolean isVisible, boolean isAvailable) {
        this.serviceName = serviceName;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.bookingDeadline = bookingDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.eventTypes = eventTypes;
        this.duration = duration;
        this.specifics = specifics;
        this.visible = isVisible;
        this.available = isAvailable;
        this.discountedPrice =  price-((100-discount)/100);
        this.deleted=false;

    }


    protected Service(Parcel in) {
        id = in.readString();
        category = in.readString();
        subcategory = in.readString();
        serviceName = in.readString();
        description = in.readString();
        specifics = in.readString();
        price = in.readDouble();
        discount = in.readDouble();
        discountedPrice = in.readDouble();
        images = in.createStringArrayList();
        eventTypes = in.createStringArrayList();
        visible = in.readByte() != 0;
        available = in.readByte() != 0;
        duration = in.readString();
        bookingDeadline = in.readString();
        cancellationDeadline = in.readString();
        reservationConfirmation = in.readString();
        this.deleted=false;

    }

    public static final Creator<Service> CREATOR = new Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSpecifics() {
        return specifics;
    }

    public void setSpecifics(String specifics) {
        this.specifics = specifics;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public List<String> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<String> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public List<User> getEmployees() {
        return employees;
    }

    public void setEmployees(List<User> employees) {
        this.employees = employees;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBookingDeadline() {
        return bookingDeadline;
    }

    public void setBookingDeadline(String bookingDeadline) {
        this.bookingDeadline = bookingDeadline;
    }

    public String getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(String cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public String getReservationConfirmation() {
        return reservationConfirmation;
    }

    public void setReservationConfirmation(String reservationConfirmation) {
        this.reservationConfirmation = reservationConfirmation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(category);
        dest.writeString(subcategory);
        dest.writeString(serviceName);
        dest.writeString(description);
        dest.writeString(specifics);
        dest.writeDouble(price);
        dest.writeDouble(discount);
        dest.writeStringList(images);
        dest.writeStringList(eventTypes);
        dest.writeByte((byte) (visible ? 1 : 0));
        dest.writeByte((byte) (available ? 1 : 0));
        dest.writeString(duration);
        dest.writeString(bookingDeadline);
        dest.writeString(cancellationDeadline);
        dest.writeString(reservationConfirmation);
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public double getDiscountedPrice() {
        return this.price-((100-this.discount)/100);

    }

    public void setDiscountedPrice(double discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }
}
