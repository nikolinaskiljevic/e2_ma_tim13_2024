package com.example.eventplanner.adapters;

import static android.app.PendingIntent.getActivity;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.example.eventplanner.notifications.NotificationRepository;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.time.LocalDateTime;
import java.util.List;

public class CompanyGradeReportAdapter extends RecyclerView.Adapter<CompanyGradeReportAdapter.CompanyGradeReportViewHolder>{

    private List<CompanyGradeReport> reportsList;
    private Context context;
    private User user;
    private FirebaseAuth mAuth;
    private NotificationRepository notificationRepository = new NotificationRepository();

    public CompanyGradeReportAdapter(List<CompanyGradeReport> reportsList, Context context) {
        this.reportsList = reportsList;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }



    @NonNull
    @Override
    public CompanyGradeReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_report_card, parent, false);
        return new CompanyGradeReportAdapter.CompanyGradeReportViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyGradeReportViewHolder holder, int position) {
        CompanyGradeReport report = reportsList.get(position);

        user=new User();
        //fetchUserById(report.getPupvId());
        // Postavljanje podataka o zaposleniku u ViewHolder
        holder.textViewPUPV.setText(user.getFirstName());
        holder.textViewReason.setText(report.getReason());
        holder.textViewDate.setText(report.getDate().toString());
        holder.textViewStatus.setText(report.getStatus());

        if(!report.getStatus().equals("REPORTED")){
            holder.acceptButton.setVisibility(View.GONE);
            holder.rejectButton.setVisibility(View.GONE);
        }else{
            holder.acceptButton.setVisibility(View.VISIBLE);
            holder.rejectButton.setVisibility(View.VISIBLE);
        }

        holder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                report.setStatus("ACCEPTED");
                updateStatus(report,"ACCEPTED");
                notifyDataSetChanged();
                deleteCompanyGrade(report.getCompanyGradeId());
            }
        });
        holder.rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CompanyGradeReport report = reportsList.get(holder.getAdapterPosition());

                report.setStatus("REJECTED");
                updateStatus(report,"REJECTED");
                notifyDataSetChanged();

                saveMessage("Your report has been denied",report.getPupv(),"8RPmBrSkLlSU62mqeRkUOzTfO5R2");
            }
        });

    }

    @Override
    public int getItemCount() {
        return reportsList.size();
    }

    public static class CompanyGradeReportViewHolder extends RecyclerView.ViewHolder {

        TextView textViewPUPV,textViewReason,textViewDate,textViewStatus;
        Button acceptButton,rejectButton;
        public CompanyGradeReportViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewPUPV = itemView.findViewById(R.id.textViewPUPV);
            textViewReason=itemView.findViewById(R.id.textViewReason);
            textViewDate=itemView.findViewById(R.id.textViewDate);
            textViewStatus=itemView.findViewById(R.id.textViewStatus);
            acceptButton = itemView.findViewById(R.id.buttonAccept);
            rejectButton = itemView.findViewById(R.id.buttonReject);
        }
    }

    private void fetchUserById(String userId) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User userr = dataSnapshot.getValue(User.class);
                if (userr != null && userr.getUid().equals(userId)) {
                    user=userr;
                    Log.d("fetchUserById", "User found: " + user.toString());

                    // Možeš ovdje dalje raditi s korisnikom
                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }

    private void updateStatus(CompanyGradeReport report,String status) {
        Log.e("report", "info"+ report.getReport());
        DatabaseReference employeeRef = FirebaseDatabase.getInstance().getReference().child("CompanyGradeReports").child(report.getReport());

        employeeRef.child("status").setValue(status);

    }

    private void deleteCompanyGrade(String gradeId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference gradeRef = databaseReference.child("companyGrades").child(gradeId);

        gradeRef.removeValue().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {

            } else {
            }
        });
    }

    private void saveMessage(String content,String recipientId,String senderId) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference().child("messages");
        String messageId = messageRef.push().getKey();

        String date = LocalDateTime.now().toString();
        Message newMessage = new Message(messageId, senderId, recipientId, date, content, "unread");

        messageRef.child(messageId).setValue(newMessage);



        saveNotification(newMessage,senderId);
    }

    private void saveNotification(Message newMessage,String sender) {
        Notification notification = new Notification();
        notification.setStatus(Notification.Status.CREATED);
        notification.setMessage(newMessage.getContent());
        notification.setText("New message from " + sender+  ": " + newMessage.getContent());
        notification.setUserId(newMessage.getRecipientId());
        notificationRepository.createNotification(notification);
    }
}
