package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.EditItemFragment;
import com.example.eventplanner.fragments.ItemDetailsFragment;
import com.example.eventplanner.model.BudgetItem;

import java.util.ArrayList;

public class BudgetItemAdapter extends RecyclerView.Adapter<BudgetItemAdapter.ViewHolder> {

    private ArrayList<BudgetItem> organizerBudgetItems;
    private Context context;

    public BudgetItemAdapter(Context context, ArrayList<BudgetItem> items) {
        this.context = context;
        this.organizerBudgetItems = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        BudgetItem item = organizerBudgetItems.get(position);
        holder.textViewCategory.setText(item.getCategory());
        holder.textViewSubcategory.setText(item.getSubcategory());
        holder.textViewAmount.setText(String.format("%.2f", item.getPlannedAmount()));

        // Postavljanje slušača klika na dugme za prikaz detalja
        holder.buttonsLayout.findViewById(R.id.buttonDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Poziv metode za prikaz detalja proizvoda
                showProductDetails(item);
            }
        });

        holder.buttonsLayout.findViewById(R.id.buttonDelete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.buttonsLayout.findViewById(R.id.buttonEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    // Metoda za prikaz detalja proizvoda
    private void showProductDetails(BudgetItem item) {
        // Otvaranje novog fragmenta za prikaz detalja proizvoda
        FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        // Zamijenite "ProductDetailsFragment" sa imenom vašeg fragmenta koji prikazuje detalje o proizvodu
        transaction.replace(R.id.fragment_container, ItemDetailsFragment.newInstance(item.getCategory(), item.getSubcategory(), String.format("%.2f", item.getPlannedAmount())));
        transaction.addToBackStack(null); // Dodajte fragment na back stack kako biste se mogli vratiti nazad
        transaction.commit();
    }

    private void editProductDetails(BudgetItem item) {
        // Otvaranje novog fragmenta za prikaz detalja proizvoda
        FragmentTransaction transaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
        // Zamijenite "ProductDetailsFragment" sa imenom vašeg fragmenta koji prikazuje detalje o proizvodu
        // Kreiranje instance EditItemFragment sa argumentima
        EditItemFragment fragment = EditItemFragment.newInstance(item);
        // Zamena trenutnog fragmenta sa novim fragmentom
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null); // Dodajte fragment na back stack kako biste se mogli vratiti nazad
        transaction.commit();
    }

    @Override
    public int getItemCount() {
        return organizerBudgetItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewSubcategory;
        public TextView textViewCategory;
        public TextView textViewAmount;
        public LinearLayout buttonsLayout; // Dodajte ovu referencu

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewSubcategory = itemView.findViewById(R.id.textViewSubcategory);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);
            textViewAmount = itemView.findViewById(R.id.textViewAmount);
            buttonsLayout = itemView.findViewById(R.id.buttons); // Inicijalizujte referencu
        }
    }
}
