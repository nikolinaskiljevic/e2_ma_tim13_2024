package com.example.eventplanner.model;

import java.util.Date;

public class UsersReport {

    private String id;
    private String ODid;
    private String pupvId;
    private Date reportDate;
    private String reasons;
    private String status;  //prijavljeno, prihvaceno, odbijeno

    public UsersReport(){}

    public UsersReport(String ODid, String pupvId, Date reportDate, String reasons, String status) {
        this.ODid = ODid;
        this.pupvId = pupvId;
        this.reportDate = reportDate;
        this.reasons = reasons;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getODid() {
        return ODid;
    }

    public void setODid(String ODid) {
        this.ODid = ODid;
    }

    public String getPupvId() {
        return pupvId;
    }

    public void setPupvId(String pupvId) {
        this.pupvId = pupvId;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
