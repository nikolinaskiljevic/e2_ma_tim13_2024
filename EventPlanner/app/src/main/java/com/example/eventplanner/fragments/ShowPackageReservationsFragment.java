package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackageReservationAdapter;
import com.example.eventplanner.adapters.ServiceReservationAdapter;
import com.example.eventplanner.adapters.ShowProductAdapter;
import com.example.eventplanner.adapters.ShowServiceAdapter;
import com.example.eventplanner.model.BudgetItem;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.PackageReservation;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ServiceReservation;
import com.example.eventplanner.model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import kotlin.jvm.internal.PackageReference;


public class ShowPackageReservationsFragment extends Fragment {

    RecyclerView recyclerViewReservations;
    PackageReservationAdapter adapter;

    List<PackageReservation> reservations;

    List<ServiceReservation> serviceReservations;



    public ShowPackageReservationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_show_package_reservations, container, false);
        reservations=new ArrayList<>();

        serviceReservations= new ArrayList<>();
        recyclerViewReservations = view.findViewById(R.id.recyclerViewReservations);
        recyclerViewReservations.setLayoutManager(new LinearLayoutManager(getActivity()));
        getAllPackageReservations();
        return view;
    }

    private void getAllPackageReservations() {
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference("PackageReservations");
        List<PackageReservation> newReservationsList = new ArrayList<>();
        packageRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                List<PackageReservation> newReservationsList = new ArrayList<>();

                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    PackageReservation res = resSnapshot.getValue(PackageReservation.class);
                    Log.e("TAG","resSnapshot"+res);
                    newReservationsList.add(res);

                }
                for (PackageReservation ress : newReservationsList) {
                    if (!reservations.contains(ress)) {
                        reservations.add(ress);
                    }
                }

                Log.e("TAG","reservations"+reservations);
                adapter = new PackageReservationAdapter(reservations, getActivity());
                recyclerViewReservations.setAdapter(adapter);

                for(PackageReservation packageRes:reservations ){
                    for(String serviceRes: packageRes.getServiceReservations()){
                        getServiceReservations(serviceRes);
                    }
                   boolean accepted= areAllAccepted(serviceReservations);
                    Log.e("TAG","boolean accepted"+ accepted);
                    if(accepted){
                        updateStatus(packageRes.getId(),"accepted");
                        findPackageProducts(packageRes.getPackageId());
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllReservations", "Failed to read resevations", databaseError.toException());
            }
        });
    }

    private void getServiceReservations(String resId) {
        serviceReservations.clear();
        DatabaseReference serviceRef = FirebaseDatabase.getInstance().getReference("ServiceReservations").child(resId);

        serviceRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ServiceReservation res = dataSnapshot.getValue(ServiceReservation.class);
                if (res != null) {
                  serviceReservations.add(res);

                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "Service reservation not found with ID: " + resId);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
            }
        });
    }

    public static boolean areAllAccepted(List<ServiceReservation> serviceReservations) {
        return serviceReservations.stream()
                .allMatch(reservation -> "accepted".equals(reservation.getStatus()));
    }

    public void updateStatus(String resId,String status){
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference().child("PackageReservations").child(resId);

        packageRef.child("status").setValue(status);

    }


    private void findPackageProducts(String packageId) {
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference("packages");

        packageRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                for (DataSnapshot resSnapshot : dataSnapshot.getChildren()) {
                    Package pac = resSnapshot.getValue(Package.class);
                    if (pac != null && pac.getId().equals(packageId)) {
                        if(pac.getSelectedProducts()!=null){
                            for(Product p: pac.getSelectedProducts()){
                                updateBudgetAmount(p.getSubcategory(),p.getPrice());
                            }
                        }

                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("findServiceName", "Failed to read service", databaseError.toException());
            }
        });
    }

    private void updateBudgetAmount(String subcategory,double price) {
        DatabaseReference budgetRef = FirebaseDatabase.getInstance().getReference("budgetItems");

        budgetRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {



                for (DataSnapshot bugSnap : dataSnapshot.getChildren()) {
                    BudgetItem bug = bugSnap.getValue(BudgetItem.class);
                    Log.e("TAG","SUBCAT"+ bug.getSubcategory()+subcategory);
                    if (bug != null && bug.getSubcategory().equals(subcategory)) {

                        double newAmount=bug.getSpentAmount()+ price;
                        DatabaseReference bugRef = FirebaseDatabase.getInstance().getReference().child("budgetItems").child(bug.getId());
                        Log.e("TAG","NEW PRICE"+ newAmount);
                        bugRef.child("spentAmount").setValue(newAmount);
                    }


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("findServiceName", "Failed to read service", databaseError.toException());
            }
        });
    }
}