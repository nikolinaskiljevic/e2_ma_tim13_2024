package com.example.eventplanner.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.example.eventplanner.R;
import com.example.eventplanner.model.ProductSubcategory;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AdminProductsSubcategoriesFragment extends Fragment {

    private LinearLayout layoutCategories;
    private EditText editTextCategoryName;
    private EditText editTextCategoryDescription;
    private Spinner spinnerCategoryType;
    private Button buttonAddCategory;
    private TextView subcatinfo;

    private DatabaseReference subcategoriesRef;
    private String categoryId;

    public AdminProductsSubcategoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_admin_products_subcategories, container, false);

        layoutCategories = view.findViewById(R.id.layoutCategories);
        editTextCategoryName = view.findViewById(R.id.editTextCategoryName);
        editTextCategoryDescription = view.findViewById(R.id.editTextCategoryDescription);
        spinnerCategoryType = view.findViewById(R.id.spinnerCategoryType);
        buttonAddCategory = view.findViewById(R.id.buttonAddCategory);
        subcatinfo = view.findViewById(R.id.subcatinfo);

        Bundle bundle = getArguments();
        if (bundle != null) {
            categoryId = bundle.getString("categoryId");
            String categoryName = bundle.getString("categoryName");

            // Now you have categoryId and categoryName, you can use them as needed

            // For example, you can set the category name in the EditText field
            subcatinfo.setText(categoryName + " info" );
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        subcategoriesRef = database.getReference("productSubcategories");

        buttonAddCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String categoryName = editTextCategoryName.getText().toString().trim();
                String categoryDescription = editTextCategoryDescription.getText().toString().trim();
                String categoryType = spinnerCategoryType.getSelectedItem().toString().trim();

                if (!categoryName.isEmpty()) {
                    String key = saveSubcategoryToFirebase(categoryName, categoryDescription, categoryType);
                    addCategory(key, categoryName, categoryDescription, categoryType);
                    editTextCategoryName.setText("");
                    editTextCategoryDescription.setText("");
                }
            }
        });

        // Load existing product subcategories from Firebase
        loadSubcategoriesFromFirebase();

        return view;
    }

    private String saveSubcategoryToFirebase(String categoryName, String categoryDescription, String categoryType) {
        String key = subcategoriesRef.push().getKey();
        if (key != null) {
            ProductSubcategory subcategory = new ProductSubcategory(key, categoryId, categoryName, categoryDescription, categoryType);
            subcategoriesRef.child(key).setValue(subcategory);
            return key;
        }
        return null;
    }

    private void addCategory(String categoryId, String categoryName, String categoryDescription, String categoryType) {
        // Inflate subcategory item layout
        View subcategoryView = getLayoutInflater().inflate(R.layout.item_subcategory, null);
        TextView textViewCategoryName = subcategoryView.findViewById(R.id.textViewCategoryName);
        TextView textViewCategoryDescription = subcategoryView.findViewById(R.id.textViewAdditionalInfo);
        TextView textViewCategoryType = subcategoryView.findViewById(R.id.textViewCategoryType);
        Button buttonDelete = subcategoryView.findViewById(R.id.buttonDelete);
        Button buttonEdit = subcategoryView.findViewById(R.id.buttonEdit);
        buttonEdit.setVisibility(View.GONE);

        // Set subcategory details
        textViewCategoryName.setText(categoryName);
        textViewCategoryDescription.setText(categoryDescription);
        textViewCategoryType.setText(categoryType);

        // Set padding between items
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 16); // Adjust the padding as needed
        subcategoryView.setLayoutParams(layoutParams);

        // Set click listener for delete button
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Handle delete action
                layoutCategories.removeView(subcategoryView);
                deleteSubcategory(categoryId);
            }
        });

        // Add subcategory view to the layout
        layoutCategories.addView(subcategoryView);
    }

    private void loadSubcategoriesFromFirebase() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            String categoryId = bundle.getString("categoryId");
            subcategoriesRef.orderByChild("categoryId").equalTo(categoryId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    layoutCategories.removeAllViews();
                    for (DataSnapshot subcategorySnapshot : dataSnapshot.getChildren()) {
                        ProductSubcategory subcategory = subcategorySnapshot.getValue(ProductSubcategory.class);
                        if (subcategory != null) {
                            addCategory(subcategory.getId(), subcategory.getName(), subcategory.getDescription(), subcategory.getType());
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    // Handle error
                }
            });
        }
    }

    private void deleteSubcategory(String subcategoryId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("productSubcategories").child(subcategoryId);
        databaseReference.removeValue();
    }
}
