package com.example.eventplanner.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployeeAdapter;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class EmployeeManageFragment extends Fragment {

    private RecyclerView recyclerView;
    private EmployeeAdapter adapter;
    private List<Employee> employeeList;



    private User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_employee_manage, container, false);

        user=new User();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {

            getUserById(currentUser.getUid());
        }



        // Pronađemo dugme za navigaciju na drugi fragment







        // Postavimo rukovanje klikom na dugme




        // Inicijalizacija RecyclerView-a
        recyclerView = view.findViewById(R.id.recyclerViewEmployees);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        employeeList = new ArrayList<>();
        employeeList.clear();






        return view;
    }


    private void populateEmployees(String employeeId){

        employeeList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference employeesRef = databaseReference.child("employees");

        // Dohvaćanje podataka iz čvora "employees" samo za određeni ID
        Query query = employeesRef.orderByChild("id").equalTo(employeeId);

        // Dohvat podataka prema upitu
        query.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Ovdje možete obraditi podatke koje ste dobili iz baze podataka
                // dataSnapshot sadrži sve podatke koji se podudaraju s upitom

                for (DataSnapshot employeeSnapshot : dataSnapshot.getChildren()) {
                    Employee employee = employeeSnapshot.getValue(Employee.class);
                    // Dodajte samo zaposlenika koji odgovara traženom ID-u
                    employeeList.add(employee);
                }

                // Postavite adapter za RecyclerView s dobivenim podacima
                adapter = new EmployeeAdapter(employeeList, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void getUserById(String userId){
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference usersRef = database.getReference("Users");

        usersRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // Korisnik s proslijeđenim ID-om je pronađen
                    user = dataSnapshot.getValue(User.class);
                    Log.w("TAG", "ROLE" + user.getRole());
                    populateEmployees(user.getUid());
                    // Ovdje možete manipulirati podacima o korisniku
                } else {
                    // Korisnik nije pronađen
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Greška prilikom dohvatanja podataka
                Log.w("TAG", "Greška prilikom dohvatanja korisnika.", databaseError.toException());
            }
        });


    }

}
