package com.example.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.OrganizerProfileFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UsersReport;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class AllUserReportsAdapter  extends ArrayAdapter<UsersReport> {

    private final Context context;
    private final List<UsersReport> reportsList;
    private final LayoutInflater inflater;
    private User reporter;
    private User reported;

    public AllUserReportsAdapter(@NonNull Context context, int resource, @NonNull List<UsersReport> productList) {
        super(context, resource, productList);
        this.context = context;
        this.reportsList = productList;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        AllUserReportsAdapter.ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_user_report_card, parent, false);
            holder = new AllUserReportsAdapter.ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (AllUserReportsAdapter.ViewHolder) convertView.getTag();
        }

        UsersReport report = reportsList.get(position);

        // Bind data to views
        holder.bind(report);


        return convertView;
    }


    class ViewHolder {
        final TextView reporterNameTextView;
        final TextView reportedNameTextView;
        final TextView reportDateTextView;
        final TextView reportStatusTextView;
        final TextView reportReasonTextView;
        final ImageButton acceptButton;
        final ImageButton rejectButton;
        final Button reporterButton;
        final Button reportedButton;

        ViewHolder(View itemView) {
            reporterNameTextView = itemView.findViewById(R.id.reporterNameTextView);
            reportedNameTextView = itemView.findViewById(R.id.reportedNameTextView);
            reportDateTextView = itemView.findViewById(R.id.reportDateTextView);
            reportStatusTextView = itemView.findViewById(R.id.reportStatusTextView);
            reportReasonTextView = itemView.findViewById(R.id.reportReasonTextView);
            acceptButton = itemView.findViewById(R.id.acceptButton);
            rejectButton = itemView.findViewById(R.id.rejectButton);
            reporterButton = itemView.findViewById(R.id.reporterProfileButton);
            reportedButton = itemView.findViewById(R.id.reportedProfileButton);
        }

        void bind(final UsersReport report) {
            DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("Users");

            // Fetch reporter's name
            usersRef.child(report.getPupvId()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                        reporter = snapshot.getValue(User.class);
                        if (reporter != null) {
                            reporterNameTextView.setText(reporter.getFirstName() + " " + reporter.getLastName());
                        } else {
                            reporterNameTextView.setText("Unknown Reporter");
                        }
                    } else {
                        reporterNameTextView.setText("Unknown Reporter");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(reportDateTextView.getContext(), "Failed to load reporter's name", Toast.LENGTH_SHORT).show();
                }
            });

            // Fetch reported user's name
            usersRef.child(report.getODid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    if (snapshot.exists()) {
                       reported = snapshot.getValue(User.class);
                        if (reported != null) {
                            reportedNameTextView.setText(reported.getFirstName() + " " + reported.getLastName());
                        } else {
                            reportedNameTextView.setText("Unknown Reported User");
                        }
                    } else {
                        reportedNameTextView.setText("Unknown Reported User");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Toast.makeText(reportDateTextView.getContext(), "Failed to load reported user's name", Toast.LENGTH_SHORT).show();
                }
            });

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            reportDateTextView.setText(dateFormat.format(report.getReportDate()));
            reportStatusTextView.setText(report.getStatus());
            reportReasonTextView.setText(report.getReasons());


            // Set visibility of buttons based on status
            if ("Created".equals(report.getStatus())) {
                acceptButton.setVisibility(View.VISIBLE);
                rejectButton.setVisibility(View.VISIBLE);
            } else {
                acceptButton.setVisibility(View.GONE);
                rejectButton.setVisibility(View.GONE);
            }

            // Add click listeners for accept and reject buttons
            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateReportStatus(report, "Accepted");
                    blockReportedUser(report.getODid());
                }
            });

            rejectButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateReportStatus(report, "Rejected");

                }
            });
            reporterButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrganizerProfileFragment fragment = OrganizerProfileFragment.newInstance(reporter.getUid(),reporter.getEmail(), reporter.getFirstName(), reporter.getLastName(), reporter.getAddress(), reporter.getPhoneNumber(), reporter.getImageURL());
                    FragmentActivity activity = (FragmentActivity) context;
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
            reportedButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrganizerProfileFragment fragment = OrganizerProfileFragment.newInstance(reported.getUid(),reported.getEmail(), reported.getFirstName(), reported.getLastName(), reported.getAddress(), reported.getPhoneNumber(), reported.getImageURL());
                    FragmentActivity activity = (FragmentActivity) context;
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.fragment_container, fragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            });
        }


        private void updateReportStatus(UsersReport report, String status) {
            DatabaseReference reportRef = FirebaseDatabase.getInstance().getReference().child("UserReports").child(report.getId());
            reportRef.child("status").setValue(status).addOnCompleteListener(task -> {
                if (task.isSuccessful()) {
                    Toast.makeText(context, "Report status updated to " + status, Toast.LENGTH_SHORT).show();
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "Failed to update report status", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }


    private void blockReportedUser(String userId) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
        userRef.child("blocked").setValue(true).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                if(reported.getRole().equals("organizer")){
                    cancelReservations(reported.getUid());
                    cancelAllServiceReservations(reported.getUid());
                }
                if(reported.getRole().equals("pupv")){
                   //prvo naci njegovu kompaniju
                    //unutar kompanije traziti sve pakete ciji se id poklapa sa nekim od id iz rezervacija
                    //sve te u rezervaijama otkazati

                    findCompanies(reported.getUid());
                }
                Toast.makeText(context, "Reported user blocked successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Failed to block reported user", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void cancelReservations(String userId) {
        DatabaseReference reservationsRef = FirebaseDatabase.getInstance().getReference().child("PackageReservations");
        reservationsRef.orderByChild("odId").equalTo(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot reservationSnapshot : dataSnapshot.getChildren()) {
                    String reservationId = String.valueOf(reservationSnapshot.getKey());
                    if (reservationId != null) {
                        DatabaseReference reservationRef = reservationsRef.child(reservationId);
                        reservationRef.child("status").setValue("adminCanceled").addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                reservationRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            String packageId = dataSnapshot.child("packageId").getValue(String.class);
                                            if (packageId != null) {
                                                cancelServiceReservations(packageId);
                                                Toast.makeText(context, "Reservation canceled successfully", Toast.LENGTH_SHORT).show();
                                            } else {
                                                Toast.makeText(context, "Failed to retrieve packageId", Toast.LENGTH_SHORT).show();
                                            }
                                        } else {
                                            Toast.makeText(context, "Reservation does not exist", Toast.LENGTH_SHORT).show();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                        Toast.makeText(context, "Failed to retrieve reservation", Toast.LENGTH_SHORT).show();
                                    }
                                });
                            } else {
                                Toast.makeText(context, "Failed to cancel reservation", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Failed to retrieve reservations", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void cancelServiceReservations(String packageReservationId) {
        DatabaseReference serviceReservationsRef = FirebaseDatabase.getInstance().getReference().child("ServiceReservations");
        serviceReservationsRef.orderByChild("packageId").equalTo(packageReservationId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot serviceReservationSnapshot : dataSnapshot.getChildren()) {
                    String serviceReservationId = serviceReservationSnapshot.getKey();
                    if (serviceReservationId != null) {
                        // Otkazivanje rezervacije usluge
                        serviceReservationsRef.child(serviceReservationId).child("status").setValue("adminCanceled").addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Toast.makeText(context, "Service reservation canceled successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Failed to cancel service reservation", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Failed to retrieve service reservations", Toast.LENGTH_SHORT).show();
            }
        });
    }



    public void cancelAllServiceReservations(String odId){
        DatabaseReference serviceReservationsRef = FirebaseDatabase.getInstance().getReference().child("ServiceReservations");
        serviceReservationsRef.orderByChild("odId").equalTo(odId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot serviceReservationSnapshot : dataSnapshot.getChildren()) {
                    String serviceReservationId = serviceReservationSnapshot.getKey();
                    if (serviceReservationId != null) {
                        // Otkazivanje rezervacije usluge
                        serviceReservationsRef.child(serviceReservationId).child("status").setValue("adminCanceled").addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                Toast.makeText(context, "Service reservation canceled successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Failed to cancel service reservation", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Failed to retrieve service reservations", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void findCompanies(String pupvId){
        DatabaseReference companiesRef = FirebaseDatabase.getInstance().getReference().child("Companies");
        companiesRef.orderByChild("pupvId").equalTo(pupvId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot companySnapshot : dataSnapshot.getChildren()) {
                    Company company = companySnapshot.getValue(Company.class);
                    if (company != null) {
                       findPackages(company.getId());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Failed to retrieve companies", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void findPackages(String companyId) {
        DatabaseReference packagesRef = FirebaseDatabase.getInstance().getReference().child("packages");
        packagesRef.orderByChild("companyId").equalTo(companyId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot packageSnapshot : dataSnapshot.getChildren()) {
                    // Ovdje možete manipulirati svakim pronađenim paketom
                    // Na primjer, možete ih dodati u listu ili izvršiti neku drugu operaciju
                    Package pack = packageSnapshot.getValue(Package.class);
                    if (pack != null) {
                        findPackageReservations(pack.getId());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Failed to retrieve packages", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void findPackageReservations(String packageId) {
        DatabaseReference reservationsRef = FirebaseDatabase.getInstance().getReference().child("PackageReservations");
        reservationsRef.orderByChild("packageId").equalTo(packageId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot reservationSnapshot : dataSnapshot.getChildren()) {
                    String reservationId = reservationSnapshot.getKey();
                    if (reservationId != null) {
                        reservationsRef.child(reservationId).child("status").setValue("adminCanceled").addOnCompleteListener(task -> {
                            if (task.isSuccessful()) {
                                cancelServiceReservations(packageId);
                                Toast.makeText(context, "Reservation canceled successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(context, "Failed to cancel reservation", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context, "Failed to retrieve reservations", Toast.LENGTH_SHORT).show();
            }
        });
    }

}