package com.example.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.EmployeeWorkHoursFragment;
import com.example.eventplanner.fragments.GradeCompanyFragment;
import com.example.eventplanner.fragments.ShowCompanyGradesFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.CompanyGradeReport;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.CompanyViewHolder>{


    private List<Company> companiesList;
    private Context context;
    private User user;
    private FirebaseAuth mAuth;

    private User loggedInUser ;

    private FirebaseUser userr;
    private String userRole;


    public CompanyAdapter(List<Company> companiesList, Context context) {
        this.companiesList = companiesList;
        this.context = context;
        mAuth = FirebaseAuth.getInstance();
    }



    @NonNull
    @Override
    public CompanyAdapter.CompanyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_company_card, parent, false);
        return new CompanyAdapter.CompanyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyAdapter.CompanyViewHolder holder, int position) {
        Company company = companiesList.get(position);

        // Postavljanje podataka o zaposleniku u ViewHolder
        holder.textViewName.setText(company.getName());
        holder.textViewEmail.setText(company.getEmail());
        holder.textViewPhoneNumber.setText(company.getPhoneNumber());
        holder.textViewAddress.setText(company.getAddress());
        holder.textViewDescription.setText(company.getDescription());

        loggedInUser= new User();
        mAuth = FirebaseAuth.getInstance();


        userr = mAuth.getCurrentUser();
        if (userr != null) {
            fetchUserById(userr.getUid(), new UserFetchCallback() {
                @Override
                public void onUserFetched(User user) {
                    if (user != null && !userRole.equals("organizer")) {
                        holder.buttonGrade.setVisibility(View.GONE);
                    }
                }
            });
        }



        holder.buttonGrade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String companyName = companiesList.get(position).getName();


                GradeCompanyFragment fragment = new GradeCompanyFragment();
                Bundle bundle = new Bundle();
                bundle.putString("companyName", companyName);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        holder.buttonShowGrades.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String companyName = companiesList.get(position).getName();


                ShowCompanyGradesFragment fragment = new ShowCompanyGradesFragment();
                Bundle bundle = new Bundle();
                bundle.putString("companyName", companyName);
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return companiesList.size();
    }

    public static class CompanyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName,textViewEmail,textViewPhoneNumber,textViewAddress,textViewDescription;
        Button buttonGrade,buttonShowGrades;
        public CompanyViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewName = itemView.findViewById(R.id.textViewName);
            textViewEmail=itemView.findViewById(R.id.textViewEmail);
            textViewPhoneNumber=itemView.findViewById(R.id.textViewPhoneNumber);
            textViewAddress=itemView.findViewById(R.id.textViewAddress);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            buttonGrade = itemView.findViewById(R.id.buttonGrade);
            buttonShowGrades=itemView.findViewById(R.id.buttonShowGrades);
        }
    }

    private void fetchUserById(String userId, final UserFetchCallback callback) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users").child(userId);

        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user != null) {
                   userRole=user.getRole();

                    Log.d("fetchUserById", "User found: " + user.toString());

                    // Možeš ovdje dalje raditi s korisnikom
                } else {
                    // Korisnik sa zadatim ID-em nije pronađen
                    Log.d("fetchUserById", "User not found with ID: " + userId);
                }
                callback.onUserFetched(user);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchUserById", "Failed to read user", databaseError.toException());
                callback.onUserFetched(null);
            }
        });
    }

    public interface UserFetchCallback {
        void onUserFetched(User user);
    }
}
