package com.example.eventplanner.model;


public class ProductReservation {
    private String  id;
    private String  productId;
    private String eventId;
    private String   odId;
    private String   status; // pending,pupCanceled,odCanceled,accepted,realized(ne znam,pretpostavljam, ne pise nista u spec)


    public ProductReservation() {
    }

    public ProductReservation(String id, String productId, String eventId, String odId, String status) {
        this.id = id;
        this.productId = productId;
        this.eventId = eventId;
        this.odId = odId;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getOdId() {
        return odId;
    }

    public void setOdId(String odId) {
        this.odId = odId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
