package com.example.eventplanner.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.PackagePricelistAdapter;
import com.example.eventplanner.adapters.ProductPricelistAdapter;
import com.example.eventplanner.adapters.ServicePricelistAdapter;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PracelistFragment extends Fragment implements EditProduct.OnProductUpdateListener, EditServiceFragment.OnServiceUpdateListener {

    private RecyclerView recyclerViewProducts;
    private RecyclerView recyclerViewServices;
    private RecyclerView recyclerViewPackages;

    private ProductPricelistAdapter productAdapter;
    private List<Product> products = new ArrayList<>();

    private ServicePricelistAdapter serviceAdapter;
    private List<Service> services = new ArrayList<>();

    private PackagePricelistAdapter packageAdapter;
    private List<Package> packages = new ArrayList<>();


    private ArrayList<User> allUsers = new ArrayList<>();

    private String userRole;
    private User loggedUser;

    public static PracelistFragment newInstance(String param1, String param2) {
        PracelistFragment fragment = new PracelistFragment();
        Bundle args = new Bundle();
        args.putString("param1", param1);
        args.putString("param2", param2);
        fragment.setArguments(args);
        return fragment;
    }

    public PracelistFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String mParam1 = getArguments().getString("param1");
            String mParam2 = getArguments().getString("param2");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pracelist, container, false);
        // Initialize RecyclerView
        recyclerViewProducts = view.findViewById(R.id.recyclerViewProducts);
        recyclerViewServices = view.findViewById(R.id.recyclerViewServices);
        recyclerViewPackages = view.findViewById(R.id.recyclerViewPackages);

        getAllUsers();
        
        Button buttonGenerateProductsPDF = view.findViewById(R.id.buttonGenerateProductsPDF);
        Button buttonGenerateServicesPDF = view.findViewById(R.id.buttonGenerateServicesPDF);
        Button buttonGeneratePackagesPDF = view.findViewById(R.id.buttonGeneratePackagesPDF);

        buttonGenerateProductsPDF.setOnClickListener(v -> generateProductPracelistPDF(getContext(), products));
        buttonGenerateServicesPDF.setOnClickListener(v -> generateServicesPracelistPDF(getContext(), services));
        buttonGeneratePackagesPDF.setOnClickListener(v -> generatePackagePracelistPDF(getContext(), packages));

        return view;
    }

    private void setServiceAdapter() {
        recyclerViewServices.setHasFixedSize(true);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(getContext()));
        serviceAdapter = new ServicePricelistAdapter(getContext(), services, userRole);
        recyclerViewServices.setAdapter(serviceAdapter);

        serviceAdapter.setOnEditButtonClickListener(service -> {
            FragmentManager fragmentManager = getParentFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, EditServiceFragment.newInstance(service));
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        setProductAdapter();
    }

    private void setProductAdapter() {
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setLayoutManager(new LinearLayoutManager(getContext()));
        productAdapter = new ProductPricelistAdapter(getContext(), products, userRole);
        recyclerViewProducts.setAdapter(productAdapter);

        productAdapter.setOnEditButtonClickListener(product -> {
            FragmentManager fragmentManager = getParentFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, EditProduct.newInstance(product));
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });

        setPackageAdapter();
    }

    private void setPackageAdapter() {
        recyclerViewPackages.setHasFixedSize(true);
        recyclerViewPackages.setLayoutManager(new LinearLayoutManager(getContext()));
        packageAdapter = new PackagePricelistAdapter(getContext(), packages, userRole);
        recyclerViewPackages.setAdapter(packageAdapter);

        packageAdapter.setOnEditButtonClickListener(pkg -> {
            FragmentManager fragmentManager = getParentFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            // Replace with your EditPackageFragment.newInstance(pkg)
            // fragmentTransaction.replace(R.id.fragment_container, EditPackageFragment.newInstance(pkg));
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        });
    }


    private void fetchProductsFromDatabase() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                products.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Product product = snapshot.getValue(Product.class);
                    if (product != null && !product.isDeleted()) {
                        products.add(product);
                    }
                }
                fetchServicesFromDatabase();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Error reading products from the database", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchServicesFromDatabase() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("services");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                services.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Service service = snapshot.getValue(Service.class);
                    if (service != null) {
                        services.add(service);
                    }
                }
                fetchPackagesFromDatabase();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Error reading services from the database", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fetchPackagesFromDatabase() {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("packages");
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                packages.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Package pkg = snapshot.getValue(Package.class);
                    if (pkg != null) {
                        packages.add(pkg);
                    }
                }
                setServiceAdapter();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Error reading packages from the database", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onProductUpdated(Product updatedProduct) {
        // Update product logic
    }

    @Override
    public void OnServiceUpdated(Service updatedService, Context context) {
        // Update service logic
    }




    public static void generateProductPracelistPDF(Context context, List<Product> products) {
        String pdfPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        File pdfFile = new File(pdfPath, "Products pracelist document.pdf");

        try {
            PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
            PdfDocument pdfDocument = new PdfDocument(writer);
            Document document = new Document(pdfDocument);

            document.add(new Paragraph("Products Information"));
            for (Product product : products) {
                document.add(new Paragraph("Name: " + product.getName()));
                document.add(new Paragraph("ID: " + product.getId()));
                document.add(new Paragraph("Price: " + product.getPrice()));
                document.add(new Paragraph("Discount: " + product.getDiscount()));
                document.add(new Paragraph("Discounted price: " + product.getDiscountedPrice()));
                document.add(new Paragraph("------------"));
            }



            document.close();
            Log.d("PDFGenerator", "PDF file generated successfully.");

        } catch (IOException e) {
            Log.e("PDFGenerator", "Error generating PDF file", e);
        }
    }


    public static void generateServicesPracelistPDF(Context context, List<Service> products) {
        String pdfPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        File pdfFile = new File(pdfPath, "Services pracelist document.pdf");

        try {
            PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
            PdfDocument pdfDocument = new PdfDocument(writer);
            Document document = new Document(pdfDocument);

            document.add(new Paragraph("Service pricelist Information"));
            for (Service product : products) {
                document.add(new Paragraph("Name: " + product.getServiceName()));
                document.add(new Paragraph("ID: " + product.getId()));
                document.add(new Paragraph("Price: " + product.getPrice()));
                document.add(new Paragraph("Discount: " + product.getDiscount()));
                document.add(new Paragraph("Discounted price: " + product.getDiscountedPrice()));
                document.add(new Paragraph("------------"));
            }



            document.close();
            Log.d("PDFGenerator", "PDF file generated successfully.");
            Toast.makeText(context.getApplicationContext(), "Downloaded pricelist successifully", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            Log.e("PDFGenerator", "Error generating PDF file", e);
        }
    }



    public static void generatePackagePracelistPDF(Context context, List<Package> products) {
        String pdfPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        File pdfFile = new File(pdfPath, "Packages pracelist document.pdf");

        try {
            PdfWriter writer = new PdfWriter(new FileOutputStream(pdfFile));
            PdfDocument pdfDocument = new PdfDocument(writer);
            Document document = new Document(pdfDocument);

            document.add(new Paragraph("Package pricelist Information"));
            for (Package product : products) {
                document.add(new Paragraph("Name: " + product.getName()));
                document.add(new Paragraph("ID: " + product.getId()));
                document.add(new Paragraph("Price: " + product.getPrice()));
                document.add(new Paragraph("Discount: " + product.getDiscount()));
                document.add(new Paragraph("Discounted price: " + (product.getPrice()-(100-product.getDiscount())/100)));
                document.add(new Paragraph("------------"));
            }



            document.close();
            Log.d("PDFGenerator", "PDF file generated successfully.");
            Toast.makeText(context.getApplicationContext(), "Downloaded pricelist successifully", Toast.LENGTH_SHORT).show();

        } catch (IOException e) {
            Log.e("PDFGenerator", "Error generating PDF file", e);
        }
    }


    private void getAllUsers() {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allUsers.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    if (u != null) {
                        allUsers.add(u);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findUserRole();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllPackages", "Failed to read packages", databaseError.toException());
            }
        });
    }

    private void findUserRole() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if(currentUser == null) {
            userRole = "nk";
        }
        else {
            for(User u: allUsers) {
                if(currentUser.getUid().equals(u.getUid())) {
                    loggedUser = u;
                    userRole = u.getRole();
                    break;
                }
            }
        }

        fetchProductsFromDatabase();
    }

}
