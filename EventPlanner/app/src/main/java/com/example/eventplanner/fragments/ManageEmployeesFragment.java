package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.EmployeeAdapter;
import com.example.eventplanner.adapters.EmptyAdapter;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ManageEmployeesFragment extends Fragment {

    private RecyclerView recyclerView;
    private EmployeeAdapter adapter;
    private List<Employee> employeeList;
    private SearchView searchView;

    private ImageButton imageButton;

    private User user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manage_employees, container, false);

        user=new User();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {

            getUserById(currentUser.getUid());
        }



        // Pronađemo dugme za navigaciju na drugi fragment
        Button buttonNavigateToFragment = view.findViewById(R.id.buttonNavigateToRegister);
        Button buttonNavigateToWorkHours = view.findViewById(R.id.buttonNavigateToWorkhours);
        imageButton = view.findViewById(R.id.imageButton);



        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               populateEmployees();
            }
        });


        // Postavimo rukovanje klikom na dugme
        buttonNavigateToFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigacija na drugi fragment
                RegisterEmployeeFragment fragment = new RegisterEmployeeFragment(); // Zamijenite sa imenom vašeg drugog fragmenta
                FragmentManager fragmentManager = getParentFragmentManager(); // getParentFragmentManager() za fragmente unutar fragmenta, getSupportFragmentManager() za aktivnosti
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment); // Zamijenite R.id.fragment_container s ID-om vašeg kontejnera fragmenta u aktivnosti
                fragmentTransaction.addToBackStack(null); // Dodajemo na back stack ako treba
                fragmentTransaction.commit();
            }
        });

        buttonNavigateToWorkHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigacija na drugi fragment
                UpdateEmployeeWorkHoursFragment fragment = new UpdateEmployeeWorkHoursFragment(); // Zamijenite sa imenom vašeg drugog fragmenta
                FragmentManager fragmentManager = getParentFragmentManager(); // getParentFragmentManager() za fragmente unutar fragmenta, getSupportFragmentManager() za aktivnosti
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment); // Zamijenite R.id.fragment_container s ID-om vašeg kontejnera fragmenta u aktivnosti
                fragmentTransaction.addToBackStack(null); // Dodajemo na back stack ako treba
                fragmentTransaction.commit();
            }
        });

        /*
        buttonWorkhours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigacija na drugi fragment
                UpdateEmployeeWorkHoursFragment fragment = new UpdateEmployeeWorkHoursFragment(); // Zamijenite sa imenom vašeg drugog fragmenta
                FragmentManager fragmentManager = getParentFragmentManager(); // getParentFragmentManager() za fragmente unutar fragmenta, getSupportFragmentManager() za aktivnosti
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment); // Zamijenite R.id.fragment_container s ID-om vašeg kontejnera fragmenta u aktivnosti
                fragmentTransaction.addToBackStack(null); // Dodajemo na back stack ako treba
                fragmentTransaction.commit();
            }
        });
        ImageButton buttonCalendar = view.findViewById(R.id.buttonCalendar);





        // Postavimo rukovanje klikom na dugme
        buttonCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigacija na drugi fragment
                WorkingCalendarFragment fragment = new WorkingCalendarFragment(); // Zamijenite sa imenom vašeg drugog fragmenta
                FragmentManager fragmentManager = getParentFragmentManager(); // getParentFragmentManager() za fragmente unutar fragmenta, getSupportFragmentManager() za aktivnosti
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment); // Zamijenite R.id.fragment_container s ID-om vašeg kontejnera fragmenta u aktivnosti
                fragmentTransaction.addToBackStack(null); // Dodajemo na back stack ako treba
                fragmentTransaction.commit();
            }
        });
                 */


        // Inicijalizacija RecyclerView-a
        recyclerView = view.findViewById(R.id.recyclerViewEmployees);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        employeeList = new ArrayList<>();
        employeeList.clear();

        populateEmployees();

        searchView = view.findViewById(R.id.searchView);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Ovdje možete pozvati filterEmployees metodu s unesenim tekstom

                Log.w("TAG", "QUERY" + query);
                    filterEmployees(query);





                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return true;
            }
        });


        return view;
    }

    private void filterEmployees(String searchText) {
        List<Employee> filteredList = new ArrayList<>();
        for (Employee employee : employeeList) {
            if (!filteredList.contains(employee)) {
                if (employee.getFirstName().toLowerCase().equals(searchText.toLowerCase()) ||
                        employee.getLastName().toLowerCase().equals(searchText.toLowerCase()) ||
                        employee.getEmail().toLowerCase().equals(searchText.toLowerCase())) {
                    Log.w("TAG", "ROLE" + filteredList);
                    filteredList.add(employee);
                }
            }
        }
        adapter.filterList(filteredList);
    }

    private void populateEmployees(){

        employeeList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference usersRef = databaseReference.child("Users");

        // Dohvaćanje podataka iz čvora "employees"
        usersRef.addListenerForSingleValueEvent(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Ovdje možete obraditi podatke koje ste dobili iz baze podataka
                // dataSnapshot sadrži sve podatke iz čvora "employees"

                // Prvo, kreiramo novu listu kako bismo spremili podatke
                List<Employee> newEmployeeList = new ArrayList<>();
                List<User> users= new ArrayList<>();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    User user = userSnapshot.getValue(User.class);
                    if(!users.contains(user) && user.getRole().equals("employee")){
                        users.add(user);
                    }
                }
                populateEmployeesListFromUsers(users,newEmployeeList);
                // Nakon što dobijemo podatke, provjeravamo jesu li već u listi i dodajemo ih samo ako nisu
                for (Employee employee : newEmployeeList) {
                    if (!employeeList.contains(employee)) {
                        employeeList.add(employee);
                    }
                }

                // Nakon što dobijemo podatke, postavimo adapter za RecyclerView
                adapter = new EmployeeAdapter(employeeList, getActivity());
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getActivity(), "Failed to fetch data", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void getUserById(String userId){
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference usersRef = database.getReference("Users");

        usersRef.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // Korisnik s proslijeđenim ID-om je pronađen
                    user = dataSnapshot.getValue(User.class);
                    Log.w("TAG", "ROLE" + user.getRole());
                    // Ovdje možete manipulirati podacima o korisniku
                } else {
                    // Korisnik nije pronađen
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Greška prilikom dohvatanja podataka
                Log.w("TAG", "Greška prilikom dohvatanja korisnika.", databaseError.toException());
            }
        });


    }

    public void populateEmployeesListFromUsers(List<User> users,List<Employee> employees){
        for (User user : users) {
            Employee employee = new Employee( user.getEmail(), "", "", user.getFirstName(), user.getLastName(), user.getAddress(), user.getPhoneNumber(), user.getImageURL());
            employee.setId(user.getUid());
            employees.add(employee);
        }
    }

}