package com.example.eventplanner.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

public class CompanyGrade {

    public String id;
    public String companyName;
    public String odId;
    public String comment;

    public float grade;

    public String date;

    public String pupvId;


    public CompanyGrade(){}
    public CompanyGrade(String companyName, String odId, String comment, float grade, String date, String pupvId) {
        this.companyName = companyName;
        this.odId = odId;
        this.comment = comment;
        this.grade = grade;
        this.date = date;
        this.pupvId=pupvId;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOdId() {
        return odId;
    }

    public void setOdId(String odId) {
        this.odId = odId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public float getGrade() {
        return grade;
    }

    public void setGrade(float grade) {
        this.grade = grade;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPupvId() {
        return pupvId;
    }

    public void setPupvId(String pupvId) {
        this.pupvId = pupvId;
    }
}
