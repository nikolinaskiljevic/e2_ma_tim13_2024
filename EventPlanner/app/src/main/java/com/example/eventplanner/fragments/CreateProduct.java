package com.example.eventplanner.fragments;

import static android.app.Activity.RESULT_OK;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.EventCategory;
import com.example.eventplanner.model.EventSubcategory;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductCategory;
import com.example.eventplanner.model.ProductSubcategory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateProduct#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateProduct extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static final int GALLERY_REQUEST_CODE = 1001;

    private EditText productNameEditText;
    private EditText productDescriptionEditText;
    private EditText productPriceEditText;
    private EditText productDiscountEditText;
    private CheckBox productVisibleCheckBox;
    private CheckBox productAvailableCheckBox;
    private EditText productEventTypesEditText;
    private EditText editTextCustomSubcategory;

    private Button submitButton;
    private List<ProductCategory> allCategories = new ArrayList<>();
    private List<ProductSubcategory> appropriateSubcategories = new ArrayList<>();
    private Spinner productCategorySpinner;
    private Spinner productSubcategorySpinner;
    private List<String> imagePaths = new ArrayList<>();
    private Company currentCompany;
    private List<Company> allCompanies = new ArrayList<>();

    public CreateProduct() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateProduct.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateProduct newInstance(String param1, String param2) {
        CreateProduct fragment = new CreateProduct();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_product, container, false);
        editTextCustomSubcategory = view.findViewById(R.id.editTextCustomSubcategory);

        Spinner spinnerCategory = view.findViewById(R.id.spinnerCategory);
        Spinner spinnerSubcategory = view.findViewById(R.id.spinnerSubcategory);

        getAllCategoriesFromDatabase(spinnerCategory);
        getAllCompanies();

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                updateVisibility(view, spinnerSubcategory);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // Ne radimo ništa kada nije odabrana stavka
            }
        });

        // Pozivamo metodu za ažuriranje vidljivosti kako bismo postavili početne vrednosti
        //updateVisibility(view, spinnerSubcategory);




        // Inicijalizacija polja
        productNameEditText = view.findViewById(R.id.editTextName);
        productCategorySpinner = view.findViewById(R.id.spinnerCategory);
        productSubcategorySpinner = view.findViewById(R.id.spinnerSubcategory);
        productDescriptionEditText = view.findViewById(R.id.editTextDescription);
        productPriceEditText = view.findViewById(R.id.editTextPrice);
        productDiscountEditText = view.findViewById(R.id.editTextDiscount);
        productEventTypesEditText = view.findViewById(R.id.editTextEventTypes);
        productVisibleCheckBox = view.findViewById(R.id.checkBoxVisible);
        productAvailableCheckBox = view.findViewById(R.id.checkBoxAvailable);
        submitButton = view.findViewById(R.id.buttonSubmit);
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products");

        // Dodavanje OnClickListener na dugme za potvrdu
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Čitanje unetih podataka
                String productName = productNameEditText.getText().toString().trim();

                String productCategory = productCategorySpinner.getSelectedItem().toString();
                String productSubcategory;
                if (productSubcategorySpinner != null && productSubcategorySpinner.getSelectedItem() != null) {
                    productSubcategory = productSubcategorySpinner.getSelectedItem().toString();
                } else {
                    productSubcategory = editTextCustomSubcategory.getText().toString().trim();
                }
                String productDescription = productDescriptionEditText.getText().toString().trim();
                double productPrice = Double.parseDouble(productPriceEditText.getText().toString().trim());
                int productDiscount = Integer.parseInt(productDiscountEditText.getText().toString().trim());
                String productEventTypes = productEventTypesEditText.getText().toString().trim();
                boolean productVisible = productVisibleCheckBox.isChecked();
                boolean productAvailable = productAvailableCheckBox.isChecked();

                Product product = new Product(productCategory, productSubcategory, productName, productDescription, productPrice, productDiscount, productEventTypes, productVisible, productAvailable,imagePaths);
                product.setCompanyId(currentCompany.getId());

                saveProductToDatabase(product);
            }
        });
        // Dodajemo slušač za dugme za upload slika
        Button uploadImagesButton = view.findViewById(R.id.buttonUploadImages);
        uploadImagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Ovde implementirajte logiku za odabir slika iz galerije ili snimanje novih slika kamerom
                // Na primer, možete koristiti Intent za odabir slika iz galerije:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, GALLERY_REQUEST_CODE);
            }
        });


        return view;
    }

    private void getAllCompanies() {
        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference("Companies");

        companyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allCompanies.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Company c = snapshot.getValue(Company.class);
                    if (c != null) {
                        allCompanies.add(c);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findCompany();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllCompanies", "Failed to read companies", databaseError.toException());
            }
        });
    }

    private void findCompany() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        for(Company c: allCompanies) {
            if(c.getPupvId().equals(currentUser.getUid())) {
                currentCompany = c;
                break;
            }
        }
    }

    private void saveProductToDatabase(Product product) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products");


        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //long lastId = dataSnapshot.getChildrenCount() + 1; // Generisanje novog ID-a
                String newId = databaseRef.push().getKey();

                // Postavljanje novog ID-a za proizvod i dodavanje u bazu podataka
                //String productId = String.valueOf(lastId);
                product.setId(newId);
                databaseRef.child(newId).setValue(product).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // Ostatak koda
                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze podataka
                Toast.makeText(getContext(), "Greška prilikom čitanja podataka iz baze podataka!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updateVisibility(View rootView, Spinner spinnerSubcat) {

        Spinner spinnerCategory = rootView.findViewById(R.id.spinnerCategory);
        String selectedOptionCategory = spinnerCategory.getSelectedItem().toString();
        String selectedCategory = selectedOptionCategory.substring(selectedOptionCategory.indexOf(":") + 2);

        Spinner spinnerFoodSubcategory = rootView.findViewById(R.id.spinnerSubcategoryFood);




                for(ProductCategory e: allCategories) {
                    if(e.getName().equals(selectedCategory)) {
                        getAllSubcategoriesFromDatabase(e.getId(), spinnerSubcat);
                    }
                }
                if ("None".equals(selectedCategory)){
                    spinnerFoodSubcategory.setVisibility(View.INVISIBLE);
                }
                else {
                    //spinnerFoodSubcategory.setVisibility(View.VISIBLE);
                }



    }

    private void getAllCategoriesFromDatabase(Spinner spinnerCat) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("productCategories");
        allCategories.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ProductCategory ec = snapshot.getValue(ProductCategory.class);
                    if (ec != null) {
                        allCategories.add(ec);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                setupCategorySpinner(spinnerCat);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }


    private void setupCategorySpinner(Spinner spinnerSP) {
        // Kreiramo listu imena zaposlenih
        List<String> categories = new ArrayList<>();
        for (ProductCategory e : allCategories) {
            String var = "Category: " + e.getName();
            categories.add(var);
        }

        String noneVar = "Category: None";
        categories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSP.setAdapter(spinnerAdapter);
    }

    private void getAllSubcategoriesFromDatabase(String categoryId, Spinner spinner) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("productSubcategories");
        appropriateSubcategories.clear();
        databaseRef.orderByChild("categoryId").equalTo(categoryId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ProductSubcategory esc = snapshot.getValue(ProductSubcategory.class);
                    if (esc != null && esc.getType().equals("proizvod")) {
                        appropriateSubcategories.add(esc);
                    }
                }
                // Ovdje možete izvršiti željene akcije s listom podataka podkategorija
                // Na primjer, možete pozvati metod koji postavlja Spinner za podkategorije
                // setupSubcategorySpinner();
                setupSubcategorySpinner(spinner);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Tretirajte grešku prilikom čitanja podataka iz baze
            }
        });
    }

    private void setupSubcategorySpinner(Spinner spinnerSubcat) {
        // Kreiramo listu imena zaposlenih
        List<String> subcategories = new ArrayList<>();
        for (ProductSubcategory s : appropriateSubcategories) {
            String var = "Subcategory: " + s.getName();
            subcategories.add(var);
        }

        String noneVar = "Subcategory: None";
        subcategories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSubcat.setAdapter(spinnerAdapter);
    }

}
