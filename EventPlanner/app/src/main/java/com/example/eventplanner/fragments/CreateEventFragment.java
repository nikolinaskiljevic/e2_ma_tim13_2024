package com.example.eventplanner.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.eventplanner.R;
import com.example.eventplanner.model.Event;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class CreateEventFragment extends Fragment {

    private Spinner eventType;
    private EditText eventName;
    private EditText eventDescription;
    private EditText maximumParticipants;
    private EditText eventLocation;
    private EditText eventDistance;
    private RadioButton openButton;
    private RadioButton closeButton;
    private DatePicker eventDate;
    private Button createButton;

    public CreateEventFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_event, container, false);

        eventType = view.findViewById(R.id.spinnerEventType);
        eventName = view.findViewById(R.id.editTextEventName);
        eventDescription = view.findViewById(R.id.editTextEventDescription);
        maximumParticipants = view.findViewById(R.id.editTextMaxParticipants);
        eventLocation = view.findViewById(R.id.editTextLocation);
        eventDistance = view.findViewById(R.id.editTextDistance);
        openButton = view.findViewById(R.id.radioButtonOpen);
        closeButton = view.findViewById(R.id.radioButtonClose);
        eventDate = view.findViewById(R.id.datePickerEventDate);
        createButton = view.findViewById(R.id.buttonCreateEvent);

        // Dodavanje TextWatcher-a na sva polja
        eventName.addTextChangedListener(watcher);
        eventDescription.addTextChangedListener(watcher);
        maximumParticipants.addTextChangedListener(watcher);
        eventLocation.addTextChangedListener(watcher);
        eventDistance.addTextChangedListener(watcher);

        // Postavljanje početnog stanja dugmeta "Create"
        createButton.setEnabled(false);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

                // Logika za čuvanje događaja u bazi podataka
                String selectedOption = eventType.getSelectedItem().toString();
                String type = selectedOption.substring(selectedOption.indexOf(":") + 2);
                String name = eventName.getText().toString();
                String description = eventDescription.getText().toString();
                String participants = maximumParticipants.getText().toString();
                int maxParticipants = Integer.parseInt(participants);
                String location = eventLocation.getText().toString();
                String distance = eventDistance.getText().toString();
                String locationAndDistance = location + ", up to " + distance + " km";
                String privateRule = openButton.isChecked() ? "Yes" : "No";
                int year = eventDate.getYear();
                int month = eventDate.getMonth();
                int dayOfMonth = eventDate.getDayOfMonth();
                String date = String.format("%02d.%02d.%04d", dayOfMonth, month + 1, year);

                Event event = new Event(currentUser.getUid(), type, name, description, maxParticipants, locationAndDistance, privateRule, date);

                saveEventToDatabase(event);
            }
        });

        return view;
    }

    // TextWatcher koji prati promene u svakom polju teksta
    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            // Provera da li su sva polja popunjena
            boolean allFieldsFilled = !TextUtils.isEmpty(eventName.getText().toString().trim()) &&
                    !TextUtils.isEmpty(eventDescription.getText().toString().trim()) &&
                    !TextUtils.isEmpty(maximumParticipants.getText().toString().trim()) &&
                    !TextUtils.isEmpty(eventLocation.getText().toString().trim()) &&
                    !TextUtils.isEmpty(eventDistance.getText().toString().trim());

            // Omogućavanje ili onemogućavanje dugmeta na osnovu toga da li su sva polja popunjena
            createButton.setEnabled(allFieldsFilled);
        }
    };

    private void saveEventToDatabase(Event event) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("events");
        String eventId = databaseRef.push().getKey();
        event.setId(eventId);
        databaseRef.child(eventId).setValue(event);

        Toast.makeText(getContext(), "Event added successfully!", Toast.LENGTH_SHORT).show();
    }
}
