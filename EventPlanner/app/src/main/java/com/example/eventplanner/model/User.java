package com.example.eventplanner.model;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String uid;
    private String role;
    private String email;
    private String firstName;
    private String lastName;
    private String address;
    private String phoneNumber;
    private String imageURL;
    private boolean isDeactivated;
    private boolean isAdminVerified;
    private String pupvId;
    private List<String> favouritesProductsIds;
    private List<String> favouritesServicesIds;
    private List<String> favouritesPackagesIds;
    private boolean blocked;

    public User() {
        // Default constructor required for Firestore
    }

    public User(String uid,  String role, String email, String firstName, String lastName, String address, String phoneNumber, boolean isDeactivated) {
        this.uid = uid;
        this.role = role;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.isDeactivated = isDeactivated;
        this.isAdminVerified = false;
        this.pupvId="";
        this.favouritesProductsIds = new ArrayList<>();
        this.favouritesProductsIds.add("");
        this.favouritesServicesIds = new ArrayList<>();
        this.favouritesServicesIds.add("");
        this.favouritesPackagesIds = new ArrayList<>();
        this.favouritesPackagesIds.add("");
        this.blocked = false;
    }

    // Getters and setters
    // You can generate these automatically in Android Studio
    // or write them manually as follows


    public List<String> getFavouritesProductsIds() {
        return favouritesProductsIds;
    }

    public void setFavouritesProductsIds(List<String> favouritesProductsIds) {
        this.favouritesProductsIds = favouritesProductsIds;
    }

    public List<String> getFavouritesServicesIds() {
        return favouritesServicesIds;
    }

    public void setFavouritesServicesIds(List<String> favouritesServicesIds) {
        this.favouritesServicesIds = favouritesServicesIds;
    }

    public List<String> getFavouritesPackagesIds() {
        return favouritesPackagesIds;
    }

    public void setFavouritesPackagesIds(List<String> favouritesPackagesIds) {
        this.favouritesPackagesIds = favouritesPackagesIds;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDeactivated() {
        return isDeactivated;
    }

    public void setDeactivated(boolean deactivated) {
        isDeactivated = deactivated;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean isAdminVerified() {
        return isAdminVerified;
    }

    public void setAdminVerified(boolean adminVerified) {
        isAdminVerified = adminVerified;
    }

    public String getPupvId() {
        return pupvId;
    }

    public void setPupvId(String pupvId) {
        this.pupvId = pupvId;
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }
}
