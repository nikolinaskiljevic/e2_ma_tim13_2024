package com.example.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Message;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


import java.util.ArrayList;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {
    private ArrayList<Message> oldMessages;
    private String currentUserId;
    private Context context;

    public MessageAdapter(Context context, ArrayList<Message> messages) {
        this.context = context;
        this.oldMessages = messages;
        FirebaseUser current = FirebaseAuth.getInstance().getCurrentUser();
        currentUserId = current.getUid();
    }

    @NonNull
    @Override
    public MessageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_card, parent, false);
        return new MessageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.ViewHolder holder, int position) {
        Message m = oldMessages.get(position);
        holder.textViewMessageContent.setText(m.getContent());
        holder.textViewMessageDate.setText(m.getDate());

        // Podesavanje layout_gravity na osnovu toga ko je poslao poruku
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.messageContainer.getLayoutParams();
        if (currentUserId.equals(m.getSenderId())) {
            params.gravity = Gravity.END;
            holder.messageContainer.setBackgroundResource(R.drawable.message_background_user);
        } else {
            params.gravity = Gravity.START;
            holder.messageContainer.setBackgroundResource(R.drawable.message_background);
        }

        holder.messageContainer.setLayoutParams(params);
    }

    @Override
    public int getItemCount() {
        return oldMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewMessageContent;
        public TextView textViewMessageDate;
        public LinearLayout messageContainer;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewMessageContent = itemView.findViewById(R.id.textViewMessageContent);
            textViewMessageDate = itemView.findViewById(R.id.textViewMessageDate);
            messageContainer = itemView.findViewById(R.id.messageContainer);
        }
    }
}
