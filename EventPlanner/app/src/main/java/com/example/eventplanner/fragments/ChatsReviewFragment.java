package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.ChatsReviewAdapter;
import com.example.eventplanner.adapters.MessageAdapter;
import com.example.eventplanner.model.EventAgenda;
import com.example.eventplanner.model.Message;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChatsReviewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChatsReviewFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private RecyclerView yourChats;
    private ChatsReviewAdapter chatsReviewAdapter;
    private ArrayList<Message> allMessages = new ArrayList<>();
    private ArrayList<Message> loggedUserMessages = new ArrayList<>();
    private ArrayList<User> allUsers = new ArrayList<>();


    public ChatsReviewFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ChatsReviewFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ChatsReviewFragment newInstance(String param1, String param2) {
        ChatsReviewFragment fragment = new ChatsReviewFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats_review, container, false);

        yourChats = view.findViewById(R.id.recyclerViewYourChats);
        getAllMessages();

        return view;
    }

    private void getAllMessages() {
        DatabaseReference messageRef = FirebaseDatabase.getInstance().getReference("messages");

        messageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allMessages.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Message m = snapshot.getValue(Message.class);
                    if (m != null) {
                        allMessages.add(m);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findLoggedUserMessages();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllMessages", "Failed to read messages", databaseError.toException());
            }
        });
    }

    private void findLoggedUserMessages() {
        loggedUserMessages.clear();
        FirebaseUser loggedUser = FirebaseAuth.getInstance().getCurrentUser();

        for(Message m: allMessages) {
            if(m.getSenderId().equals(loggedUser.getUid()) || m.getRecipientId().equals(loggedUser.getUid())) {
                loggedUserMessages.add(m);
            }
        }

        getAllUsers();
    }

    private void getAllUsers() {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allUsers.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    if (u != null) {
                        allUsers.add(u);
                    }
                }
                // Sada usersList sadrži sve korisnike
                groupMessages();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private void groupMessages() {
        FirebaseUser loggedUser = FirebaseAuth.getInstance().getCurrentUser();
        Map<String, ArrayList<Message>> chats = new HashMap<>();
        List<String> sendersIds = new ArrayList<>();
        List<String> recipientsIds = new ArrayList<>();

        for(Message m: loggedUserMessages) {
            if(m.getSenderId().equals(loggedUser.getUid())) {
                //ulogovani korisnik je poslao poruku nekome
                recipientsIds.add(m.getRecipientId());
            }
            else {
                //ulogovani korisnik je primio poruku od nekoga
                sendersIds.add(m.getSenderId());
            }
        }

        List<String> personsIds = findUnionWithoutDuplicates(sendersIds, recipientsIds);
        for(String p: personsIds) {
            ArrayList<Message> messagesWithPerson = new ArrayList<>();
            for(Message m: loggedUserMessages) {
                if(m.getSenderId().equals(p) || m.getRecipientId().equals(p)) {
                    messagesWithPerson.add(m);
                }
            }

            chats.put(p, messagesWithPerson);
        }

        setChatsReviewAdapter(chats);
    }

    private List<String> findUnionWithoutDuplicates(List<String> list1, List<String> list2) {
        Set<String> unionSet = new HashSet<>(); // Koristimo HashSet kako bismo eliminisali duplikate

        unionSet.addAll(list1); // Dodajemo sve elemente iz prve liste
        unionSet.addAll(list2); // Dodajemo sve elemente iz druge liste

        return new ArrayList<>(unionSet); // Pretvaramo set u listu i vraćamo
    }


    private void setChatsReviewAdapter(Map<String, ArrayList<Message>> chats) {
        yourChats.setHasFixedSize(true);
        yourChats.setLayoutManager(new LinearLayoutManager(getContext()));

        chatsReviewAdapter = new ChatsReviewAdapter(getContext(), chats, allUsers);
        yourChats.setAdapter(chatsReviewAdapter);
    }
}