package com.example.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.fragments.ChatFragment;
import com.example.eventplanner.fragments.CompanyProfileFragment;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.Package;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.ProductReservation;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ServiceDetailsAdapter extends RecyclerView.Adapter<ServiceDetailsAdapter.ViewHolder> {

    private ArrayList<Service> allServices;
    private ArrayList<User> allUsers = new ArrayList<>();
    private ArrayList<Event> organizerEvents;
    private User currentUser;
    private String role;
    private Context context;

    public ServiceDetailsAdapter(Context context, ArrayList<Service> services, User user, String role, ArrayList<Event> events) {
        this.context = context;
        this.allServices = services;
        this.currentUser = user;
        this.role = role;
        this.organizerEvents = events;
    }

    @NonNull
    @Override
    public ServiceDetailsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_details_card, parent, false);
        return new ServiceDetailsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ServiceDetailsAdapter.ViewHolder holder, int position) {
        Service s = allServices.get(position);
        holder.textViewServiceName.setText(s.getServiceName());
        holder.textViewServiceCategory.setText(s.getCategory());
        holder.textViewServiceSubcategory.setText(s.getSubcategory());
        holder.textViewServiceDescription.setText("Description: " + s.getDescription());
        holder.textViewServiceSpecifics.setText("Specifics: " + s.getSpecifics());
        holder.textViewServicePrice.setText("Price: " + String.format("%.2f", s.getPrice()) + "$");
        holder.textViewServiceDiscount.setText("Discount: " + String.format("%.2f", s.getDiscount()) + "%");
        holder.textViewServiceDuration.setText("Duration: " + s.getDuration());
        holder.textViewServiceBookingDeadline.setText("Booking deadline: " + s.getBookingDeadline());
        holder.textViewServiceCancellationDeadline.setText("Cancellation deadline: " + s.getCancellationDeadline());
        holder.textViewServiceReservationConfirmation.setText("Confirmation method: " + s.getReservationConfirmation());
        holder.textViewServiceAvailable.setText("Availability: " + (s.isAvailable() ? "Available" : "Unavailable"));
        holder.buttonReserveService.setText("Reserve " + s.getServiceName());
        if(s.isAvailable()) {
            holder.buttonReserveService.setEnabled(true);
        }
        if(role.equals("organizer")) {
            holder.buttonAddToFavourites.setVisibility(View.VISIBLE);
            holder.spinnerOrganizerEvents.setVisibility(View.VISIBLE);
            holder.buttonOpenEmployeeChat.setVisibility(View.VISIBLE);
            holder.spinnerServiceEmployees.setVisibility(View.VISIBLE);
            setupSpinner(holder.spinnerOrganizerEvents);
            setupEmployeeSpinner(holder.spinnerServiceEmployees, s.getEmployees());
        }
        else {
            holder.buttonReserveService.setVisibility(View.GONE);
            holder.spinnerOrganizerEvents.setVisibility(View.GONE);
            holder.buttonOpenEmployeeChat.setVisibility(View.GONE);
            holder.spinnerServiceEmployees.setVisibility(View.GONE);
        }
        if(role.equals("organizer") && currentUser.getFavouritesServicesIds().contains(s.getId())) {
            holder.buttonAddToFavourites.setEnabled(false);
        }

        holder.buttonOpenEmployeeChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User selectedUser = findSelectedEmployee(holder.spinnerServiceEmployees);
                // Poziv metode za rezervisanje usluge

                ChatFragment chatFragment = ChatFragment.newInstance(selectedUser.getUid());

                // Pokreni transakciju fragmenta
                FragmentActivity activity = (FragmentActivity) context;
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, chatFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        holder.buttonAddToFavourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = holder.getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    Service s = allServices.get(position);
                    // Ovde radite šta je potrebno sa objektom product, na primer dodavanje u omiljene
                    addToFavourites(s);
                }
            }
        });

        holder.buttonReserveService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Event selectedEvent = findSelectedEvent(holder.spinnerOrganizerEvents);
                if (selectedEvent != null) {
                    reserveService(s, selectedEvent);
                } else {
                    Toast.makeText(context, "Please select an event to reserve the service.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.buttonCompanyProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Pretpostavljam da vaš proizvod ima atribut companyId
                String companyId = s.getCompanyId();

                // Kreiraj instancu CompanyProfileFragment sa companyId
                CompanyProfileFragment companyProfileFragment = CompanyProfileFragment.newInstance(companyId, role);

                // Pokreni transakciju fragmenta
                FragmentActivity activity = (FragmentActivity) context;
                FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, companyProfileFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    private void reserveService(Service service, Event event) {

    }

    private void getAllUsers() {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference("Users");

        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allUsers.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User u = snapshot.getValue(User.class);
                    if (u != null) {
                        allUsers.add(u);
                    }
                }
                // Sada usersList sadrži sve korisnike
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllUsers", "Failed to read users", databaseError.toException());
            }
        });
    }

    private User findSelectedEmployee(Spinner spinner) {
        String selected = "";
        if(spinner.getSelectedItem() != null) {
            selected = spinner.getSelectedItem().toString();
        }
        String regex = "Contact employee: (.+?) (.+)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selected);

        if (matcher.find()) {
            String employeeName = matcher.group(1); // Ekstraktovano ime događaja
            String employeeSurname = matcher.group(2);
            return findEmployee(employeeName, employeeSurname);
        } else {
            System.out.println("Not founded employee");
            return null;
        }
    }

    private User findEmployee(String employeeName, String employeeSurname) {
        for(User e: allUsers) {
            if(e.getFirstName().equals(employeeName) && e.getLastName().equals(employeeSurname)) {
                Toast.makeText((FragmentActivity) context, "Contact employee: " + e.getFirstName() + " " + e.getLastName(), Toast.LENGTH_SHORT).show();
                return e;
            }
        }

        return null;
    }

    private void setupEmployeeSpinner(Spinner spinner, List<User> employees) {
        // Kreiramo listu imena dogadjaja
        List<String> employeeList = new ArrayList<>();
        for (User e : employees) {
            String var = "Contact employee: " + e.getFirstName() + " " + e.getLastName();
            employeeList.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>((FragmentActivity) context, android.R.layout.simple_spinner_item, employeeList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinner.setAdapter(spinnerAdapter);
        getAllUsers();
    }

    private Event findSelectedEvent(Spinner spinner) {
        String selected = "";
        if(spinner.getSelectedItem() != null) {
            selected = spinner.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selected);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            return findEvent(eventName);
        } else {
            System.out.println("Not founded event");
            return null;
        }
    }

    private Event findEvent(String eventName) {
        for(Event e: organizerEvents) {
            if(e.getEventName().equals(eventName)) {
                Toast.makeText((FragmentActivity) context, "Selected event: " + e.getEventName(), Toast.LENGTH_SHORT).show();
                return e;
            }
        }

        return null;
    }

    private void setupSpinner(Spinner spinner) {
        // Kreiramo listu imena dogadjaja
        List<String> events = new ArrayList<>();
        for (Event e : organizerEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>((FragmentActivity) context, android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinner.setAdapter(spinnerAdapter);
    }

    private void addToFavourites(Service s) {
        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());
        List<String> favServices = currentUser.getFavouritesServicesIds();
        favServices.add(s.getId());
        currentUser.setFavouritesServicesIds(favServices);

        userRef.setValue(currentUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText((FragmentActivity) context, "Well done!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText((FragmentActivity) context, "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allServices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewServiceName;
        public TextView textViewServiceCategory;
        public TextView textViewServiceSubcategory;
        public TextView textViewServiceDescription;
        public TextView textViewServiceSpecifics;
        public TextView textViewServicePrice;
        public TextView textViewServiceDiscount;
        public TextView textViewServiceDuration;
        public TextView textViewServiceBookingDeadline;
        public TextView textViewServiceCancellationDeadline;
        public TextView textViewServiceReservationConfirmation;
        public TextView textViewServiceAvailable;
        public Button buttonReserveService;
        public Button buttonCompanyProfile;
        public Button buttonAddToFavourites;
        public Spinner spinnerOrganizerEvents;
        public Button buttonOpenEmployeeChat;
        public Spinner spinnerServiceEmployees;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewServiceName = itemView.findViewById(R.id.textViewServiceName);
            textViewServiceCategory = itemView.findViewById(R.id.textViewServiceCategory);
            textViewServiceSubcategory = itemView.findViewById(R.id.textViewServiceSubcategory);
            textViewServiceDescription = itemView.findViewById(R.id.textViewServiceDescription);
            textViewServiceSpecifics = itemView.findViewById(R.id.textViewServiceSpecifics);
            textViewServicePrice = itemView.findViewById(R.id.textViewServicePrice);
            textViewServiceDiscount = itemView.findViewById(R.id.textViewServiceDiscount);
            textViewServiceDuration = itemView.findViewById(R.id.textViewServiceDuration);
            textViewServiceBookingDeadline = itemView.findViewById(R.id.textViewServiceBookingDeadline);
            textViewServiceCancellationDeadline = itemView.findViewById(R.id.textViewServiceCancellationDeadline);
            textViewServiceReservationConfirmation = itemView.findViewById(R.id.textViewServiceReservationConfirmation);
            textViewServiceAvailable = itemView.findViewById(R.id.textViewServiceAvailable);
            buttonReserveService = itemView.findViewById(R.id.buttonReserveService);
            buttonCompanyProfile = itemView.findViewById(R.id.buttonCompanyProfile);
            buttonAddToFavourites = itemView.findViewById(R.id.buttonAddToFavourites);
            spinnerOrganizerEvents = itemView.findViewById(R.id.spinnerOrganizerEvents);
            buttonOpenEmployeeChat = itemView.findViewById(R.id.buttonOpenEmployeeChat);
            spinnerServiceEmployees = itemView.findViewById(R.id.spinnerServiceEmployees);
        }
    }

}
