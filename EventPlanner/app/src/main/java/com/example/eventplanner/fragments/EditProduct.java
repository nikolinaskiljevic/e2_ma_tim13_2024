package com.example.eventplanner.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EditProduct#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProduct extends Fragment implements Parcelable {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PRODUCT = "product";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public EditProduct() {
        // Required empty public constructor
    }

    protected EditProduct(Parcel in) {
        mParam1 = in.readString();
        mParam2 = in.readString();
    }

    public static final Creator<EditProduct> CREATOR = new Creator<EditProduct>() {
        @Override
        public EditProduct createFromParcel(Parcel in) {
            return new EditProduct(in);
        }

        @Override
        public EditProduct[] newArray(int size) {
            return new EditProduct[size];
        }
    };

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment EditProduct.
     */
    // TODO: Rename and change types and number of parameters
    public static EditProduct newInstance(Product product) {
        EditProduct fragment = new EditProduct();
        Bundle args = new Bundle();
        args.putParcelable(ARG_PRODUCT, (Parcelable) product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_product, container, false);

        // Inicijalizacija Views
        Spinner spinnerSubcategory = view.findViewById(R.id.spinnerSubcategory);
        EditText editTextName = view.findViewById(R.id.editTextName);
        EditText editTextDescription = view.findViewById(R.id.editTextDescription);
        EditText editTextPrice = view.findViewById(R.id.editTextPrice);
        EditText editTextDiscount = view.findViewById(R.id.editTextDiscount);
        EditText editTextEventTypes = view.findViewById(R.id.editTextEventTypes);
        CheckBox checkBoxVisible = view.findViewById(R.id.checkBoxVisible);
        CheckBox checkBoxAvailable = view.findViewById(R.id.checkBoxAvailable);
        Button buttonSubmit = view.findViewById(R.id.buttonSubmit);


        // Dobijanje prosleđenog proizvoda iz argumenta fragmenta
        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_PRODUCT)) {
            Product product = args.getParcelable(ARG_PRODUCT);


            spinnerSubcategory.setSelection(product.getSubcategory().length()-1);


            editTextName.setText(product.getName());
            editTextDescription.setText(product.getDescription());
            editTextPrice.setText(String.valueOf(product.getPrice()));
            editTextDiscount.setText(String.valueOf(product.getDiscount()));
            editTextEventTypes.setText(product.getEventTypes());
            checkBoxVisible.setChecked(product.isVisible());
            checkBoxAvailable.setChecked(product.isAvailable());

            // Postavljanje slušača na dugme za potvrdu izmene
            buttonSubmit.setOnClickListener(v -> {
                // Ažuriranje proizvoda sa novim vrednostima
                Product updatedProduct = new Product();
                //updatedProduct.setSubcategory(spinnerSubcategory.getSelectedItem().toString());
                updatedProduct.setName(editTextName.getText().toString());
                updatedProduct.setDescription(editTextDescription.getText().toString());
                updatedProduct.setPrice((int) Double.parseDouble(editTextPrice.getText().toString()));
                updatedProduct.setDiscount(Integer.parseInt(editTextDiscount.getText().toString()));
                updatedProduct.setEventTypes(editTextEventTypes.getText().toString());
                updatedProduct.setVisible(checkBoxVisible.isChecked());
                updatedProduct.setAvailable(checkBoxAvailable.isChecked());

                // Poziv metode za čuvanje izmenjenog proizvoda
                saveProductChanges(updatedProduct);
                // Zatvaranje EditProduct fragmenta i povratak na prethodni fragment
                getActivity().getSupportFragmentManager().popBackStack();

            });
        }
        return view;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(mParam1);
        dest.writeString(mParam2);
    }

    public interface OnProductUpdateListener {
        void onProductUpdated(Product updatedProduct);
    }

    private OnProductUpdateListener mListener;

    // Ostatak koda fragmenta EditProduct


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnProductUpdateListener) {
            mListener = (OnProductUpdateListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnProductUpdateListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void saveProductChanges(Product updatedProduct) {
        if (mListener != null) {
            mListener.onProductUpdated(updatedProduct);


            // Pristupanje bazi podataka "products" kako bismo pronašli odgovarajući proizvod
            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products");

            // Dodavanje slušača koji se poziva kada se dohvate podaci iz baze podataka
            databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Product product = snapshot.getValue(Product.class);
                        Log.d("nadjen",snapshot.getKey());
                        Log.d("nadjen",snapshot.getKey());
                        //int productid = Integer.parseInt(snapshot.getKey());
                        String productId = snapshot.getKey();// Dohvatanje ključa proizvoda
                        if (product != null && productId.equals(product.getId())) {
                            DatabaseReference productRef = databaseRef.child(productId);
                            Log.d("azuriiraan ", product.getName());
                            productRef.setValue(updatedProduct)
                                    .addOnSuccessListener(aVoid -> {
                                        // Ažuriranje uspešno završeno
                                    })
                                    .addOnFailureListener(e -> {
                                        // Greška pri ažuriranju
                                    });
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Greška pri čitanju iz baze podataka
                    // Možete ovde izvršiti neku radnju ako dođe do greške
                }
            });
        }
    }

}


