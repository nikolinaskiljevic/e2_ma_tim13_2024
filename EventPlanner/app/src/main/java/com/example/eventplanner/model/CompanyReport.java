package com.example.eventplanner.model;

public class CompanyReport {

    private String id;
    private String companyId;
    private String reasons;
    private String userId;
    public CompanyReport(){}

    public CompanyReport(String id, String companyId, String reasons, String userId) {
        this.id = id;
        this.companyId = companyId;
        this.reasons = reasons;
        this.userId = userId;
    }

    public CompanyReport(String companyId, String reasons, String userId) {
        this.companyId = companyId;
        this.reasons = reasons;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reasons) {
        this.reasons = reasons;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
