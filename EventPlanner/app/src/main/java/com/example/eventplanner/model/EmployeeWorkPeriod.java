package com.example.eventplanner.model;

import java.util.Date;
import java.util.List;

public class EmployeeWorkPeriod {

    private String id;
    private List<String> employeesId;

    private String from;
    private String to;
    private String weeklyWorkHoursId;

    public EmployeeWorkPeriod(){}
    public EmployeeWorkPeriod(String from, String to) {
        this.from = from;
        this.to = to;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getEmployeesId() {
        return employeesId;
    }

    public void setEmployeesId(List<String> employeesId) {
        this.employeesId = employeesId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getWeeklyWorkHoursId() {
        return weeklyWorkHoursId;
    }

    public void setWeeklyWorkHoursId(String weeklyWorkHoursId) {
        this.weeklyWorkHoursId = weeklyWorkHoursId;
    }
}
