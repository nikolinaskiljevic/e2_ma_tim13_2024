package com.example.eventplanner.model;

public class EventSubcategory {
    private String id;
    private String categoryId; // ID of the category to which this subcategory belongs
    private String name;

    public EventSubcategory() {
        // Default constructor required for Firebase
    }

    public EventSubcategory(String id, String categoryId, String name) {
        this.id = id;
        this.categoryId = categoryId;
        this.name = name;
    }

    // Getters and setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
