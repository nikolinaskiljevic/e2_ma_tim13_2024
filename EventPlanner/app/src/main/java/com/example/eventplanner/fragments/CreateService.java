package com.example.eventplanner.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Company;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.ProductCategory;
import com.example.eventplanner.model.ProductSubcategory;
import com.example.eventplanner.model.Service;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateService#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateService extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private EditText editTextCustomSubcategory;
    private List<ProductCategory> allCategories = new ArrayList<>();
    private List<ProductSubcategory> appropriateSubcategories = new ArrayList<>();
    private List<Company> allCompanies = new ArrayList<>();
    private Company currentCompany;

    public CreateService() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateService.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateService newInstance(String param1, String param2) {
        CreateService fragment = new CreateService();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_service, container, false);


        getAllCompanies();
        // Initialize Views

        Spinner spinnerCategory = view.findViewById(R.id.spinnerCategory);
        Spinner spinnerSubcategory = view.findViewById(R.id.spinnerSubcategory);
        EditText editTextServiceName = view.findViewById(R.id.editTextServiceName);
        EditText editTextDescription = view.findViewById(R.id.editTextDescription);
        EditText editTextPrice = view.findViewById(R.id.editTextPrice);
        EditText editTextDiscount = view.findViewById(R.id.editTextDiscount);
        MultiAutoCompleteTextView editTextEventTypes = view.findViewById(R.id.multiAutoCompleteTextViewEventTypes);
        editTextEventTypes.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        CheckBox checkBoxVisibility = view.findViewById(R.id.checkBoxVisibility);
        CheckBox checkBoxAvailability = view.findViewById(R.id.checkBoxAvailability);
        EditText editTextDuration = view.findViewById(R.id.editTextDuration);
        RadioGroup radioGroupConfirmationMethod = view.findViewById(R.id.radioGroupConfirmationMethod);
        Button buttonSubmit = view.findViewById(R.id.buttonSubmit);
        editTextCustomSubcategory = view.findViewById(R.id.editTextCustomSubcategory);
        EditText editTextReservationDeadline = view.findViewById(R.id.editTextReservationDeadline);
        EditText editTextCancellationDeadline = view.findViewById(R.id.editTextCancellationDeadline);

        //-----------------------------------------------------------------------------
        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(requireContext(),
                R.array.category_options, android.R.layout.simple_spinner_item);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(categoryAdapter);

        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedCategory = parent.getItemAtPosition(position).toString();
                ArrayAdapter<CharSequence> subcategoryAdapter;

                switch (selectedCategory) {
                    case "Category: Food and catering":
                        subcategoryAdapter = ArrayAdapter.createFromResource(requireContext(),
                                R.array.subcategory_food_options, android.R.layout.simple_spinner_item);
                        break;
                    case "Photo and video":
                        subcategoryAdapter = ArrayAdapter.createFromResource(requireContext(),
                                R.array.subcategory_photo_options, android.R.layout.simple_spinner_item);
                        break;
                    default:
                        // Ukoliko nema odgovarajuće kategorije, koristimo praznu listu
                        subcategoryAdapter = new ArrayAdapter<>(requireContext(),
                                android.R.layout.simple_spinner_item);
                        break;
                }

                subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinnerSubcategory.setAdapter(subcategoryAdapter);
                // Ako nema dostupnih podkategorija, prikaži EditText polje za unos prilagođene podkategorije
                if (subcategoryAdapter.getCount() == 0) {
                    editTextCustomSubcategory.setVisibility(View.VISIBLE);
                } else {
                    editTextCustomSubcategory.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Ne radimo ništa kada ništa nije izabrano
            }
        });

        // Ostatak koda fragmenta...

        // Pronađite LinearLayout koji će sadržati dinamički generisane CheckBox-ove
        LinearLayout employeeCheckBoxContainer = view.findViewById(R.id.employeeCheckBoxContainer);
        List<User> employees = new ArrayList<>();
        // Pozovite metodu getEmployeeListFromDatabase() sa odgovarajućim callback-om
        getEmployeeListFromDatabase(new EmployeeDataCallback() {
            @Override
            public void onEmployeeDataReceived(List<User> employeeList) {
                // Ovde možete koristiti listu zaposlenih koja je preuzeta iz baze
                // Napravite CheckBox-ove za svakog zaposlenog i dodajte ih u LinearLayout
                for (User employee : employeeList) {
                    CheckBox checkBox = new CheckBox(requireContext());
                    checkBox.setText(employee.getFirstName() + " " + employee.getLastName());
                    employeeCheckBoxContainer.addView(checkBox);
                    employees.add(employee);
                }
            }
        });


        // Firebase database reference
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("services");

        // Submit button click listener
        buttonSubmit.setOnClickListener(v -> {
            // Create a new Service object with the provided data
            Service service = new Service();

            service.setCategory(spinnerCategory.getSelectedItem().toString());


            // Generisanje novog ID-a
            DatabaseReference serviceRef = databaseRef.push(); // Kreiranje referenca za novi servis
            //String serviceId = serviceRef.getKey(); // Dobijanje generisanog ID-a


            // Proverite da li je polje za unos podkategorije vidljivo
            if (editTextCustomSubcategory.getVisibility() == View.VISIBLE) {
                // Ako jeste, postavite unetu vrednost kao podkategoriju servisa
                service.setSubcategory(editTextCustomSubcategory.getText().toString());
            } else {
                // Ako nije, preuzmite vrednost iz spinner-a za podkategoriju
                service.setSubcategory(spinnerSubcategory.getSelectedItem().toString());
            }
            service.setServiceName(editTextServiceName.getText().toString());
            service.setDescription(editTextDescription.getText().toString());
            service.setDescription(editTextDescription.getText().toString());
            service.setPrice(Double.parseDouble(editTextPrice.getText().toString()));
            service.setDiscount(Integer.parseInt(editTextDiscount.getText().toString()));
            // Add event types
            String reservationDeadline = editTextReservationDeadline.getText().toString();
            String cancellationDeadline = editTextCancellationDeadline.getText().toString();
            service.setBookingDeadline(reservationDeadline);
            service.setCancellationDeadline(cancellationDeadline);

            List<String> eventTypes = new ArrayList<>();
            eventTypes.add(editTextEventTypes.getText().toString());
            service.setEventTypes(eventTypes);
            service.setVisible(checkBoxVisibility.isChecked());
            service.setAvailable(checkBoxAvailability.isChecked());
            service.setDuration(editTextDuration.getText().toString());

            service.setCompanyId(currentCompany.getId());

            //----

            service.setEmployees(employees);
            // Set confirmation method based on radio button selection
            RadioButton radioButton = view.findViewById(radioGroupConfirmationMethod.getCheckedRadioButtonId());
            service.setReservationConfirmation(radioButton.getText().toString());


            // Save the service to the database
            databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    long lastId = dataSnapshot.getChildrenCount() + 1; // Generisanje novog ID-a

                    // Postavljanje novog ID-a za proizvod i dodavanje u bazu podataka
                    //String serviceId = String.valueOf(lastId);
                    String serviceId = databaseRef.push().getKey();
                    //service.setId(Integer.parseInt(serviceId));
                    service.setId(serviceId);
                    databaseRef.child(serviceId).setValue(service).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            // Ostatak koda
                        }
                    });
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Greška prilikom čitanja podataka iz baze podataka
                    Toast.makeText(getContext(), "Greška prilikom čitanja podataka iz baze podataka!", Toast.LENGTH_SHORT).show();
                }
            });

        });
        return view;
    }

    private void getAllCompanies() {
        DatabaseReference companyRef = FirebaseDatabase.getInstance().getReference("Companies");

        companyRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allCompanies.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Company c = snapshot.getValue(Company.class);
                    if (c != null) {
                        allCompanies.add(c);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findCompany();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllCompanies", "Failed to read companies", databaseError.toException());
            }
        });
    }

    private void findCompany() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        for(Company c: allCompanies) {
            if(c.getPupvId().equals(currentUser.getUid())) {
                currentCompany = c;
                break;
            }
        }
    }

    public interface EmployeeDataCallback {
        void onEmployeeDataReceived(List<User> employeeList);
    }

    public void getEmployeeListFromDatabase(EmployeeDataCallback callback) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference employeesRef = database.getReference("Users");
        List<User> employeeList = new ArrayList<>();
        employeesRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                employeeList.clear();
                for (DataSnapshot employeeSnapshot : dataSnapshot.getChildren()) {
                    User employee = employeeSnapshot.getValue(User.class);
                    if (employee != null) {
                        if(employee.getRole().equals("employee")) {
                            employeeList.add(employee);
                        }
                    }
                }
                // Pozovite callback sa listom zaposlenih kada se podaci preuzmu iz baze
                callback.onEmployeeDataReceived(employeeList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška pri čitanju podataka iz baze
            }
        });
    }


    private void getAllCategoriesFromDatabase(Spinner spinnerCat) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("productCategories");
        allCategories.clear();
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // Pročitajte sve podatke iz baze
                //List<Product> productList = new ArrayList<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ProductCategory ec = snapshot.getValue(ProductCategory.class);
                    if (ec != null) {
                        allCategories.add(ec);
                    }
                }
                // Sada imate listu svih događaja iz baze podataka
                // Možete izvršiti željene akcije sa ovim podacima
                setupCategorySpinner(spinnerCat);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Greška prilikom čitanja podataka iz baze
                // Tretirajte ovaj slučaj kako želite
            }
        });
    }


    private void setupCategorySpinner(Spinner spinnerSP) {
        // Kreiramo listu imena zaposlenih
        List<String> categories = new ArrayList<>();
        for (ProductCategory e : allCategories) {
            String var = "Category: " + e.getName();
            categories.add(var);
        }

        String noneVar = "Category: None";
        categories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSP.setAdapter(spinnerAdapter);
    }

    private void getAllSubcategoriesFromDatabase(String categoryId, Spinner spinner) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("eventSubcategories");
        appropriateSubcategories.clear();
        databaseRef.orderByChild("categoryId").equalTo(categoryId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    ProductSubcategory esc = snapshot.getValue(ProductSubcategory.class);
                    if (esc != null && esc.getType().equals("usluga")) {
                        appropriateSubcategories.add(esc);
                    }
                }
                // Ovdje možete izvršiti željene akcije s listom podataka podkategorija
                // Na primjer, možete pozvati metod koji postavlja Spinner za podkategorije
                // setupSubcategorySpinner();
                setupSubcategorySpinner(spinner);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Tretirajte grešku prilikom čitanja podataka iz baze
            }
        });
    }

    private void setupSubcategorySpinner(Spinner spinnerSubcat) {
        // Kreiramo listu imena zaposlenih
        List<String> subcategories = new ArrayList<>();
        for (ProductSubcategory s : appropriateSubcategories) {
            String var = "Subcategory: " + s.getName();
            subcategories.add(var);
        }

        String noneVar = "Subcategory: None";
        subcategories.add(noneVar);

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        spinnerSubcat.setAdapter(spinnerAdapter);
    }

}



