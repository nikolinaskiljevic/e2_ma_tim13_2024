package com.example.eventplanner.fragments;

import static com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.DailyWorkHours;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.EmployeeWorkPeriod;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.WeeklyWorkHours;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class RegisterEmployeeFragment extends Fragment {


    private EditText editTextEmail, editTextPassword, editTextConfirmPassword,
            editTextFirstName, editTextLastName, editTextAddress, editTextPhoneNumber,
            editTextMondayStart, editTextMondayEnd, editTextTuesdayStart, editTextTuesdayEnd, editTextWednesdayStart, editTextWednesdayEnd, editTextThursdayStart, editTextFridayStart, editTextFridayEnd, editTextThursdayEnd, editTextStartDate, editTextEndDate;
    private Button buttonRegister, buttonUploadPhoto;
    private static final int PICK_IMAGE_REQUEST = 1;
    private ImageView imageViewSelectedPhoto;
    private Uri picURL = null;
    private FirebaseAuth mAuth;

    private Employee employee;


    private WeeklyWorkHours weeklyWorkHours;
    private String photoUrl;

    private String emId;

    private String loggedInId;
    public RegisterEmployeeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_employee, container, false);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            loggedInId = currentUser.getUid();
        }

        // Inicijalizacija polja za unos teksta i gumba za registraciju
        editTextEmail = view.findViewById(R.id.editTextEmail);
        editTextPassword = view.findViewById(R.id.editTextPassword);
        editTextConfirmPassword = view.findViewById(R.id.editTextConfirmPassword);
        editTextFirstName = view.findViewById(R.id.editTextFirstName);
        editTextLastName = view.findViewById(R.id.editTextLastName);
        editTextAddress = view.findViewById(R.id.editTextAddress);
        editTextPhoneNumber = view.findViewById(R.id.editTextPhoneNumber);
        editTextMondayStart = view.findViewById(R.id.editTextMondayStart);
        editTextMondayEnd = view.findViewById(R.id.editTextMondayEnd);

        editTextTuesdayStart = view.findViewById(R.id.editTextTuesdayStart);
        editTextTuesdayEnd = view.findViewById(R.id.editTextTuesdayEnd);

        editTextWednesdayStart = view.findViewById(R.id.editTextWednesdayStart);
        editTextWednesdayEnd = view.findViewById(R.id.editTextWednesdayEnd);

        editTextThursdayStart = view.findViewById(R.id.editTextThursdayStart);
        editTextThursdayEnd = view.findViewById(R.id.editTextThursdayEnd);

        editTextFridayStart = view.findViewById(R.id.editTextFridayStart);
        editTextFridayEnd = view.findViewById(R.id.editTextFridayEnd);

        editTextStartDate = view.findViewById(R.id.editTextStartDate);
        editTextEndDate = view.findViewById(R.id.editTextEndDate);


        buttonRegister = view.findViewById(R.id.buttonRegister);
        buttonUploadPhoto = view.findViewById(R.id.buttonUploadPhoto);
        imageViewSelectedPhoto = view.findViewById(R.id.imageViewSelectedPhoto);
        weeklyWorkHours = new WeeklyWorkHours();


        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String email = editTextEmail.getText().toString().trim();
                String password = editTextPassword.getText().toString().trim();
                String confirmPassword = editTextConfirmPassword.getText().toString().trim();
                String firstName = editTextFirstName.getText().toString().trim();
                String lastName = editTextLastName.getText().toString().trim();
                String address = editTextAddress.getText().toString().trim();
                String phoneNumber = editTextPhoneNumber.getText().toString().trim();


                employee = new Employee(email, password, confirmPassword, firstName, lastName, address, phoneNumber, photoUrl);
                employee.setActive(false);

                if(isInputValid()) {
                    //  saveEmployeeToDatabase(employee);


                    DailyWorkHours monday = new DailyWorkHours("Monday", editTextMondayStart.getText().toString(), editTextMondayEnd.getText().toString());
                    DailyWorkHours tuesday = new DailyWorkHours("Tuesday", editTextTuesdayStart.getText().toString().trim(), editTextTuesdayEnd.getText().toString().trim());
                    DailyWorkHours wednesday = new DailyWorkHours("Wednesday", editTextWednesdayStart.getText().toString().trim(), editTextWednesdayEnd.getText().toString().trim());
                    DailyWorkHours thursday = new DailyWorkHours("Thursday", editTextThursdayStart.getText().toString().trim(), editTextThursdayEnd.getText().toString().trim());
                    DailyWorkHours friday = new DailyWorkHours("Friday", editTextFridayStart.getText().toString().trim(), editTextFridayEnd.getText().toString().trim());

                    List<DailyWorkHours> dailyWorkHoursList = new ArrayList<>();
                    dailyWorkHoursList.add(monday);
                    dailyWorkHoursList.add(tuesday);
                    dailyWorkHoursList.add(wednesday);
                    dailyWorkHoursList.add(thursday);
                    dailyWorkHoursList.add(friday);


                    weeklyWorkHours.setDailyWorkHoursList(dailyWorkHoursList);
                    //AKO VEC POSTOJI U BAZI NE KREIRAS NOVI


                    EmployeeWorkPeriod employeeWorkPeriod = new EmployeeWorkPeriod(editTextStartDate.getText().toString().trim(), editTextEndDate.getText().toString().trim());
                   // saveWeeklyWorkhours(employeeWorkPeriod,employee);
                    saveEmployeeToDatabase(employee,employeeWorkPeriod);
                }else{
                    Toast.makeText(getContext(), "Cannot register,invalid input!", Toast.LENGTH_SHORT).show();
                }


            }
        });
        buttonUploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Kreirajte implicitni intent za odabir slike iz galerije
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
            }
        });


        return view;
    }
    public interface SaveEmployeesCallback {
        void onSaveEmployeesSuccess();
    }

    private void saveEmployeeToDatabase(Employee employee,EmployeeWorkPeriod period) {

        //prvo cuvaj u auth
        mAuth.createUserWithEmailAndPassword(employee.getEmail(), employee.getPassword())
                .addOnCompleteListener(requireActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                            if (firebaseUser != null) {
                                // Create User object
                                User user = new User(firebaseUser.getUid(), "employee",employee.getEmail(), employee.getFirstName(), employee.getLastName(), employee.getAddress(), employee.getPhoneNumber(),false);
                                employee.setId(firebaseUser.getUid());
                                emId= firebaseUser.getUid();
                                user.setImageURL(employee.getProfilePicture());
                                // Save user to database

                                saveUserToDatabase(user);
                               // saveEmployee(employee);
                                // Send email verification
                                sendEmailVerification(firebaseUser);
                                saveWeeklyWorkhours(period,employee);
                            }
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(requireContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();


                        }
                    }
                });


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            Uri selectedImageUri = data.getData();
            photoUrl=selectedImageUri.toString();
            imageViewSelectedPhoto.setVisibility(View.VISIBLE);
            imageViewSelectedPhoto.setImageURI(selectedImageUri);
        }
    }

    private void sendEmail(String recipientEmail, String subject, String content) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{recipientEmail});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, content);

        try {
            startActivity(Intent.createChooser(emailIntent, "Pošalji mejl..."));
        } catch (ActivityNotFoundException e) {


        }
    }



    private void saveWeeklyWorkhours(EmployeeWorkPeriod period,Employee employee) {
        Log.e(TAG, "Employeeee ID"+ employee.getId());

        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("weeklyWorkHours");

        // Izvršavanje upita za provjeru postojećih podataka
        databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean exists = false;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    WeeklyWorkHours existingWeeklyWorkHours = snapshot.getValue(WeeklyWorkHours.class);
                    // Provjera jednakosti objekata po dailyWorkHoursList
                    if (existingWeeklyWorkHours != null && isEqualByDailyWorkHoursList(existingWeeklyWorkHours, weeklyWorkHours)) {
                        exists = true;
                        weeklyWorkHours.setId(existingWeeklyWorkHours.getId());
                        period.setWeeklyWorkHoursId(weeklyWorkHours.getId());

                        saveEmployeeWorkPeriod(period, employee.getId());
                        break;
                    }
                }
                // Ako objekt već postoji, ne spremamo ga ponovno
                if (!exists) {
                    long lastId = dataSnapshot.getChildrenCount() + 1; // Generisanje novog ID-a

                    // Postavljanje novog ID-a za proizvod i dodavanje u bazu podataka
                    String Id = String.valueOf(lastId);


                    weeklyWorkHours.setId(Id);

                    databaseRef.child(Id).setValue(weeklyWorkHours);
                    period.setWeeklyWorkHoursId(weeklyWorkHours.getId());

                    saveEmployeeWorkPeriod(period, employee.getId());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Error fetching data", databaseError.toException());
            }
        });



    }


    private boolean isEqualByDailyWorkHoursList(WeeklyWorkHours a, WeeklyWorkHours b) {
        List<DailyWorkHours> aList = a.getDailyWorkHoursList();
        List<DailyWorkHours> bList = b.getDailyWorkHoursList();

        if (aList.size() != bList.size()) {
            return false;
        }

        // Provjera jednakosti elemenata po svakom indeksu
        for (int i = 0; i < aList.size(); i++) {
            DailyWorkHours aDaily = aList.get(i);
            DailyWorkHours bDaily = bList.get(i);
            // Usporedba po svim poljima
            if (!aDaily.getDay().equals(bDaily.getDay()) ||
                    !aDaily.getFrom().equals(bDaily.getFrom()) ||
                    !aDaily.getTo().equals(bDaily.getTo())) {
                return false;
            }
        }

        return true;
    }


    private void saveEmployeeWorkPeriod(EmployeeWorkPeriod employeeWorkPeriod, String newEmployeeId) {
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("employeeWorkPeriod");

        // Provjera postojećih podataka s istim weeklyWorkHoursId
        databaseRef.orderByChild("weeklyWorkHoursId").equalTo(weeklyWorkHours.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                boolean found = false;
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EmployeeWorkPeriod existingEmployeeWorkPeriod = snapshot.getValue(EmployeeWorkPeriod.class);
                    if (existingEmployeeWorkPeriod.getFrom().equals(employeeWorkPeriod.getFrom()) &&
                            existingEmployeeWorkPeriod.getTo().equals(employeeWorkPeriod.getTo())) {
                        found = true;
                        existingEmployeeWorkPeriod.setWeeklyWorkHoursId(weeklyWorkHours.getId());
                        updateEmployeeWorkPeriod(existingEmployeeWorkPeriod, newEmployeeId);

                        break;
                    }
                }
                // Ako ne pronađemo postojeći EmployeeWorkPeriod, stvaramo novi
                if (!found) {



                    long lastId = dataSnapshot.getChildrenCount() + 1; // Generisanje novog ID-a

                    // Postavljanje novog ID-a za proizvod i dodavanje u bazu podataka
                    String Id = String.valueOf(lastId);
                    employeeWorkPeriod.setWeeklyWorkHoursId(weeklyWorkHours.getId());

                    employeeWorkPeriod.setId(Id);
                    List<String> employeesId = new ArrayList<>();
                    Log.e(TAG, "emID" + emId);
                    employeesId.add(emId);
                    employeeWorkPeriod.setEmployeesId(employeesId);
                    Log.e(TAG, "ADDING NEW PERIOD" + employeeWorkPeriod);
                    Log.e(TAG, "EMPLOYEESID" + employeeWorkPeriod.getEmployeesId());
                    databaseRef.child(Id).setValue(employeeWorkPeriod);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Pogreška pri čitanju podataka iz baze
                Log.e(TAG, "Error fetching data", databaseError.toException());
            }
        });
    }

    // Metoda za ažuriranje postojećeg EmployeeWorkPeriod
    private void updateEmployeeWorkPeriod(EmployeeWorkPeriod existing, String newEmployeeId) {

        List<String> employeesId = existing.getEmployeesId();
        employeesId.add(newEmployeeId); // Dodajemo novog zaposlenika
        existing.setEmployeesId(employeesId);
        existing.setWeeklyWorkHoursId(weeklyWorkHours.getId());
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("employeeWorkPeriod");
        Log.e(TAG, "UPDATE  PERIOD" + existing);
        databaseRef.child(existing.getId()).child("employeesId").setValue(existing.getEmployeesId());
    }


    //VALIDACIJAA

    private boolean isInputValid() {
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String confirmPassword = editTextConfirmPassword.getText().toString().trim();
        String startDate = editTextStartDate.getText().toString().trim();
        String endDate = editTextEndDate.getText().toString().trim();

        // Provera da li su sva polja popunjena
        if (email.isEmpty() || password.isEmpty() || confirmPassword.isEmpty() || startDate.isEmpty() || endDate.isEmpty()) {
            Toast.makeText(getActivity(), "All fields are required", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Provera da li je email u ispravnom formatu
        if (!isValidEmail(email)) {
            Toast.makeText(getActivity(), "Invalid email address", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Provera da li su password i confirm password isti
        if (!password.equals(confirmPassword)) {
            Toast.makeText(getActivity(), "Passwords do not match", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Provera da li je startDate manji od endDate
        if (!isStartDateBeforeEndDate(startDate, endDate)) {
            Toast.makeText(getActivity(), "Start date must be before end date", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Provera formata unosa za radne sate (HH:MM)
        if (!isValidTimeFormat(editTextMondayStart.getText().toString().trim()) || !isValidTimeFormat(editTextMondayEnd.getText().toString().trim()) ||
                !isValidTimeFormat(editTextTuesdayStart.getText().toString().trim()) || !isValidTimeFormat(editTextTuesdayEnd.getText().toString().trim()) ||
                !isValidTimeFormat(editTextWednesdayStart.getText().toString().trim()) || !isValidTimeFormat(editTextWednesdayEnd.getText().toString().trim()) ||
                !isValidTimeFormat(editTextThursdayStart.getText().toString().trim()) || !isValidTimeFormat(editTextThursdayEnd.getText().toString().trim()) ||
                !isValidTimeFormat(editTextFridayStart.getText().toString().trim()) || !isValidTimeFormat(editTextFridayEnd.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Invalid time format. Use HH:MM", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Provera da li su za svaki dan fromHours manji od toHours
        if (!isFromHoursBeforeToHours()) {
            Toast.makeText(getActivity(), "From hours must be before to hours", Toast.LENGTH_SHORT).show();
            return false;
        }

        // Svi uslovi su ispunjeni
        return true;
    }

    private boolean isValidEmail(String email) {
        // Regex za proveru formata email adrese
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.matches(emailPattern);
    }

    private boolean isStartDateBeforeEndDate(String startDate, String endDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try {
            Date start = dateFormat.parse(startDate);
            Date end = dateFormat.parse(endDate);
            return start.before(end);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isValidTimeFormat(String time) {
        // Regex za proveru formata vremena (HH:MM)
        String timePattern = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
        return time.matches(timePattern);
    }

    private boolean isFromHoursBeforeToHours() {
        // Provera za svaki dan
        return isFromHoursBeforeToHoursForDay(editTextMondayStart.getText().toString().trim(), editTextMondayEnd.getText().toString().trim()) &&
                isFromHoursBeforeToHoursForDay(editTextTuesdayStart.getText().toString().trim(), editTextTuesdayEnd.getText().toString().trim()) &&
                isFromHoursBeforeToHoursForDay(editTextWednesdayStart.getText().toString().trim(), editTextWednesdayEnd.getText().toString().trim()) &&
                isFromHoursBeforeToHoursForDay(editTextThursdayStart.getText().toString().trim(), editTextThursdayEnd.getText().toString().trim()) &&
                isFromHoursBeforeToHoursForDay(editTextFridayStart.getText().toString().trim(), editTextFridayEnd.getText().toString().trim());
    }

    private boolean isFromHoursBeforeToHoursForDay(String fromTime, String toTime) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            Date from = dateFormat.parse(fromTime);
            Date to = dateFormat.parse(toTime);
            return from.before(to);
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void sendEmailVerification(FirebaseUser user) {
        user.sendEmailVerification()
                .addOnCompleteListener(requireActivity(), new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(requireContext(),
                                    "Verification email sent to " + user.getEmail(),
                                    Toast.LENGTH_SHORT).show();
                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, new RegisterPUPV2Fragment())
                                    .commit();
                        } else {
                            Toast.makeText(requireContext(),
                                    "Failed to send verification email.",
                                    Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }

    private void saveUserToDatabase(User user) {
        user.setPupvId(loggedInId);
        DatabaseReference usersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        usersRef.child(user.getUid()).setValue(user)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("User Creation", "User saved to database");
                        } else {
                            Log.e("User Creation", "Failed to save user to database", task.getException());
                        }
                    }
                });
    }


}