package com.example.eventplanner.adapters;

import static android.view.View.GONE;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Product;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.FileNotFoundException;
import java.util.List;


import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

public class ProductPricelistAdapter extends RecyclerView.Adapter<ProductPricelistAdapter.ViewHolder> {

    private final Context context;
    private final List<Product> productList;
    private EditButtonClickListener editListener;

    private FirebaseAuth auth;
    private String userRole;



    private String role;
    public ProductPricelistAdapter(Context context, List<Product> productList,String role) {
        this.context = context;
        this.productList = productList;
        this.userRole = role;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_product_item, parent, false);


        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Product product = productList.get(position);

        // Bind data to views
        holder.bind(product);

        // Set click listeners
        holder.buttonEdit.setOnClickListener(v -> {
            if (editListener != null) {
                editListener.onEditButtonClick(product);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setOnEditButtonClickListener(EditButtonClickListener listener) {
        this.editListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final ImageButton buttonEdit, buttonDelete,buttonEditPrice,buttonEditDiscount;
        final TextView textViewName, textViewPrice, textViewId, textViewDiscount, textViewDiscountedPrice;
        final TextView textViewCategory, textViewSubcategory, textViewDescription, textViewAvailable, textViewEventTypes;
        final EditText editTextPrice, editTextDiscount;
        ViewHolder(View itemView) {
            super(itemView);
            buttonEdit = itemView.findViewById(R.id.buttonEdit);
            buttonDelete = itemView.findViewById(R.id.buttonDelete);
            buttonEditPrice = itemView.findViewById(R.id.buttonEditPrice);
            buttonEditDiscount = itemView.findViewById(R.id.buttonEditDiscount);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewPrice = itemView.findViewById(R.id.textViewPrice);
            textViewId = itemView.findViewById(R.id.textViewProductId);
            textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
            textViewDiscountedPrice = itemView.findViewById(R.id.textViewDiscountedPrice);
            textViewCategory = itemView.findViewById(R.id.textViewCategory);
            textViewSubcategory = itemView.findViewById(R.id.textViewSubcategory);
            textViewDescription = itemView.findViewById(R.id.textViewDescription);
            textViewEventTypes = itemView.findViewById(R.id.textViewEventTypes);
            textViewAvailable = itemView.findViewById(R.id.textViewAvailable);
            editTextPrice = itemView.findViewById(R.id.editTextPrice);
            editTextDiscount = itemView.findViewById(R.id.editTextDiscount);

            // Hide additional fields that are not set
            buttonDelete.setVisibility(GONE);
            textViewCategory.setVisibility(GONE);
            textViewSubcategory.setVisibility(GONE);
            textViewDescription.setVisibility(GONE);
            textViewEventTypes.setVisibility(GONE);
            textViewAvailable.setVisibility(GONE);
            buttonEdit.setVisibility(GONE);
            if(!userRole.equals("pupv")){
                buttonEditPrice.setVisibility(GONE);
                buttonEditDiscount.setVisibility(GONE);
            }


        }

        void bind(Product product) {
            textViewName.setText(product.getName());
            textViewId.setText(String.valueOf(product.getId()));
            textViewPrice.setText(String.valueOf(product.getPrice()));
            textViewDiscount.setText(String.valueOf(product.getDiscount()));
            textViewDiscountedPrice.setText(String.valueOf(product.getPrice() * (100 - product.getDiscount()) / 100));

            buttonEditPrice.setOnClickListener(v -> {
                editTextPrice.setVisibility(View.VISIBLE);
                editTextPrice.setText(textViewPrice.getText());

                textViewPrice.setVisibility(GONE);
                editTextPrice.requestFocus();
            });

            buttonEditDiscount.setOnClickListener(v -> {
                editTextDiscount.setVisibility(View.VISIBLE);
                editTextDiscount.setText(textViewDiscount.getText());
                textViewDiscount.setVisibility(GONE);
                editTextDiscount.requestFocus();
            });

            editTextPrice.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    textViewPrice.setText(editTextPrice.getText());
                    editTextPrice.setVisibility(GONE);
                    textViewPrice.setVisibility(View.VISIBLE);
                    // Update product price
                    product.setPrice(Double.parseDouble(editTextPrice.getText().toString()));
                    // Notify adapter about item change
                    product.setDiscountedPrice(Double.parseDouble(editTextPrice.getText().toString()) * (100 - product.getDiscount()) / 100);
                    saveProductChanges(product);
                    notifyItemChanged(getAdapterPosition());
                    return true;
                }
                return false;
            });

            editTextDiscount.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    textViewDiscount.setText(editTextDiscount.getText());
                    editTextDiscount.setVisibility(GONE);
                    textViewDiscount.setVisibility(View.VISIBLE);
                    // Update product discount
                    product.setDiscount(Double.parseDouble(editTextDiscount.getText().toString()));
                    product.setDiscountedPrice(product.getPrice() * (100 - Double.parseDouble(editTextDiscount.getText().toString())) / 100);

                    saveProductChanges(product);
                    // Notify adapter about item change
                    notifyItemChanged(getAdapterPosition());
                    return true;
                }
                return false;
            });
        }
    }

    public interface EditButtonClickListener {
        void onEditButtonClick(Product product);
    }


    private void saveProductChanges(Product updatedProduct) {

            // Pristupanje bazi podataka "products" kako bismo pronašli odgovarajući proizvod
            DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("products");

            // Dodavanje slušača koji se poziva kada se dohvate podaci iz baze podataka
            databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        Product product = snapshot.getValue(Product.class);
                        Log.d("nadjen",snapshot.getKey());
                        Log.d("nadjen",snapshot.getKey());
                        //int productid = Integer.parseInt(snapshot.getKey());
                        String productId = snapshot.getKey();// Dohvatanje ključa proizvoda
                        if (product != null && productId.equals(product.getId())) {
                            DatabaseReference productRef = databaseRef.child(productId);
                            Log.d("azuriiraan ", product.getName());
                            productRef.setValue(updatedProduct)
                                    .addOnSuccessListener(aVoid -> {
                                        updatePackagesWithProduct(updatedProduct);
                                        // Ažuriranje uspešno završeno
                                    })
                                    .addOnFailureListener(e -> {
                                        // Greška pri ažuriranju
                                    });
                            break;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Greška pri čitanju iz baze podataka
                    // Možete ovde izvršiti neku radnju ako dođe do greške
                }
            });

    }


    private void updatePackagesWithProduct(Product updatedProduct) {
        DatabaseReference packagesRef = FirebaseDatabase.getInstance().getReference("packages");

        packagesRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot packageSnapshot : dataSnapshot.getChildren()) {
                    for (DataSnapshot productSnapshot : packageSnapshot.child("selectedProducts").getChildren()) {
                        Product product = productSnapshot.getValue(Product.class);
                        if (product != null && product.getId().equals(updatedProduct.getId())) {
                            productSnapshot.getRef().setValue(updatedProduct);
                            updatePackagePrice(packageSnapshot.getKey());
                            Log.d("updatePackage", "Product with ID " + product.getId() + " updated in package " + packageSnapshot.getKey());
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("updatePackages", "Error updating packages", databaseError.toException());
            }
        });
    }



    private void updatePackagePrice(String packageId) {
        DatabaseReference packageRef = FirebaseDatabase.getInstance().getReference("packages").child(packageId);

        packageRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                double totalPrice = 0.0;
                for (DataSnapshot productSnapshot : dataSnapshot.child("products").getChildren()) {
                    double productPrice = productSnapshot.child("price").getValue(Double.class);
                    totalPrice += productPrice;
                }
                for (DataSnapshot serviceSnapshot : dataSnapshot.child("services").getChildren()) {
                    double servicePrice = serviceSnapshot.child("price").getValue(Double.class);
                    totalPrice += servicePrice;
                }
                // Update package price
                packageRef.child("price").setValue(totalPrice)
                        .addOnSuccessListener(aVoid -> {
                            // Price updated successfully
                        })
                        .addOnFailureListener(e -> {
                            // Error updating price
                        });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Error reading package data
            }
        });
    }


}
