package com.example.eventplanner.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.example.eventplanner.R;
import com.example.eventplanner.activities.AuthActivity;
import com.example.eventplanner.activities.MainActivity;
import com.example.eventplanner.model.EmployeeWorkPeriod;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private TextView textViewUserName;
    private ImageView imageViewUserPhoto;
    private Button buttonViewCatalog;
    private List<Notification> myNotifications;
    private String userId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        textViewUserName = view.findViewById(R.id.textViewUserName);
        imageViewUserPhoto = view.findViewById(R.id.imageViewUserPhoto);
        buttonViewCatalog = view.findViewById(R.id.buttonViewCatalog);

        myNotifications= new ArrayList<>();

        // Load user data
        loadUserData();

        buttonViewCatalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Replace with RegisterPUPVFragment
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new DetailsFragment())
                        .addToBackStack(null)  // Optional: Add fragment to back stack
                        .commit();
            }
        });

        return view;
    }

    private void loadUserData() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String userId = currentUser.getUid();
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        if(user.getRole().equals("pupv") && !user.isAdminVerified()){
                            MainActivity a = (MainActivity)getActivity();
                            a.logoutUser();
                            Toast.makeText(a.getApplicationContext(), "PUPV unverified",
                                    Toast.LENGTH_SHORT).show();
                        }
                        if(user.getRole().equals("pupv") && user.isAdminVerified() && !currentUser.isEmailVerified()){

                            MainActivity a = (MainActivity)getActivity();
                            currentUser.sendEmailVerification();
                            a.logoutUser();
                            Toast.makeText(a.getApplicationContext(), "Verification sent",
                                    Toast.LENGTH_SHORT).show();
                        }

                        textViewUserName.setText(user.getFirstName() + " " + user.getLastName());
                        if (user.getImageURL() != null && !user.getImageURL().isEmpty()) {
                            // Use Glide to load and display the image

                            Glide.with(requireContext())
                                    .load(user.getImageURL())
                                    .apply(new RequestOptions()
                                            .placeholder(R.drawable.lavender_border)
                                            .error(R.drawable.logo))
                                    .into(imageViewUserPhoto);


                        }


                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Handle database error
                }
            });
           // getNotificationsForUser(currentUser.getUid());
        }
    }


    private void getNotificationsForUser(String userId){



        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference notRefs = databaseReference.child("notifications");


        notRefs.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Ovdje možete obraditi podatke koje ste dobili iz baze podataka
                // dataSnapshot sadrži sve podatke iz čvora "employees"

                for (DataSnapshot notSnapshot : dataSnapshot.getChildren()) {
                    Notification not = notSnapshot.getValue(Notification.class);


                    /*if( !not.isRead() && not.getEmployeeId().equals(userId)){
                        Toast.makeText(getActivity(), not.getMessage(), Toast.LENGTH_SHORT).show();
                        updateNotification(not);
                    }*/
                }



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

    public void updateNotification(Notification not){

        //not.setRead(true);

        //DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("notifications");
        //databaseRef.child(not.getId()).child("isRead").setValue(true);
    }


}