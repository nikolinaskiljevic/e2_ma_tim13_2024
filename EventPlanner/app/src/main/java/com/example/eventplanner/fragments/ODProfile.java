package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.eventplanner.R;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ODProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ODProfile extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView textViewUserName, textViewUserAddress, textViewUserPhone, textViewUserEmail;
    private ImageView imageViewUserPhoto;
    private String profileImageURL;
    private EditText newName, newAddress, newPhone, oldPassword, newPassword, confirmPassword;
    private Button saveName, saveAddress, savePhone, savePassword, changePassword, deactivate;

    public ODProfile() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ODProfile.
     */
    // TODO: Rename and change types and number of parameters
    public static ODProfile newInstance(String param1, String param2) {
        ODProfile fragment = new ODProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_o_d_profile, container, false);

        textViewUserName = view.findViewById(R.id.textViewUserName);
        textViewUserAddress = view.findViewById(R.id.textViewUserAddress);
        textViewUserPhone = view.findViewById(R.id.textViewUserPhone);
        textViewUserEmail = view.findViewById(R.id.textViewUserEmail);
        imageViewUserPhoto = view.findViewById(R.id.imageViewProfile);

        newName = view.findViewById(R.id.editTextName);
        newAddress = view.findViewById(R.id.editTextAddress);
        newPhone = view.findViewById(R.id.editTextPhone);
        saveName = view.findViewById(R.id.buttonSaveName);
        saveAddress = view.findViewById(R.id.buttonSaveAddress);
        savePhone = view.findViewById(R.id.buttonSavePhone);

        loadUserData();

        oldPassword = view.findViewById(R.id.editTextOldPassword);
        newPassword = view.findViewById(R.id.editTextNewPassword);
        confirmPassword = view.findViewById(R.id.editTextConfirmNewPassword);
        savePassword = view.findViewById(R.id.buttonSavePassword);
        changePassword = view.findViewById(R.id.buttonChangePassword);
        deactivate = view.findViewById(R.id.buttonDeactivate);

        deactivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean hasNoServices = true;
                //obraditi provjeru da li korisnik ima rezervisane usluge (pomocu Uid)

                if(hasNoServices) {
                    updateUser(0, "", true);
                }
                else {
                    Toast.makeText(getActivity(), "You can not deactivate your profile. You have reserved services.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                oldPassword.setVisibility(View.VISIBLE);
                newPassword.setVisibility(View.VISIBLE);
                confirmPassword.setVisibility(View.VISIBLE);
                savePassword.setVisibility(View.VISIBLE);
            }
        });

        savePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword();
            }
        });

        saveName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //currentUser.setName(newName.getText().toString());
                updateUser(1, newName.getText().toString(), false);
            }
        });

        saveAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser(2, newAddress.getText().toString(), false);
            }
        });

        savePhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser(3, newPhone.getText().toString(), false);
            }
        });

        return view;
    }

    private void loadUserData() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            String userId = currentUser.getUid();
            DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        textViewUserName.setText(user.getFirstName() + " " + user.getLastName());
                        textViewUserAddress.setText(user.getAddress());
                        textViewUserPhone.setText(user.getPhoneNumber());
                        textViewUserEmail.setText(currentUser.getEmail());
                        if (user.getImageURL() != null && !user.getImageURL().isEmpty()) {
                            profileImageURL = user.getImageURL();
                            // Use Glide to load and display the image
                            Glide.with(requireContext())
                                    .load(user.getImageURL())
                                    .apply(new RequestOptions()
                                            .placeholder(R.drawable.lavender_border)
                                            .error(R.drawable.logo))
                                    .into(imageViewUserPhoto);
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // Handle database error
                }
            });
        }
    }

    private void updateUser(int field, String data, boolean isDeactivated) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String userId = currentUser.getUid();
        User updatedUser;

        if(field == 1) {
            String[] parts = data.split(" ");
            updatedUser = new User(userId, "organizer", textViewUserEmail.getText().toString(), parts[0], parts[1], textViewUserAddress.getText().toString(), textViewUserPhone.getText().toString(), isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }
        else if(field == 2) {
            String[] parts = textViewUserName.getText().toString().split(" ");
            updatedUser = new User(userId, "organizer", textViewUserEmail.getText().toString(), parts[0], parts[1], data, textViewUserPhone.getText().toString(), isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }
        else if(field == 3) {
            String[] parts = textViewUserName.getText().toString().split(" ");
            updatedUser = new User(userId, "organizer", textViewUserEmail.getText().toString(), parts[0], parts[1], textViewUserAddress.getText().toString(), data, isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }
        else {
            String[] parts = textViewUserName.getText().toString().split(" ");
            updatedUser = new User(userId, "organizer", textViewUserEmail.getText().toString(), parts[0], parts[1], textViewUserAddress.getText().toString(), textViewUserPhone.getText().toString(), isDeactivated);
            updatedUser.setImageURL(profileImageURL);
        }

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child("Users").child(userId);
        userRef.setValue(updatedUser).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getActivity(), "Well done!", Toast.LENGTH_SHORT).show();
                    loadUserData();
                    newName.setText("");
                    newAddress.setText("");
                    newPhone.setText("");
                } else {
                    Toast.makeText(getActivity(), "Updating error", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void changePassword() {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String oldPass = oldPassword.getText().toString();
        String newPass = newPassword.getText().toString();
        String confirmNewPass = confirmPassword.getText().toString();

        if (!newPass.equals(confirmNewPass)) {
            Toast.makeText(getActivity(), "New passwords do not match", Toast.LENGTH_SHORT).show();
            return;
        }

        if (currentUser != null && currentUser.getEmail() != null) {
            // Reauthenticate the user
            AuthCredential credential = EmailAuthProvider.getCredential(currentUser.getEmail(), oldPass);
            currentUser.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()) {
                        // Update the password
                        currentUser.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Toast.makeText(getActivity(), "Password updated successfully", Toast.LENGTH_SHORT).show();
                                    oldPassword.setVisibility(View.INVISIBLE);
                                    newPassword.setVisibility(View.INVISIBLE);
                                    confirmPassword.setVisibility(View.INVISIBLE);
                                    savePassword.setVisibility(View.INVISIBLE);
                                } else {
                                    Toast.makeText(getActivity(), "Error updating password", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    } else {
                        Toast.makeText(getActivity(), "Authentication failed. Check your old password", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}