package com.example.eventplanner.fragments;

import static com.google.firebase.messaging.Constants.MessageNotificationKeys.TAG;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Event2;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class WorkingCalendarFragment extends Fragment {


    private EditText editTextFrom,editTextTo;
    private Button buttonShow;

    private List<Event2> eventsList;

    private TableLayout tableLayout;

    private String employeeId ;
    public WorkingCalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_working_calendar, container, false);

        ImageButton addEventButton = view.findViewById(R.id.addEvent);

        Bundle bundle = getArguments();
        if (bundle != null) {
            employeeId = bundle.getString("employeeId");

        }
        Log.e(TAG, "EmployeeId u calendar" + employeeId);


        eventsList=new ArrayList<>();
        buttonShow=view.findViewById(R.id.buttonShow);
        editTextFrom=view.findViewById(R.id.editTextFrom);
        editTextTo=view.findViewById(R.id.editTextTo);
        tableLayout=view.findViewById(R.id.tableLayout);
        //selektovani


        buttonShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                int childCount = tableLayout.getChildCount();
                if (childCount > 1) {
                    tableLayout.removeViews(1, childCount - 1);
                }
                eventsList.clear();
                populateEvents(employeeId);

            }
        });


        // Postavimo rukovanje klikom na dugme
        addEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Navigacija na drugi fragment
                AddEventFragment fragment = new AddEventFragment(); // Zamijenite sa imenom vašeg drugog fragmenta

                Bundle bundle = new Bundle();
                bundle.putString("employeeId", employeeId); // Prosledite ID zaposlenog kao argument fragmentu
                fragment.setArguments(bundle);

                FragmentManager fragmentManager = getParentFragmentManager(); // getParentFragmentManager() za fragmente unutar fragmenta, getSupportFragmentManager() za aktivnosti
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_container, fragment); // Zamijenite R.id.fragment_container s ID-om vašeg kontejnera fragmenta u aktivnosti
                fragmentTransaction.addToBackStack(null); // Dodajemo na back stack ako treba
                fragmentTransaction.commit();


            }
        });


        return view;
    }

    public void populateEvents(String employeeId) {

        Log.e(TAG, "EmployeeId" + employeeId);
        eventsList.clear();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
        DatabaseReference employeeWorkPeriodsRef = databaseReference.child("eventss");

        employeeWorkPeriodsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                eventsList.clear();
                for (DataSnapshot periodSnapshot : dataSnapshot.getChildren()) {
                    Event2 event = periodSnapshot.getValue(Event2.class);
                    Log.e(TAG, "EventemId" + event.employeeId);
                    if (event.getEmployeeId() != null && event.getEmployeeId().contains(employeeId)) {
                       eventsList.add(event);
                    }else{
                        Toast.makeText(getActivity(), "There are no events for entered period", Toast.LENGTH_SHORT).show();
                    }
                }
                updateTableUI();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Greška prilikom dohvaćanja podataka iz baze podataka
            }
        });
    }

    private void updateTableUI() {



        for (Event2 event : eventsList) {
            if(isDateInRange(editTextFrom.getText().toString().trim(),editTextTo.getText().toString().trim(),event.getDate())) {
                TableRow row = new TableRow(requireContext());

                TextView date = create2TextView(event.getDate());
                TextView from = create2TextView(event.getDailyWorkHours().getFrom());
                TextView to = create2TextView(event.getDailyWorkHours().getTo());
                TextView status = create2TextView(event.getEventType());

                row.addView(date);
                row.addView(from);
                row.addView(to);
                row.addView(status);


                // Dodavanje TableRow u TableLayout
                tableLayout.addView(row);
            }else{
                Toast.makeText(getActivity(), "There are no events for entered period", Toast.LENGTH_SHORT).show();
            }
        }
    }
    private TextView create2TextView(String text) {
        TextView textView = new TextView(requireContext());
        textView.setText(text);
        textView.setPadding(8, 8, 8, 8);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 9); // Postavljanje font size na 14sp
        return textView;
    }

    public  boolean isDateInRange(String fromDate, String toDate, String eventDate) {
        try {
            // Parsiranje stringova u objekte Date
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
            Date from = dateFormat.parse(fromDate);
            Date to = dateFormat.parse(toDate);
            Date event = dateFormat.parse(eventDate);

            // Provjera da li je eventDate između fromDate i toDate
            return event.after(from) && event.before(to);
        } catch (ParseException e) {
            // Greška prilikom parsiranja datuma
            e.printStackTrace();
            return false;
        }
    }
}