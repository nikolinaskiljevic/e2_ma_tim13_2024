package com.example.eventplanner.model;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Event {
    private String id;
    private String organizerId;
    private String EventType;
    private String EventName;
    private String EventDescription;
    private int MaximumParticipants;
    private String LocationAndDistance;
    private String PrivateRule;
    private String MaintenanceDate;
    private List<String> guestIds;

    public Event(String organizerId, String type, String name, String description, int maxParticipants, String locationAndDistance, String privateRule, String date) {
        this.organizerId = organizerId;
        this.EventType = type;
        this.EventName = name;
        this.EventDescription = description;
        this.MaximumParticipants = maxParticipants;
        this.LocationAndDistance = locationAndDistance;
        this.PrivateRule = privateRule;
        this.MaintenanceDate = date;
        this.guestIds = new ArrayList<>();
        this.guestIds.add("");
    }

    public List<String> getGuestIds() {
        return guestIds;
    }

    public void setGuestIds(List<String> guestIds) {
        this.guestIds = guestIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Event() {
    }

    public String getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(String organizerId) {
        this.organizerId = organizerId;
    }

    public String getEventType() {
        return EventType;
    }

    public void setEventType(String eventType) {
        EventType = eventType;
    }

    public String getEventName() {
        return EventName;
    }

    public void setEventName(String eventName) {
        EventName = eventName;
    }

    public String getEventDescription() {
        return EventDescription;
    }

    public void setEventDescription(String eventDescription) {
        EventDescription = eventDescription;
    }

    public int getMaximumParticipants() {
        return MaximumParticipants;
    }

    public void setMaximumParticipants(int maximumParticipants) {
        MaximumParticipants = maximumParticipants;
    }

    public String getLocationAndDistance() {
        return LocationAndDistance;
    }

    public void setLocationAndDistance(String locationAndDistance) {
        LocationAndDistance = locationAndDistance;
    }

    public String getPrivateRule() {
        return PrivateRule;
    }

    public void setPrivateRule(String privateRule) {
        PrivateRule = privateRule;
    }

    public String getMaintenanceDate() {
        return MaintenanceDate;
    }

    public void setMaintenanceDate(String maintenanceDate) {
        MaintenanceDate = maintenanceDate;
    }
}
