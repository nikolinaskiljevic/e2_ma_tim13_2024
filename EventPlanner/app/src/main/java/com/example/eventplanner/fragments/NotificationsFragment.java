package com.example.eventplanner.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.example.eventplanner.R;
import com.example.eventplanner.adapters.FavouritesServicesAdapter;
import com.example.eventplanner.adapters.NotificationAdapter;
import com.example.eventplanner.model.Notification;
import com.example.eventplanner.model.Service;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;


public class NotificationsFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private NotificationAdapter notificationAdapter;
    private RadioGroup radioGroup;
    private RadioButton radioButtonAll, radioButtonRead, radioButtonUnread;
    private RecyclerView recyclerViewAll, recyclerViewRead, recyclerViewUnread;
    private ArrayList<Notification> allNotifications = new ArrayList<>();
    private ArrayList<Notification> userNotifications = new ArrayList<>();
    private ArrayList<Notification> userReadNotifications = new ArrayList<>();
    private ArrayList<Notification> userUnreadNotifications = new ArrayList<>();
    private int radioButtonIndex = 0;
    public NotificationsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NotificationsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NotificationsFragment newInstance(String param1, String param2) {
        NotificationsFragment fragment = new NotificationsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);

        getAllNotifications();

        radioGroup = view.findViewById(R.id.radioGroupNotifications);

        radioButtonAll = view.findViewById(R.id.radioButtonAllNotifications);
        int radioButtonAllId = radioButtonAll.getId();
        radioButtonRead = view.findViewById(R.id.radioButtonReadNotifications);
        int radioButtonReadId = radioButtonRead.getId();
        radioButtonUnread = view.findViewById(R.id.radioButtonUnreadNotifications);



        recyclerViewAll = view.findViewById(R.id.recyclerViewAllNotifications);
        recyclerViewRead = view.findViewById(R.id.recyclerViewReadNotifications);
        recyclerViewUnread = view.findViewById(R.id.recyclerViewUnreadNotifications);

        radioGroup.check(radioButtonAll.getId());
        recyclerViewAll.setVisibility(View.VISIBLE);
        recyclerViewRead.setVisibility(View.GONE);
        recyclerViewUnread.setVisibility(View.GONE);
        radioButtonIndex = 0;

        SensorManager sensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
        Sensor sensorShake = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        SensorEventListener sensorEventListener = new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                if (sensorEvent != null) {
                    float x_accl = sensorEvent.values[0];
                    float y_accl = sensorEvent.values[1];
                    float z_accl = sensorEvent.values[2];

                    float floatSum = Math.abs(x_accl) + Math.abs(y_accl) + Math.abs(z_accl);


                    if (floatSum > 38) {
                        cycleRadioButton();
                    } else {

                    }
                }
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }
        };
        sensorManager.registerListener(sensorEventListener, sensorShake, SensorManager.SENSOR_DELAY_NORMAL);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == radioButtonAllId) {
                    recyclerViewAll.setVisibility(View.VISIBLE);
                    recyclerViewRead.setVisibility(View.GONE);
                    recyclerViewUnread.setVisibility(View.GONE);
                    radioButtonIndex = 0;
                }
                else if(checkedId == radioButtonReadId){
                    recyclerViewAll.setVisibility(View.GONE);
                    recyclerViewRead.setVisibility(View.VISIBLE);
                    recyclerViewUnread.setVisibility(View.GONE);
                    radioButtonIndex = 1;
                }
                else {
                    recyclerViewAll.setVisibility(View.GONE);
                    recyclerViewRead.setVisibility(View.GONE);
                    recyclerViewUnread.setVisibility(View.VISIBLE);
                    radioButtonIndex = 2;
                }

            }
        });

        return view;


    }

    private void cycleRadioButton() {
        radioButtonIndex = (radioButtonIndex + 1) % 3;
        switch (radioButtonIndex) {
            case 0:
                radioGroup.check(radioButtonAll.getId());
                break;
            case 1:
                radioGroup.check(radioButtonRead.getId());
                break;
            case 2:
                radioGroup.check(radioButtonUnread.getId());
                break;
        }
    }

    private void getAllNotifications() {
        DatabaseReference notifRef = FirebaseDatabase.getInstance().getReference("notifications");

        notifRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allNotifications.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Notification n = snapshot.getValue(Notification.class);
                    if (n != null) {
                        allNotifications.add(n);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findCurrentUserNotifications();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("getAllServices", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void findCurrentUserNotifications() {
        userNotifications.clear();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        for(Notification n: allNotifications) {
            if(n.getUserId().equals(user.getUid())) {
                userNotifications.add(n);
            }
        }

        setAllAdapter();
    }

    private void setAllAdapter() {
        recyclerViewAll.setHasFixedSize(true);
        recyclerViewAll.setLayoutManager(new LinearLayoutManager(getContext()));
        notificationAdapter = new NotificationAdapter(getContext(), userNotifications, "All");
        recyclerViewAll.setAdapter(notificationAdapter);

        setReadAdapter();
    }

    private void setReadAdapter() {
        recyclerViewRead.setHasFixedSize(true);
        recyclerViewRead.setLayoutManager(new LinearLayoutManager(getContext()));
        userReadNotifications.clear();
        for(Notification n: userNotifications) {
            if(n.getStatus() == Notification.Status.READ){
                userReadNotifications.add(n);
            }
        }

        notificationAdapter = new NotificationAdapter(getContext(), userReadNotifications, "Read");
        recyclerViewRead.setAdapter(notificationAdapter);

        setUnreadAdapter();
    }

    private void setUnreadAdapter() {
        recyclerViewUnread.setHasFixedSize(true);
        recyclerViewUnread.setLayoutManager(new LinearLayoutManager(getContext()));
        userUnreadNotifications.clear();
        for(Notification n: userNotifications) {
            if(n.getStatus() != Notification.Status.READ){
                userUnreadNotifications.add(n);
            }
        }

        notificationAdapter = new NotificationAdapter(getContext(), userUnreadNotifications, "Unread");
        recyclerViewUnread.setAdapter(notificationAdapter);
    }
}