package com.example.eventplanner.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.eventplanner.R;
import com.example.eventplanner.model.User;
import com.example.eventplanner.model.UsersReport;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OrganizerProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrganizerProfileFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView emailTextView;
    private TextView firstNameTextView;
    private TextView lastNameTextView;
    private TextView addressTextView;
    private TextView phoneNumberTextView;
    private ImageView profileImageView;
    private EditText reasonEditText;
private Button createReportButton;
private ImageButton reasonImageButton;

    public OrganizerProfileFragment() {
        // Required empty public constructor
    }



    public static OrganizerProfileFragment newInstance(String id,String email, String firstName, String lastName, String address, String phoneNumber, String imageURL) {
        OrganizerProfileFragment fragment = new OrganizerProfileFragment();
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("email", email);
        args.putString("firstName", firstName);
        args.putString("lastName", lastName);
        args.putString("address", address);
        args.putString("phoneNumber", phoneNumber);
        args.putString("imageURL", imageURL);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_organizer_profile, container, false);
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        emailTextView = view.findViewById(R.id.emailTextView);
        firstNameTextView = view.findViewById(R.id.firstNameTextView);
        lastNameTextView = view.findViewById(R.id.lastNameTextView);
        addressTextView = view.findViewById(R.id.addressTextView);
        phoneNumberTextView = view.findViewById(R.id.phoneNumberTextView);
        profileImageView = view.findViewById(R.id.profileImageView);


        reasonImageButton = view.findViewById(R.id.reasonImageButton);
        reasonEditText = view.findViewById(R.id.reasonEditText);
        createReportButton = view.findViewById(R.id.createReportButton);

        if (getArguments() != null) {
            emailTextView.setText(getArguments().getString("email"));
            firstNameTextView.setText(getArguments().getString("firstName"));
            lastNameTextView.setText(getArguments().getString("lastName"));
            addressTextView.setText(getArguments().getString("address"));
            phoneNumberTextView.setText(getArguments().getString("phoneNumber"));

            String imageUrl = getArguments().getString("imageURL");
            if (imageUrl != null && !imageUrl.isEmpty()) {
                Glide.with(this).load(imageUrl).into(profileImageView);
            }
        }



        reasonImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reasonEditText.setVisibility(View.VISIBLE);
                createReportButton.setVisibility(View.VISIBLE);
            }
        });


        createReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = reasonEditText.getText().toString().trim();
                if (!reason.isEmpty()) {
                    createUsersReport(currentUser.getUid(), reason,getArguments().getString("id")); // Implement this method to create a report
                } else {
                    Toast.makeText(getContext(), "Please enter a reason", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }


    // Method to create a user report
    private void createUsersReport(String pupvId, String reason, String odId) {
        DatabaseReference reportsRef = FirebaseDatabase.getInstance().getReference().child("UserReports");
        String reportId = reportsRef.push().getKey();
        if (reportId != null) {
            UsersReport report = new UsersReport(odId, pupvId,  new Date(),reason,"Created");

            report.setId(reportId);
            reportsRef.child(reportId).setValue(report)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getContext(), "Report created successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Failed to create report", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }
}