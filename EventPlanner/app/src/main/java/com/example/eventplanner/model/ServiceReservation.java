package com.example.eventplanner.model;

public class ServiceReservation {

   private String id;
    private String odId;
    private String serviceId;
    private String employeeId;
    private String  from;
    private String  to;

    private String  status;  //new,pupCanceled,odCanceled,adminCanceled,accepted,realized
    private boolean isFromPackage;
    private String cancellationTimeLimit;
    private String eventId;

    private String packageId;

    public ServiceReservation(){}

    public ServiceReservation(String odId, String serviceId, String employeeId, String from, String to, String status, boolean isFromPackage, String cancellationTimeLimit, String eventId,String packageId) {
        this.odId = odId;
        this.serviceId = serviceId;
        this.employeeId = employeeId;
        this.from = from;
        this.to = to;
        this.status = status;
        this.isFromPackage = isFromPackage;
        this.cancellationTimeLimit = cancellationTimeLimit;
        this.eventId = eventId;
        this.packageId=packageId;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOdId() {
        return odId;
    }

    public void setOdId(String odId) {
        this.odId = odId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isFromPackage() {
        return isFromPackage;
    }

    public void setFromPackage(boolean fromPackage) {
        isFromPackage = fromPackage;
    }

    public String getCancellationTimeLimit() {
        return cancellationTimeLimit;
    }

    public void setCancellationTimeLimit(String cancellationTimeLimit) {
        this.cancellationTimeLimit = cancellationTimeLimit;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
