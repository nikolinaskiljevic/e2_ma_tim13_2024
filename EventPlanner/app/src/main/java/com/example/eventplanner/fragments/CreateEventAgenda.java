package com.example.eventplanner.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.eventplanner.R;
import com.example.eventplanner.model.Employee;
import com.example.eventplanner.model.Event;
import com.example.eventplanner.model.EventAgenda;
import com.example.eventplanner.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.HorizontalAlignment;

import java.io.File;
import java.io.FileOutputStream;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CreateEventAgenda#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateEventAgenda extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final int PERMISSION_REQUEST_CODE = 200;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private EditText agendaName, agendaDescription, agendaLocation;
    private TimePicker pickerFrom, pickerTo;
    private Button saveAgenda, downloadPdf;
    private Spinner organizerEvents;
    private List<Event> allEvents = new ArrayList<>();
    private List<Event> myEvents = new ArrayList<>();
    private List<EventAgenda> allAgendas = new ArrayList<>();
    private List<EventAgenda> selectedEventAgendas = new ArrayList<>();
    private String selectedEventId;
    private Event selectedEvent;


    public CreateEventAgenda() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateEventAgenda.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateEventAgenda newInstance(String param1, String param2) {
        CreateEventAgenda fragment = new CreateEventAgenda();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_create_event_agenda, container, false);

        agendaName = view.findViewById(R.id.editTextAgendaName);
        agendaDescription = view.findViewById(R.id.editTextAgendaDescription);
        agendaLocation = view.findViewById(R.id.editTextAgendaLocation);
        pickerFrom = view.findViewById(R.id.timePickerFrom);
        pickerTo = view.findViewById(R.id.timePickerTo);
        organizerEvents = view.findViewById(R.id.spinnerOrganizerEvents);
        saveAgenda = view.findViewById(R.id.buttonCreateAgenda);
        downloadPdf = view.findViewById(R.id.buttonDownloadPdf);

        getAllEvents();
        getAllAgendas();

        // Dodavanje TextWatcher-a na sva polja
        agendaName.addTextChangedListener(watcher);
        agendaDescription.addTextChangedListener(watcher);
        agendaLocation.addTextChangedListener(watcher);

        // Postavljanje početnog stanja dugmeta "Save agenda"
        saveAgenda.setEnabled(false);

        final String[] timeFrom = new String[1];
        final String[] timeTo = new String[1];

        pickerFrom.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                // Handle time selection
                timeFrom[0] = String.format("%02d:%02d", hourOfDay, minute);
                //Toast.makeText(getActivity(), "Selected time: " + selectedTime, Toast.LENGTH_SHORT).show();
            }
        });

        pickerTo.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            @Override
            public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
                // Handle time selection
                timeTo[0] = String.format("%02d:%02d", hourOfDay, minute);
                //Toast.makeText(getActivity(), "Selected time: " + selectedTime, Toast.LENGTH_SHORT).show();
            }
        });

        saveAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = agendaName.getText().toString();
                String description = agendaDescription.getText().toString();
                String location = agendaLocation.getText().toString();

                String selectedEvent = "";
                if(organizerEvents.getSelectedItem() != null) {
                    selectedEvent = organizerEvents.getSelectedItem().toString();
                }
                String regex = "Selected event: (.+?) - .+? type";
                Pattern pattern = Pattern.compile(regex);
                Matcher matcher = pattern.matcher(selectedEvent);

                if (matcher.find()) {
                    String eventName = matcher.group(1); // Ekstraktovano ime događaja
                    selectedEventId = findEvent(eventName).getId();
                } else {
                    System.out.println("Event name not found in the string.");
                }

                saveAgenda(selectedEventId, name, description, location, timeFrom[0], timeTo[0]);

                agendaName.setText("");
                agendaDescription.setText("");
                agendaLocation.setText("");
            }
        });

        downloadPdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePdf();
            }
        });


        return view;
    }

    private TextWatcher watcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            // Provera da li su sva polja popunjena
            boolean allFieldsFilled = !TextUtils.isEmpty(agendaName.getText().toString().trim()) &&
                    !TextUtils.isEmpty(agendaDescription.getText().toString().trim()) &&
                    !TextUtils.isEmpty(agendaLocation.getText().toString().trim());

            // Omogućavanje ili onemogućavanje dugmeta na osnovu toga da li su sva polja popunjena
            saveAgenda.setEnabled(allFieldsFilled);
        }
    };

    /*
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
            } else {
                generatePdf();
            }
        } else {
            generatePdf();
        }
    }
    */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                generatePdf();
            } else {
                Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void generatePdf() {
        getAllAgendas();

        String pdfPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
        String pdfName = selectedEvent.getEventName() + "_agenda.pdf";
        File pdfFile = new File(pdfPath, pdfName);

        try {
            FileOutputStream fos = new FileOutputStream(pdfFile);
            PdfWriter writer = new PdfWriter(fos);
            PdfDocument pdfDocument = new PdfDocument(writer);
            Document document = new Document(pdfDocument);

            // Dodajte naslov
            String title = "List of activities for event: " + selectedEvent.getEventName();
            document.add(new Paragraph(title).setBold().setFontSize(18).setTextAlignment(TextAlignment.CENTER));

            // Kreirajte tabelu sa odgovarajućim brojem kolona
            float[] columnWidths = {2, 3, 2, 2, 2};
            Table table = new Table(columnWidths);
            table.setHorizontalAlignment(HorizontalAlignment.CENTER);

            // Dodajte zaglavlja kolona
            table.addHeaderCell(new Cell().add(new Paragraph("Name")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("Description")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("Location")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("From")).setBold().setTextAlignment(TextAlignment.CENTER));
            table.addHeaderCell(new Cell().add(new Paragraph("To")).setBold().setTextAlignment(TextAlignment.CENTER));

            // Dodajte podatke iz liste selectedEventAgendas
            for (EventAgenda agenda : selectedEventAgendas) {
                table.addCell(new Cell().add(new Paragraph(agenda.getName())).setTextAlignment(TextAlignment.CENTER));
                table.addCell(new Cell().add(new Paragraph(agenda.getDescription())).setTextAlignment(TextAlignment.CENTER));
                table.addCell(new Cell().add(new Paragraph(agenda.getLocation())).setTextAlignment(TextAlignment.CENTER));
                table.addCell(new Cell().add(new Paragraph(agenda.getFrom())).setTextAlignment(TextAlignment.CENTER));
                table.addCell(new Cell().add(new Paragraph(agenda.getTo())).setTextAlignment(TextAlignment.CENTER));
            }

            document.add(table);
            document.close();

            Toast.makeText(getContext(), "PDF generated successfully", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Error generating PDF: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    private Event findEvent(String eventName) {
        for(Event e: myEvents) {
            if(e.getEventName().equals(eventName)) {
                selectedEvent = e;
                return e;
            }
        }

        return null;
    }

    private void setupSpinner() {
        // Kreiramo listu imena dogadjaja
        List<String> events = new ArrayList<>();
        for (Event e : allEvents) {
            String var = "Selected event: " + e.getEventName() + " - " + e.getEventType() + " type";
            events.add(var);
        }

        // Kreiramo adapter za spinner
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, events);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Postavimo adapter za spinner
        organizerEvents.setAdapter(spinnerAdapter);
    }

    private void getAllEvents() {
        DatabaseReference eventRef = FirebaseDatabase.getInstance().getReference("events");

        eventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allEvents.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Event e = snapshot.getValue(Event.class);
                    if (e != null) {
                        allEvents.add(e);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findOrganizerEvents();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllEvents", "Failed to read events", databaseError.toException());
            }
        });
    }

    private void getAllAgendas() {
        DatabaseReference agendaRef = FirebaseDatabase.getInstance().getReference("eventAgendas");

        agendaRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                allAgendas.clear(); // Čisti listu pre nego što je napuni novim podacima
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    EventAgenda a = snapshot.getValue(EventAgenda.class);
                    if (a != null) {
                        allAgendas.add(a);
                    }
                }
                // Sada usersList sadrži sve korisnike
                findSelectedEventAgenda();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("fetchAllAgendas", "Failed to read agendas", databaseError.toException());
            }
        });
    }

    private void findSelectedEventAgenda() {
        selectedEventAgendas.clear();

        String selectedEvent = "";
        if(organizerEvents.getSelectedItem() != null) {
            selectedEvent = organizerEvents.getSelectedItem().toString();
        }
        String regex = "Selected event: (.+?) - .+? type";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(selectedEvent);

        if (matcher.find()) {
            String eventName = matcher.group(1); // Ekstraktovano ime događaja
            selectedEventId = findEvent(eventName).getId();
        } else {
            System.out.println("PDF generating: Event name not found in the string.");
        }

        for(EventAgenda a: allAgendas) {
            if(a.getEventId().equals(selectedEventId)) {
                selectedEventAgendas.add(a);
            }
        }
    }

    private void findOrganizerEvents() {
        myEvents.clear();
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();

        for(Event e: allEvents) {
            if(e.getOrganizerId().equals(currentUser.getUid())) {
                myEvents.add(e);
            }
        }

        setupSpinner();
    }

    private void saveAgenda(String eventId, String name, String description, String location, String timeFrom, String timeTo) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference agendaRef = FirebaseDatabase.getInstance().getReference().child("eventAgendas");
        String agendaId = agendaRef.push().getKey();

        EventAgenda newAgenda = new EventAgenda(agendaId, eventId, currentUser.getUid(), name, description, location, timeFrom, timeTo);

        agendaRef.child(agendaId).setValue(newAgenda);

        Toast.makeText(getContext(), "Event agenda added successfully!", Toast.LENGTH_SHORT).show();
    }
}